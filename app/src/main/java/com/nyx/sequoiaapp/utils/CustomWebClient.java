package com.nyx.sequoiaapp.utils;

import android.content.Context;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.nyx.sequoiaapp.other.BaseFunctions;

public class CustomWebClient extends WebViewClient {
    private Context context;

    public CustomWebClient(Context context) {
        this.context = context;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        BaseFunctions.openBrowser(context,url);
        return true;
    }
}
