package com.nyx.sequoiaapp.APIClass;

import android.content.Context;
import android.util.Log;

import com.nyx.sequoiaapp.API.CommissionsAPIs;
import com.nyx.sequoiaapp.API.OrdersAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 11/12/2019.
 */

public class CommissionsAPIsClass extends BaseRetrofit {

    public static void get_info(final Context context,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        CommissionsAPIs api = retrofit.create(CommissionsAPIs.class);
        Call<BaseResponse> call = api.get_info(APIUrl.token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.i("error_retret", "onFailure: "+t.getMessage());
                onFailure.onFailure();
            }
        });
    }
}
