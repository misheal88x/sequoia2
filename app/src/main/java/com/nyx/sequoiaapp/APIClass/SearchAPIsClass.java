package com.nyx.sequoiaapp.APIClass;

import android.content.Context;

import com.nyx.sequoiaapp.API.SearchAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import java.lang.annotation.Retention;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 10/21/2019.
 */

public class SearchAPIsClass extends BaseRetrofit {
    public static void search(final Context context,
                              String words,
                              String city_id,
                              int page,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.search(APIUrl.token,words,city_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
