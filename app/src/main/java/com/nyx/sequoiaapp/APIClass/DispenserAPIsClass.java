package com.nyx.sequoiaapp.APIClass;

import android.content.Context;

import com.nyx.sequoiaapp.API.DispenserAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 11/3/2019.
 */

public class DispenserAPIsClass extends BaseRetrofit{

    public static void search(final Context context,
                              String user_id,
                              String words,
                              int start,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        DispenserAPIs api = retrofit.create(DispenserAPIs.class);
        Call<BaseResponse> call = api.search(APIUrl.token,user_id,words,start);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
