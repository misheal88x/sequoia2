package com.nyx.sequoiaapp.APIClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.nyx.sequoiaapp.API.OfferAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 10/16/2019.
 */

public class OffersAPIsClass extends BaseRetrofit {

    private static ProgressDialog progressDialog;

    public static void getAllOffers(final Context context,
                                    String user_id,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OfferAPIs api = retrofit.create(OfferAPIs.class);
        Call<BaseResponse> call = api.getAllOffers(APIUrl.token,user_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void remove(final Context context,
                                    String user_id,
                                    String offer_id,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("يرجى الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        OfferAPIs api = retrofit.create(OfferAPIs.class);
        Call<BaseResponse> call = api.remove(APIUrl.token,user_id,offer_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                //BaseFunctions.processResponse(response,onResponse,context);
                if (response.code() == 200){
                    onResponse.onResponse();
                }else {
                    Toast.makeText(context, "حدث خطأ ما أعد المحاولة", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void add(final Context context,
                           String user_id,
                           String description,
                           int fixed,
                           int percent,
                           String start_date,
                           String end_date,
                           final String product_id,
                           IResponse onResponse1,
                           IFailure onFailure1){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("يرجى الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        OfferAPIs api = retrofit.create(OfferAPIs.class);
        Call<BaseResponse> call = api.addOffer(APIUrl.token,user_id,description,fixed,percent,start_date,end_date,product_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void update(final Context context,
                              String user_id,
                              String offer_id,
                              String description,
                              int fixed,
                              int percent,
                              String start_date,
                              String end_date,
                              final String product_id,
                              IResponse onResponse1,
                              IFailure onFailure1){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("يرجى الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        OfferAPIs api = retrofit.create(OfferAPIs.class);
        Call<BaseResponse> call = api.updateOffer(APIUrl.token,user_id,offer_id,description,fixed,percent,start_date,end_date,product_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getOffer(final Context context,
                              String user_id,
                              String offer_id,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        OfferAPIs api = retrofit.create(OfferAPIs.class);
        Call<BaseResponse> call = api.getOffer(APIUrl.token,user_id,offer_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void search(final Context context,
                                    String user_id,
                                    String words,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OfferAPIs api = retrofit.create(OfferAPIs.class);
        Call<BaseResponse> call = api.search(APIUrl.token,user_id,words,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
