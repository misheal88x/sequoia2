package com.nyx.sequoiaapp.APIClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.nyx.sequoiaapp.API.UserAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 10/22/2019.
 */

public class UsersAPIsClass extends BaseRetrofit {

    private static ProgressDialog progressDialog;

    public static void check_seller_status(final Context context,
                                           String user_id,
                                           IResponse onResponse1,
                                           IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.check_seller_status(APIUrl.token,user_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void update_seller_info(final Context context,
                                           String user_id,
                                           String store_name,
                                           String cities_ids,
                                           int has_branches,
                                           int orders_delivery,
                                           int accept_commission,
                                           int orders_delivery_days,
                                           String rejection_commission_text,
                                           String gender,
                                           IResponse onResponse1,
                                           IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_seller_info(APIUrl.token,user_id,store_name,cities_ids,has_branches
        ,orders_delivery,accept_commission,orders_delivery_days,rejection_commission_text,gender);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void update_image(final Context context,
                                           String user_id,
                                           String image_file,
                                           IResponse onResponse1,
                                           IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        //Image Icon
        File file = new File(image_file);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image_file",file.getName(),requestBody);


        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_image(APIUrl.token,user_id,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
