package com.nyx.sequoiaapp.APIClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nyx.sequoiaapp.API.OfferAPIs;
import com.nyx.sequoiaapp.API.OrdersAPIs;
import com.nyx.sequoiaapp.API.ProductsAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 10/21/2019.
 */

public class ProductsAPIsClass extends BaseRetrofit {
    private static ProgressDialog progressDialog;

    public static void addProduct(final Context context,
                                  String user_id,
                                  String name,
                                  String description,
                                  String prime_price,
                                  String price,
                                  String commission_fixed,
                                  String commission_percent,
                                  String status,
                                  String qty,
                                  String features,
                                  String category_id,
                                  String store_id,
                                  String featured,
                                  String brand_id,
                                  String bounty,
                                  List<String> images,
                                  IResponse onResponse1,
                                  IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[images.size()];
        for (int i = 0; i <images.size() ; i++) {
            File file = new File(images.get(i));
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
            multipartTypedOutput[i] = MultipartBody.Part.createFormData("images[]", file.getName(), requestBody);
        }

        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //Description
        RequestBody descRequest = RequestBody.create(MultipartBody.FORM,description);
        //Prime price
        RequestBody primeRequest = RequestBody.create(MultipartBody.FORM,prime_price);
        //Price
        RequestBody priceRequest = RequestBody.create(MultipartBody.FORM,price);
        //Fixed
        RequestBody fixedRequest = RequestBody.create(MultipartBody.FORM,commission_fixed);
        //Percent
        RequestBody percentRequest = RequestBody.create(MultipartBody.FORM,commission_percent);
        //Status
        RequestBody statusRequest = RequestBody.create(MultipartBody.FORM,status);
        //Quantity
        RequestBody qtyRequest = RequestBody.create(MultipartBody.FORM,qty);
        //Features
        RequestBody featuresRequest = RequestBody.create(MultipartBody.FORM,features);
        //Category id
        RequestBody categoryRequest = RequestBody.create(MultipartBody.FORM,category_id);
        //Store id
        RequestBody storeRequest = RequestBody.create(MultipartBody.FORM,store_id);
        //Featured
        RequestBody featuredRequest = RequestBody.create(MultipartBody.FORM,featured);
        //Brand id
        RequestBody brandRequest = RequestBody.create(MultipartBody.FORM,brand_id);
        //User id
        RequestBody userIdRequest = RequestBody.create(MultipartBody.FORM,user_id);
        //Bounty
        RequestBody bountyRequest = RequestBody.create(MultipartBody.FORM,bounty);

        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.addProduct(APIUrl.token,
                user_id,
                nameRequest,
                descRequest,
                primeRequest,
                priceRequest,
                fixedRequest,
                percentRequest,
                statusRequest,
                qtyRequest,
                featuresRequest,
                categoryRequest,
                storeRequest,
                featuredRequest,
                brandRequest,
                bountyRequest,
                multipartTypedOutput);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                //BaseFunctions.processResponse(response,onResponse,context);
                if (response.body()!=null){
                    if (response.body().getStatus()==1){
                        if (response.body().getData()!=null){
                            onResponse.onResponse(response.body().getData());
                        }
                    }else {
                        try {
                            Toast.makeText(context, String.valueOf(response.body().getMessage()), Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            Toast.makeText(context, "حدث خطأ ما, أعد المحاولة", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    //Toast.makeText(context, "يوجد خطأ بالخدمة أعد المحاولة", Toast.LENGTH_SHORT).show();
                    BufferedReader reader = null;
                    StringBuilder sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    String finallyError = sb.toString();
                    try {
                        sendMessageByWhatsapp(context, "onResponse: " + finallyError.substring(0, 364));
                    }catch (Exception e){
                        try{
                            sendMessageByWhatsapp(context, "onResponse: " + finallyError.substring(0, 250));
                        }catch (Exception e1){
                            sendMessageByWhatsapp(context, "onResponse: " + e1.getMessage());
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void updateProduct(final Context context,
                                  String user_id,
                                  String product_id,
                                  String name,
                                  String description,
                                  String prime_price,
                                  String price,
                                  String commission_fixed,
                                  String commission_percent,
                                  String status,
                                  String qty,
                                  String features,
                                  String category_id,
                                  String store_id,
                                  String featured,
                                  String brand_id,
                                  String bounty,
                                  List<String> images,
                                  IResponse onResponse1,
                                  IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[images.size()];
        if (images.size()>0){
            for (int i = 0; i <images.size() ; i++) {
                File file = new File(images.get(i));
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
                multipartTypedOutput[i] = MultipartBody.Part.createFormData("images[]", file.getName(), requestBody);
            }
        }
        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //Description
        RequestBody descRequest = RequestBody.create(MultipartBody.FORM,description);
        //Prime price
        RequestBody primeRequest = RequestBody.create(MultipartBody.FORM,prime_price);
        //Price
        RequestBody priceRequest = RequestBody.create(MultipartBody.FORM,price);
        //Fixed
        RequestBody fixedRequest = RequestBody.create(MultipartBody.FORM,commission_fixed);
        //Percent
        RequestBody percentRequest = RequestBody.create(MultipartBody.FORM,commission_percent);
        //Status
        RequestBody statusRequest = RequestBody.create(MultipartBody.FORM,status);
        //Quantity
        RequestBody qtyRequest = RequestBody.create(MultipartBody.FORM,qty);
        //Features
        RequestBody featuresRequest = RequestBody.create(MultipartBody.FORM,features);
        //Category id
        RequestBody categoryRequest = RequestBody.create(MultipartBody.FORM,category_id);
        //Store id
        RequestBody storeRequest = RequestBody.create(MultipartBody.FORM,store_id);
        //Featured
        RequestBody featuredRequest = RequestBody.create(MultipartBody.FORM,featured);
        //Brand id
        RequestBody brandRequest = RequestBody.create(MultipartBody.FORM,brand_id);
        //Bounty
        RequestBody bountyRequest = RequestBody.create(MultipartBody.FORM,bounty);

        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.updateProduct(APIUrl.token,
                user_id,
                product_id,
                nameRequest,
                descRequest,
                primeRequest,
                priceRequest,
                fixedRequest,
                percentRequest,
                statusRequest,
                qtyRequest,
                featuresRequest,
                categoryRequest,
                storeRequest,
                featuredRequest,
                brandRequest,
                bountyRequest,
                multipartTypedOutput);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                Log.i("eeeeee", "onFailure: "+t.getMessage());
                onFailure.onFailure();
            }
        });
    }

    public static void getMyProducts(final Context context,
                                    String user_id,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.getMyProducts(APIUrl.token,user_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.i("kjggn", "onFailure: "+t.getMessage());
                onFailure.onFailure();
            }
        });
    }
    public static void getProductsLess(final Context context,
                                     String user_id,
                                     int page,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.getProductsLess(APIUrl.token,user_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void switch_state(final Context context,
                                       String product_id,
                                       String status,
                                       IResponse onResponse1,
                                       IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.switch_visibility(APIUrl.token,product_id,status);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_product_info(final Context context,
                                     String user_id,
                                     String product_id,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.product_info(APIUrl.token,user_id,product_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void deleteProduct(final Context context,
                                       String user_id,
                                       String product_id,
                                       IResponse onResponse1,
                                       IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.deleteProduct(APIUrl.token,user_id,product_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void search(final Context context,
                              String user_id,
                              String words,
                              int page,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.search(APIUrl.token,user_id,words,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void deleteImage(final Context context,
                              String user_id,
                              String product_id,
                              String image_name,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("جاري حذف الصورة, الرجاء الانتظار..");
        dialog.setCancelable(false);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        ProductsAPIs api = retrofit.create(ProductsAPIs.class);
        Call<BaseResponse> call = api.deleteImage(APIUrl.token,user_id,product_id,image_name);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void sendMessageByWhatsapp(Context context,String message){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{"mishealibrahim1994@gmail.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Error");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }else {
            PackageManager pm=context.getPackageManager();
            try {

                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                String text = message;

                PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp");

                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                context.startActivity(Intent.createChooser(waIntent, "Share with"));

            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(context, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
