package com.nyx.sequoiaapp.APIClass;

import android.app.ProgressDialog;
import android.content.Context;

import com.nyx.sequoiaapp.API.ProductsAPIs;
import com.nyx.sequoiaapp.API.SellerSlidersAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SellerSlidersAPIsClass extends BaseRetrofit {
    private static ProgressDialog progressDialog;

    public static void getSliders(final Context context,
                                     String user_id,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        SellerSlidersAPIs api = retrofit.create(SellerSlidersAPIs.class);
        Call<BaseResponse> call = api.getSliders(APIUrl.token,user_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void addSlider(final Context context,
                                     String user_id,
                                     String image,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);

        Retrofit retrofit = configureRetrofitWithoutBearer();
        SellerSlidersAPIs api = retrofit.create(SellerSlidersAPIs.class);
        Call<BaseResponse> call = api.addSlider(APIUrl.token,
                user_id,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void deleteSlider(final Context context,
                                     String user_id,
                                     String slider_id,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        SellerSlidersAPIs api = retrofit.create(SellerSlidersAPIs.class);
        Call<BaseResponse> call = api.deleteSlider(APIUrl.token,user_id,"delete",slider_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
