package com.nyx.sequoiaapp.APIClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.nyx.sequoiaapp.API.OfferAPIs;
import com.nyx.sequoiaapp.API.OrdersAPIs;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BaseResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.nyx.sequoiaapp.other.BaseRetrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Misheal on 10/23/2019.
 */

public class OrdersAPIsClass extends BaseRetrofit {

    public static void getAllOrders(final Context context,
                                    String user_id,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.getOrders(APIUrl.token,user_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void getAllOrdersMore(final Context context,
                                    String user_id,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.getOrdersMore(APIUrl.token,user_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.i("more_error", "onFailure: "+t.getMessage());
                onFailure.onFailure();
            }
        });
    }

    public static void search(final Context context,
                              String user_id,
                              String words,
                              int page,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.search(APIUrl.token,user_id,words,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void search_less(final Context context,
                              String user_id,
                              String words,
                              int page,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.search_less(APIUrl.token,user_id,words,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void search_more(final Context context,
                              String user_id,
                              String words,
                              int page,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.search_more(APIUrl.token,user_id,words,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void order_details(final Context context,
                                    String order_id,
                                    String user_id,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.order_details(APIUrl.token,user_id,order_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void get_statistics(final Context context,
                                     String user_id,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.get_stats(APIUrl.token,user_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void confirmOrder(final Context context,
                                      String user_id,
                                      String order_id,
                                      IResponse onResponse1,
                                      IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("جاري تأكيد الطلبية, الرجاء الانتظار..");
        dialog.setCancelable(false);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.confirm_order(APIUrl.token,user_id,order_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void addOrder(final Context context,
                                String user_id,
                                String user_token,
                                final String items,
                                String notes,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl("http://e-sequoia.net/test-api/public/api/v2/").addConverterFactory(GsonConverterFactory.create()).build();
        OrdersAPIs api = retrofit.create(OrdersAPIs.class);
        Call<BaseResponse> call = api.add_order(APIUrl.token,user_id,user_token,items,notes);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                //BaseFunctions.processResponse(response,onResponse,context);
                if (response.body()!=null){
                    if (response.body().getStatus()==1){
                        if (response.body().getData()!=null){
                            onResponse.onResponse(response.body().getData());
                        }
                    }else {
                        try {
                            sendMessageByEmail(context,items.substring(0,250));
                            Toast.makeText(context, String.valueOf(response.body().getMessage()), Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            sendMessageByEmail(context,items.substring(0,250));
                            Toast.makeText(context, "حدث خطأ ما, أعد المحاولة", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    sendMessageByEmail(context,items.substring(0,250));
                    Toast.makeText(context, "يوجد خطأ بالخدمة أعد المحاولة", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void sendMessageByEmail(Context context, String message){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{"mishealibrahim1994@gmail.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Error");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }else {
            PackageManager pm=context.getPackageManager();
            try {

                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                String text = message;

                PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp");

                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                context.startActivity(Intent.createChooser(waIntent, "Share with"));

            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(context, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
