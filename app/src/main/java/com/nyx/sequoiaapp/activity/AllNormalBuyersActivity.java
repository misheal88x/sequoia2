package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.NormalBuyersAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.NormalBuyerObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class AllNormalBuyersActivity extends RootActivity {

    private RelativeLayout root;
    private LinearLayout title_layout;
    private androidx.appcompat.widget.Toolbar toolbar;
    private RecyclerView rv_recycler;
    private List<NormalBuyerObject> list;
    private GridLayoutManager layoutManager;
    private NormalBuyersAdapter adapter;
    private ImageView empty_image;
    private TextView tv_desc;
    private NestedScrollView scrollView;
    private FloatingActionButton fab_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_normal_buyers);
        init_views();
        init_events();
        init_recycler();
        hide_elements();
        callAPI();
    }

    private void init_views(){
        //Root
        root = findViewById(R.id.all_buyers_layout);
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("كل الباعة");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Nested Scroll View
        scrollView = findViewById(R.id.all_buyers_scroll);
        //Linear layout
        title_layout = findViewById(R.id.all_buyers_title_layout);
        //Recycler View
        rv_recycler = findViewById(R.id.all_buyers_recycler);
        //Empty image
        empty_image = findViewById(R.id.no_items_found);
        //TextView
        tv_desc = findViewById(R.id.description);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
    }

    private void init_events(){
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllNormalBuyersActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
    }
    private void hide_elements(){
        rv_recycler.setVisibility(View.GONE);
    }
    private void show_elements(){
        rv_recycler.setVisibility(View.GONE);
    }
    private void init_recycler(){
        list = new ArrayList<>();
        layoutManager = new GridLayoutManager(AllNormalBuyersActivity.this,2){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        adapter = new NormalBuyersAdapter(AllNormalBuyersActivity.this,list);
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);
    }

    private void callAPI(){
        if (SharedPrefManager.getInstance(AllNormalBuyersActivity.this).getNormalBuyers().size()>0){
            for (NormalBuyerObject nbo : SharedPrefManager.getInstance(AllNormalBuyersActivity.this).getNormalBuyers()){
                list.add(nbo);
                adapter.notifyDataSetChanged();
            }
            rv_recycler.setVisibility(View.VISIBLE);
            BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
        }else {
            empty_image.setVisibility(View.VISIBLE);
            tv_desc.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
