package com.nyx.sequoiaapp.activity;

import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.View;

import com.nyx.sequoiaapp.adapters.AdsAdapterGrid;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.ExtendedPost;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SearchResultsActivity extends ViewProductsActivity {
    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);

            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
        final User user = SharedPrefManager.getInstance(this).getUser();
        HashMap<String  ,String> params=new HashMap<String, String>();
        params.put("text" ,getIntent().getStringExtra("query") );
        params.put("start" ,current+"");
        BackgroundServices.getInstance(this).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);


                try {

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray products = heart.getJSONObject("data").getJSONArray("products");
                    max=heart.getJSONObject("data").getInt("total");
                    setDescription("جرب التجول ضمن أكثر من "+max+" منتجاً مختلفا " +
                            " يتعلق ب"+getIntent().getStringExtra("query")+ " , نتمنى لك تسوقاً ممتعاً");

                    for (int i = 0; i < products.length(); i++) {

                        items.add(new ExtendedPost(products.getJSONObject(i)
                                )


                        );

                    }

                    adapter.notifyDataSetChanged();
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription("لم يتم العثور على منتجات متعلقة ب( " + getIntent().getStringExtra("query") + ") حالياً , قم تجد طلبك اذا قمت بالبحث في وقت لاحق او قمت بتغيير عبارات البحث لديك او تدقيق العبارة المكتوبة , ننتظر اطلاعك على منتجاتنا في سوق سيكويا ,أهلا بك .. ");
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                }
            }
        }, APIUrl.SERVER  +
                "product/search" , params);
    }
    int current=0;

    boolean loadingFlag = false;

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore(0);
            }

        }
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle(getIntent().getStringExtra("query"));
    }

    @Override
    void fetchData() {
        setWindowTitle("بحث عن  "+getIntent().getStringExtra("query"));
        setDescription("تمتع بتجربة التسوق الذكية من سيكويا");

        GridLayoutManager lm = new GridLayoutManager(this,
                2) ;
        ;
        adapter= new AdsAdapterGrid(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
