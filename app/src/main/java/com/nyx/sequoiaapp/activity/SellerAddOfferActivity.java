package com.nyx.sequoiaapp.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.OffersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.DatePickerFragment;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.TimePickerFragment;
import com.nyx.sequoiaapp.models.OfferObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

public class SellerAddOfferActivity extends RootActivity implements DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener{

    private RelativeLayout root;
    private ScrollView scrollView;
    private ProgressBar loading_progress;
    private TextView tv_title;
    private Button tv_start_date,tv_end_date,tv_start_time,tv_end_time;
    private EditText edt_desc,edt_discont;
    private RadioButton rb_fixed,rb_percent;
    private TextView tv_price_after_txt,tv_price_after_value;
    private Button btn_create;
    private int discount_type = 0;
    private int fixed_val = 0;
    private int percent_val = 0;
    private int date_type = 0;
    private int time_type = 0;
    private String chosed_start_date = "",chosed_end_date = "",chosed_start_time = "",chosed_end_time = "";
    private Intent myIntent;
    private String offer_id = "";
    private String product_id = "";
    private FloatingActionButton fab_chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_add_offer);
        init_views();
        init_events();
        tv_title.setText("عرض على "+myIntent.getStringExtra("product_name"));
        if (myIntent.getStringExtra("activity_type").equals("edit")){
            scrollView.setVisibility(View.GONE);
            loading_progress.setVisibility(View.VISIBLE);
            btn_create.setText("تحديث");
            getSupportActionBar().setTitle("تحديث العرض");
            callDetailsAPI();

        }
        Log.i("today_date", "onCreate: "+BaseFunctions.todayDateString());
    }

    private void init_views(){
        //Root
        root = findViewById(R.id.seller_add_offer_layout);
        //ScrollView
        scrollView = findViewById(R.id.seller_add_offer_scrollview);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
        //Progress Bar
        loading_progress = findViewById(R.id.seller_add_offer_progress);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("أنشئ عرضا");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //TextView
        tv_title = findViewById(R.id.title);
        tv_start_date = findViewById(R.id.add_seller_offer_start_date);
        tv_end_date = findViewById(R.id.add_seller_offer_end_date);
        tv_start_time = findViewById(R.id.add_seller_offer_start_time);
        tv_end_time = findViewById(R.id.add_seller_offer_end_time);
        tv_price_after_txt = findViewById(R.id.add_seller_offer_price_after_discount_txt);
        tv_price_after_value = findViewById(R.id.add_seller_offer_price_after_discount_value);
        //Button
        btn_create = findViewById(R.id.add_seller_offer_create);
        //EditText
        edt_desc = findViewById(R.id.add_seller_offer_desc_edt);
        edt_discont = findViewById(R.id.add_seller_offer_discont_value_edt);
        //Radio button
        rb_fixed = findViewById(R.id.add_seller_offer_fixed);
        rb_percent = findViewById(R.id.add_seller_offer_percent);
        //Intent
        myIntent = getIntent();

    }

    private void init_events(){
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerAddOfferActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        rb_fixed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    discount_type = 1;
                    edt_discont.setVisibility(View.VISIBLE);
                    tv_price_after_txt.setVisibility(View.VISIBLE);
                    tv_price_after_value.setVisibility(View.VISIBLE);
                    edt_discont.setText("0");
                }
            }
        });
        rb_percent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    discount_type = 2;
                    edt_discont.setVisibility(View.VISIBLE);
                    tv_price_after_txt.setVisibility(View.VISIBLE);
                    tv_price_after_value.setVisibility(View.VISIBLE);
                    edt_discont.setText("0");
                }
            }
        });
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date_type = 1;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });
        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date_type = 2;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });
        tv_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time_type = 1;
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(),"time picker");
            }
        });
        tv_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time_type = 2;
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(),"time picker");
            }
        });
        edt_discont.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length()>0){
                    int dis = Integer.valueOf(s.toString());
                    if (discount_type == 1){
                        tv_price_after_value.setText(String.valueOf(myIntent.getIntExtra("prime_price",0)-dis));
                    }else {
                        tv_price_after_value.setText(String.valueOf(myIntent.getIntExtra("prime_price",0)-
                                ((dis*myIntent.getIntExtra("prime_price",0))/100)));
                    }
                }else {
                    tv_price_after_value.setText(String.valueOf(myIntent.getIntExtra("prime_price",0)));
                }
            }
        });
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (discount_type == 0){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب عليك اختيار طريقة الحسم", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_discont.getText().toString().equals("")||edt_discont.getText().toString().equals("0")){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب عليك تحديد قيمة الحسم", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosed_start_date.equals("")){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب عليك اختيار تاريخ البداية", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosed_start_time.equals("")){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب عليك اختيار وقت البداية", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosed_end_date.equals("")){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب عليك اختيار تاريخ النهاية", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosed_end_time.equals("")){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب عليك اختيار وقت النهاية", Toast.LENGTH_SHORT).show();
                    return;
                }
                long x = BaseFunctions.deferenceBetweenTwoDates(BaseFunctions.stringToDateConverter(BaseFunctions.todayDateString()),
                        BaseFunctions.stringToDateConverter(chosed_start_date+" "+chosed_start_time));
                if (x<0){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب أن تختار تاريخ و وقت البداية قبل التاريخ و الوقت الحالي", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (BaseFunctions.deferenceBetweenTwoDates(BaseFunctions.stringToDateConverter(chosed_end_date+" "+chosed_end_time),BaseFunctions.stringToDateConverter(BaseFunctions.todayDateString()))<0){
                    Toast.makeText(SellerAddOfferActivity.this, "يجب أن تختار تاريخ و وقت النهاية بعد التاريخ و الوقت الحالي", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (myIntent.getStringExtra("activity_type").equals("add")){
                    CallAddOfferAPI();
                }else {
                    CallUpdateOfferAPI();
                }
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String year_str = new String();
        String month_str = new String();
        String day_str = new String();
        year_str = String.valueOf(year);
        if (month+1<10){
            month_str = "0"+String.valueOf(month+1);
        }else {
            month_str = String.valueOf(month+1);
        }
        if (day<10){
            day_str = "0"+String.valueOf(day);
        }else {
            day_str = String.valueOf(day);
        }
        String desDate = year_str+"-"+month_str+"-"+day_str;
        if (date_type == 1){
            tv_start_date.setText(desDate);
            chosed_start_date = desDate;
        }else {
            tv_end_date.setText(desDate);
            chosed_end_date = desDate;
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        String hour_str = new String();
        String minute_str = new String();
        if (hour<10){
            hour_str = "0"+String.valueOf(hour);
        }else {
            hour_str = String.valueOf(hour);
        }
        if (minute<10){
            minute_str = "0"+String.valueOf(minute);
        }else {
            minute_str = String.valueOf(minute);
        }
        String fullTime = hour_str+":"+minute_str+":00";
        if (time_type == 1){
            tv_start_time.setText(fullTime);
            chosed_start_time = fullTime;
        }else {
            tv_end_time.setText(fullTime);
            chosed_end_time = fullTime;
        }
    }

    private void CallAddOfferAPI(){
        if (discount_type == 1){
            fixed_val = Integer.valueOf(edt_discont.getText().toString());
        }else {
            percent_val = Integer.valueOf(edt_discont.getText().toString());
        }
        String desc = "";
        if (!edt_desc.getText().toString().equals("")){
            desc = edt_desc.getText().toString();
        }
        String user_id = SharedPrefManager.getInstance(SellerAddOfferActivity.this).getUser().getUser_id();
        OffersAPIsClass.add(SellerAddOfferActivity.this,
                user_id,
                desc,
                fixed_val,
                percent_val,
                chosed_start_date + " " + chosed_start_time,
                chosed_end_date + " " + chosed_end_time,
                String.valueOf(myIntent.getIntExtra("product_id",0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(SellerAddOfferActivity.this, "تم إضافة العرض بنجاح", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CallAddOfferAPI();
                                    }
                                }).show();
                    }
                });
    }

    private void callDetailsAPI(){
        String user_id = SharedPrefManager.getInstance(SellerAddOfferActivity.this).getUser().getUser_id();
        OffersAPIsClass.getOffer(SellerAddOfferActivity.this,
                user_id,
                String.valueOf(myIntent.getIntExtra("offer_id", 0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            OfferObject success = new Gson().fromJson(json1,OfferObject.class);
                            offer_id = String.valueOf(success.getId());
                            if (success.getDescription()!=null){
                                edt_desc.setText(success.getDescription());
                            }
                            if (success.getFixed()>0){
                                edt_discont.setVisibility(View.VISIBLE);
                                tv_price_after_txt.setVisibility(View.VISIBLE);
                                tv_price_after_value.setVisibility(View.VISIBLE);
                                discount_type = 1;
                                fixed_val = success.getFixed();
                                rb_fixed.setChecked(true);
                                edt_discont.setText(String.valueOf(success.getFixed()));
                                tv_price_after_value.setText(String.valueOf(myIntent.getIntExtra("prime_price",0)-
                                        success.getFixed()));
                            }else if (success.getPercent()>0){
                                edt_discont.setVisibility(View.VISIBLE);
                                tv_price_after_txt.setVisibility(View.VISIBLE);
                                tv_price_after_value.setVisibility(View.VISIBLE);
                                discount_type = 2;
                                percent_val = success.getPercent();
                                rb_percent.setChecked(true);
                                edt_discont.setText(String.valueOf(success.getPercent()));
                                tv_price_after_value.setText(String.valueOf(myIntent.getIntExtra("prime_price",0)-
                                        ((percent_val*myIntent.getIntExtra("prime_price",0))/100)));
                            }
                            if (success.getStart_date()!= null){
                                chosed_start_date = BaseFunctions.dateExtractor(success.getStart_date());
                                tv_start_date.setText(chosed_start_date);
                                chosed_start_time = BaseFunctions.timeExtractor(success.getStart_date());
                                tv_start_time.setText(chosed_start_time);
                            }
                            if (success.getEnd_date()!= null){
                                chosed_end_date = BaseFunctions.dateExtractor(success.getEnd_date());
                                tv_end_date.setText(chosed_end_date);
                                chosed_end_time = BaseFunctions.timeExtractor(success.getEnd_date());
                                tv_end_time.setText(chosed_end_time);
                            }
                            product_id = String.valueOf(success.getProduct_id());
                            loading_progress.setVisibility(View.GONE);
                            scrollView.setVisibility(View.VISIBLE);
                        }else {
                            loading_progress.setVisibility(View.GONE);
                            Snackbar.make(root, "حدث خطأ ما يرجى إعادة المحاولة", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("أعد المحاولة", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            callDetailsAPI();
                                        }
                                    }).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading_progress.setVisibility(View.GONE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDetailsAPI();
                                    }
                                }).show();
                    }
                });
    }

    private void CallUpdateOfferAPI(){
        if (discount_type == 1){
            fixed_val = Integer.valueOf(edt_discont.getText().toString());
            percent_val = 0;
        }else {
            percent_val = Integer.valueOf(edt_discont.getText().toString());
            fixed_val = 0;
        }
        String desc = "";
        if (!edt_desc.getText().toString().equals("")){
            desc = edt_desc.getText().toString();
        }
        String user_id = SharedPrefManager.getInstance(SellerAddOfferActivity.this).getUser().getUser_id();
        Log.i("detaillll", "CallUpdateOfferAPI: "+user_id);
        Log.i("detaillll", "CallUpdateOfferAPI: "+offer_id);
        Log.i("detaillll", "CallUpdateOfferAPI: "+desc);
        Log.i("detaillll", "CallUpdateOfferAPI: "+fixed_val);
        Log.i("detaillll", "CallUpdateOfferAPI: "+percent_val);
        Log.i("detaillll", "CallUpdateOfferAPI: "+chosed_start_date + " " + chosed_start_time);
        Log.i("detaillll", "CallUpdateOfferAPI: "+chosed_end_date + " " + chosed_end_time);
        Log.i("detaillll", "CallUpdateOfferAPI: "+myIntent.getIntExtra("product_id",0));
        OffersAPIsClass.update(SellerAddOfferActivity.this,
                user_id,
                offer_id,
                desc,
                fixed_val,
                percent_val,
                chosed_start_date + " " + chosed_start_time,
                chosed_end_date + " " + chosed_end_time,
                String.valueOf(myIntent.getIntExtra("product_id",0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(SellerAddOfferActivity.this, "تم تحديث العرض بنجاح", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CallUpdateOfferAPI();
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
