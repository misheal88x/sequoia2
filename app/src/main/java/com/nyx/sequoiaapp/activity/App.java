package com.nyx.sequoiaapp.activity;

import androidx.multidex.MultiDex;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
//import com.instabug.library.Instabug;
//import com.instabug.library.invocation.InstabugInvocationEvent;
import android.content.Context;

//import org.acra.annotation.AcraToast;


/**
 * Created by Luminance on 5/30/2018.
 */
//@AcraToast(resText= R.string.problem,
  //      length = Toast.LENGTH_LONG)
//@AcraCore(buildConfigClass = BuildConfig.class)
//@AcraMailSender(mailTo = "mishealibrahim1994@gmail.com")
//@AcraHttpSender(uri = "http://e-sequoia.net/crash_reporter/save_log.php",
        //httpMethod = HttpSender.Method.POST)
public class App    extends androidx.multidex.MultiDexApplication  {
    public static boolean is_connected = false;
    public static boolean is_login_called = false;


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        Fresco.initialize(this);
        //ACRA.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        //ACRA.init(this);

    }
}
