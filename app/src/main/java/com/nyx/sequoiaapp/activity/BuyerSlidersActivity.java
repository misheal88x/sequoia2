package com.nyx.sequoiaapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;

import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.SellerSlidersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.BuyerSlidersAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.BuyerSliderObject;
import com.nyx.sequoiaapp.other.BaseFunctions;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BuyerSlidersActivity extends RootActivity implements IPickResult,IMove {

    private RelativeLayout root;
    private ProgressBar pb_loading;
    private RecyclerView rv_recycler;
    private List<BuyerSliderObject> list;
    private GridLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private BuyerSlidersAdapter adapter;
    private ImageView empty_image;
    private TextView tv_desc;
    private Button btn_add_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_sliders);
        init_views();
        init_events();
        init_activity();
        init_recycler();
        callAPI();
    }

    private void init_views() {
        //Root
        root = findViewById(R.id.buyer_sliders_layout);
        //Nested ScrollView
        scrollView = findViewById(R.id.my_store_scrollview);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("الصور الإعلانية");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //ProgressBar
        pb_loading = findViewById(R.id.my_store_progress);
        //RecyclerView
        rv_recycler = findViewById(R.id.my_store_recycler);
        //Empty Image
        empty_image = findViewById(R.id.no_items_found);
        //TextView
        tv_desc = findViewById(R.id.description);
        //Button
        btn_add_image = findViewById(R.id.buyer_sliders_add);
    }

    private void init_events() {
        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(BuyerSlidersActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        1);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                PickImageDialog.build(new PickSetup()
                        .setTitle("اختر..")
                        .setCancelText("إلغاء")
                        .setCameraButtonText("الكاميرا")
                        .setGalleryButtonText("المعرض"))
                        .show(BuyerSlidersActivity.this);
                return;
            }
        }
    }

    private void init_activity() {
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new BuyerSlidersAdapter(BuyerSlidersActivity.this,list,this);
        layoutManager = new GridLayoutManager(BuyerSlidersActivity.this,3){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);
    }


    @Override
    public void onPickResult(PickResult r) {
        Uri pickedImage = r.getUri();
        String destinationFileName = "SampleCropImage.jpg";
        UCrop.of(pickedImage,
                Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                .withAspectRatio(4, 3)
                .withMaxResultSize(800, 600)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP: {
                    if (data != null) {
                        final Uri resultUri = UCrop.getOutput(data);
                        try {
                            callAddSliderAPI(resultUri.getPath());
                        }catch (Exception e){
                            Toast.makeText(this, "error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            }
        }

    }

    @Override
    public void move(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BuyerSlidersActivity.this);
        builder.setMessage("هل أنت متأكد من أنك تريد حذف هذه الصورة ؟");
        builder.setTitle("حذف الصورة");
        builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callDeleteAPI(String.valueOf(list.get(position).getId()),position);
            }
        });
        builder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void callAPI(){
        pb_loading.setVisibility(View.VISIBLE);
        SellerSlidersAPIsClass.getSliders(
                BuyerSlidersActivity.this,
                SharedPrefManager.getInstance(BuyerSlidersActivity.this).getUser().getUser_id(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        pb_loading.setVisibility(View.GONE);
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            BuyerSliderObject[] success = new Gson().fromJson(j,BuyerSliderObject[].class);
                            if (success.length > 0){
                                if (success.length == 5){
                                    btn_add_image.setVisibility(View.GONE);
                                }else {
                                    btn_add_image.setVisibility(View.VISIBLE);
                                }
                                for (BuyerSliderObject bso : success){
                                    list.add(bso);
                                    adapter.notifyDataSetChanged();
                                }
                                empty_image.setVisibility(View.GONE);
                                rv_recycler.setVisibility(View.VISIBLE);
                                BaseFunctions.runAnimationVertical(rv_recycler,0,adapter);
                            }else {
                                empty_image.setVisibility(View.VISIBLE);
                                btn_add_image.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_loading.setVisibility(View.GONE);
                        empty_image.setVisibility(View.VISIBLE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).show();
                    }
                });
    }

    private void callAddSliderAPI(final String imagePath){
        SellerSlidersAPIsClass.addSlider(BuyerSlidersActivity.this,
                SharedPrefManager.getInstance(BuyerSlidersActivity.this).getUser().getUser_id(),
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            BuyerSliderObject success = new Gson().fromJson(j,BuyerSliderObject.class);
                            list.add(success);
                            adapter.notifyDataSetChanged();
                            if (list.size()==5){
                                btn_add_image.setVisibility(View.GONE);
                            }
                            rv_recycler.setVisibility(View.VISIBLE);
                            empty_image.setVisibility(View.GONE);
                            Toast.makeText(BuyerSlidersActivity.this, "تمت إضافة الصورة بنجاح", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAddSliderAPI(imagePath);
                                    }
                                }).show();
                    }
                });
    }

    private void callDeleteAPI(final String slider_id, final int position){
        SellerSlidersAPIsClass.deleteSlider(
                BuyerSlidersActivity.this,
                SharedPrefManager.getInstance(BuyerSlidersActivity.this).getUser().getUser_id(),
                slider_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                btn_add_image.setVisibility(View.VISIBLE);
                                list.remove(position);
                                adapter.notifyDataSetChanged();
                                Toast.makeText(BuyerSlidersActivity.this, "تم حذف الصورة بنجاح", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDeleteAPI(slider_id,position);
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
