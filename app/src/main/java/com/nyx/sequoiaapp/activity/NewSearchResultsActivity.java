package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.SearchAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.AddSellerProductHorizontalCategoriesAdapter;
import com.nyx.sequoiaapp.adapters.SearchCitiesAdapter;
import com.nyx.sequoiaapp.adapters.SearchProductsAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Category;
import com.nyx.sequoiaapp.models.City;
import com.nyx.sequoiaapp.models.Product;
import com.nyx.sequoiaapp.models.SearchResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class NewSearchResultsActivity extends RootActivity {

    private RelativeLayout root;
    private TextView tv_title,tv_desc,tv_cats,tv_products;
    private ImageView empty_image;
    private ProgressBar pb_first,pb_second;
    private RecyclerView rv_categories,rv_products;
    private List<Product> listOfProducts;
    private List<Category> listOfCategories;
    private AddSellerProductHorizontalCategoriesAdapter categories_adapter;
    private SearchProductsAdapter productsAdapter;
    private LinearLayoutManager categories_layout_manager;
    private GridLayoutManager product_layout_manager;
    private NestedScrollView scrollView;
    private int currentPage = 1;
    private int per_page = 20;
    private FloatingActionButton fab_chat;
    private EditText edt_search;
    private TextView tv_city;
    private String selected_city_id = "-1";
    private String selected_city_name = "كل المدن";
    private String selected_text = "";

    private Intent myIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_search_results);
        init_views();
        init_events();
        init_activity();
        init_products_recycler();
        init_categories_recycler();
    }

    private void init_events() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfProducts.size()>=per_page){
                            currentPage++;
                            CallSearchAPI(currentPage,1,selected_text,selected_city_id);
                        }
                    }
                }
            }
        });
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewSearchResultsActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(NewSearchResultsActivity.this, "الرجاء كتابة ما تريد البحث عنه", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    getSupportActionBar().setTitle("بحث عن  "+edt_search.getText().toString());
                    tv_title.setText("بحث عن  "+edt_search.getText().toString());
                    tv_cats.setVisibility(View.GONE);
                    tv_products.setVisibility(View.GONE);
                    currentPage = 1;
                    listOfCategories.clear();
                    listOfProducts.clear();
                    rv_products.setAdapter(new SearchProductsAdapter(NewSearchResultsActivity.this,
                            new ArrayList<Product>()));
                    rv_categories.setAdapter(new AddSellerProductHorizontalCategoriesAdapter(NewSearchResultsActivity.this,
                            new ArrayList<Category>(), new IMove() {
                        @Override
                        public void move(int position) {
                            Intent ii = new Intent(NewSearchResultsActivity.this, CategoryProductsActivity.class);
                            ii.putExtra("id", listOfCategories.get(position).getId());
                            ii.putExtra("title", listOfCategories.get(position).getName());
                            startActivity(ii);
                        }
                    }));
                    selected_text = edt_search.getText().toString();
                    CallSearchAPI(currentPage,0,selected_text,selected_city_id);
                    return true;
                }
                return false;
            }
        });

        tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(NewSearchResultsActivity.this);
                View view = getLayoutInflater().inflate(R.layout.dialog_search_city,null);
                RecyclerView rv_recycler = view.findViewById(R.id.dialog_city_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                final ArrayList cities = SharedPrefManager.getInstance(NewSearchResultsActivity.this).getCities();
                final ArrayList source_cities = new ArrayList();
                City cc = new City("-1","كل المدن");
                source_cities.add(cc);
                if (cities.size() > 0){
                    for (int i = 0; i < cities.size() ; i++) {
                        City c = (City) cities.get(i);
                        if (c.getName().equals("دمشق")||c.getName().equals("طرطوس")||c.getName().equals("حمص")){
                            source_cities.add(c);
                        }
                    }
                }
                SearchCitiesAdapter adapter = new SearchCitiesAdapter(NewSearchResultsActivity.this, source_cities,selected_city_id, new IMove() {
                    @Override
                    public void move(int position) {
                        City c = (City) source_cities.get(position);
                        selected_city_id = c.getId();
                        selected_city_name = c.getName();
                        tv_city.setText(selected_city_name);
                        dialog.dismiss();
                    }
                });
                LinearLayoutManager layoutManager = new LinearLayoutManager(NewSearchResultsActivity.this,LinearLayoutManager.VERTICAL,false);
                rv_recycler.setLayoutManager(layoutManager);
                rv_recycler.setAdapter(adapter);
                dialog.show();
            }
        });
    }

    private void init_views() {
        //Root
        root = findViewById(R.id.search_results_layout);
        //Intent
        myIntent = getIntent();
        //Toolbar
        toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("بحث عن  "+myIntent.getStringExtra("query"));
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
        //TextView
        tv_title = findViewById(R.id.title);
        tv_desc = findViewById(R.id.description);
        tv_cats = findViewById(R.id.search_results_categories_text);
        tv_products = findViewById(R.id.search_results_products_text);
        tv_city = findViewById(R.id.search_txt);
        tv_city.setText("كل المدن");
        //ImageView
        empty_image = findViewById(R.id.no_items_found);
        //Progress Bar
        pb_first = findViewById(R.id.loading);
        pb_second = findViewById(R.id.loading_more);
        //RecyclerView
        rv_categories = findViewById(R.id.recyclerview5);
        rv_products = findViewById(R.id.recyclerview6);
        //ScrollView
        scrollView = findViewById(R.id.search_results_scroll);
        //EditText
        edt_search = findViewById(R.id.home_search);


    }
    private void init_activity() {
        tv_title.setText("بحث عن  "+myIntent.getStringExtra("query"));
        tv_desc.setText("تمتع بتجربة التسوق الذكية من سيكويا");
        //edt_search.setText(myIntent.getStringExtra("query"));
        selected_city_id = myIntent.getStringExtra("city_id");
        selected_text = myIntent.getStringExtra("query");
        tv_city.setText(myIntent.getStringExtra("city_name"));
        CallSearchAPI(currentPage,0,selected_text,selected_city_id);
    }

    private void init_products_recycler(){
        listOfProducts = new ArrayList<>();
        productsAdapter = new SearchProductsAdapter(NewSearchResultsActivity.this,listOfProducts);
        product_layout_manager = new GridLayoutManager(NewSearchResultsActivity.this,2){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_products.setLayoutManager(product_layout_manager);
        rv_products.setAdapter(productsAdapter);

    }
    private void init_categories_recycler(){
        listOfCategories = new ArrayList<>();
        categories_adapter = new AddSellerProductHorizontalCategoriesAdapter(NewSearchResultsActivity.this, listOfCategories, new IMove() {
            @Override
            public void move(int position) {
                Intent ii = new Intent(NewSearchResultsActivity.this, CategoryProductsActivity.class);
                ii.putExtra("id", listOfCategories.get(position).getId());
                ii.putExtra("title", listOfCategories.get(position).getName());
                startActivity(ii);
            }
        });
        categories_layout_manager = new LinearLayoutManager(NewSearchResultsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_categories.setLayoutManager(categories_layout_manager);
        rv_categories.setAdapter(categories_adapter);
    }


    private void CallSearchAPI(final int page, final int type, final String text, final String city_id){
        final boolean[] cat_found = {false};
        final boolean[] prod_fount = {false};
        empty_image.setVisibility(View.GONE);
        if (type == 1){
            pb_first.setVisibility(View.GONE);
            pb_second.setVisibility(View.VISIBLE);
        }else if (type == 0){
            pb_first.setVisibility(View.VISIBLE);
            pb_second.setVisibility(View.GONE);
        }
        SearchAPIsClass.search(NewSearchResultsActivity.this,
                text,
                city_id,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            SearchResponse success = new Gson().fromJson(json1,SearchResponse.class);
                            if (success.getCategories()!=null){
                                if (success.getCategories().size() > 0){
                                    for (Category c : success.getCategories()){
                                        listOfCategories.add(c);
                                        categories_adapter.notifyDataSetChanged();
                                    }
                                    tv_cats.setVisibility(View.VISIBLE);
                                    rv_categories.setVisibility(View.VISIBLE);
                                    if (type == 0){
                                        cat_found[0] = true;
                                    }
                                }else {
                                    if (type == 0){
                                        tv_cats.setVisibility(View.GONE);
                                        rv_categories.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    tv_cats.setVisibility(View.GONE);
                                    rv_categories.setVisibility(View.GONE);
                                }

                            }
                            if (success.getProducts()!=null){
                                per_page = success.getProducts().getPer_page();
                                if (success.getProducts().getData()!=null){
                                    if (success.getProducts().getData().size()>0){
                                        for (Product p : success.getProducts().getData()){
                                            listOfProducts.add(p);
                                            productsAdapter.notifyDataSetChanged();
                                        }
                                        tv_products.setVisibility(View.VISIBLE);
                                        rv_products.setVisibility(View.VISIBLE);
                                        if (type == 0){
                                            BaseFunctions.runAnimationVertical(rv_products,0,productsAdapter);
                                        }
                                        if (type == 0){
                                            prod_fount[0] = true;
                                        }
                                    }else {
                                        if (type == 0){
                                            tv_products.setVisibility(View.GONE);
                                            rv_products.setVisibility(View.GONE);
                                        }
                                    }
                                }else {
                                    if (type == 0){
                                        tv_products.setVisibility(View.GONE);
                                        rv_products.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    tv_products.setVisibility(View.GONE);
                                    rv_products.setVisibility(View.GONE);
                                }
                            }
                            if (type == 0){
                                pb_first.setVisibility(View.GONE);
                            }else {
                                pb_second.setVisibility(View.GONE);
                            }
                            if (type == 0){
                                tv_desc.setVisibility(View.VISIBLE);
                                if (!cat_found[0]&&!prod_fount[0]){
                                    empty_image.setVisibility(View.VISIBLE);
                                    tv_desc.setText("لم يتم العثور على نتائج متعلقة ب( "+text +") حالياً , قم تجد طلبك اذا قمت بالبحث في وقت لاحق او قمت بتغيير عبارات البحث لديك او تدقيق العبارة المكتوبة , ننتظر اطلاعك على منتجاتنا في سوق سيكويا ,أهلا بك .. ");
                                }else if (!cat_found[0]&&prod_fount[0]){
                                    tv_desc.setText("جرب التجول ضمن أكثر من "+String.valueOf(success.getProducts().getData().size())+
                                            " منتجاً مختلفا "+" يتعلق ب"+text+ " , نتمنى لك تسوقاً ممتعاً");
                                }else if (cat_found[0]&&!prod_fount[0]){
                                    tv_desc.setText("جرب التجول ضمن أكثر من "+String.valueOf(success.getCategories().size())+
                                            " صنفا مختلفا "+" يتعلق ب"+text+ " , نتمنى لك تسوقاً ممتعاً");
                                }else if (cat_found[0]&&prod_fount[0]){
                                    tv_desc.setText("جرب التجول ضمن أكثر من "+String.valueOf(success.getProducts().getData().size())+
                                            " منتجاً مختلفا "+" و أكثر من "+String.valueOf(success.getCategories().size())+" صنفا مختلفا "+" يتعلق ب"+text+ " , نتمنى لك تسوقاً ممتعاً");
                                }
                            }
                        }else {
                            pb_first.setVisibility(View.GONE);
                            pb_second.setVisibility(View.GONE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_first.setVisibility(View.GONE);
                        pb_second.setVisibility(View.GONE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CallSearchAPI(page,type,text,city_id);
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
