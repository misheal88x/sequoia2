package com.nyx.sequoiaapp.activity;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CitiesAdapter;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.City;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class CreateAccountByRefActivity extends SignupActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("حساب جديد");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final ArrayList cities = SharedPrefManager.getInstance(this).getCities();
        final Spinner spinner = (Spinner)findViewById(R.id.city_spinner);
        CitiesAdapter adapter = new CitiesAdapter(this ,cities);
        final TextView textView = (TextView)findViewById(R.id.description);
        textView.setText( "انشاء حساب من قبل الدليل " +
        SharedPrefManager.getInstance(this).getUser().getName()
        );

        spinner.setAdapter(adapter);

        findViewById(R.id.login_button).setVisibility(View.INVISIBLE);

        findViewById(R.id.go_login).setVisibility(View.INVISIBLE);



        findViewById(R.id.go_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = ((EditText)findViewById(R.id.phone_input)).getText().toString().trim();
                final String password = ((EditText)findViewById(R.id.password_input)).getText().toString().trim();
                final String name = ((EditText)findViewById(R.id.name_input)).getText().toString().trim();
                final String address = ((EditText)findViewById(R.id.address_input)).getText().toString().trim();
                final String email = ((EditText)findViewById(R.id.email_input)).getText().toString().trim();
                final String city =
                        (cities.size()>0)?
                        ((City)cities.get(spinner.getSelectedItemPosition())).getId():"0";

                if(name.equals("")){
                    Toast.makeText(getApplicationContext(), "أدخل اسم الزبون من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(name.split(" ").length<2){
                    Toast.makeText(getApplicationContext(), "أدخل الاسم و الكنية ..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(address.equals("")){
                    Toast.makeText(getApplicationContext(), "أدخل عنوان الزبون من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
//                if(!email.equals("") && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
//                    Toast.makeText(getApplicationContext(), "أدخل   البريد  الالكتروني من فضلك..", Toast.LENGTH_SHORT).show();
//                    return;
//                }

                if(phone.equals("")){
                    Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف  من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                Pattern p = Pattern.compile("^09[0-9]{2}[0-9]{6}$");
                if(!p.matcher(phone).matches()){
                    Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف بصيغة صحيحة  من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(password.equals("")){
                    Toast.makeText(getApplicationContext(), "أدخل كلمة المرور  من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(password.length()<8){
                    Toast.makeText(getApplicationContext(), "كلمة المرور يجب ان تكون أكثر من 8 رموز", Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String  ,String> params=new HashMap<String, String>();
                params.put("mobile_number" ,phone);
                params.put("password" ,password);
                params.put("fullname" ,name);
                params.put("email" ,email);
                params.put("city" ,city);
                params.put("address" ,address);
                params.put("ref_id" ,
                        SharedPrefManager.getInstance(CreateAccountByRefActivity.this).getUser().getUser_id()
                        );

                showLoading(true);

                BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
                    @Override
                    public void doTask() {

                    }

                    @Override
                    public void doTask(String s) {
                        if(!s.equals("")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showLoading(false);
                                }
                            });
                            try {
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("status") == 1) {
                                    Toast.makeText(getApplicationContext(), "تم انشاء حساب الزبون  " +
                                                    name + " بنجاح"
                                            , Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception fdr) {
                                //Log.d("FDR SIGNUPREF : ", fdr.getMessage());
                            }
                        }else{
                            Toast.makeText(CreateAccountByRefActivity.this,  "حدث خلل اثناء الاتصال , حاول من جديد", Toast.LENGTH_SHORT).show();
                        }


                    }
                }  , APIUrl.SERVER + "user/register",params);


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
