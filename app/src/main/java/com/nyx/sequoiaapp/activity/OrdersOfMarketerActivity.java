package com.nyx.sequoiaapp.activity;

import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.View;

import com.nyx.sequoiaapp.adapters.OrderAdapterEditable;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.ExtendedOrder;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrdersOfMarketerActivity extends ViewProductsActivity {

    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);
            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);

        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);


                try {

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray orders = heart.getJSONObject("data").getJSONArray("orders");
                    setDescription( "يوجد " + heart.getJSONObject("data")
                            .getInt("total_count")+" طلبية حاليا تمت من قبل زبائن  "
                            + SharedPrefManager.getInstance(OrdersOfMarketerActivity.this).getUser().getName());
                    for (int i = 0; i < orders.length(); i++) {
                        ExtendedOrder or = new ExtendedOrder(
                                orders.getJSONObject(i).getString("id"),
                                orders.getJSONObject(i).getString("create_date") ,
                                orders.getJSONObject(i).getString("status_str") ,
                                orders.getJSONObject(i).getJSONArray("items"),
                                orders.getJSONObject(i).getString("total"),

                                orders.getJSONObject(i).getString("fullname"),
                                orders.getJSONObject(i).getString("status"),
                                orders.getJSONObject(i).getString("ref_note") ,

                                orders.getJSONObject(i).getString("fullname")+"\r\n"+
                                        orders.getJSONObject(i).getString("mobile")+"\r\n"+
                                        orders.getJSONObject(i).getString("city"),
                                orders.getJSONObject(i).getString("notes_customer")
                        );
                        items.add(or);

                    }

                    adapter.notifyDataSetChanged();
                    if (current<20){
                        BaseFunctions.runAnimationHorizontal(mainRecylceView,0,adapter);
                    }
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription("لا يوجد أي طلبيات نشطة حالياً من زبائن المسوق  " + SharedPrefManager.
                                getInstance(getApplicationContext()).getUser().getName()+".") ;
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                    //Log.d("order_jex in : " , jex.getMessage());
                }
            }
        }, APIUrl.SERVER  +
                "order/city/marketer/"+ SharedPrefManager.getInstance(this).getUser().getUser_id()+"/"+current);
    }
    int current=0;

    boolean loadingFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle("الطلبيات");
    }

    @Override
    void fetchData() {
        setWindowTitle("طلبيات زبائني " );
        setDescription( "طلبيات زبائن المسوق "+SharedPrefManager.getInstance(this).getUser().getName()+ " التي تم ارسالها مؤخراً");
        LinearLayoutManager lm = new LinearLayoutManager(this) ;
        adapter= new OrderAdapterEditable(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);

    }

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore(0);
            }

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
