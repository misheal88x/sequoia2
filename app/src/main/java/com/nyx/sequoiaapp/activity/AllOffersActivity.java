package com.nyx.sequoiaapp.activity;

import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.View;

import com.nyx.sequoiaapp.adapters.AdsAdapterOffers;
import com.nyx.sequoiaapp.adapters.AdsAdapterOffersGrid;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllOffersActivity extends ViewProductsActivity {



    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);
            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);


                try {
                    ArrayList temp_offers = new ArrayList();

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray products = heart.getJSONObject("data").getJSONArray("discount");
                    max=heart.getJSONObject("data").getInt("total_count");
                    setDescription("تابع هنا آخر العروض من سيكويا لفترة محدودة " +
                            "و احصل على اسعار و خصومات لن تجدها في أي سوق الكتروني آخر !"

                    +"\r\n" +
                    " يوجد حالياً لهذا اليوم أكثر من " + max + " عرضاً .");



                    for (int i = 0; i < products.length(); i++) {
                        ExtendedPost ep =  new ExtendedPost(products.getJSONObject(i));
                        if (Integer.valueOf(ep.getRemaining())>0){
                            items.add(ep);
                        }
                    }

                    adapter.notifyDataSetChanged();
                    if (current<20){
                        BaseFunctions.runAnimationVertical(mainRecylceView,0,adapter);
                    }
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription("لم يتم العثور على منتجات في الوقت الحالي , جرب زيارة هذه الصفحة في وقت لاحق لتكون على إطلاع بآخر المنتجات التي قد تهمك ");
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                }
            }
        }, APIUrl.SERVER  +
                "discount/get_all/"+ current);
    }
    int current=0;

    boolean loadingFlag = false;

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore(0);
            }

        }
    }

    @Override
    void setTitle() {
getSupportActionBar().setTitle("العروض");
    }

    @Override
    void fetchData() {
        setWindowTitle("عروض سيكويا");
        setDescription("تابع هنا آخر العروض من سيكويا لفترة محدودة و احصل على اسعار و خصومات لن تجدها في أي سوق الكتروني آخر !");

        GridLayoutManager lm = new GridLayoutManager(this,
                2) ;
        ;
        adapter= new AdsAdapterOffersGrid(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
