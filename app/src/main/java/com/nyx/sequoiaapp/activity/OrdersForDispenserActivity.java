package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.DispenserAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.adapters.OrderAdapterEditable;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.ExtendedOrder;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrdersForDispenserActivity extends ViewProductsActivity {
    private int get_type = 0;

    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            get_type = 1;
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);
            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);

        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);


                try {

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray orders = heart.getJSONObject("data").getJSONArray("orders");
                    setDescription( "يوجد " + heart.getJSONObject("data").getInt("total_count")+" طلبية حاليا تمت من قبل زبائن  "
                            + SharedPrefManager.
                            getInstance(getApplicationContext()).getUser().getName());
                    for (int i = 0; i < orders.length(); i++) {
                        ExtendedOrder or = new ExtendedOrder(
                                orders.getJSONObject(i).getString("id"),
                                orders.getJSONObject(i).getString("create_date") ,
                                orders.getJSONObject(i).getString("status_str") ,
                                orders.getJSONObject(i).getJSONArray("items"),
                                orders.getJSONObject(i).getString("total"),
                                orders.getJSONObject(i).getString("fullname"),
                                orders.getJSONObject(i).getString("status") ,
                                orders.getJSONObject(i).getString("ref_note") ,
                                orders.getJSONObject(i).getString("fullname")+"\r\n"+
                                        orders.getJSONObject(i).getString("mobile")+"\r\n"+
                                        orders.getJSONObject(i).getString("city") ,
                                orders.getJSONObject(i).getString("notes_customer")

                        );
                        items.add(or);

                    }

                    adapter.notifyDataSetChanged();
                    if (current<20){
                        BaseFunctions.runAnimationHorizontal(mainRecylceView,0,adapter);
                    }
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription("لا يوجد أي طلبيات نشطة حالياً تخص الموزع  " + SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()+".") ;
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                    //Log.d("order_jex in : " , jex.getMessage());
                }
            }
        }, APIUrl.SERVER  +
                "order/city/dispenser/"+ SharedPrefManager.getInstance(this).getUser().getUser_id()+"/"+current);
    }
    int current=0;

    boolean loadingFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle(SharedPrefManager.
                getInstance(getApplicationContext()).getUser().getName());
    }

    @Override
    void fetchData() {
        this.lyt_search.setVisibility(View.VISIBLE);
        setWindowTitle("طلبيات زبائني " );
        setDescription( "طلبيات تخص الموزع "+SharedPrefManager.getInstance(this).getUser().getName()+ " التي تم ارسالها مؤخراً");
        LinearLayoutManager lm = new LinearLayoutManager(this) ;
        adapter= new OrderAdapterEditable(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        get_type = 1;
        loadMore(0);
        edt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length()>0){
                    //btn_clear.setVisibility(View.VISIBLE);
                }else {
                    items.clear();
                    mainRecylceView.setAdapter(new OrderAdapterEditable(items,OrdersForDispenserActivity.this));
                    current = 0;
                    get_type = 1;
                    loadMore(0);
                    //btn_clear.setVisibility(View.GONE);
                }
            }
        });
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_search.getText().toString().equals("")){
                    Toast.makeText(OrdersForDispenserActivity.this, "الرجاء كتابة اسم المنتج للبحث", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    current = 0;
                    items.clear();
                    mainRecylceView.setAdapter(new OrderAdapterEditable(items,OrdersForDispenserActivity.this));
                    get_type = 2;
                    callSearchAPI();
                }
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(OrdersForDispenserActivity.this, "الرجاء كتابة اسم المنتج للبحث", Toast.LENGTH_SHORT).show();
                        return false;
                    }else {
                        current = 0;
                        items.clear();
                        mainRecylceView.setAdapter(new OrderAdapterEditable(items,OrdersForDispenserActivity.this));
                        get_type = 2;
                        callSearchAPI();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                if (get_type == 1){
                    loadMore(0);
                }else {
                    callSearchAPI();
                }

            }

        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void callSearchAPI(){
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
        String user_id = SharedPrefManager.getInstance(OrdersForDispenserActivity.this).getUser().getUser_id();
        DispenserAPIsClass.search(OrdersForDispenserActivity.this,
                user_id,
                edt_search.getText().toString(),
                current,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            try {
                                JSONObject heart = new JSONObject(json1);
                                //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                                JSONArray orders = heart.getJSONObject("data").getJSONArray("orders");
                                setDescription( "يوجد " + heart.getJSONObject("data").getInt("total_count")+" طلبية حاليا تمت من قبل زبائن  "
                                        + SharedPrefManager.
                                        getInstance(getApplicationContext()).getUser().getName());
                                for (int i = 0; i < orders.length(); i++) {
                                    ExtendedOrder or = new ExtendedOrder(
                                            orders.getJSONObject(i).getString("id"),
                                            orders.getJSONObject(i).getString("create_date") ,
                                            orders.getJSONObject(i).getString("status_str") ,
                                            orders.getJSONObject(i).getJSONArray("items"),
                                            orders.getJSONObject(i).getString("total"),
                                            orders.getJSONObject(i).getString("fullname"),
                                            orders.getJSONObject(i).getString("status") ,
                                            orders.getJSONObject(i).getString("ref_note") ,
                                            orders.getJSONObject(i).getString("fullname")+"\r\n"+
                                                    orders.getJSONObject(i).getString("mobile")+"\r\n"+
                                                    orders.getJSONObject(i).getString("city") ,
                                            orders.getJSONObject(i).getString("notes_customer")

                                    );
                                    items.add(or);

                                }

                                adapter.notifyDataSetChanged();
                                loadingFlag = false;
                                current += 10;
                                if(items.size()==0){
                                    noItemsFound.setVisibility(View.VISIBLE);
                                    setDescription("لا يوجد أي طلبيات نشطة حالياً تخص الموزع  " + SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()+".") ;
                                }
                                hideKeyboard(OrdersForDispenserActivity.this);
                            } catch (JSONException jex) {
                                hideKeyboard(OrdersForDispenserActivity.this);
                                loadingFlag = false;
                            }
                        }else {
                            hideKeyboard(OrdersForDispenserActivity.this);
                            loading.setVisibility(View.GONE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        hideKeyboard(OrdersForDispenserActivity.this);
                        Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        get_type = 2;
                                        callSearchAPI();
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
