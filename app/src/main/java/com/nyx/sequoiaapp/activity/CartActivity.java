package com.nyx.sequoiaapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.OrdersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CartAdapter;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.Cart;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.models.LoginObject;
import com.nyx.sequoiaapp.models.OrderItem;
import com.nyx.sequoiaapp.models.OrderObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Luminance on 5/20/2018.
 */

public class CartActivity extends ViewProductsActivity {
    private ProgressDialog progressDialog;
    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
//
        if (y > 100) fab.show();
        else
            fab.hide();
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle("السلة");
    }

    @Override
    void fetchData() {
        Glide.with(this).load(R.drawable.emptycart).into(noItemsFound);
        loading.setVisibility(View.GONE);
        Cart cart = SavedCacheUtils.getCart(this);
        this.items = cart.getItems();
        if (this.items.size() > 0) {
            //  Toast.makeText(this, "loading " + products.size() + " items", Toast.LENGTH_SHORT).show();
            adapter = new CartAdapter(items, this, new PostAction() {
                @Override
                public void doTask() {
                    final Cart c1 = SavedCacheUtils.getCart(CartActivity.this);
                    updateInfo(c1);
                    setNotifCount(c1.size());
                }

                @Override
                public void doTask(String s) {
                    //
                }
            });
            GridLayoutManager lm = new GridLayoutManager(this,
                    2) {
                @Override
                public boolean supportsPredictiveItemAnimations() {
                    return true;
                }
            };
            ;
            mainRecylceView.setLayoutManager(lm);
            mainRecylceView.setItemAnimator(new DefaultItemAnimator());
            mainRecylceView.setAdapter(adapter);
            updateInfo(cart);
            mainScrollView.scrollTo(0, 0);
        } else {
            showEmpty();
        }
    }

    void showEmpty() {
        this.noItemsFound.setImageResource(R.drawable.emptycart);
        this.noItemsFound.setVisibility(View.VISIBLE);
        this.continue_shopping.setVisibility(View.VISIBLE);
        setWindowTitle("سلة شرائك فارغة");
        setDescription("تسوق ضمن فئات سيكويا و اعلاناته و انتقي ما تشاء من المنتجات و أضفها لسلة مشترياتك ..");

    }

    private void showEmptyHappy(){
        this.noItemsFound.setImageResource(R.drawable.emptycarthappy);
        this.noItemsFound.setVisibility(View.VISIBLE);
        this.continue_shopping.setVisibility(View.VISIBLE);
        setWindowTitle("سلة شرائك فارغة");
        setDescription("تسوق ضمن فئات سيكويا و اعلاناته و انتقي ما تشاء من المنتجات و أضفها لسلة مشترياتك ..");
    }

    void updateInfo(Cart cart) {
        if (cart.getItems().size() == 0) {
            showEmpty();
            craete_order.setVisibility(View.GONE);
            findViewById(R.id.notes_layout).setVisibility(View.GONE);
        } else {
            craete_order.setVisibility(View.VISIBLE);
            findViewById(R.id.notes_layout).setVisibility(View.VISIBLE);
            double total = 0;
            //todo
            ArrayList cartItems = cart.getItems();
            final ArrayList<OrderItem> order = new ArrayList<>();
            for (int i = 0; i < cartItems.size(); i++) {
                CartItem cartItem = (CartItem) cartItems.get(i);
                ExtendedPost post = cartItem.getPost();
                total += (double) cartItem.getQuantity() * Double.parseDouble(post.getNewprice());
                order.add(new OrderItem(cartItem.getPost().getId(), cartItem.getPost().getNewprice(),
                        cartItem.getQuantity() + "", cartItem.getSize(), cartItem.getColor(), cartItem.getMeasure()
                ));
            }
            setWindowTitle("محتويات سلة الشراء");
            if (cart.size() == 1){
                setDescription("لديك منتج واحد في سلة المشتريات الان");
            }else if (cart.size() == 2){
                setDescription("لديك منتجين في سلة المشتريات الان");
            }else {
                setDescription("لديك " + cart.size() + " منتجات في سلة المشتريات الان");
            }
            //setDescription("لديك " + cart.size() + " منتج , من " + cartItems.size() + " عنصر , " + " بكلفة اجمالية : " +
                    //((int)total) + " ل.س");


            craete_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(!ConnectionUtils.isNetworkAvailable(CartActivity.this)) {
                        Toast.makeText(CartActivity.this, "اتصل بالانترنت من فضلك", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!SharedPrefManager.getInstance(CartActivity.this).IsUserLoggedIn()) {
                        startActivity(new Intent(CartActivity.this, LoginActivty.class));
                        return;
                    }
                    final User user = SharedPrefManager.getInstance(CartActivity.this).getUser();

                    if (user.getName().equals("")) {
                        Toast.makeText(CartActivity.this, "من فضلك قم بإضافة اسمك !", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CartActivity.this, AccountActivity.class));
                        return;
                    }

                    if (user.getPhone().equals("")) {
                        Toast.makeText(CartActivity.this, "من فضلك قم بإضافة رقم هاتفك !", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CartActivity.this, AccountActivity.class));
                        return;
                    }

                    if (user.getAddress().equals("")) {
                        Toast.makeText(CartActivity.this, "من فضلك قم بإضافة عنوانك !", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CartActivity.this, AccountActivity.class));
                        return;
                    }
                    if (user.getCity().equals("")) {
                        Toast.makeText(CartActivity.this, "من فضلك قم بإضافة مدينتك !", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CartActivity.this, AccountActivity.class));
                        return;
                    }
                    double total = 0;
                    Cart cart = SavedCacheUtils.getCart(CartActivity.this);
                    ArrayList cartItems = cart.getItems();
                    for (int i = 0; i < cartItems.size(); i++) {
                        CartItem cartItem = (CartItem) cartItems.get(i);
                        ExtendedPost post = cartItem.getPost();
                        total += (double) cartItem.getQuantity() * Double.parseDouble(post.getNewprice());
                    }
                    String firstMessage = "إن قيمة فاتورتك الإجماليةهي : "+String.valueOf(total)+" ل.س";
                    String secondMessage = "\n\n"+ SharedPrefManager.getInstance(CartActivity.this).getFreeDeliveryDetails();
                    String thirdMessage = "\n\n"+"هل ترغب بالمتابعة في تأكيد طلبيتك ؟";
                    String finalMessage = "";
                    //if (total < Double.valueOf(String.valueOf(SharedPrefManager.getInstance(CartActivity.this).getLowPrice()))){
                      //  finalMessage = firstMessage+secondMessage+thirdMessage;
                    //}else {
                        finalMessage = firstMessage+secondMessage+thirdMessage;
                    //}
                    AlertDialog.Builder confirmBuilder = new AlertDialog.Builder(CartActivity.this);
                    confirmBuilder.setTitle("قيمة الفاتورة").
                            setMessage(finalMessage).
                            setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    showConfirmPaymentDialog(user,order);
                                }
                            }).
                            setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();


                    /*
                    new AlertDialog.Builder(CartActivity.this)
                            .setTitle("ارسال الطلبية")
                            .setMessage("سيتم الآن ارسال طلبيتك الى قسم التوصيل و الشحن ليتم توصيلها اليك حالاً , يمكنك تفقد حالة الطلبية لاحقاً من خلال الذهاب الى محفظتي في التطبيق , و يمكنك التواصل معنا في قسم الدعم الفني للحصول على أي معلومات إضافية")
                            .setIcon(android.R.drawable.ic_menu_info_details)
                            .setPositiveButton("تأكيد الطلب", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {









                                }})
                            .setNegativeButton("الغاء الامر", null).show();
**/


                }
            });
        }
    }

    private void showConfirmPaymentDialog(final User user, final ArrayList<OrderItem> order){
        AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_confirm_order,null);
        Button btn_ok = view.findViewById(R.id.dialog_confirm_order_ok);
        Button btn_cancel = view.findViewById(R.id.dialog_confirm_order_cancel);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                craete_order.setText("جاري ارسال الطلبية");
                craete_order.setEnabled(false);
                Gson gson = new Gson();
                String itemsJson = gson.toJson(order);
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id", user.getUser_id());
                params.put("user_token", user.getToken());
                params.put("items", itemsJson);
                String notes = ((EditText)  findViewById(R.id.notes))
                        .getText().toString().trim();
                params.put("notes", notes);
                Log.i("detillll", "onClick: "+user.getUser_id());
                Log.i("detillll", "onClick: "+user.getToken());
                Log.i("detillll", "onClick: "+itemsJson);
                Log.i("detillll", "onClick: "+notes);
                dialog.cancel();
                order(itemsJson,notes);
                //login(itemsJson,notes);
                /*
                BackgroundServices.getInstance(CartActivity.this).CallPost(new PostAction() {
                    @Override
                    public void doTask() {

                    }

                    @Override
                    public void doTask(String s) {
                        dialog.cancel();
                        craete_order.setEnabled(true);
                        craete_order.setText("إرسال الطلبية من جديد");
                        if (!s.equals("")) {
                            try {

                                JSONObject res = new JSONObject(s);
                                Toast.makeText(CartActivity.this, res.getString("message"),
                                        Toast.LENGTH_SHORT).show();
                                if(res.getInt("status")==1){
                                    craete_order.setVisibility(View.GONE);
                                    SavedCacheUtils.clearCart(CartActivity.this);
                                    showEmptyHappy();
                                    items.clear();
                                    adapter.notifyDataSetChanged();
                                    Snackbar.make(mainScrollView, "سيصل طلبك اليك خلال فترة أقصاها  1 يوم.", Snackbar.LENGTH_INDEFINITE)
                                            .setAction("موافق", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    finish();
                                                }
                                            }).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(CartActivity.this, "مشكلة في الشبكة , حاول من جديد", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, APIUrl.SERVER + "order", params);
**/
            }
        });

        dialog.show();
    }

    private void login(final String items, final String notes){
        final HashMap<String  ,String> params=new HashMap<String, String>();
        String type = SharedPrefManager.getInstance(CartActivity.this).getLoginType();
        LoginObject lo = SharedPrefManager.getInstance(CartActivity.this).getLoginData();
        if (type.equals("normal")){
            params.put("mobile_number",lo.getMobile_number());
            params.put("login_type" ,lo.getLogin_type());
            params.put("password" ,lo.getPassword());
        }else if (type.equals("facebook")){
            params.put("login_type" ,lo.getLogin_type());
            params.put("facebook_id" ,lo.getFacebook_id());
            params.put("access_token" , lo.getAccess_token());
        }
        progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(!s.equals("")){
                    try {
                        JSONObject object =new JSONObject(s);
                        if(object.getInt("status")==1)
                        {
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                                    object.getJSONObject("data") ,params.get("password"));
                            Log.i("tokennn", "doTask: "+SharedPrefManager.getInstance(CartActivity.this).getUser().getToken());
                            //order(items,notes,object.getJSONObject("data").getString("reset_token"));
                        }else{
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception fdr){ }
                }else{
                    Toast.makeText(CartActivity.this, "حدث خلل اثناء الاتصال , حاول من جديد", Toast.LENGTH_SHORT).show();
                }
            }
        }  , APIUrl.SERVER2 + "user/login",params);
    }

    private void order(final String itemss, String notes){
        progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        User user = SharedPrefManager.getInstance(CartActivity.this).getUser();
        OrdersAPIsClass.addOrder(CartActivity.this,
                user.getUser_id(),
                user.getToken(),
                itemss,
                notes, new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        OrderObject[] success = new Gson().fromJson(j, OrderObject[].class);
                        progressDialog.cancel();
                        craete_order.setEnabled(true);
                        craete_order.setText("إرسال الطلبية من جديد");
                        Toast.makeText(CartActivity.this, "تم إضافة الطلبية بنجاح",
                                Toast.LENGTH_SHORT).show();
                        craete_order.setVisibility(View.GONE);
                        SavedCacheUtils.clearCart(CartActivity.this);
                        showEmptyHappy();
                        items.clear();
                        adapter.notifyDataSetChanged();
                        Snackbar.make(mainScrollView, "تتبع طلبك من زر القائمة و شكرا.", Snackbar.LENGTH_INDEFINITE)
                                .setAction("موافق", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                }).show();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        sendMessageByEmail(CartActivity.this,itemss.substring(0,250));
                        Toast.makeText(CartActivity.this, "مشكلة في الشبكة , حاول من جديد", Toast.LENGTH_SHORT).show();
                    }
                });
        /*
        HashMap<String  ,String> params=new HashMap<String, String>();
        User user = SharedPrefManager.getInstance(CartActivity.this).getUser();
        params.put("user_id", user.getUser_id());
        params.put("user_token", token);
        params.put("items", itemss);
        params.put("notes", notes);

        BackgroundServices.getInstance(CartActivity.this).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                progressDialog.cancel();
                craete_order.setEnabled(true);
                craete_order.setText("إرسال الطلبية من جديد");
                if (!s.equals("")) {
                    try {
                        JSONObject res = new JSONObject(s);
                        Toast.makeText(CartActivity.this, res.getString("message"),
                                Toast.LENGTH_SHORT).show();
                        if(res.getInt("status")==1){
                            craete_order.setVisibility(View.GONE);
                            SavedCacheUtils.clearCart(CartActivity.this);
                            showEmptyHappy();
                            items.clear();
                            adapter.notifyDataSetChanged();
                            Snackbar.make(mainScrollView, "تتبع طلبك من زر القائمة و شكرا.", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("موافق", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            finish();
                                        }
                                    }).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(CartActivity.this, "مشكلة في الشبكة , حاول من جديد", Toast.LENGTH_SHORT).show();
                }

            }
        }, APIUrl.SERVER + "order", params);

         */
    }

    public static void sendMessageByEmail(Context context, String message){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{"mishealibrahim1994@gmail.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Error");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }else {
            PackageManager pm=context.getPackageManager();
            try {

                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                String text = message;

                PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp");

                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                context.startActivity(Intent.createChooser(waIntent, "Share with"));

            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(context, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}