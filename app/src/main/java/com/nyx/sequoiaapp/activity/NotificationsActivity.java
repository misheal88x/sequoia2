package com.nyx.sequoiaapp.activity;

import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.nyx.sequoiaapp.adapters.NotificationsAdapter;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Notifications;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationsActivity extends ViewProductsActivity {

    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);

            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);

//        HashMap<String  ,String> params=new HashMap<String, String>();
//        params.put("text" ,getIntent().getStringExtra("query") );
//        params.put("start" ,current+"");
        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);


                try {

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray products = heart.getJSONObject("data").getJSONArray("notifications");
                    max=heart.getJSONObject("data").getInt("total");

//                    setDescription("جرب التجول ضمن أكثر من "+max+" منتجاً مختلفا " +
//                            " يتعلق ب"+getIntent().getStringExtra("query")+ " , نتمنى لك تسوقاً ممتعاً");

                    for (int i = 0; i < products.length(); i++) {
                        items.add(new Notifications(products.getJSONObject(i).getString("id") ,

                                products.getJSONObject(i).getString("title") ,
                                products.getJSONObject(i).getString("content") ,
                                products.getJSONObject(i).getString("notification_time")
                        , products.getJSONObject(i).getString("target_type")
                                 , products.getJSONObject(i).getString("target_id")
                                 , products.getJSONObject(i).getString("payload")
                                 ,NotificationsActivity.this
                        ));

//                        JSONArray picsJson= new JSONArray();
//                        try {
//                            //String quer = products.getJSONObject(i).getString("images");
//                            //if (quer != null && !quer.equals("null"))
//                            picsJson =products.getJSONObject(i).getJSONArray("all_pics");
//                        }catch (Exception e){}
//                        String[] pics = new String[picsJson.length()];
//                        for (int j = 0; j < picsJson.length(); j++)
//                            pics[j] =
//                                    // APIUrl.POSTS_PICS_SERVER +
//                                    picsJson.getString(j);
//                        items.add(new ExtendedPost(products.getJSONObject(i)
//                                        .getString("id")
//                                        , products.getJSONObject(i)
//                                        .getString("name"),
//                                        APIUrl.POSTS_PICS_SERVER + (new JSONObject(products.getJSONObject(i).getString("thumb")))
//                                                .getJSONArray("thumbs").getString(0)
//                                        , products.getJSONObject(i)
//                                        .getString("price")
//                                        , products.getJSONObject(i)
//                                        .getString("prime_price"), products.getJSONObject(i)
//                                        .getString("description")
//                                        , pics, products.getJSONObject(i)
//                                        .getInt("search_count"), Float.parseFloat(products.getJSONObject(i)
//                                        .getString("avg_rate")) ,1   ,products.getJSONObject(i)
//                                        .getInt("is_offer") ,products.getJSONObject(i)
//                                        .getInt("time_remaining")
//                                )
//
//
//                        );

                    }

                    adapter.notifyDataSetChanged();
                    if (current<20){
                        BaseFunctions.runAnimationHorizontal(mainRecylceView,0,adapter);
                    }
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription("لا يوجد إشعارات حاليا.");
                    }

                } catch (JSONException jex) {
                    Log.i("errororrr", "doTask: "+jex.getMessage());
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                }
            }
        }, APIUrl.SERVER  +
                "notifications/my/"+
                (SharedPrefManager.getInstance(NotificationsActivity.this).IsUserLoggedIn()?
                        SharedPrefManager.getInstance(this).getUser().getUser_id():"-1") +"/"+current);
    }
    int current=0;

    boolean loadingFlag = false;

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore(0);
            }

        }
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle("الاشعارات");
    }

    @Override
    void fetchData() {
        setWindowTitle("الاشعارات");
        setDescription("تتبع الاشعارات المرسلة اليك من سيكويا");

        LinearLayoutManager lm = new LinearLayoutManager(this) ;
        adapter= new NotificationsAdapter(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);
    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPrefManager.getInstance(this).saveNotiCount(0);
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}


