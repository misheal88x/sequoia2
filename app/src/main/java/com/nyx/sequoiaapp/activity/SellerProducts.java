package com.nyx.sequoiaapp.activity;

import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.View;
import android.widget.Button;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.MyAdsAdapter;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SellerProducts extends ViewProductsActivity {
    boolean loadingFlag = false;
    private Button btn_load_more;
    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);
            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);
                btn_load_more.setVisibility(View.GONE);
                try {

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray products = heart.getJSONObject("data").getJSONArray("products");
                    max=heart.getJSONObject("data").getInt("total_count");


                    for (int i = 0; i < products.length(); i++) {


                        items.add(new ExtendedPost(products.getJSONObject(i)

                        ));

                    }

                    adapter.notifyDataSetChanged();
                    if (current<10){
                        BaseFunctions.runAnimationVertical(mainRecylceView,0,adapter);
                    }
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription("لم يتم العثور على منتجات في الوقت الحالي , جرب زيارة هذه الصفحة في وقت لاحق لتكون على إطلاع بآخر المنتجات التي قد تهمك ");
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                }
            }
        }, APIUrl.SERVER  +
                "product/get_by_seller/"+
                SharedPrefManager.getInstance(this).getUser().getUser_id()
                +"/"+ current);
    }
    int current=0;
    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                //loadMore(0);
                btn_load_more.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    void setTitle() {
getSupportActionBar().setTitle("منتجاتي");
    }

    @Override
    void fetchData() {
        btn_load_more = findViewById(R.id.get_more_btn);
        btn_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMore(0);
            }
        });
        setWindowTitle("منتجات "+SharedPrefManager.getInstance(this).getUser().getName());
        setDescription("قم بادارة منتجاتك بسهولة من هنا , تحكم بنشر او الغاء نشر اي منتج بسهولة و يسر");

        GridLayoutManager lm = new GridLayoutManager(this,
                2) ;
        ;
        adapter= new MyAdsAdapter(items , this,null);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
