package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import im.delight.android.webview.AdvancedWebView;

import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.BasicRecycleviewAdapter;
import com.nyx.sequoiaapp.adapters.ColorsAdapter;
import com.nyx.sequoiaapp.adapters.CommentsAdapter;
import com.nyx.sequoiaapp.adapters.ProductShippingAdapter;
import com.nyx.sequoiaapp.adapters.SizesAdapter;
import com.nyx.sequoiaapp.costumes.SequoiaViewPager;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.Comment;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.models.LoginObject;
import com.nyx.sequoiaapp.models.Post;
import com.nyx.sequoiaapp.utils.CustomWebClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SingleProductActivity extends RootActivity{
    int quantity_amount = 1;
    ExtendedPost p;
    TextView oldprice;
    Spinner colorSpinner , sizeSpinner , measuresSpinner;
    private ImageView iv_exist;
    private TextView tv_exist;
    private FloatingActionButton fab_chat;
    private String store_id = "";
    private String store_name = "";
    private String shipping = "";
    private Button btn_desc_more;
    private TextView tv_shipping;
    private View v_shipping;
    private LinearLayout lyt_shipping;
    private RelativeLayout lyt_shipping2;
    private RecyclerView rv_shipping;
    private List<String> shipping_list;
    private ProductShippingAdapter shipping_adapter;
    private LinearLayoutManager shipping_layout_manager;
    private AlertDialog rate_dialog;
    private ProgressDialog progressDialog;
    private boolean isRated = false;
    private TextView offer_time;
    private TextView old_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_product);
        start();

    }
    void start(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initShippingRecycler();
        p = (ExtendedPost) getIntent().getSerializableExtra("post");
        getSupportActionBar().setTitle(p.getTitle());
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Activity v = this;
        btn_desc_more = findViewById(R.id.product_desc_more);
        iv_exist = findViewById(R.id.product_exist_icon);
        tv_exist = findViewById(R.id.product_exist_txt);
        fab_chat = findViewById(R.id.open_chat);
        tv_shipping = findViewById(R.id.product_details_shipping);
        v_shipping = findViewById(R.id.shipping_view);
        lyt_shipping = findViewById(R.id.shipping_layout);
        offer_time = findViewById(R.id.offer_time);
        old_price = findViewById(R.id.old_pricee);

        if (getIntent().hasExtra("is_offer")){
            if (getIntent().getBooleanExtra("is_offer",false)){
                offer_time.setVisibility(View.VISIBLE);
                offer_time.setText(getIntent().getStringExtra("remaining"));
                old_price.setVisibility(View.VISIBLE);
                old_price.setText(getIntent().getStringExtra("old_price"));
                Log.i("dfdrevr", "start: "+getIntent().getStringExtra("old_price"));
                old_price.setPaintFlags(  old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }

        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SingleProductActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        findViewById(R.id.product_whatsapp_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone=+963 "+"949101014";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        // fetch();
        ((SimpleRatingBar) findViewById(R.id.do_rating)).setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(final SimpleRatingBar ratingBar, final float rating, boolean fromUser) {
                if (!isRated) {
                    isRated = true;
                    if (SharedPrefManager.getInstance(SingleProductActivity.this).IsUserLoggedIn()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SingleProductActivity.this);
                        View view = getLayoutInflater().inflate(R.layout.rating_dialog, null);
                        final EditText input = view.findViewById(R.id.txt_dia);
                        Button yes = view.findViewById(R.id.btn_yes);
                        Button no = view.findViewById(R.id.btn_no);
                        input.setHint("رأيك ب " + p.getTitle());
                        builder.setView(view);
                        rate_dialog = builder.create();
                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rate_dialog.cancel();
                                isRated = false;
                            }
                        });
                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                isRated = false;
                                login(0, input.getText().toString().trim(), ((int) rating) + "", ratingBar);
                                //rateme(0 ,input.getText().toString().trim() ,((int)rating)+"" ,ratingBar);
                            }
                        });
                        rate_dialog.show();
                    /*
                    final RatingDialog dia = new RatingDialog(SingleProductActivity.this ,"رأيك ب " + p.getTitle()) {
                        @Override
                        public void onYesClicked(EditText t) {

                            rateme(0 ,t.getText().toString().trim() ,((int)rating)+"" ,ratingBar);
                            dismiss();
                        }
                    };

                    dia.show();
                   **/

                    } else {
                        Toast.makeText(getApplicationContext(), "سجل دخولك لتتمكن من التقييم و التعليق", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SingleProductActivity.this, LoginActivty.class));
                    }
                }
            }
        });

        ArrayList pics = new ArrayList();
        pics.add(p.getThumb());
        if (p.getGood_images()!=null && p.getGood_images().length!=0){
            for (int i = 0; i < p.getGood_images().length; i++)
                pics.add("http://e-sequoia.net/upload/product/"+p.getGood_images()[i]);
        }else {
            for (int i = 0; i < p.getImgs().length; i++)
                pics.add(p.getImgs()[i]);
        }
      SequoiaViewPager nvz =  (SequoiaViewPager)findViewById(R.id.market_slider);
        nvz.setItems(pics);
        nvz.init(this);



        final EditText quantity = (EditText) findViewById(R.id.quantity);
        oldprice = (TextView) findViewById(R.id.old_price);
//        oldprice.setPaintFlags(oldprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        oldprice.setText(p.getOldprice());



        ((TextView) findViewById(R.id.real_price)).setText(p.getNewprice() + " " +getResources().getString(R.string.curremcy));
        ((TextView) findViewById(R.id.title)).setText(p.getTitle());
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            ((TextView) findViewById(R.id.description)).setText(Html.fromHtml(p.getDescription(),
//                    Html.FROM_HTML_MODE_LEGACY));
//        else
//            ((TextView) findViewById(R.id.description)).setText(Html.fromHtml(p.getDescription()));
//
//
//todo was here


        ((TextView) findViewById(R.id.search_counter)).setText(p.getSearchCounter() + "");
        ((RatingBar) findViewById(R.id.rating_stars)).setRating(Float.parseFloat(p.getRating()));
        ((TextView) findViewById(R.id.rating_text)).setText(String.format("%.1f", Float.parseFloat(p.getRating())));
         findViewById(R.id.share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                String shareBody = "تسوق " + p.getTitle() +  "  على متجر سيكويا" + "\r\n" +
                        "من الرابط التالي : \r\n" +
                        "\r\n" + "www.e-sequoia.net/?page=product&id="+p.getId();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "مشاركة المنتج");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "شارك من خلال : "));
            }
        });


        findViewById(R.id.btn_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity_amount++;
                if (quantity_amount < 1) quantity_amount = 1;
                else  if (quantity_amount > in_stock_quant){quantity_amount=in_stock_quant;}
                quantity.setText(quantity_amount+"");
            }
        });
        findViewById(R.id.btn_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity_amount--;
                if (quantity_amount < 1) quantity_amount = 1;
              else  if (quantity_amount > in_stock_quant){quantity_amount=in_stock_quant;}
                    quantity.setText(quantity_amount+"");
            }
        });

        RecyclerView recyclerView2 = (RecyclerView) v.findViewById(R.id.recyclerview2);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                false
        ));

        final BasicRecycleviewAdapter adapter = new BasicRecycleviewAdapter(SharedPrefManager.getInstance(this)
                .getExtendedPosts(new String[]{"home", Post.FEATURED}), this);
        recyclerView2.setAdapter(adapter);

        final FloatingActionButton fablike = (FloatingActionButton) findViewById(R.id.fablike);
        final FloatingActionButton fabdislike = (FloatingActionButton) findViewById(R.id.fabdislike);
        //final Button love = (Button) findViewById(R.id.add_to_fav);


        if (SavedCacheUtils.isInFavourietes(this, p.getId())) {
            fablike.hide();
            fabdislike.show();
            //love.setText("إزالة من المفضلة");
        } else {
            fablike.show();
            fabdislike.hide();
            //love.setText("إضافة للمفضلة");
        }
        /*
        love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SavedCacheUtils.isInFavourietes(SingleProductActivity.this, p.getId())) {
                    if (SavedCacheUtils.addToFavourites(SingleProductActivity.this, p)) {
                        Snackbar.make(v, "تم الاضافة للمفضلة", Snackbar.LENGTH_LONG)
                                .setAction("عرض المفضلة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(SingleProductActivity.this, FavouritesActivity.class));
                                        //   SavedCacheUtils.removeFromFavourite(SingleProductActivity.this ,p.getId());
                                        //  Toast.makeText(SingleProductActivity.this, "تم الازالة من المفضلة", Toast.LENGTH_SHORT).show();
                                    }
                                }).show();
                        love.setText("حذف من المفضلة");
                    }
                } else {
                    love.setText("إضافة للمفضلة");
                    SavedCacheUtils.removeFromFavourite(SingleProductActivity.this, p.getId());
                    Toast.makeText(SingleProductActivity.this, "تم الازالة من المفضلة", Toast.LENGTH_SHORT).show();
                }
            }
        });
**/
        fablike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SavedCacheUtils.addToFavourites(SingleProductActivity.this, p)) {
                    fablike.hide();
                    fabdislike.show();
                    //love.setText("إزالة من المفضلة");
                    Snackbar.make(view, "تم الاضافة للمفضلة", Snackbar.LENGTH_LONG)
                            .setAction("عرض المفضلة", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(SingleProductActivity.this, FavouritesActivity.class));

//                                    SavedCacheUtils.removeFromFavourite(SingleProductActivity.this ,p.getId());
//                                    Toast.makeText(SingleProductActivity.this, "تم الازالة من المفضلة", Toast.LENGTH_SHORT).show();
//                                    fablike.show();
//                                    fabdislike.hide();
//                                    love.setText("إضافة المفضلة");
                                }
                            }).show();
                }
            }
        });
        fabdislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SavedCacheUtils.removeFromFavourite(SingleProductActivity.this, p.getId());
                fablike.show();
                fabdislike.hide();
                //love.setText("إضافة إلى المفضلة");
                Snackbar.make(view, "تم الحذف من المفضلة", Snackbar.LENGTH_LONG)
                        .setAction("عرض المفضلة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(SingleProductActivity.this, FavouritesActivity.class));
//                                if(SavedCacheUtils.addToFavourites(SingleProductActivity.this ,p)) {
//                                    Toast.makeText(SingleProductActivity.this, "تمت الإضافة إلى المفضلة", Toast.LENGTH_SHORT).show();
//                                    fablike.hide();
//                                    fabdislike.show();
//                                    love.setText("حذف نن المفضلة");
//                                }
                            }
                        }).show();
            }
        });


//        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swifeRefresh);
//
//
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (ConnectionUtils.isNetworkAvailable(SingleProductActivity.this))
//                  go(0);
//                else {
//                    mSwipeRefreshLayout.setRefreshing(false);
//
//                    Snackbar.make(mSwipeRefreshLayout, "لا يوجد انترنت !", Snackbar.LENGTH_LONG)
//                            .setAction("حاول مرة ثانية", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    BackgroundServices.getInstance(SingleProductActivity.this).Call(new PostAction() {
//                                        @Override
//                                        public void doTask() {
////Nothing here
//
//                                        }
//
//                                        @Override
//                                        public void doTask(String s) {
//                                            readJson(s);
//                                        }
//                                    }, APIUrl.SERVER + "product/get/-1/" + p.getId());
//                                }
//                            }).show();
//                }
//
//
//            }
//        });


        if (ConnectionUtils.isNetworkAvailable(this))
            go(0);

                else {
            Snackbar.make(findViewById(R.id.root_layout), "لا يوجد انترنت !", Snackbar.LENGTH_LONG)
                    .setAction("حاول مرة ثانية", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BackgroundServices.getInstance(SingleProductActivity.this).Call(new PostAction() {
                                @Override
                                public void doTask() {
//Nothing here

                                }

                                @Override
                                public void doTask(String s) {
                                    readJson(s);
                                }
                            }, APIUrl.SERVER + "product/get/-1/" + p.getId());
                        }
                    }).show();
        }

    }

    private void initShippingRecycler(){
        rv_shipping = findViewById(R.id.product_details_shipping_recycler);
        lyt_shipping = findViewById(R.id.shipping_layout);
        lyt_shipping2 = findViewById(R.id.product_details_shipping_layout2);
        shipping_list = new ArrayList<>();
        shipping_adapter = new ProductShippingAdapter(SingleProductActivity.this,shipping_list);
        shipping_layout_manager = new LinearLayoutManager(SingleProductActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_shipping.setLayoutManager(shipping_layout_manager);
        rv_shipping.setAdapter(shipping_adapter);
        lyt_shipping.setVisibility(View.GONE);
        lyt_shipping2.setVisibility(View.GONE);
    }
void go(final int tries){
        if (isOnline()){
            BackgroundServices.getInstance(this).Call(new PostAction() {
                @Override
                public void doTask() {
//Nothing here
                }

                @Override
                public void doTask(String s) {
                    Log.i("prod_deta", "doTask: "+s);
                    if(!s.equals(""))
                        readJson(s);
                    else{
                        if(tries<10)
                            go(tries+1);}

                }
            }, APIUrl.SERVER + "product/get/-1/" + p.getId());
        }else {
            findViewById(R.id.loading2).setVisibility(View.GONE);
        }


}

    void rateme(final int tries , final String m_Text , final String rating  , final View ratingBar){
    User user = SharedPrefManager.getInstance(SingleProductActivity.this).getUser();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("user_id", user.getUser_id());
    params.put("user_token", user.getToken());
    params.put("product_id", p.getId());
    params.put("value", rating);
    params.put("comment_text", m_Text);


    ratingBar.setEnabled(false);
    //Log.d("SENDINF" , params.toString());
    BackgroundServices.getInstance(SingleProductActivity.this).CallPost(new PostAction() {
        @Override
        public void doTask() {

        }

        @Override
        public void doTask(String s) {
            progressDialog.cancel();
            rate_dialog.cancel();
            if (!s.equals("")) {
                try {
                    JSONObject res = new JSONObject(s);
                    Toast.makeText(SingleProductActivity.this, res.getString("message"),
                            Toast.LENGTH_SHORT).show();
                    if(res.getInt("status")==1){
                        try {
                            if (comments == null)
                                comments = new ArrayList<Comment>();
                                comments.add(0, new Comment("890",
                                    SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                                    ,
                                    "", rating + "",
                                    new Date().toString(),
                                    m_Text,new JSONArray()));

                            adapterOfComments.notifyDataSetChanged();
                            findViewById(R.id.product_people).setVisibility(View.VISIBLE);
                        }catch (Exception e){
                            Log.i("rerer", "doTask: "+e.getMessage());
                            Toast.makeText(SingleProductActivity.this, "تم التقييم", Toast.LENGTH_SHORT).show();
                        }

                    }else
                        ratingBar.setEnabled(true);



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if(tries<10)rateme(tries+1 ,m_Text ,rating+"" ,ratingBar);
                else
                ratingBar.setEnabled(true);

            }

        }
    }, APIUrl.SERVER + "rate_comment", params);

}




    JSONObject remainingData;
    RecyclerView recyclerViewcomments;

    void readJson(String s) {
     //   mSwipeRefreshLayout.setRefreshing(false);
        try {
            JSONObject all = new JSONObject(s);
            remainingData = all.getJSONObject("data");
             store_id = remainingData.getString("store_id");
             store_name = remainingData.getString("store_name");
            Log.i("storee_name", "readJson: "+store_name);
             //shipping = remainingData.getString("free_delivery_to_cities");

            if (remainingData.getJSONObject("delivery_services_options")!=null){
                if (!remainingData.getJSONObject("delivery_services_options").equals("")) {
                    if (remainingData.getJSONObject("delivery_services_options").getString("lower_value") != null &&
                            remainingData.getJSONObject("delivery_services_options").getString("value_to_pay") != null) {
                        if (!remainingData.getJSONObject("delivery_services_options").getString("lower_value").equals("") &&
                                !remainingData.getJSONObject("delivery_services_options").getString("value_to_pay").equals("")) {

                            Log.i("jfhjfhgjf", "readJson: "+remainingData.getJSONObject("delivery_services_options").getString("lower_value"));
                            Log.i("jfhjfhgjf", "readJson: "+remainingData.getJSONObject("delivery_services_options").getString("value_to_pay"));
                            //Paid
                            if (Float.valueOf(remainingData.getJSONObject("delivery_services_options").getString("lower_value"))>
                                    Float.valueOf(remainingData.getString("price"))){
                                v_shipping.setVisibility(View.VISIBLE);
                                tv_shipping.setVisibility(View.VISIBLE);
                                lyt_shipping.setVisibility(View.VISIBLE);
                                lyt_shipping2.setVisibility(View.VISIBLE);
                                if (remainingData.has("supported_delivery_services")) {
                                    if (remainingData.getJSONArray("supported_delivery_services") != null) {
                                        if (!remainingData.getJSONArray("supported_delivery_services").equals("")) {
                                            if (remainingData.getJSONArray("supported_delivery_services").length() > 0) {
                                                for (int i = 0; i < remainingData.getJSONArray("supported_delivery_services").length(); i++) {
                                                    shipping_list.add(remainingData.getJSONArray("supported_delivery_services").getString(i));
                                                    shipping_adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }
                                    }
                                }
                                shipping_list.add("تضاف "+remainingData.getJSONObject("delivery_services_options").getString("value_to_pay")+" ل.س أجور توصيل");
                                shipping_adapter.notifyDataSetChanged();
                            }
                            //Free
                            else {
                                if (remainingData.has("supported_delivery_services")){
                                    if(remainingData.getJSONArray("supported_delivery_services")!=null) {
                                        if (!remainingData.getJSONArray("supported_delivery_services").equals("")) {
                                            if (remainingData.getJSONArray("supported_delivery_services").length() > 0) {
                                                for (int i = 0; i < remainingData.getJSONArray("supported_delivery_services").length(); i++) {
                                                    shipping_list.add(remainingData.getJSONArray("supported_delivery_services").getString(i));
                                                    shipping_adapter.notifyDataSetChanged();
                                                }
                                                v_shipping.setVisibility(View.VISIBLE);
                                                tv_shipping.setVisibility(View.VISIBLE);
                                                lyt_shipping.setVisibility(View.VISIBLE);
                                                lyt_shipping2.setVisibility(View.VISIBLE);
                                            }else {
                                                v_shipping.setVisibility(View.GONE);
                                                tv_shipping.setVisibility(View.GONE);
                                                lyt_shipping.setVisibility(View.GONE);
                                                lyt_shipping2.setVisibility(View.GONE);
                                            }
                                        }else {
                                            v_shipping.setVisibility(View.GONE);
                                            tv_shipping.setVisibility(View.GONE);
                                            lyt_shipping.setVisibility(View.GONE);
                                            lyt_shipping2.setVisibility(View.GONE);
                                        }
                                    }else {
                                        v_shipping.setVisibility(View.GONE);
                                        tv_shipping.setVisibility(View.GONE);
                                        lyt_shipping.setVisibility(View.GONE);
                                        lyt_shipping2.setVisibility(View.GONE);
                                    }
                                }else {
                                    v_shipping.setVisibility(View.GONE);
                                    tv_shipping.setVisibility(View.GONE);
                                    lyt_shipping.setVisibility(View.GONE);
                                    lyt_shipping2.setVisibility(View.GONE);
                                }
                            }
                        }else {
                            v_shipping.setVisibility(View.GONE);
                            tv_shipping.setVisibility(View.GONE);
                            lyt_shipping.setVisibility(View.GONE);
                            lyt_shipping2.setVisibility(View.GONE);
                        }
                    }else {
                        v_shipping.setVisibility(View.GONE);
                        tv_shipping.setVisibility(View.GONE);
                        lyt_shipping.setVisibility(View.GONE);
                        lyt_shipping2.setVisibility(View.GONE);
                    }
                }else {
                    v_shipping.setVisibility(View.GONE);
                    tv_shipping.setVisibility(View.GONE);
                    lyt_shipping.setVisibility(View.GONE);
                    lyt_shipping2.setVisibility(View.GONE);
                }
            }else {
                v_shipping.setVisibility(View.GONE);
                tv_shipping.setVisibility(View.GONE);
                lyt_shipping.setVisibility(View.GONE);
                lyt_shipping2.setVisibility(View.GONE);
            }

            /*
            if (remainingData.has("supported_delivery_services")){
                if(remainingData.getJSONArray("supported_delivery_services")!=null){
                    if (!remainingData.getJSONArray("supported_delivery_services").equals("")){
                        if (remainingData.getJSONArray("supported_delivery_services").length()>0){
                            for (int i = 0; i < remainingData.getJSONArray("supported_delivery_services").length(); i++) {
                                shipping_list.add(remainingData.getJSONArray("supported_delivery_services").getString(i));
                                shipping_adapter.notifyDataSetChanged();
                            }
                            v_shipping.setVisibility(View.VISIBLE);
                            tv_shipping.setVisibility(View.VISIBLE);
                            lyt_shipping.setVisibility(View.VISIBLE);
                            lyt_shipping2.setVisibility(View.VISIBLE);
                        }else {
                            v_shipping.setVisibility(View.GONE);
                            tv_shipping.setVisibility(View.GONE);
                            lyt_shipping.setVisibility(View.GONE);
                            lyt_shipping2.setVisibility(View.GONE);
                        }
                    }else {
                        v_shipping.setVisibility(View.GONE);
                        tv_shipping.setVisibility(View.GONE);
                        lyt_shipping.setVisibility(View.GONE);
                        lyt_shipping2.setVisibility(View.GONE);
                    }
                }else {
                    v_shipping.setVisibility(View.GONE);
                    tv_shipping.setVisibility(View.GONE);
                    lyt_shipping.setVisibility(View.GONE);
                    lyt_shipping2.setVisibility(View.GONE);
                }
            }else {
                v_shipping.setVisibility(View.GONE);
                tv_shipping.setVisibility(View.GONE);
                lyt_shipping.setVisibility(View.GONE);
                lyt_shipping2.setVisibility(View.GONE);
            }

             if (remainingData.getJSONObject("delivery_services_options")!=null){
                 if (!remainingData.getJSONObject("delivery_services_options").equals("")){
                     if (remainingData.getJSONObject("delivery_services_options").getString("lower_value")!=null&&
                             remainingData.getJSONObject("delivery_services_options").getString("value_to_pay")!=null){
                         if (!remainingData.getJSONObject("delivery_services_options").getString("lower_value").equals("")&&
                                 !remainingData.getJSONObject("delivery_services_options").getString("value_to_pay").equals("")){
                             if (Float.valueOf(remainingData.getString("price"))<
                                     Float.valueOf(remainingData.getJSONObject("delivery_services_options").getString("lower_value"))){
                                 shipping_list.add("تضاف "+remainingData.getJSONObject("delivery_services_options").getString("value_to_pay")+" ل.س أجور توصيل");
                                 shipping_adapter.notifyDataSetChanged();
                                 v_shipping.setVisibility(View.VISIBLE);
                                 tv_shipping.setVisibility(View.VISIBLE);
                                 lyt_shipping.setVisibility(View.VISIBLE);
                                 lyt_shipping2.setVisibility(View.VISIBLE);
                             }
                         }
                     }
                 }
             }
             **/
             /*
             if (shipping != null && !shipping.equals("")){
                 tv_shipping.setText(shipping);
             }else {
                 v_shipping.setVisibility(View.GONE);
                 tv_shipping.setVisibility(View.GONE);
                 lyt_shipping.setVisibility(View.GONE);
             }
             **/
            init();
        } catch (JSONException e) {

            Log.e("XXT56", e.getMessage() + " S: " + APIUrl.SERVER + "/product/get/-1/" + p.getId());

        }
    }

    CommentsAdapter adapterOfComments;
   // SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<Comment> comments=new ArrayList<>() ;

    void addToCart(View v, final ExtendedPost p , String color , String size , String measure) {

        boolean b = SavedCacheUtils.addToCart(this, p, quantity_amount
         ,color , size , measure);
        if (b) {
            setNotifCount(SavedCacheUtils.getCart(this).size());
            /*
            Snackbar snack = Snackbar.make(findViewById(R.id.root_layout), "اذهب إلى السلة لتأكيد طلبك", Snackbar.LENGTH_LONG);
            View view = snack.getView();
            FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(getResources().getColor(R.color.white));
            snack.show();
            **/
            AlertDialog.Builder builder = new AlertDialog.Builder(SingleProductActivity.this);
            View view = getLayoutInflater().inflate(R.layout.dialog_go_to_cart,null);
            ImageView arro = view.findViewById(R.id.dialog_go_to_cart_arrow);
            builder.setView(view);
            final AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            arro.setRotation(-45f);
            dialog.show();
            CountDownTimer countDownTimer = new CountDownTimer(2000,1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    dialog.cancel();
                }
            }.start();
            //Snackbar.make(v, "تم اضافة " + quantity_amount + " عنصر إلى سلة المشتريات", Snackbar.LENGTH_LONG)
                   // .show();

        }
        setNotifCount(SavedCacheUtils.getCart(this).size());
    }
int in_stock_quant = 0;
    void init(){
        final AdvancedWebView browser = findViewById(R.id.description);

        browser.setClickable(true);
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String html = "";
        String description= "";
        try {
            description = remainingData.getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (description.length()>175){
            html = description.substring(0,175)+"...";
            btn_desc_more.setVisibility(View.VISIBLE);
        }else {
            html = description;
        }
        browser.loadHtml("<html dir=\"rtl\" lang=\"\"><body>" +html+"</body></html>","","",encoding);
        final String finalDescription = description;
        final String finalHtml = html;
        btn_desc_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String h = finalDescription;
                //browser.loadDataWithBaseURL("", "<html dir=\"rtl\" lang=\"\"><body>" +spn.toString()+"</body></html>", mimeType, encoding, "");
                browser.loadHtml( "<html dir=\"rtl\" lang=\"\"><body>" + finalHtml +"</body></html>","","", encoding );
                btn_desc_more.setVisibility(View.GONE);
            }
        });
        browser.setWebViewClient(new CustomWebClient(SingleProductActivity.this));
        findViewById(R.id.loading2).setVisibility(View.GONE);
        try {
            in_stock_quant=remainingData.getInt("qty_in_stock");
            final boolean isInStock = remainingData.getInt("qty_in_stock") > 0;
            ((EditText) findViewById(R.id.quantity)).setInputType(InputType.TYPE_CLASS_NUMBER);
            int views =remainingData.getInt("p_views");
            ((TextView)findViewById(R.id.num_views)).setText(views+"");
            oldprice.setText("الكمية المتوفرة : " + remainingData.getInt("qty_in_stock") +" قطعة");
            if (isInStock)
            {
                //findViewById(R.id.in_stock).setVisibility(View.VISIBLE);
                iv_exist.setImageResource(R.drawable.ic_product_exist);
                tv_exist.setText("متوفر ضمن المتجر");
                //findViewById(R.id.not_in_stock).setVisibility(View.INVISIBLE);
                findViewById(R.id.add_to_cart).setEnabled(true);
                final JSONArray colors= remainingData.getJSONObject("features").getJSONArray("colors");
                if(colors!=null && colors.length()>0){
                    findViewById(R.id.color_view).setVisibility(View.VISIBLE);
                    colorSpinner = (Spinner)findViewById(R.id.color_spinner);
                    ColorsAdapter cadp = new ColorsAdapter(this ,colors);
                    colorSpinner.setAdapter(cadp);
                }
                final JSONArray sizes= remainingData.getJSONObject("features").getJSONArray("sizes");
                if(sizes!=null && sizes.length()>0){
                    if(!sizes.get(0).equals("")){
                        findViewById(R.id.size_view).setVisibility(View.VISIBLE);
                        sizeSpinner = (Spinner)findViewById(R.id.size_spinner);
                        SizesAdapter cadp = new SizesAdapter(this ,sizes);
                        sizeSpinner.setAdapter(cadp);
                    }
                }
                final JSONArray measures= remainingData.getJSONObject("features").getJSONArray("measure");
                if(measures!=null && measures.length()>0){
                    if(!measures.get(0).equals("")){
                        findViewById(R.id.measures_view).setVisibility(View.VISIBLE);
                        measuresSpinner = (Spinner)findViewById(R.id.measures_spinner);
                        SizesAdapter cadp = new SizesAdapter(this ,measures);
                        measuresSpinner.setAdapter(cadp);
                    }
                }
                if (remainingData.getJSONObject("features").has("weight")){
                    String weight = remainingData.getJSONObject("features").getString("weight");
                    if (weight!=null && !weight.equals("") && !weight.equals("null")){
                        ((TextView)findViewById(R.id.weight_txt)).setText(weight);
                        findViewById(R.id.weight_view).setVisibility(View.VISIBLE);
                    }
                }

                findViewById(R.id.add_to_cart).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo

                        if (!isInStock) {
                            Toast.makeText(SingleProductActivity.this, "العنصر غير متوفر حالياً , جرب لاحقا..", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        try {
                            addToCart(v, p,
                                    (colors != null && colors.length() > 0) ?
                                            colors.getString(colorSpinner.getSelectedItemPosition()) : ""
                                    ,
                                    (sizes != null && sizes.length() > 0) ?
                                            sizes.getString(sizeSpinner.getSelectedItemPosition()) : ""
                                    ,
                                    (measures != null && measures.length() > 0) ?
                                            measures.getString(measuresSpinner.getSelectedItemPosition()) : ""
                            );
                            findViewById(R.id.add_to_cart).setEnabled(false);
                        }catch (JSONException jex){
                            Log.i("dfdfrfrf", "onClick: "+jex.getMessage());
                        }
                    }
                });
            }
            else {
                iv_exist.setImageResource(R.drawable.ic_product_not_exist);
                tv_exist.setText("المنتج غير متوفر حاليا");
                //findViewById(R.id.in_stock).setVisibility(View.INVISIBLE);
                //findViewById(R.id.not_in_stock).setVisibility(View.VISIBLE);
                oldprice.setVisibility(View.GONE);
            }

            JSONArray comments_json = remainingData.getJSONArray("latest_comments");
            if(comments_json.length()==0){
                findViewById(R.id.product_people).setVisibility(View.GONE);
                comments  = new ArrayList<>();
                adapterOfComments = new CommentsAdapter(comments, this,store_id,store_name);
                recyclerViewcomments = (RecyclerView) findViewById(R.id.recyclerviewcomments);
                recyclerViewcomments.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                        false
                ));
                recyclerViewcomments.setAdapter(adapterOfComments);
            }
            else {

                comments  = new ArrayList<>();
                for (int i = 0; i < comments_json.length(); i++)
                    comments.add(new Comment(comments_json.getJSONObject(i).getString("comment_id"),
                            comments_json.getJSONObject(i).getString("fullname"),
                            "", comments_json.getJSONObject(i).getString("avg_rate"),
                            comments_json.getJSONObject(i).getString("comment_time"),
                            comments_json.getJSONObject(i).getString("content"),
                            comments_json.getJSONObject(i).getJSONArray("replies")));

                adapterOfComments = new CommentsAdapter(comments, this,store_id,store_name);
                recyclerViewcomments = (RecyclerView) findViewById(R.id.recyclerviewcomments);
                recyclerViewcomments.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                        false
                ));
                recyclerViewcomments.setAdapter(adapterOfComments);

                adapterOfComments.notifyDataSetChanged();

            }

            ArrayList related = new ArrayList();
            JSONArray array = remainingData.getJSONArray("related_products");

            for (int i = 0; i < array.length(); i++) {
                String pic = "";
                //Log.d("STEP1", "DONE");
                JSONObject object = array.getJSONObject(i);
                if (object == null) continue;

                ExtendedPost item = new ExtendedPost(object
                );
                //Log.d("STEP4", "DONE");

                related.add(item);
                RecyclerView recyclerView2 = (RecyclerView) findViewById(R.id.recyclerview2);
                recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                        false
                ));
                final BasicRecycleviewAdapter adapter = new BasicRecycleviewAdapter(related, this);
                recyclerView2.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }


        } catch (JSONException ex) {
            Log.d("VVC", ex.getMessage() + " line : " + ex.getStackTrace());

        }

    }
    public boolean isOnline(){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(this, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    private void login(final int tries , final String m_Text , final String rating  , final View ratingBar){
        final HashMap<String  ,String> params=new HashMap<String, String>();
        String type = SharedPrefManager.getInstance(SingleProductActivity.this).getLoginType();
        LoginObject lo = SharedPrefManager.getInstance(SingleProductActivity.this).getLoginData();
        if (type.equals("normal")){
            params.put("mobile_number",lo.getMobile_number());
            params.put("login_type" ,lo.getLogin_type());
            params.put("password" ,lo.getPassword());
        }else if (type.equals("facebook")){
            params.put("login_type" ,lo.getLogin_type());
            params.put("facebook_id" ,lo.getFacebook_id());
            params.put("access_token" , lo.getAccess_token());
        }
        progressDialog = new ProgressDialog(SingleProductActivity.this);
        progressDialog.setMessage("الرجاء الانتظار..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(!s.equals("")){
                    try {
                        JSONObject object =new JSONObject(s);
                        if(object.getInt("status")==1)
                        {
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                                    object.getJSONObject("data") ,params.get("password"));
                            Log.i("tokennn", "doTask: "+SharedPrefManager.getInstance(SingleProductActivity.this).getUser().getToken());
                            rateme(tries,m_Text,rating,ratingBar);
                        }else{
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception fdr){ }
                }else{
                    Toast.makeText(SingleProductActivity.this, "حدث خلل اثناء الاتصال , حاول من جديد", Toast.LENGTH_SHORT).show();
                }
            }
        }  , APIUrl.SERVER2 + "user/login",params);
    }
    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
