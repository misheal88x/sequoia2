package com.nyx.sequoiaapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.UsersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CitiesAdapter;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.City;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class AccountActivity extends RootActivity {

    private CircleImageView profile_pic;
    private ImageView edit_profile_pic;
    private ProgressBar edit_profile_progress;
    private FloatingActionButton fab_chat;


void updateData(final String type, String newval , final View button){
 button.setEnabled(false);
    user = SharedPrefManager.getInstance(this).getUser();
    HashMap<String  ,String> params=new HashMap<String, String>();
    params.put("user_id" ,user.getUser_id());
    params.put("user_token" ,user.getToken());
    params.put("new_value" ,newval);
    params.put("type" ,type);
Log.d("ETSSE" ,params.toString());


    BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
        @Override
        public void doTask() {

        }

        @Override
        public void doTask(String s) {
            if(!s.equals("")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        button.setEnabled(true);
                    }
                });
                try {
                    JSONObject object = new JSONObject(s);
                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                    if (object.getInt("status") == 1) {
                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                                object.getJSONObject("data"), user.getPassword()
                        );
                        /*
                        if (type.equals("7") || type.equals("1")) {
                            Intent intent = new Intent(AccountActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                        **/
                    }


                } catch (Exception fdr) {
                    //Log.d("FDR : ", fdr.getMessage());
                }
            }else{
                Toast.makeText(AccountActivity.this, "حدث خلل في الشبكة , حاول من جديد ..", Toast.LENGTH_SHORT).show();
                button.setEnabled(true);
            }

        }
    }  , APIUrl.SERVER + "user/update",params);


}




    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    String imagePath;

    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("حسابي");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        profile_pic = findViewById(R.id.profile_pic);
        edit_profile_pic = findViewById(R.id.edit_image_profile);
        edit_profile_progress = findViewById(R.id.edit_image_progress);
        fab_chat = findViewById(R.id.open_chat);
         user = SharedPrefManager.getInstance(this).getUser();



        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        ((EditText)findViewById(R.id.name_input)).setText(user.getName());
        ((TextView)findViewById(R.id.title)).setText(user.getName());

        ((EditText)findViewById(R.id.email_input)).setText(user.getEmail());
        ((EditText)findViewById(R.id.phone_input)).setText(user.getPhone());
        ((EditText)findViewById(R.id.address_input)).setText(user.getAddress());


        findViewById(R.id.btn_save_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = ((EditText)findViewById(R.id.name_input)).getText().toString().trim();
                if(val.equals("")){
                    Toast.makeText(AccountActivity.this, "أدخل اسمك الكامل من فضلك", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateData("1" ,val ,v);
            }
        });


        findViewById(R.id.button_save_mobile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = ((EditText)findViewById(R.id.phone_input)).getText().toString().trim();
                if(val.equals("")){
                    Toast.makeText(AccountActivity.this, "أدخل رقم هاتفك من فضلك", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateData("2" ,val ,v);
            }
        });
        findViewById(R.id.button_save_address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = ((EditText)findViewById(R.id.address_input)).getText().toString().trim();
                if(val.equals("")){
                    Toast.makeText(AccountActivity.this, "أدخل عنوانك من فضلك", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateData("5" ,val ,v);
            }
        });

        findViewById(R.id.button_save_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = ((EditText)findViewById(R.id.email_input)).getText().toString().trim();
                if(val.equals("")){
                    Toast.makeText(AccountActivity.this, "أدخل بريدك الالكتروني من فضلك", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateData("3" ,val ,v);
            }
        });

        findViewById(R.id.button_save_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = ((EditText)findViewById(R.id.password_input)).getText().toString().trim();
                if(val.equals("")){
                    Toast.makeText(AccountActivity.this, "أدخل كلمة المرور من فضلك", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateData("6" ,val ,v);
            }
        });
        final ArrayList cities = SharedPrefManager.getInstance(this).getCities();
        ArrayList temp_cities = new ArrayList();
        if (cities.size()>0){
            for (int i = 0; i <cities.size() ; i++) {
                City c = (City) cities.get(i);
                if (!c.getName().equals("ادلب")&&!c.getName().equals("الرقة")&&
                        !c.getName().equals("الحسكة")&&!c.getName().equals("القنيطرة")){
                    temp_cities.add(c);
                }
            }
        }
        final Spinner spinner = (Spinner) findViewById(R.id.city_spinner);
        CitiesAdapter adapter = new CitiesAdapter(this, temp_cities);
        spinner.setAdapter(adapter);
        String city_id = user.getCity();
        int selection = -1;
        for(int i=0;i<cities.size();i++)
            if(((City)cities.get(i)).getId().equals(city_id))selection=i;
      if(selection>-1)
        spinner.setSelection(selection);

        findViewById(R.id.button_save_city).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = ((City)cities.get(spinner.getSelectedItemPosition())).getId();
                if(val.equals("")){
                    Toast.makeText(AccountActivity.this, "أدخل المدينة من فضلك", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateData("4" ,val ,v);
            }
        });

        /*
        findViewById(R.id.save_pic).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(encodedImage==null ||  encodedImage.equals("")){
            Toast.makeText(AccountActivity.this, "اختر صورة جديدة من فضلك ..", Toast.LENGTH_SHORT).show();
            return;
        }
        updateData("7" ,encodedImage ,v);
    }
});
**/

findViewById(R.id.go_logout).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        SharedPrefManager.getInstance(AccountActivity.this).Logout();
        Intent intent = new Intent(AccountActivity.this ,SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
});

if(!SharedPrefManager.getInstance(this).getUserPic().equals(""))
{
    Glide.with(this).load(SharedPrefManager.getInstance(this).getUserPic()).into( (ImageView) findViewById(R.id.profile_pic));
}

        edit_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(AccountActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AccountActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private static final int PICK_FROM_GALLERY = 14436;


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(),
                           "لا يتم الحصول على الصلاحية", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    void pick(){
        Intent photoPickerIntent =  new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }




String encodedImage;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED " ,requestCode+" and "+resultCode );

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if(data!=null) {
                        Log.e("GOTCHA " ,"yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : "  , resultUri.toString());
                        Log.e("Picsaved2 in : "  , resultUri.getPath());
                        Log.e("Picsaved3 in : "  , resultUri.getEncodedPath());

                        try {
                            imagePath=resultUri.getPath();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            Bitmap bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
                            profile_pic.setImageBitmap(bitmap);
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                            byte[] b = baos.toByteArray();
                            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                            callUpdateProfileAPI();

                        }catch (Exception e){

                            Toast.makeText(this, "error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        // Do something with the bitmap
                        // At the end remember to close the cursor or you will end with the RuntimeException!
                    }
                    break;


                case SELECT_PHOTO:
                    if(data!=null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME+".jpg";
                           UCrop.of(pickedImage,
                                   Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(3, 3)
                                .withMaxResultSize(800, 800)
                                .start(this);
                    }
                    break;




            }
        }
        else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR" ,cropError.getMessage() );

        }


    }

    private void callUpdateProfileAPI() {
        edit_profile_progress.setVisibility(View.VISIBLE);
        String user_id = SharedPrefManager.getInstance(this).getUser().getUser_id();
        UsersAPIsClass.update_image(this, user_id, imagePath, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if (json != null) {
                    String json1 = new Gson().toJson(json);
                    String success = new Gson().fromJson(json1, String.class);
                    SharedPrefManager.getInstance(AccountActivity.this).setUserPic(APIUrl.USERS_PICS_SERVER + success);
                    Toast.makeText(AccountActivity.this, "تم تحديث الصورة الشخصية بنجاح", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AccountActivity.this, "حدث خطأ ما أعد المحاولة", Toast.LENGTH_SHORT).show();
                }
                edit_profile_progress.setVisibility(View.GONE);
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                edit_profile_progress.setVisibility(View.GONE);
                Snackbar.make(findViewById(R.id.drawer_layout), "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                        .setAction("أعد المحاولة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callUpdateProfileAPI();
                            }
                        }).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
