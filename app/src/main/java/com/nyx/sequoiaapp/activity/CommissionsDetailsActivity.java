package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.CommissionsAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CommissionsAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.CommissionObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class CommissionsDetailsActivity extends RootActivity {

    private RelativeLayout root;
    private ProgressBar pb_loading;
    private RecyclerView rv_recycler;
    private List<CommissionObject> list;
    private LinearLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private CommissionsAdapter adapter;
    private ImageView empty_image;
    private TextView tv_desc;
    private FloatingActionButton fab_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commissions_details);
        init_views();
        init_events();
        init_activity();
        init_recycler();
        callAPI();
    }

    private void init_events() {
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommissionsDetailsActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
    }

    private void init_views() {
        //Root
        root = findViewById(R.id.commissions_details_layout);
        //Nested ScrollView
        scrollView = findViewById(R.id.commissions_details_scrollview);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("تفاصيل العمولات");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //ProgressBar
        pb_loading = findViewById(R.id.commissions_details_progress);
        //RecyclerView
        rv_recycler = findViewById(R.id.commissions_details_recycler);
        //Empty Image
        empty_image = findViewById(R.id.no_items_found);
        //TextView
        tv_desc = findViewById(R.id.description);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
    }

    private void init_activity(){

    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new CommissionsAdapter(CommissionsDetailsActivity.this,list);
        layoutManager = new LinearLayoutManager(CommissionsDetailsActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);
    }

    private void callAPI(){
        pb_loading.setVisibility(View.VISIBLE);
        CommissionsAPIsClass.get_info(CommissionsDetailsActivity.this,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        pb_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        pb_loading.setVisibility(View.GONE);
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            CommissionObject[] success = new Gson().fromJson(j,CommissionObject[].class);
                            if (success.length > 0){
                                for (CommissionObject co : success){
                                    list.add(co);
                                    adapter.notifyDataSetChanged();
                                }
                                empty_image.setVisibility(View.GONE);
                                rv_recycler.setVisibility(View.VISIBLE);
                                BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                            }else {
                                empty_image.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_loading.setVisibility(View.GONE);
                        empty_image.setVisibility(View.VISIBLE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
