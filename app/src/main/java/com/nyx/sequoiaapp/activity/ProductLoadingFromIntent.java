package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.models.ExtendedPost;

import org.json.JSONObject;

public class ProductLoadingFromIntent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_loading_from_intent);

        Intent intent = getIntent();
        String iamdine = intent.getStringExtra("id");
        if(iamdine==null || iamdine.equals("")) {
            String action = intent.getAction();
            Uri data = intent.getData();

            go(0, data.getQueryParameter("id"));
        }else{
            go(0, iamdine);
        }
    }



    void go(final int tries , final String id){
        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {
//Nothing here
            }

            @Override
            public void doTask(String s) {
                if(!s.equals("")){
                    try{
                String pic = "";
                String[] pics=new String[0];
                JSONObject object = new JSONObject(s).getJSONObject("data");


                String quer = object.getString("thumb");
                if (quer != null && !quer.equals("null"))
                    try {

                        ExtendedPost item=null;

                        item = new ExtendedPost(object);
                        Intent in = new Intent(ProductLoadingFromIntent.this, SingleProductActivity.class);
                        in.putExtra("post" , item);
                        startActivity(in);
                         finish();

                    }catch (Exception e){
                        Log.e("EFC139", e.getMessage());}






                    }catch (Exception e){
                        Log.e("EFC139",e.getMessage());
                    }
                }
                else{
                    if(tries<10)
                        go(tries+1 , id);}

            }
        }, APIUrl.SERVER + "product/get/-1/" + id);

    }

}
