package com.nyx.sequoiaapp.activity;

import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.AdsAdapterGrid;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.Config;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.CustomSliderView;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.BuyerSliderObject;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BrandProductsActivity extends ViewProductsActivity {
    int max=100000;
    private Button btn_load_more;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);

            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);
                btn_load_more.setVisibility(View.GONE);

                try {

                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray products = heart.getJSONObject("data").getJSONArray("products");
                    max=heart.getJSONObject("data").getInt("total");
                    setDescription(getIntent().getStringExtra("desc") +" :  "+max+" منتجاً  ");


                    for (int i = 0; i < products.length(); i++) {


                        items.add(new ExtendedPost(products.getJSONObject(i)));

                    }

                    adapter.notifyDataSetChanged();
                    Log.i("current_num", "doTask: "+current);
                    if (current<10){
                        BaseFunctions.runAnimationVertical(mainRecylceView,0,adapter);
                    }
                    loadingFlag = false;
                    current += 10;
                    if(items.size()==0){
                        noItemsFound.setVisibility(View.VISIBLE);
                        setDescription(
                                getIntent().getStringExtra("desc") + "\r\n"+
                                "لم يتم العثور على منتجات متعلقة ب( " + getIntent().getStringExtra("name") + ") حالياً . ");
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                }
            }
        }, APIUrl.SERVER  +
                "product/get_by_brand/"+getIntent().getStringExtra("id")+"/"+current);
    }
    int current=0;

    boolean loadingFlag = false;

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                //loadMore(0);
                btn_load_more.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle(getIntent().getStringExtra("query"));
    }

    @Override
    void fetchData() {
        btn_load_more = findViewById(R.id.get_more_btn);
        btn_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMore(0);
            }
        });
        setWindowTitle(getIntent().getStringExtra("name"));
        setDescription(getIntent().getStringExtra("desc"));
        try {
            rb_rate.setRating(Float.valueOf(String.valueOf(Math.round(Float.valueOf(getIntent().getStringExtra("rate"))))));
            rb_rate.setVisibility(View.VISIBLE);
        }catch (Exception e){}
        slider.setVisibility(View.VISIBLE);
        init_slider();
        GridLayoutManager lm = new GridLayoutManager(this,
                2) ;
        adapter= new AdsAdapterGrid(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);
    }

    private void init_slider(){
        if (getIntent().getStringExtra("sliders") == null||
                getIntent().getStringExtra("sliders").equals("")||
                getIntent().getStringExtra("sliders").equals("[]")){
            slider.setVisibility(View.GONE);
        }else {
            String json = getIntent().getStringExtra("sliders");
            BuyerSliderObject[] success = new Gson().fromJson(json,BuyerSliderObject[].class);
            if (success.length>0){
                for (int i = 0; i < success.length; i++) {
                    CustomSliderView textSliderView = new CustomSliderView(BrandProductsActivity.this);
                    textSliderView
                            .description("")
                            .image(APIUrl.SELLERS_SLIDERS_SERVER+success[i].getImage_url())
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    slider.addSlider(textSliderView);
                }

                slider.setPresetTransformer(Config.SLIDER_THEME);
                slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                slider.setCustomAnimation(new DescriptionAnimation());
                slider.setDuration(2000);
                slider.startAutoCycle();
            }else {
                slider.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
