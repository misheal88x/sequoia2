package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.MenuRecycleviewAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.MenuItem;

import java.util.ArrayList;

public class ControlPanelActivity extends RootActivity {

    public static final String SELLER="2" ,MARKETER="3",DISPENSER="5",CUSTOMER="4";
    private FloatingActionButton fab_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_panel);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("الإدارة");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab_chat = findViewById(R.id.open_chat);
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ControlPanelActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        ArrayList menus = new ArrayList();

        User me = SharedPrefManager.getInstance(this).getUser();


      if(me.getUser_type().equals(MARKETER))
{
    //---------------add menu item
    menus.add(new MenuItem("إَضافة حساب", R.drawable.add_account) {
        @Override
        public void doTask() {
            startActivity(new Intent(ControlPanelActivity.this, CreateAccountByRefActivity.class));
        }
    });
    //-------------------------------
    //-------------------FOR MARKETER-------------
    //---------------add menu item
    menus.add(new MenuItem("زبائن الدليل" , R.drawable.accounts) {
        @Override
        public void doTask() {
            startActivity(new Intent(ControlPanelActivity.this ,MyUsersActivity.class));
        }
    });
    //---------------add menu item
    menus.add(new MenuItem("طلبيات الدليل" , R.drawable.orders) {
        @Override
        public void doTask() {
            startActivity(new Intent(ControlPanelActivity.this ,OrdersOfMarketerActivity.class));
        }
    });
    //-------------------------------


    //---------------add menu item
    menus.add(new MenuItem("عمولاتي" , R.drawable.comsissions) {
        @Override
        public void doTask() {
            startActivity(new Intent(ControlPanelActivity.this ,MyComissionsRecordsActivity.class));
        }
    });



    //---------------add menu item
    menus.add(new MenuItem("لائحة العمولات" , R.drawable.bill) {
        @Override
        public void doTask() {
            startActivity(new Intent(ControlPanelActivity.this ,ComissionsListActivity.class));
        }
    });

}


        //-------------------------------


         if(me.getUser_type().equals(SELLER))
        {
            //---------------add menu item
            /*
            menus.add(new MenuItem("منتجاتي", R.drawable.products) {
                @Override
                public void doTask() {
                    startActivity(new Intent(ControlPanelActivity.this, SellerProducts.class));
                }
            });
            **/
            //-------------------------------

            //---------------add menu item

            //-------------------------------


        }






        if(me.getUser_type().equals(DISPENSER))
        {
            //---------------add menu item
            menus.add(new MenuItem("طلبيات الموزع" , R.drawable.orders) {
                @Override
                public void doTask() {
                    startActivity(new Intent(ControlPanelActivity.this ,OrdersForDispenserActivity.class));
                }
            });
            //-------------------FOR DISPNSER-------------
            //---------------add menu item
//            menus.add(new MenuItem("طلبيات المدن", R.drawable.orders) {
//                @Override
//                public void doTask() {
//                    startActivity(new Intent(ControlPanelActivity.this, SelectCityActivity.class));
//                }
//            });
            //-------------------------------


        }
        //===================



        //---------------add menu item
        menus.add(new MenuItem("حسابي الشخصي" , R.drawable.profile) {
            @Override
            public void doTask() {
                startActivity(new Intent(ControlPanelActivity.this ,AccountActivity.class));
            }
        });
        //-------------------------------
        //---------------add menu item
        menus.add(new MenuItem("تسجيل الخروج" , R.drawable.logout) {
            @Override
            public void doTask() {
                SharedPrefManager.getInstance(ControlPanelActivity.this).Logout();
                Intent intent = new Intent(ControlPanelActivity.this ,SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });


        //------------INIT LIST


        RecyclerView recyclerView5 = (RecyclerView)findViewById(R.id.recyclerview5);

        GridLayoutManager lm = new GridLayoutManager(this,
                2) {
            @Override
            public boolean supportsPredictiveItemAnimations() {
                return true;
            }
        };
        ;


        recyclerView5.setLayoutManager(lm);
        recyclerView5.setItemAnimator(new DefaultItemAnimator());

        MenuRecycleviewAdapter adapter = new MenuRecycleviewAdapter(menus, this
        );
        recyclerView5.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
