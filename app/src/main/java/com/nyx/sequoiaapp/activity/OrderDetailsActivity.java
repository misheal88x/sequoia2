package com.nyx.sequoiaapp.activity;

import android.app.NotificationManager;
import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.OrdersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.OrderDetailsProductsAdapter;
import com.nyx.sequoiaapp.app.Config;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.OrderDetailsResponse;
import com.nyx.sequoiaapp.models.Product;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailsActivity extends RootActivity {

    private RelativeLayout root;
    private ProgressBar pb_loading;
    private NestedScrollView scrollView;
    private TextView tv_name,tv_address,tv_title,tv_notes;
    private RecyclerView rv_products;
    private List<Product> listOfProducts;
    private OrderDetailsProductsAdapter adapter;
    private GridLayoutManager layoutManager;
    private Button btn_confirm1,btn_confirm2;
    private Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Root
        root = findViewById(R.id.order_details_layout);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("تفاصيل الطلب");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Progress bar
        pb_loading = findViewById(R.id.order_details_loading);
        //ScrollView
        scrollView = findViewById(R.id.order_details_scroll);
        //TextView
        tv_name = findViewById(R.id.order_details_name);
        tv_address = findViewById(R.id.order_details_address);
        tv_title = findViewById(R.id.title);
        tv_notes = findViewById(R.id.order_details_notes);
        //RecyclerView
        rv_products = findViewById(R.id.order_details_products_recycler);
        //Button
        btn_confirm1 = findViewById(R.id.order_details_confirm_btn_1);
        btn_confirm2 = findViewById(R.id.order_details_confirm_btn_2);
        //Intent
        myIntent = getIntent();
    }

    private void init_events(){
        btn_confirm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callConfirmOrderAPI(myIntent.getStringExtra("order_id"));
            }
        });
        btn_confirm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callConfirmOrderAPI(myIntent.getStringExtra("order_id"));
            }
        });
    }

    private void init_recycler(){
        listOfProducts = new ArrayList<>();
        adapter = new OrderDetailsProductsAdapter(OrderDetailsActivity.this,listOfProducts);
        layoutManager = new GridLayoutManager(OrderDetailsActivity.this,2){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_products.setLayoutManager(layoutManager);
        rv_products.setAdapter(adapter);
    }

    private void init_activity(){
        init_recycler();
        close_notification();
        callAPI();
    }

    private void close_notification(){
        try{
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(Config.NOTIFICATION_ID);
        }catch (Exception e){}
    }

    private void callAPI(){
        pb_loading.setVisibility(View.VISIBLE);
        OrdersAPIsClass.order_details(OrderDetailsActivity.this,
                myIntent.getStringExtra("order_id"),
                SharedPrefManager.getInstance(OrderDetailsActivity.this).getUser().getUser_id(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            OrderDetailsResponse success = new Gson().fromJson(json1,OrderDetailsResponse.class);
                            if (success.getUserInfo()!=null){
                                tv_name.setText(success.getUserInfo().getFullname());
                                tv_address.setText(success.getUserInfo().getAddress());
                            }
                            if (success.getOrder()!=null){
                                tv_title.setText("الطلب : "+success.getOrder().getId());
                                if (success.getOrder().getNotes()!=null){
                                    if (!success.getOrder().getNotes().equals("")){
                                        tv_notes.setText(success.getOrder().getNotes());
                                    }else {
                                        tv_notes.setText("لا يوجد ملاحظات");
                                    }
                                }else {
                                    tv_notes.setText("لا يوجد ملاحظات");
                                }
                                if (success.getOrder().getItems()!=null){
                                    if (success.getOrder().getItems().size()>0){
                                        if (success.getOrder().getItems().size()>2){
                                            if (myIntent.getStringExtra("type").equals("with")) {
                                                btn_confirm1.setVisibility(View.VISIBLE);
                                            }
                                        }else {
                                            if (myIntent.getStringExtra("type").equals("with")) {
                                                btn_confirm1.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        for (Product p : success.getOrder().getItems()){
                                            listOfProducts.add(p);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                            scrollView.setVisibility(View.VISIBLE);
                        }
                        pb_loading.setVisibility(View.GONE);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_loading.setVisibility(View.GONE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).show();
                    }
                });
    }

    private void callConfirmOrderAPI(final String order_id){
        OrdersAPIsClass.confirmOrder(OrderDetailsActivity.this,
                SharedPrefManager.getInstance(OrderDetailsActivity.this).getUser().getUser_id(),
                order_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(OrderDetailsActivity.this, "تم تأكيد الطلبية بنجاح", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(OrderDetailsActivity.this , MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callConfirmOrderAPI(order_id);
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
