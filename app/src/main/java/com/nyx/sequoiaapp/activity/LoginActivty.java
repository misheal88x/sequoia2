package com.nyx.sequoiaapp.activity;

import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.UsersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.costumes.ForgetPassowrdDialogClass;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import com.facebook.FacebookSdk;
import com.nyx.sequoiaapp.models.LoginObject;
import com.valdesekamdem.library.mdtoast.MDToast;

public class LoginActivty extends RootActivity {

    //LoginButton loginButton;
    private CallbackManager callbackManager;
    private RelativeLayout root;
    private ImageButton btn_facebook;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    void showLoading(boolean w){
        findViewById(R.id.go_login).setEnabled(!w);
        findViewById(R.id.fb).setEnabled(!w);

        findViewById(R.id.loading_view).setVisibility(w?View.VISIBLE:View.INVISIBLE);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_login_activty);
        facebook_init();
        btn_facebook = findViewById(R.id.fb);
        //callbackManager = CallbackManager.Factory.create();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        root = findViewById(R.id.login_layout);
        findViewById(R.id.forget_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(LoginActivty.this)){
                    Toast.makeText(LoginActivty.this, "قم بالاتصال بالانترنت من فضلك !", Toast.LENGTH_SHORT).show();
                    return;
                }
                ForgetPassowrdDialogClass fgot  =new ForgetPassowrdDialogClass(LoginActivty.this);
                fgot.show();
            }
        });
        final TextView textView = (TextView)findViewById(R.id.description);
        final String[] array = {
                "عند امتلاكك لحساب في سيكويا , ستتمكن من تسجيل طلبياتك و الحصول على كل ماترغب عليه ليصل الى منزلك في أي مكان",
                "جرب الاطلاع على العروض المذهلة من سيكويا و التي لن تراها في اي سوق الكتروني آخر",
                "هدفنا تقريب المسافة بين البائع و المشتري و ايصال السلعة الى المشتري حيثما كان",
                "يمكنك تسجيل الدخول ببريدك الالكتروني او رقم الهاتف",
                "حسابك على سيكويا مجاني مدى الحياة ! سجل لدينا الآن"
        };
        textView.post(new Runnable() {
            int i = 0;
            @Override
            public void run() {
                textView.setText(array[i]);
                i++;
                if (i ==array.length)
                    i = 0;
                textView.postDelayed(this, 5000);
            }
        });


        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("تسجيل الدخول");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.go_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(LoginActivty.this)){
                    Toast.makeText(LoginActivty.this, "قم بالاتصال بالانترنت من فضلك !", Toast.LENGTH_SHORT).show();
                    return;
                }
                startActivity(new Intent(LoginActivty.this , SignupActivity.class));
                finish();
            }
        });



        /*
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));
                **/

        findViewById(R.id.fb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(LoginActivty.this)){
                    Toast.makeText(LoginActivty.this, "قم بالاتصال بالانترنت من فضلك !", Toast.LENGTH_SHORT).show();
                    return;
                }
                //showLoading(true);
                LoginManager.getInstance().logInWithReadPermissions(LoginActivty.this,
                        Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
                //loginButton.performClick();
            }
        });
        /*
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {



            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {


                                    HashMap<String  ,String> params=new HashMap<String, String>();
                                    params.put("login_type" ,"login_fb");
                                    params.put("facebook_id" ,Profile.getCurrentProfile().getId());
                                    params.put("access_token" , AccessToken.getCurrentAccessToken().getToken());

                                    go(0 , params);

                                }catch (Exception J){
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext() ,"Cancelled",Toast.LENGTH_SHORT).show();

                showLoading(false);


            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                showLoading(false);
            }
        });
**/




        findViewById(R.id.go_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(LoginActivty.this)){
                    Toast.makeText(LoginActivty.this, "قم بالاتصال بالانترنت من فضلك !", Toast.LENGTH_SHORT).show();
                    return;
                }
        String phone = ((EditText)findViewById(R.id.email_input)).getText().toString().trim();
        final String password = ((EditText)findViewById(R.id.password_input)).getText().toString().trim();
        if(phone.equals("")){
            Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف أو البريد  الالكتروني من فضلك..", Toast.LENGTH_SHORT).show();
            return;
        }
                if(password.equals("")){
                    Toast.makeText(getApplicationContext(), "أدخل كلمة المرور  من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String  ,String> params=new HashMap<String, String>();
                params.put("mobile_number" ,phone);
                params.put("login_type" ,"login_app");
                params.put("password" ,password);
                showLoading(true);
             go(0 , params);
            }
        });


    }

    private void facebook_init(){
        //FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {}

                    @Override
                    public void onCancel() {}

                    @Override
                    public void onError(FacebookException exception) {}});
    }

    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken==null){
                Toast.makeText(LoginActivty.this, "User logged out", Toast.LENGTH_SHORT).show();
            }else {
                getFacebookInfos(currentAccessToken);
            }
        }
    };

    private void getFacebookInfos(final AccessToken accessToken){
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                String FEmail = "";
                String FAccessToken = "";
                String FId = "";
                String FImage = "";
                try {
                    FAccessToken = accessToken.getToken();
                    FId = object.getString("id");
                    HashMap<String  ,String> params=new HashMap<String, String>();
                    params.put("login_type" ,"login_fb");
                    params.put("facebook_id" ,FId);
                    params.put("access_token" , FAccessToken);
                    go(0 , params);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }


    void go(final int try_ , final HashMap params ){
        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(!s.equals("")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showLoading(false);
                        }
                    });
                    try {
                        JSONObject object =new JSONObject(s);
                        if(object.getInt("status")==1)
                        {
                            LoginObject lo = new LoginObject();
                            if (params.get("login_type").toString().equals("login_app")){
                                lo.setMobile_number(params.get("mobile_number").toString());
                                lo.setLogin_type(params.get("login_type").toString());
                                lo.setPassword(params.get("password").toString());
                                SharedPrefManager.getInstance(LoginActivty.this).saveLoginType("normal");
                                SharedPrefManager.getInstance(LoginActivty.this).saveLoginData(lo);
                            }else if (params.get("login_type").toString().equals("login_fb")){
                                lo.setFacebook_id(params.get("facebook_id").toString());
                                lo.setAccess_token(params.get("access_token").toString());
                                SharedPrefManager.getInstance(LoginActivty.this).saveLoginType("facebook");
                                SharedPrefManager.getInstance(LoginActivty.this).saveLoginData(lo);
                            }
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                                    object.getJSONObject("data") ,((EditText)findViewById(R.id.password_input)).getText().toString().trim());
                            Log.i("user_idddd", "doTask: "+SharedPrefManager.getInstance(LoginActivty.this).getUser().getUser_id());
                            if (SharedPrefManager.getInstance(LoginActivty.this).getUser().getUser_type().equals("2")){
                                callCheckStoreAPI();
                            }else {
                                MDToast mdToast = MDToast.makeText(LoginActivty.this, "مرحبا بك , "+SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                                        , MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS);
                                mdToast.show();
                                //Toast.makeText(getApplicationContext(), "مرحبا بك , " +
                                               // SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                                       // , Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivty.this ,SplashActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                           // refreshFBToken();
                            //todo

                        }else{
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            LoginManager.getInstance().logOut();
                        }
                    }catch (Exception fdr){
                        //Log.d("FDR SIGNUP : " , fdr.getMessage());
                        LoginManager.getInstance().logOut();
                    }
                }else{
                    if(try_<10)go(try_+1 , params);
                    else
                    { runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showLoading(false);
                        }
                    });
                        Toast.makeText(LoginActivty.this, "حدث خلل اثناء الاتصال , حاول من جديد", Toast.LENGTH_SHORT).show();
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        }  , APIUrl.SERVER2 + "user/login",params);
    }





//
//    void refreshFBToken(){
//        HashMap<String  ,String> params=new HashMap<String, String>();
//
//
//        SharedPreferences pref =
//                getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
//        String regId = pref.getString("regId", null);
//
//
//        params.put("user_id" ,SharedPrefManager.getInstance(this).getUser().getUser_id());
//        params.put("user_token" ,SharedPrefManager.getInstance(this).getUser().getToken());
//        params.put("new_value" ,regId);
//        params.put("type" ,"8");
//
//        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
//            @Override
//            public void doTask() {
//
//            }
//
//            @Override
//            public void doTask(String s) {
//                if(s.equals(""))refreshFBToken();
//
//            }
//        }  , APIUrl.SERVER + "user/update",params);
//
//
//    }

    private void callCheckStoreAPI(){
        String user_id = SharedPrefManager.getInstance(LoginActivty.this).getUser().getUser_id();
        UsersAPIsClass.check_seller_status(LoginActivty.this, user_id, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if (json != null){
                    String json1 = new Gson().toJson(json);
                    boolean success = new Gson().fromJson(json1,Boolean.class);
                    SharedPrefManager.getInstance(LoginActivty.this).setStoreUpdated(success);
                }
                MDToast mdToast = MDToast.makeText(LoginActivty.this, "مرحبا بك , "+SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                        , MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS);
                mdToast.show();
               // Toast.makeText(getApplicationContext(), "مرحبا بك , " +
                //                SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                //        , Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivty.this ,SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                        .setAction("أعد المحاولة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callCheckStoreAPI();
                            }
                        }).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
