package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.SharedPrefManager;

public class OnBoardActivity extends TutorialActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addFragment(new Step.Builder().setTitle("نصائح عامة")
                .setContent("لتتميز في العرض والبيع عليك الاهتمام بجمالية الصورة ودقتها")
                .setBackgroundColor(Color.parseColor("#F1780F")) // int background color
                .setDrawable(R.drawable.true_wrong) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("نصائح عامة")
                .setContent("عليك كتابة كل ما يمكن كتابته من تفاصيل عن المنتج لأن المستهلك يحتاج إلى الكثير من المعلومات ليأخذ قرار الشراء")
                .setBackgroundColor(Color.parseColor("#F1780F")) // int background color
                .setDrawable(R.drawable.attractive_product2)// int top drawable
                .build());
        addFragment(new Step.Builder()
                .setTitle("نصائح عامة")
                .setContent("من أهم النقاط التي ستجعل العميل يبحث عن منتجاتك هي سرعتك في التوصيل وهذا يعتمد على سرعتك في تسليمنا الطلبيات لذا راقب اشعاراتك يوميا")
                .setBackgroundColor(Color.parseColor("#F1780F")) // int background color
                .setDrawable(R.drawable.notification_bell2) // int top drawable
                .build());
        setPrevText("السابق");
        setNextText("التالي");
        setCancelText("إلغاء");
        setFinishText("إنهاء");
    }

    @Override
    public void currentFragmentPosition(int position) {


    }

    @Override
    public void finishTutorial() {
        super.finishTutorial();
        SharedPrefManager.getInstance(OnBoardActivity.this).setTutorial(true);
        Intent ii = new Intent(OnBoardActivity.this , AddSellerProductActivity.class);
        ii.putExtra("activity_type","add");
        startActivity(ii);
    }
}
