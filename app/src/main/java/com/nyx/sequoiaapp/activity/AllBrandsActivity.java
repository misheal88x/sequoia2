package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.BrandsAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.AllBrandsAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.NewBrandObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class AllBrandsActivity extends RootActivity {

    private RelativeLayout root;
    private LinearLayout title_layout;
    private androidx.appcompat.widget.Toolbar toolbar;
    private ProgressBar pb_first;
    private RecyclerView rv_recycler;
    private List<NewBrandObject> list;
    private GridLayoutManager layoutManager;
    private AllBrandsAdapter adapter;
    private ImageView empty_image;
    private TextView tv_desc;
    private NestedScrollView scrollView;
    private FloatingActionButton fab_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_brands);
        init_views();
        init_events();
        init_recycler();
        hide_elements();
        callAPI();
    }

    private void init_views(){
        //Root
        root = findViewById(R.id.all_brands_layout);
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("الشركات و العلامات التجارية");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Nested Scroll View
        scrollView = findViewById(R.id.all_brands_scroll);
        //Linear layout
        title_layout = findViewById(R.id.all_brands_title_layout);
        //Progress bar
        pb_first = findViewById(R.id.all_brands_first_progress);
        //Recycler View
        rv_recycler = findViewById(R.id.all_brands_recycler);
        //Empty image
        empty_image = findViewById(R.id.no_items_found);
        //TextView
        tv_desc = findViewById(R.id.description);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
    }

    private void init_events(){
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllBrandsActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
    }
    private void hide_elements(){
        rv_recycler.setVisibility(View.GONE);
    }
    private void show_elements(){
        rv_recycler.setVisibility(View.GONE);
        pb_first.setVisibility(View.GONE);
    }
    private void init_recycler(){
        list = new ArrayList<>();
        layoutManager = new GridLayoutManager(AllBrandsActivity.this,2){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        adapter = new AllBrandsAdapter(AllBrandsActivity.this,list);
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);

    }

    private void callAPI(){
        String user_id = SharedPrefManager.getInstance(AllBrandsActivity.this).getUser().getUser_id();
        BrandsAPIsClass.get_brands(AllBrandsActivity.this, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if(json != null){
                    String json1 = new Gson().toJson(json);
                    NewBrandObject[] success = new Gson().fromJson(json1,NewBrandObject[].class);
                    if (success!=null){
                        if (success.length>0){
                            pb_first.setVisibility(View.GONE);
                                for (NewBrandObject oo : success){
                                    list.add(oo);
                                    adapter.notifyDataSetChanged();
                                }
                                rv_recycler.setVisibility(View.VISIBLE);
                            BaseFunctions.runAnimationVertical(rv_recycler,0,adapter);

                        }else {
                            pb_first.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }
                    }else {
                        pb_first.setVisibility(View.GONE);
                        empty_image.setVisibility(View.VISIBLE);
                        tv_desc.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                pb_first.setVisibility(View.GONE);
                Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                        .setAction("أعد المحاولة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callAPI();
                            }
                        }).show();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
