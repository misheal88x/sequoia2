package com.nyx.sequoiaapp.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.StaticPagesListAdapter;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.StaticContent;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StaticPagesListActivity  extends RootActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        String title= "حول سيكويا" ;
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        findViewById(R.id.categories_show_all).setVisibility(View.GONE);


        getSupportActionBar().setTitle("سيكويا");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView)findViewById(R.id.title)).setText(title);
        ((TextView)findViewById(R.id.description)).setText("احصل على ما تحتاجه من معلومات عبر صفحات الدعم من سيكويا");
init(0);


    }


    void init(final int tries){
        BackgroundServices.getInstance(this).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(s.equals("")){init(tries+1);}
                else
                try {
                    findViewById(R.id.loading).setVisibility(View.GONE);

                    JSONArray all = new JSONObject(s).getJSONArray("data");
                    ArrayList data = new ArrayList();
                    for(int i=0;i<all.length();i++)
                        data.add(new StaticContent(all.getJSONObject(i).getString("id") ,all.getJSONObject(i).getString("title")
                                ,all.getJSONObject(i).getString("content")));
                    RecyclerView recyclerView5 = (RecyclerView) findViewById(R.id.recyclerview5);
                    recyclerView5.setLayoutManager(new LinearLayoutManager(StaticPagesListActivity.this, LinearLayoutManager.VERTICAL,
                            false
                    ));
                    StaticPagesListAdapter adapter = new StaticPagesListAdapter(data ,StaticPagesListActivity.this);
                    recyclerView5.setAdapter(adapter);
                    BaseFunctions.runAnimationHorizontal(recyclerView5,0,adapter);
                }catch (JSONException RR){
                    //Log.d("NAXA" , RR.getMessage());

                }

            }
        } , APIUrl.SERVER + "page/100");

    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
