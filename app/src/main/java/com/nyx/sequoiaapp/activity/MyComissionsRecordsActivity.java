package com.nyx.sequoiaapp.activity;

import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.View;
import android.widget.Toast;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.ComissionsRecordsAdapter;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.MyComissionRecord;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MyComissionsRecordsActivity extends ViewProductsActivity {


    int max=100000;
    void loadMore(final int t) {
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            Snackbar.make(mainScrollView, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                    .setAction("أعد المحاولة", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadMore(0);
                        }
                    }).show();
            loading.setVisibility(View.GONE);
            return;
        }
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
       final User user = SharedPrefManager.getInstance(this).getUser();
        HashMap<String  ,String> params=new HashMap<String, String>();
        params.put("user_id" ,user.getUser_id());
        params.put("user_token" ,user.getToken());
        params.put("start" ,current+"");
        BackgroundServices.getInstance(this).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);


                try {

                    JSONObject heart = new JSONObject(s);
                    JSONObject full = heart.getJSONObject("data");

                    setDescription( "عرض " + " المبيعات و العمولات التي انجزها المسوق " +
                          user.getName() +"." +
                            "\r\nاجمالي العمولات : " + full.getString("full_ammount") + " "+getResources().getString(R.string.curremcy)
                    + "\r\nعمولات سابقة : " + full.getString("past_ammount") + " "+getResources().getString(R.string.curremcy)
                    + "\r\nعمولات متبقية : " + full.getString("remain_ammount") + " "+getResources().getString(R.string.curremcy));

                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    if(heart.getInt("status")==1) {
                        JSONArray orders = full.getJSONArray("details");
                        for (int i = 0; i < orders.length(); i++) {
                            MyComissionRecord or = new MyComissionRecord(
                                    orders.getJSONObject(i).getString("id"),
                                    orders.getJSONObject(i).getString("uid"),
                                    orders.getJSONObject(i).getString("oid"),
                                    orders.getJSONObject(i).getString("value"),
                                    orders.getJSONObject(i).getString("status_str"),
                                    orders.getJSONObject(i).getString("date"),
                                    orders.getJSONObject(i).getString("customer"),
                                    orders.getJSONObject(i).getString("ref"),
                                    orders.getJSONObject(i).getString("product_name"),
                                    orders.getJSONObject(i).getString("pic")
                            );
                            items.add(or);

                        }

                        adapter.notifyDataSetChanged();
                        if (current<20){
                            BaseFunctions.runAnimationHorizontal(mainRecylceView,0,adapter);
                        }
                        loadingFlag = false;
                        current += 10;
                        if (items.size() == 0) {
                            noItemsFound.setVisibility(View.VISIBLE);
                            setDescription("لا يوجد أي عمولات  تخصك . ");
                        }
                    }else{
                        Toast.makeText(MyComissionsRecordsActivity.this, heart.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException jex) {
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                    //Log.d("order_jex in : " , jex.getMessage());
                }
            }
        }, APIUrl.SERVER  +
                "user/comissions" ,params);
    }
    int current=0;

    boolean loadingFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    void setTitle() {
        getSupportActionBar().setTitle("عمولاتي");
    }

    @Override
    void fetchData() {
        setWindowTitle("عمولات المسوق ");
        setDescription( "عرض " + " المبيعات و العمولات التي انجزها المسوق " +
                SharedPrefManager.getInstance(this).getUser().getName() +".");
        LinearLayoutManager lm = new LinearLayoutManager(this) ;
        adapter= new ComissionsRecordsAdapter(items , this);
        mainRecylceView.setLayoutManager(lm);
        mainRecylceView.setAdapter(adapter);
        loadMore(0);
    }

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();

        if(items.size()>=max)return;

        if (!loadingFlag) {

            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore(0);
            }

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
