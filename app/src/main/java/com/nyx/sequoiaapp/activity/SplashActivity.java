package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.RequiresApi;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.other.BaseFunctions;

public class SplashActivity extends Activity {


//    private boolean checkIfAlreadyhavePermission() {
//        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
//        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        int result3 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
//        if (result == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
//                && result3 == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//
//    private void requestForSpecificPermission() {
//        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE}, 101);
//    }

    void init(final int t) {

        //Test if there is an internet connection
        if (ConnectionUtils.isNetworkAvailable(this))
            BackgroundServices.getInstance(this).init(new PostAction() {
                @Override
                public void doTask() {

                }

                @Override
                public void doTask(String s) {
                    if (!s.equals("")) {
                        try {
                            String x = s;
                            SharedPrefManager.getInstance(SplashActivity.this).saveData(s);
                            if (BaseFunctions.getVersionCode(SplashActivity.this)<Integer.valueOf(SharedPrefManager.getInstance(SplashActivity.this).getCurrentVersion())){
                                if (SharedPrefManager.getInstance(SplashActivity.this).getCanIgnore()){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                                    builder.setIcon(R.drawable.high_logo);
                                    builder.setTitle("تحديث");
                                    builder.setMessage("نسختك من تطبيق سيكويا أصبحت قديمة, اضغط على تحديث لتحميل النسخة الجديدة من متجر جوجل بلاي");
                                    builder.setPositiveButton("تحديث", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            BaseFunctions.lunchPlayStore(SplashActivity.this,SplashActivity.this.getPackageName());
                                            finish();
                                        }
                                    });
                                    builder.setNegativeButton("متابعة", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (SharedPrefManager.getInstance(SplashActivity.this).hasData()) {
                                                Intent ii = new Intent(SplashActivity.this, MainActivity.class);
                                                startActivity(ii);
                                                finish();
                                            } else {
                                                Snackbar.make(findViewById(R.id.ssp), "حدث خطأ أثناء التهيئة", Snackbar.LENGTH_INDEFINITE)
                                                        .setAction("حاول من جديد", new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                init(0);
                                                            }
                                                        }).show();
                                            }
                                        }
                                    });
                                    builder.show();
                                }else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                                    builder.setIcon(R.drawable.high_logo);
                                    builder.setTitle("تحديث");
                                    builder.setMessage("نسختك من تطبيق سيكويا أصبحت قديمة, اضغط على تحديث لتحميل النسخة الجديدة من متجر جوجل بلاي");
                                    builder.setPositiveButton("تحديث", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            BaseFunctions.lunchPlayStore(SplashActivity.this,SplashActivity.this.getPackageName());
                                            finish();
                                        }
                                    });
                                    builder.show();
                                }
                            }else {
                                if (SharedPrefManager.getInstance(SplashActivity.this).hasData()) {
                                    Intent ii = new Intent(SplashActivity.this, MainActivity.class);
                                    startActivity(ii);
                                    finish();
                                } else {
                                    Snackbar.make(findViewById(R.id.ssp), "حدث خطأ أثناء التهيئة", Snackbar.LENGTH_INDEFINITE)
                                            .setAction("حاول من جديد", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    init(0);
                                                }
                                            }).show();
                                }
                            }

                        } catch (Exception ew) {
                        }
                    } else {
                        //t is number of shots
                        if (t < 10) init(t + 1);
                        else
                            Snackbar.make(findViewById(R.id.ssp), "حدث خطأ أثناء التهيئة", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("حاول من جديد", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            init(0);
                                        }
                                    }).show();
                    }
                }
            });
        else {
            if (SharedPrefManager.getInstance(this).hasData()) {
                if (BaseFunctions.getVersionCode(SplashActivity.this)<Integer.valueOf(SharedPrefManager.getInstance(SplashActivity.this).getCurrentVersion())){
                    if (SharedPrefManager.getInstance(SplashActivity.this).getCanIgnore()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                        builder.setIcon(R.drawable.high_logo);
                        builder.setTitle("تحديث");
                        builder.setMessage("نسختك من تطبيق سيكويا أصبحت قديمة, اضغط على تحديث لتحميل النسخة الجديدة من متجر جوجل بلاي");
                        builder.setPositiveButton("تحديث", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BaseFunctions.lunchPlayStore(SplashActivity.this,SplashActivity.this.getPackageName());
                            }
                        });
                        builder.setNegativeButton("متابعة", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent s = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(s);
                                finish();
                            }
                        });
                        builder.show();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                        builder.setIcon(R.drawable.high_logo);
                        builder.setTitle("تحديث");
                        builder.setMessage("نسختك من تطبيق سيكويا أصبحت قديمة, اضغط على تحديث لتحميل النسخة الجديدة من متجر جوجل بلاي");
                        builder.setPositiveButton("تحديث", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BaseFunctions.lunchPlayStore(SplashActivity.this,SplashActivity.this.getPackageName());
                            }
                        });
                        builder.show();
                    }
                }else {
                    Intent s = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(s);
                    finish();
                }
            } else {
                Toast.makeText(this, "اتصل بالانترنت من فضلك و حاول مرة أخرى", Toast.LENGTH_SHORT).show();
                finish();
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init(0);
                    //granted
                } else {
                    //not granted
                    Toast.makeText(this, "لا يمكن المتابعة بدون الصلاحيات السابقة !", Toast.LENGTH_SHORT).show();
                    finish();

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    boolean handle_firebase() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            String target_id = extras.getString("target_id");
            String target_type = extras.getString("target_type");
            if (target_type != null && target_id != null) {
                int noti_count = SharedPrefManager.getInstance(getApplicationContext()).getNotiCount();
                noti_count++;
                SharedPrefManager.getInstance(getApplicationContext()).saveNotiCount(noti_count);
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), NotificationsActivity.class);
                if (target_type.toString().equals("1")
                        || target_type.toString().equals("2")||target_type.toString().equals("212")) {
                    resultIntent = new Intent(getApplicationContext(), ProductLoadingFromIntent.class);
                    resultIntent.putExtra("id", target_id.toString());
                }
                if (target_type.toString().equals("3")) {
                    resultIntent = new Intent(getApplicationContext(), ChatActivity.class);
                    resultIntent.putExtra("title","الدعم");
                }

                startActivity(resultIntent);
                return true;
            }
        }

        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!handle_firebase()) {
           // if (SharedPrefManager.getInstance(SplashActivity.this).hasData())
            {
                setContentView(R.layout.activity_splash);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /*
                        Intent ii = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(ii);
                        finish();
                        **/
                        init(0);
                    }
                }, 2000);
            }
//            else {
//
//                int MyVersion = Build.VERSION.SDK_INT;
//                if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
//                    getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
//                }
//
//
//                setContentView(R.layout.activity_splash);
//
//                try {
//                    PackageInfo info = getPackageManager().getPackageInfo(
//                            "com.nyx.sequoiaapp",
//                            PackageManager.GET_SIGNATURES);
//                    for (Signature signature : info.signatures) {
//                        MessageDigest md = MessageDigest.getInstance("SHA");
//                        md.update(signature.toByteArray());
//                        Log.d("SeqouiaKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                    }
//                } catch (PackageManager.NameNotFoundException e) {
//
//                } catch (NoSuchAlgorithmException e) {
//
//                }
//                init(0);
//
//
//                {
//
//                }
//            }
        } else {
            finish();
        }
    }
}
