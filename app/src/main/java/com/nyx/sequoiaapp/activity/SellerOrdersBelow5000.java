package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.OrdersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.OrdersUnder5000Adapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.NewOrderObject;
import com.nyx.sequoiaapp.models.NewOrdersResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class SellerOrdersBelow5000 extends RootActivity {

    private RelativeLayout root;
    private ProgressBar pb_loading,pb_more;
    private RecyclerView rv_recycler;
    private List<NewOrderObject> list;
    private LinearLayoutManager layoutManager;
    private OrdersUnder5000Adapter adapter;
    private ImageView empty_image;
    private TextView tv_desc;
    private int currentPage = 1;
    private int per_page = 20;
    private NestedScrollView scrollView;
    private EditText edt_search;
    private TextView tv_search;
    private int get_type = 0;
    private Intent myIntent;
    private String type = "";
    private FloatingActionButton fab_chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_orders_below5000);
        init_views();
        init_events();
        init_recycler();
        get_type = 1;
        if (type.equals("less")){
            callAPI(currentPage,0);
        }else {
            callMoreAPI(currentPage,0);
        }

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            currentPage++;
                            if (get_type == 1) {
                                if (type.equals("less")){
                                    callAPI(currentPage,1);
                                }else {
                                    callMoreAPI(currentPage,1);
                                }
                            }else {
                                if (myIntent.getStringExtra("type").equals("less")){
                                    callSearchLessAPI(currentPage,0,edt_search.getText().toString());
                                }else {
                                    callSearchMoreAPI(currentPage,0,edt_search.getText().toString());
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void init_views(){
        //Intent
        myIntent = getIntent();
        type = myIntent.getStringExtra("type");
        //Root
        root = findViewById(R.id.seller_orders_less_layout);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
        //Nested ScrollView
        scrollView = findViewById(R.id.seller_orders_less_scroll);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (type.equals("less")){
            getSupportActionBar().setTitle("طلبيات قيمتها أقل من 5000");
        }else {
            getSupportActionBar().setTitle("طلبيات قيمتها أكثر من 5000");
        }

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //ProgressBar
        pb_loading = findViewById(R.id.seller_orders_less_progress);
        pb_more = findViewById(R.id.my_store_progress_more);
        //RecyclerView
        rv_recycler = findViewById(R.id.seller_orders_less_recycler);
        pb_more = findViewById(R.id.my_store_progress_more);
        //Empty Image
        empty_image = findViewById(R.id.no_items_found);
        //TextView
        tv_desc = findViewById(R.id.description);
        tv_search = findViewById(R.id.search_txt);
        //EditText
        edt_search = findViewById(R.id.orders_less_search);

    }

    private void init_events(){
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerOrdersBelow5000.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        edt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length()>0){
                    //btn_clear.setVisibility(View.VISIBLE);
                }else {
                    pb_loading.setVisibility(View.VISIBLE);
                    list.clear();
                    rv_recycler.setAdapter(new OrdersUnder5000Adapter(SellerOrdersBelow5000.this,list));
                    currentPage = 1;
                    get_type = 1;
                    if (type.equals("less")){
                        callAPI(currentPage,0);
                    }else {
                        callMoreAPI(currentPage,0);
                    }
                    //btn_clear.setVisibility(View.GONE);
                }
            }
        });
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_search.getText().toString().equals("")){
                    Toast.makeText(SellerOrdersBelow5000.this, "الرجاء كتابة اسم الزبون للبحث", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    currentPage = 1;
                    list.clear();
                    rv_recycler.setAdapter(new OrdersUnder5000Adapter(SellerOrdersBelow5000.this,list));
                    pb_loading.setVisibility(View.VISIBLE);
                    get_type = 2;
                    if (myIntent.getStringExtra("type").equals("less")){
                        callSearchLessAPI(currentPage,0,edt_search.getText().toString());
                    }else {
                        callSearchMoreAPI(currentPage,0,edt_search.getText().toString());
                    }
                }
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(SellerOrdersBelow5000.this, "الرجاء كتابة اسم الزبون للبحث", Toast.LENGTH_SHORT).show();
                        return false;
                    }else {
                        currentPage = 1;
                        list.clear();
                        rv_recycler.setAdapter(new OrdersUnder5000Adapter(SellerOrdersBelow5000.this,list));
                        pb_loading.setVisibility(View.VISIBLE);
                        get_type = 2;
                        if (myIntent.getStringExtra("type").equals("less")){
                            callSearchLessAPI(currentPage,0,edt_search.getText().toString());
                        }else {
                            callSearchMoreAPI(currentPage,0,edt_search.getText().toString());
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void init_recycler(){
        list = new ArrayList();
        adapter = new OrdersUnder5000Adapter(SellerOrdersBelow5000.this,list);
        layoutManager = new LinearLayoutManager(SellerOrdersBelow5000.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void callAPI(final int page, final int type){
        if (type == 1){
            pb_loading.setVisibility(View.GONE);
            pb_more.setVisibility(View.VISIBLE);
        }else {
            pb_loading.setVisibility(View.VISIBLE);
            pb_more.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerOrdersBelow5000.this).getUser().getUser_id();
        OrdersAPIsClass.getAllOrders(SellerOrdersBelow5000.this,
                user_id,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            NewOrdersResponse success = new Gson().fromJson(json1,NewOrdersResponse.class);
                            per_page = success.getPer_page();
                            if (success.getData()!=null){
                                per_page = success.getPer_page();
                                if (success.getData().size()>0){
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                        rv_recycler.setVisibility(View.VISIBLE);
                                        empty_image.setVisibility(View.GONE);
                                        tv_desc.setVisibility(View.GONE);
                                        if (type == 0){
                                            BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                        }
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        empty_image.setVisibility(View.VISIBLE);
                                        tv_desc.setVisibility(View.VISIBLE);
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    pb_loading.setVisibility(View.GONE);
                                    empty_image.setVisibility(View.VISIBLE);
                                    tv_desc.setVisibility(View.VISIBLE);
                                }else {
                                    pb_more.setVisibility(View.GONE);
                                }
                            }
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0){
                            pb_loading.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_more.setVisibility(View.GONE);
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(page,type);
                                    }
                                }).show();
                    }
                });
    }

    private void callMoreAPI(final int page, final int type){
        if (type == 1){
            pb_loading.setVisibility(View.GONE);
            pb_more.setVisibility(View.VISIBLE);
        }else {
            pb_loading.setVisibility(View.VISIBLE);
            pb_more.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerOrdersBelow5000.this).getUser().getUser_id();
        OrdersAPIsClass.getAllOrdersMore(SellerOrdersBelow5000.this,
                user_id,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            NewOrdersResponse success = new Gson().fromJson(json1,NewOrdersResponse.class);
                            per_page = success.getPer_page();
                            if (success.getData()!=null){
                                per_page = success.getPer_page();
                                if (success.getData().size()>0){
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                        rv_recycler.setVisibility(View.VISIBLE);
                                        empty_image.setVisibility(View.GONE);
                                        tv_desc.setVisibility(View.GONE);
                                        if (type == 0){
                                            BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                        }
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        empty_image.setVisibility(View.VISIBLE);
                                        tv_desc.setVisibility(View.VISIBLE);
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    pb_loading.setVisibility(View.GONE);
                                    empty_image.setVisibility(View.VISIBLE);
                                    tv_desc.setVisibility(View.VISIBLE);
                                }else {
                                    pb_more.setVisibility(View.GONE);
                                }
                            }
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0){
                            pb_loading.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_more.setVisibility(View.GONE);
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callMoreAPI(page,type);
                                    }
                                }).show();
                    }
                });
    }

    private void callSearchAPI(final int page, final int type, final String words){
        if (type == 1){
            pb_loading.setVisibility(View.GONE);
            pb_more.setVisibility(View.VISIBLE);
        }else {
            pb_loading.setVisibility(View.VISIBLE);
            pb_more.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerOrdersBelow5000.this).getUser().getUser_id();
        OrdersAPIsClass.search(SellerOrdersBelow5000.this,
                user_id,
                words,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            NewOrdersResponse success = new Gson().fromJson(json1,NewOrdersResponse.class);
                            per_page = success.getPer_page();
                            if (success.getData()!=null){
                                per_page = success.getPer_page();
                                if (success.getData().size()>0){
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                        rv_recycler.setVisibility(View.VISIBLE);
                                        empty_image.setVisibility(View.GONE);
                                        tv_desc.setVisibility(View.GONE);
                                        if (type == 0){
                                            BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                        }
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        empty_image.setVisibility(View.VISIBLE);
                                        tv_desc.setVisibility(View.VISIBLE);
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    pb_loading.setVisibility(View.GONE);
                                    empty_image.setVisibility(View.VISIBLE);
                                    tv_desc.setVisibility(View.VISIBLE);
                                }else {
                                    pb_more.setVisibility(View.GONE);
                                }
                            }
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0){
                            pb_loading.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_more.setVisibility(View.GONE);
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchAPI(page,type,words);
                                    }
                                }).show();
                    }
                });
    }

    private void callSearchLessAPI(final int page, final int type, final String words){
        if (type == 1){
            pb_loading.setVisibility(View.GONE);
            pb_more.setVisibility(View.VISIBLE);
        }else {
            pb_loading.setVisibility(View.VISIBLE);
            pb_more.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerOrdersBelow5000.this).getUser().getUser_id();
        OrdersAPIsClass.search_less(SellerOrdersBelow5000.this,
                user_id,
                words,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            NewOrdersResponse success = new Gson().fromJson(json1,NewOrdersResponse.class);
                            per_page = success.getPer_page();
                            if (success.getData()!=null){
                                per_page = success.getPer_page();
                                if (success.getData().size()>0){
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                        rv_recycler.setVisibility(View.VISIBLE);
                                        empty_image.setVisibility(View.GONE);
                                        tv_desc.setVisibility(View.GONE);
                                        if (type == 0){
                                            BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                        }
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        empty_image.setVisibility(View.VISIBLE);
                                        tv_desc.setVisibility(View.VISIBLE);
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    pb_loading.setVisibility(View.GONE);
                                    empty_image.setVisibility(View.VISIBLE);
                                    tv_desc.setVisibility(View.VISIBLE);
                                }else {
                                    pb_more.setVisibility(View.GONE);
                                }
                            }
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0){
                            pb_loading.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_more.setVisibility(View.GONE);
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchLessAPI(page,type,words);
                                    }
                                }).show();
                    }
                });
    }

    private void callSearchMoreAPI(final int page, final int type, final String words){
        if (type == 1){
            pb_loading.setVisibility(View.GONE);
            pb_more.setVisibility(View.VISIBLE);
        }else {
            pb_loading.setVisibility(View.VISIBLE);
            pb_more.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerOrdersBelow5000.this).getUser().getUser_id();
        OrdersAPIsClass.search_more(SellerOrdersBelow5000.this,
                user_id,
                words,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            NewOrdersResponse success = new Gson().fromJson(json1,NewOrdersResponse.class);
                            per_page = success.getPer_page();
                            if (success.getData()!=null){
                                per_page = success.getPer_page();
                                if (success.getData().size()>0){
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                        rv_recycler.setVisibility(View.VISIBLE);
                                        empty_image.setVisibility(View.GONE);
                                        tv_desc.setVisibility(View.GONE);
                                        if (type == 0){
                                            BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                        }
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                        for (NewOrderObject oo : success.getData()){
                                            list.add(oo);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }else {
                                    if (type == 0){
                                        pb_loading.setVisibility(View.GONE);
                                        empty_image.setVisibility(View.VISIBLE);
                                        tv_desc.setVisibility(View.VISIBLE);
                                    }else {
                                        pb_more.setVisibility(View.GONE);
                                    }
                                }
                            }else {
                                if (type == 0){
                                    pb_loading.setVisibility(View.GONE);
                                    empty_image.setVisibility(View.VISIBLE);
                                    tv_desc.setVisibility(View.VISIBLE);
                                }else {
                                    pb_more.setVisibility(View.GONE);
                                }
                            }
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0){
                            pb_loading.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_more.setVisibility(View.GONE);
                        }
                        hideKeyboard(SellerOrdersBelow5000.this);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchMoreAPI(page,type,words);
                                    }
                                }).show();
                    }
                });
    }
    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
