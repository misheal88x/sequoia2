package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.costumes.EndlessScrollListener;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.SharedPrefManager;

import java.util.ArrayList;

public abstract class ViewProductsActivity extends RootActivity implements EndlessScrollListener {

    RecyclerView mainRecylceView;
    ProgressBar loading;
    RecyclerView.Adapter adapter;
    ArrayList items;
    ImageView noItemsFound;
    private TextView title , details;
    EndlessScrollView mainScrollView;
    Button craete_order;
    FloatingActionButton fab;
    public EditText searchFilter;
    public View searchLayout;
    public ImageButton goSearch;
    public Button continue_shopping,second_continue_shopping;
    public RelativeLayout lyt_search;
    public EditText edt_search;
    public TextView tv_search;
    private FloatingActionButton fab_chat;
    public SimpleRatingBar rb_rate;
    public SliderLayout slider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_products);
        craete_order = (Button)findViewById(R.id.create_order);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        title= (TextView)findViewById(R.id.title);
        details= (TextView)findViewById(R.id.description);
        searchFilter= (EditText)findViewById(R.id.editTextSearch);
        searchLayout= findViewById(R.id.search_layout);
        goSearch= (ImageButton)findViewById(R.id.go_search);
        continue_shopping = findViewById(R.id.continue_shopping);
        second_continue_shopping = findViewById(R.id.second_continue_shopping);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mainRecylceView = (RecyclerView)findViewById(R.id.recyclerview5);
        mainScrollView = (EndlessScrollView)findViewById(R.id.heart_scroll_view);
        mainScrollView.setScrollViewListener(this);
        noItemsFound= (ImageView)findViewById(R.id.no_items_found);
        lyt_search = findViewById(R.id.base_search_layout);
        edt_search = findViewById(R.id.base_search);
        tv_search = findViewById(R.id.search_txt);
        loading = (ProgressBar)findViewById(R.id.loading);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainScrollView.scrollTo(0,0);
            }
        });
        fab_chat = findViewById(R.id.open_chat);
        rb_rate = findViewById(R.id.rate);
        slider = findViewById(R.id.slider);
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewProductsActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        items = new ArrayList();
        continue_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewProductsActivity.this,MainActivity.class));
                finish();
            }
        });
        second_continue_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewProductsActivity.this,MainActivity.class));
                finish();
            }
        });
        setTitle();
        fetchData();
    }
    public void setWindowTitle(String s){
       title.setText(s);
    }
    public void setDescription(String s){
        details.setText(s);
    }

     abstract void setTitle();
    abstract void  fetchData();

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }

}
