package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.speech.RecognizerIntent;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;

import java.util.ArrayList;
import java.util.Locale;

public class RootActivity extends AppCompatActivity {
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int MyVersion = Build.VERSION.SDK_INT;
        if(MyVersion>Build.VERSION_CODES.LOLLIPOP_MR1)
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        super.onCreate(savedInstanceState);



    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "ابحث صوتيا في سيكويا");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "البحث الصوتي غير مدعوم في جهازك",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                  //  Toast.makeText(this, result.get(0), Toast.LENGTH_SHORT).show();
                    Intent ii= new Intent(RootActivity.this,NewSearchResultsActivity.class);
                    ii.putExtra("query" , result.get(0));
                    startActivity(ii);
                }
                break;
            }

        }
    }



    static Button notifCount;
    TextView textCartItemCount;
    public static int mNotifCount = 0;
    public static int mRealNotifCount = 0;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
//        if (navItemIndex == 0) {
//            inflater.inflate(R.menu.main, menu);
//        }
//        // when fragment is notifications, load the menu created for notifications
//        else if (navItemIndex == 3) {
//            inflater.inflate(R.menu.notifications, menu);
//        }else         inflater.inflate(R.menu.search_view_menu_item, menu);
//

        inflater.inflate(R.menu.main, menu);


        MenuItem item = menu.findItem(R.id.badge);
        final MenuItem notif_item = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(item, R.layout.feed_update_count);
        MenuItemCompat.setActionView(notif_item, R.layout.custom_action_item_layout);
        notifCount = (Button) MenuItemCompat.getActionView(item);
        View actionView = MenuItemCompat.getActionView(notif_item);
        textCartItemCount = notif_item.getActionView().findViewById(R.id.cart_badge);
        mNotifCount = SavedCacheUtils.getCart(this).size();
        mRealNotifCount = SavedCacheUtils.getNotiCount(this);

        notifCount.setText(' '+String.valueOf(mNotifCount)+' ');
        textCartItemCount.setText(mRealNotifCount+"");
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RootActivity.this , CartActivity.class));
            }
        });
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(notif_item);
            }
        });



        /*
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        ((SearchView.SearchAutoComplete)searchViewAndroidActionBar.findViewById(
                android.support.v7.appcompat.R.id.search_src_text)).setHint("عن ماذا تبحث ؟ ");
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                Intent ii= new Intent(RootActivity.this,NewSearchResultsActivity.class);
                ii.putExtra("query" , query);
                startActivity(ii);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        **/
        return super.onCreateOptionsMenu(menu);
    }

    public void setNotifCount(int count){
        mNotifCount = count;
        invalidateOptionsMenu();
    }

    public void setRealNotifCount(int count){
        mRealNotifCount = count;
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.badge) {

            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notifications) {
            startActivity(new Intent(this , NotificationsActivity.class));
            return true;
        }
//        if (id == R.id.action_voice_search) {
//            promptSpeechInput();
//            return true;
//        }

        // user is in notifications fragment
        // and selected 'Mark all as Read'
        if (id == R.id.action_mark_all_read) {
            Toast.makeText(getApplicationContext(), "All notifications marked as read!", Toast.LENGTH_LONG).show();
        }

        // user is in notifications fragment
        // and selected 'Clear All'
        if (id == R.id.action_clear_notifications) {
            Toast.makeText(getApplicationContext(), "Clear all notifications!", Toast.LENGTH_LONG).show();
        }
        if (id == R.id.action_home) {
            if(!(this instanceof MainActivity)){
            Intent intent = new Intent(RootActivity.this , MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);}
        }

        if (id == android.R.id.home) {
            this.finish();
        }




        return super.onOptionsItemSelected(item);
    }





}
