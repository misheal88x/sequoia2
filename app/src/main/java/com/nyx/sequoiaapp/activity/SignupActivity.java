package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CitiesAdapter;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.City;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class SignupActivity extends RootActivity {

    LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        callbackManager = CallbackManager.Factory.create();
        init_views();


        final ArrayList cities = SharedPrefManager.getInstance(this).getCities();
        final Spinner spinner = (Spinner) findViewById(R.id.city_spinner);
        CitiesAdapter adapter = new CitiesAdapter(this, cities);
        final TextView textView = (TextView) findViewById(R.id.description);
        final String[] array = {
                "عند امتلاكك لحساب في سيكويا , ستتمكن من تسجيل طلبياتك و الحصول على كل ماترغب عليه ليصل الى منزلك في أي مكان",
                "جرب الاطلاع على العروض المذهلة من سيكويا و التي لن تراها في اي سوق الكتروني آخر",
                "هدفنا تقريب المسافة بين البائع و المشتري و ايصال السلعة الى المشتري حيثما كان",
                "يمكنك تسجيل الدخول ببريدك الالكتروني او رقم الهاتف",
                "حسابك على سيكويا مجاني مدى الحياة ! سجل لدينا الآن"
        };
        textView.post(new Runnable() {
            int i = 0;

            @Override
            public void run() {
                textView.setText(array[i]);
                i++;
                if (i == array.length)
                    i = 0;
                textView.postDelayed(this, 5000);
            }
        });

        spinner.setAdapter(adapter);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));
        findViewById(R.id.fb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(SignupActivity.this)){
                    Toast.makeText(SignupActivity.this, "قم بالاتصال بالانترنت من فضلك !", Toast.LENGTH_SHORT).show();
                    return;
                }

//
//        String phone = ((EditText)findViewById(R.id.phone_input)).getText().toString().trim();
//        final String password = ((EditText)findViewById(R.id.password_input)).getText().toString().trim();
//
//
//        final String address = ((EditText)findViewById(R.id.address_input)).getText().toString().trim();
//        final String city = ((City)cities.get(spinner.getSelectedItemPosition())).getId();
//
//
//        if(address.equals("")){
//            Toast.makeText(getApplicationContext(), "أدخل عنوانك من فضلك..", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if(phone.equals("")){
//            Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف من فضلك..", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if(password.equals("")){
//            Toast.makeText(getApplicationContext(), "أدخل كلمة المرور  من فضلك..", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if(password.length()<8){
//            Toast.makeText(getApplicationContext(), "كلمة المرور يجب ان تكون أكثر من 8 رموز", Toast.LENGTH_SHORT).show();
//            return;
//        }


                showLoading(true);

                loginButton.performClick();
            }
        });
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {


            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {

                                    //  String phone = ((EditText)findViewById(R.id.phone_input)).getText().toString().trim();
                                    //  final String password = ((EditText)findViewById(R.id.password_input)).getText().toString().trim();
                                    String name = "";
                                    try {
                                        name = Profile.getCurrentProfile().getFirstName().trim()
                                                + " " + Profile.getCurrentProfile().getMiddleName().trim() + " " +
                                                Profile.getCurrentProfile().getLastName().trim();
                                    } catch (Exception re) {
                                        name = "مستخدم فيسبوك";
                                    }
                                    //  ((EditText)findViewById(R.id.name_input)).setText(name);


                                    //  final String address = ((EditText)findViewById(R.id.address_input)).getText().toString().trim();
                                    final String email = object.getString("email").trim();
                                    ((EditText) findViewById(R.id.email_input)).setText(email);
                                    // final String gender = ((RadioButton)findViewById(R.id.male)).isChecked()?"m":"f";

                                    //  final String city = ((City)cities.get(spinner.getSelectedItemPosition())).getId();
//
//                                    if(name.equals("")){
//                                        Toast.makeText(getApplicationContext(), "أدخل اسمك من فضلك..", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }
//                                    if(name.split(" ").length<2){
//                                        Toast.makeText(getApplicationContext(), "أدخل الاسم و الكنية ..", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }
//                                    if(address.equals("")){
//                                        Toast.makeText(getApplicationContext(), "أدخل عنوانك من فضلك..", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }
//                                    if(email.equals("")){
//                                        Toast.makeText(getApplicationContext(), "أدخل   البريد  الالكتروني من فضلك..", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }

//                                    if(phone.equals("")){
//                                        Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف من فضلك..", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }
//                                    if(password.equals("")){
//                                        Toast.makeText(getApplicationContext(), "أدخل كلمة المرور  من فضلك..", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }
//                                    if(password.length()<8){
//                                        Toast.makeText(getApplicationContext(), "كلمة المرور يجب ان تكون أكثر من 8 رموز", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }

                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("login_type", "signup_fb");
                                    params.put("mobile_number", "");
                                    params.put("password", "");
                                    params.put("fullname", name);
                                    params.put("email", email);
                                    params.put("gender", "");
                                    params.put("city", "");
                                    params.put("facebook_id", Profile.getCurrentProfile().getId());
                                    params.put("access_token", AccessToken.getCurrentAccessToken().getToken());
                                    params.put("address", "");
                                    params.put("ref_id", "1");


                                    go(0, params);

                                } catch (Exception J) {
                                    Log.d("JACCASS", J.getMessage());
//                                    showLoading(false , LoginActivity.this);
                                    SharedPrefManager.getInstance(SignupActivity.this)
                                            .Logout();
                                }
                                //   String birthday = object.getString("birthday"); // 01/31/1980 format
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                showLoading(false);
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                showLoading(false);

            }
        });


        findViewById(R.id.go_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActivity.this, LoginActivty.class));
                finish();
            }
        });


        findViewById(R.id.go_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(SignupActivity.this)){
                    Toast.makeText(SignupActivity.this, "قم بالاتصال بالانترنت من فضلك !", Toast.LENGTH_SHORT).show();
                    return;
                }
                String phone = ((EditText) findViewById(R.id.phone_input)).getText().toString().trim();
                final String password = ((EditText) findViewById(R.id.password_input)).getText().toString().trim();
                final String name = ((EditText) findViewById(R.id.name_input)).getText().toString().trim();
                final String address = ((EditText) findViewById(R.id.address_input)).getText().toString().trim();
                final String email = ((EditText) findViewById(R.id.email_input)).getText().toString().trim();
                final String city = ((City) cities.get(spinner.getSelectedItemPosition())).getId();
                final String gender = ((RadioButton) findViewById(R.id.male)).isChecked() ? "m" : "f";

                if (name.equals("")) {
                    Toast.makeText(getApplicationContext(), "أدخل اسمك من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (name.split(" ").length < 2) {
                    Toast.makeText(getApplicationContext(), "أدخل الاسم و الكنية ..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (address.equals("")) {
                    Toast.makeText(getApplicationContext(), "أدخل عنوانك من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!email.equals("") &&
                        !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Toast.makeText(getApplicationContext(), "أدخل   البريد  الالكتروني من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (phone.equals("")) {
                    Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
//                Pattern p = Pattern.compile("^09[0-9]{2}[0-9]{6}$");
//                if (!p.matcher(phone).matches()) {
//                    Toast.makeText(getApplicationContext(), "أدخل رقم الهاتف بصيغة صحيحة  من فضلك..", Toast.LENGTH_SHORT).show();
//                    return;
//                }
                if (password.equals("")) {
                    Toast.makeText(getApplicationContext(), "أدخل كلمة المرور  من فضلك..", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.length() < 8) {
                    Toast.makeText(getApplicationContext(), "كلمة المرور يجب ان تكون أكثر من 8 رموز", Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("login_type", "signup_app");
                params.put("mobile_number", phone);
                params.put("password", password);
                params.put("fullname", name);
                params.put("email", email);
                params.put("city", city);
                params.put("gender", gender);
                params.put("address", address);
                params.put("ref_id", "1");

                showLoading(true);

                go(0, params);


            }
        });


    }


    private void init_views(){
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("حساب جديد");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    void showLoading(boolean w) {
        findViewById(R.id.go_signup).setEnabled(!w);
        findViewById(R.id.fb).setEnabled(!w);

        findViewById(R.id.loading_view).setVisibility(w ? View.VISIBLE : View.INVISIBLE);
    }



    void go(final int try_, final HashMap params) {
        Log.d("SIGNUPWITHPARAMS" ,params.toString());
        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showLoading(false);
                    }
                });
                if (!s.equals("")) {
                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("status") == 1) {
                            //todo save the user data
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                                    object.getJSONObject("data"), ""
                            );
                            Toast.makeText(getApplicationContext(), "مرحبا بك , " +
                                            SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                                    , Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignupActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            LoginManager.getInstance().logOut();

                        }
                    } catch (Exception fdr) {
                        Toast.makeText(SignupActivity.this, "حصل خلل في السيرفر أثناء انشاء الحساب" +
                                " ", Toast.LENGTH_SHORT).show();
                        LoginManager.getInstance().logOut();
                        //Log.d("FDR SIGNUP : " , fdr.getMessage());
                    }
                } else{
                    Toast.makeText(SignupActivity.this, "فشل الاتصال بالشبكة , جرب من جديد", Toast.LENGTH_SHORT).show();
                }
            }
        }, APIUrl.SERVER + "user/login", params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
