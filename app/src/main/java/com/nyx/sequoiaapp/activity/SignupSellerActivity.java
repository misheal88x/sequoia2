package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.UsersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CitiesAdapter;
import com.nyx.sequoiaapp.adapters.CitiesDialogAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.City;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

public class SignupSellerActivity extends RootActivity {

    private RadioButton rb_commission_yes,rb_commission_no;
    private RadioButton rb_branches_yes,rb_branches_no;
    private RadioButton rb_today,rb_tommorrow,rb_days;
    private RadioButton rb_male,rb_female;
    private EditText edt_days,edt_custom_com;
    private TextView edt_fullname,edt_phone_number,edt_telephone,edt_password,edt_address,tv_commissions_details;
    private EditText edt_store_name;
    private Spinner city_spinner;
    private ArrayList source_cities;
    private ImageView img_profile;
    private Button btn_create;
    private TextView tv_chat,tv_number;
    private int selected_commision = -1;
    private int selected_branches = -1;
    private String branches_json = "";
    private int selected_deleiver = -1;
    private String selected_city_id="";
    private String selected_gender = "";
    private int selected_days = 0;
    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private static final int PICK_FROM_GALLERY = 14436;
    private String imagePath;
    private LinearLayout root;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_seller);
        init_views();
        init_events();
        restore_data();
    }

    private void init_views(){
        //Root
        root = findViewById(R.id.add_seller_layout);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("أنشئ متجرك مجانا");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Radio buttons
        rb_commission_yes = findViewById(R.id.add_seller_commission_yes);
        rb_commission_no = findViewById(R.id.add_seller_commission_no);
        rb_branches_yes = findViewById(R.id.add_seller_branches_yes);
        rb_branches_no = findViewById(R.id.add_seller_branches_no);
        rb_today = findViewById(R.id.add_seller_deleiver_today);
        rb_tommorrow = findViewById(R.id.add_seller_deleiver_tommorow);
        rb_days = findViewById(R.id.add_seller_deleiver_days);
        rb_male = findViewById(R.id.add_seller_male);
        rb_female = findViewById(R.id.add_seller_female);
        //EditTexts
        edt_days = findViewById(R.id.add_seller_num_days);
        edt_fullname = findViewById(R.id.add_seller_fullName_edt);
        edt_phone_number = findViewById(R.id.add_seller_phone_number_edt);
        edt_telephone = findViewById(R.id.add_seller_telephone_edt);
        edt_password = findViewById(R.id.add_seller_password_edt);
        edt_address = findViewById(R.id.add_seller_address_edt);
        edt_custom_com = findViewById(R.id.add_seller_custom_commissions);
        edt_store_name = findViewById(R.id.sign_up_seller_store_name);
        //ImageView
        img_profile = findViewById(R.id.add_seller_profile_pic);
        //Button
        btn_create = findViewById(R.id.add_seller_create);
        //Spinner
        city_spinner = findViewById(R.id.city_spinner);
        city_spinner.setEnabled(false);
        city_spinner.setClickable(false);
        final ArrayList cities = SharedPrefManager.getInstance(this).getCities();
        source_cities = new ArrayList();
        if (cities.size() > 0){
            for (int i = 0; i < cities.size() ; i++) {
                City c = (City) cities.get(i);
                //if (c.getName().equals("دمشق")||c.getName().equals("طرطوس")||c.getName().equals("حمص")){
                    source_cities.add(c);
                //}
            }
        }
        CitiesAdapter adapter = new CitiesAdapter(this, source_cities);
        city_spinner.setAdapter(adapter);
        //TextView
        tv_chat = findViewById(R.id.sign_up_seller_chat_txt);
        tv_commissions_details = findViewById(R.id.add_seller_commission_details);
        tv_number = findViewById(R.id.sign_up_seller_phone_call_txt);
        SpannableString content1 = new SpannableString("الدردشة");
        SpannableString content2 = new SpannableString("0949101014");
        content1.setSpan(new UnderlineSpan(), 0, content1.length(), 0);
        content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);
        tv_chat.setText(content1);
        tv_number.setText(content2);
    }
    private void init_events() {
        /*
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(SignupSellerActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SignupSellerActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        **/
        tv_commissions_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupSellerActivity.this,CommissionsDetailsActivity.class));
            }
        });
        rb_commission_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_commision = 1;
                }
            }
        });
        rb_commission_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_commision = 0;
                    edt_custom_com.setVisibility(View.VISIBLE);
                    Snackbar.make(root, "نتشرف بتواصلكم معنا للاتفاق", Snackbar.LENGTH_LONG).show();
                }
            }
        });
        rb_branches_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_branches = 1;
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignupSellerActivity.this);
                    View view = getLayoutInflater().inflate(R.layout.dialog_branches_cities, null);
                    RecyclerView recycler = view.findViewById(R.id.dialog_branches_recycler);
                    Button btn_done = view.findViewById(R.id.dialog_branches_recycler_done);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(SignupSellerActivity.this, LinearLayoutManager.VERTICAL, false);
                    final CitiesDialogAdapter adapter = new CitiesDialogAdapter(SignupSellerActivity.this);
                    recycler.setLayoutManager(layoutManager);
                    recycler.setAdapter(adapter);
                    builder.setView(view);
                    final AlertDialog dialog = builder.create();
                    btn_done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (adapter.getSelectedCities().size() > 0) {
                                branches_json = new Gson().toJson(adapter.getSelectedCities());
                            } else {
                                rb_branches_no.setChecked(true);
                            }
                            dialog.cancel();
                        }
                    });
                    dialog.show();
                }
            }
        });
        rb_branches_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_branches = 0;
                }
            }
        });
        rb_today.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_deleiver = 0;
                    edt_days.setVisibility(View.GONE);
                }
            }
        });
        rb_tommorrow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_deleiver = 1;
                    edt_days.setVisibility(View.GONE);
                }
            }
        });
        rb_days.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_deleiver = 2;
                    edt_days.setVisibility(View.VISIBLE);
                }
            }
        });
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                City c = (City)source_cities.get(position);
                selected_city_id = c.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rb_male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selected_gender = "m";
                }
            }
        });
        rb_female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selected_gender = "f";
                }
            }
        });
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_store_name.getText().toString().equals("")){
                    edt_store_name.requestFocus();
                    Toast.makeText(SignupSellerActivity.this, "يجب عليك تحديد اسم المتجر", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_commision == -1){
                    Toast.makeText(SignupSellerActivity.this, "يجب أن تختار خيار الموافقة أو عدم الموافقة على العمولات المقترحة من سيكويا", Toast.LENGTH_LONG).show();
                    return;
                }
                if (selected_commision == 0 && edt_custom_com.getText().toString().equals("")){
                    Toast.makeText(SignupSellerActivity.this, "يجب عليك كتابة تفاصيل عمولتك في الحقل المخصص لها", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_gender.equals("")){
                    Toast.makeText(SignupSellerActivity.this, "يجب عليك اختيار الجنس", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_branches == -1){
                    Toast.makeText(SignupSellerActivity.this, "يجب عليك تحديد هل تمتلك فروعا في باقي المحافظات أم لا", Toast.LENGTH_SHORT).show();
                    return;
                }else if (selected_branches == 1){
                    if (branches_json.equals("")){
                        Toast.makeText(SignupSellerActivity.this, "يجب عليك اختيار المدن التي تمتلك فروعا فيها", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (selected_deleiver == -1){
                    Toast.makeText(SignupSellerActivity.this, "يجب عليك اختيار احد خيارات تسليم الطلبات لموظفي سيكويا", Toast.LENGTH_SHORT).show();
                    return;
                }else if (selected_deleiver == 2){
                    if (edt_days.getText().toString().equals("")){
                        Toast.makeText(SignupSellerActivity.this, "يجب عليك تحديد عدد الايام للتسليم", Toast.LENGTH_SHORT).show();
                        return;
                    }else {
                        if (Integer.valueOf(edt_days.getText().toString())>4){
                            Toast.makeText(SignupSellerActivity.this, "أقصى عدد أيام للتسليم هو 4 أيام", Toast.LENGTH_SHORT).show();
                            return;
                        }else {
                            selected_days = Integer.valueOf(edt_days.getText().toString());
                        }
                    }
                }
                callUpdateAPI(edt_store_name.getText().toString(),
                        branches_json,selected_branches,selected_deleiver,selected_commision,selected_days,selected_gender);
            }
        });
        tv_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupSellerActivity.this,ChatActivity.class);
                intent.putExtra("title","الدعم");
                startActivity(new Intent());
            }
        });
        tv_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SignupSellerActivity.this);
                View view = getLayoutInflater().inflate(R.layout.dialog_call_choice,null);
                final RelativeLayout lyt_call = view.findViewById(R.id.dialog_call_choice_call_layout);
                final RelativeLayout lyt_whatsapp = view.findViewById(R.id.dialog_call_choice_whatsapp_layout);
                TextView tv_confirm = view.findViewById(R.id.dialog_call_choice_confirm);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                final int[] choice = {0};
                lyt_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lyt_call.setBackgroundColor(getResources().getColor(R.color.grey));
                        lyt_whatsapp.setBackgroundColor(getResources().getColor(R.color.white));
                        choice[0] = 1;
                    }
                });
                lyt_whatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lyt_whatsapp.setBackgroundColor(getResources().getColor(R.color.grey));
                        lyt_call.setBackgroundColor(getResources().getColor(R.color.white));
                        choice[0] = 2;
                    }
                });
                tv_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (choice[0] == 0){
                            Toast.makeText(SignupSellerActivity.this, "يجب عليك اختيار إجراء", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (choice[0] == 1){
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:0949101014"));
                            startActivity(intent);
                        }else {
                            String url = "https://api.whatsapp.com/send?phone=+963 "+"949101014";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });
    }

    void pick(){
        Intent photoPickerIntent =  new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "لا يتم الحصول على الصلاحية", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED " ,requestCode+" and "+resultCode );

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if(data!=null) {
                        Log.e("GOTCHA " ,"yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : "  , resultUri.toString());
                        Log.e("Picsaved2 in : "  , resultUri.getPath());
                        Log.e("Picsaved3 in : "  , resultUri.getEncodedPath());

                        try {
                            imagePath=resultUri.getPath();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            Bitmap bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
                            img_profile.setImageBitmap(bitmap);
                        }catch (Exception e){

                            Toast.makeText(this, "error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case SELECT_PHOTO:
                    if(data!=null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME+".jpg";
                        UCrop.of(pickedImage,
                                Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(3, 3)
                                .withMaxResultSize(800, 800)
                                .start(this);
                    }
                    break;




            }
        }
        else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR" ,cropError.getMessage() );

        }
    }

    private void restore_data(){
        User user = SharedPrefManager.getInstance(SignupSellerActivity.this).getUser();

        //Image
        if (SharedPrefManager.getInstance(SignupSellerActivity.this).getUserPic()!=null){
            try {
                Glide.with(this).load(SharedPrefManager.getInstance(this).getUserPic())
                        .thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform())
                        .into(img_profile);
            }catch (Exception e){}
        }
        //FullName
        if (user.getName()!=null){
            edt_fullname.setText(user.getName());
        }
        //Mobile
        if (user.getPhone()!=null){
            edt_phone_number.setText(user.getPhone());
        }
        //Password
        if (user.getPassword()!=null){
            edt_password.setText(user.getPassword());
        }
        //City
        if (source_cities.size() > 0){

        }
        for (int i = 0; i < source_cities.size(); i++) {
            City c = (City) source_cities.get(i);
            if (c.getId().equals(user.getCity())){
                city_spinner.setSelection(i);
                break;
            }
        }
        //Address
        if (user.getAddress()!=null){
            edt_address.setText(user.getAddress());
        }
        //Gender
        if (user.getGender()!=null){
            if (user.getGender().equals("1")){
                selected_gender = "m";
                rb_male.setChecked(true);
                rb_female.setChecked(false);
            }else if (user.getGender().equals("2")){
                selected_gender = "f";
                rb_male.setChecked(false);
                rb_female.setChecked(true);
            }
        }
    }

    private void callUpdateAPI(final String store_name,
                               final String cities_ids,
                               final int has_branches,
                               final int orders_delivery,
                               final int accept_comission,
                               final int orders_delvery_days,
                               final String gender){
        String new_gen = "";
        if (selected_gender.equals("m")){
            new_gen = "1";
        }else {
            new_gen = "2";
        }
        String user_id = SharedPrefManager.getInstance(SignupSellerActivity.this).getUser().getUser_id();
        UsersAPIsClass.update_seller_info(SignupSellerActivity.this, user_id,store_name, cities_ids,
                has_branches, orders_delivery, accept_comission, orders_delvery_days,
                edt_custom_com.getText().toString(),
                new_gen
                , new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            SharedPrefManager.getInstance(SignupSellerActivity.this).setStoreUpdated(true);
                            Toast.makeText(SignupSellerActivity.this, "تم إضافة المتجر بنجاح", Toast.LENGTH_SHORT).show();
                            SharedPrefManager.getInstance(SignupSellerActivity.this).setTutorial(false);
                            String new_gen = "";
                            if (selected_gender.equals("m")){
                                new_gen = "1";
                            }else {
                                new_gen = "2";
                            }
                            SharedPrefManager.getInstance(SignupSellerActivity.this).setGender(new_gen);
                            startActivity(new Intent(SignupSellerActivity.this,MainActivity.class));
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callUpdateAPI(store_name,cities_ids,has_branches,orders_delivery,accept_comission,orders_delvery_days,gender);
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
