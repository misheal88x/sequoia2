package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.MessageListAdapter;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.BaseMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class OpenComplaintsActivity extends RootActivity {

    private ArrayList<BaseMessage> messageList;
    private MessageListAdapter mMessageAdapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mMessageRecycler;
    private EditText edittext;
    private FloatingActionButton btn_send;
    private SimpleDateFormat sdf;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Handler mHandler;
    int current=0;
    private int mInterval = 5000;
    boolean go=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_complaints);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("الشكاوي المفتوحة");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Swipe Refresh Layout
        mSwipeRefreshLayout = findViewById(R.id.swifeRefresh);
        //RecyclerView
        mMessageRecycler = findViewById(R.id.reyclerview_message_list);
        //EditText
        edittext =  findViewById(R.id.edittext_chatbox);
        //Floating Action Button
        btn_send = findViewById(R.id.button_chatbox_send);
        //Simple Date Format
        sdf = new SimpleDateFormat("HH:mm:ss");
        //Handler
        mHandler = new Handler();

    }
    private void init_events() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "اتصل بالشبكة من فضلك..", Toast.LENGTH_SHORT).show();
                    return ;
                }
                if(!SharedPrefManager.getInstance(getApplicationContext()).IsUserLoggedIn() ){
                    Toast.makeText(getApplicationContext(), "قم بتسجيل الدخول لحسابك  للمتابعة", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(OpenComplaintsActivity.this , LoginActivty.class));

                    return ;
                }

                final  String message =  edittext .getText().toString().trim();
                if( message.equals("")){
                    Toast.makeText(OpenComplaintsActivity.this, "أدخل الرسالة من فضلك ..", Toast.LENGTH_SHORT).show();
                    return;
                }
                btn_send.setEnabled(false);
                edittext.setEnabled(false);
                callSendMessageAPI(edittext.getText().toString());
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //   loadMessages(0 ,9999);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    private void init_activity() {
        init_recycler();
        if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
            Toast.makeText(getApplicationContext(), "اتصل بالشبكة من فضلك..", Toast.LENGTH_SHORT).show();
            mSwipeRefreshLayout.setRefreshing(false);
            finish();
        }
        if(!SharedPrefManager.getInstance(getApplicationContext()).IsUserLoggedIn() ){
            Toast.makeText(getApplicationContext(), "قم بتسجيل الدخول لحسابك  للمتابعة", Toast.LENGTH_SHORT).show();
            mSwipeRefreshLayout.setRefreshing(false);
            finish();
        }
        startRepeatingTask();
        loadMessages(0 ,9999);
    }

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    private void init_recycler(){
        messageList= new ArrayList<>();
        mMessageAdapter = new MessageListAdapter(this, messageList);
        mLayoutManager=new LinearLayoutManager(this);
        mMessageRecycler.setLayoutManager(mLayoutManager);
        mMessageRecycler.setAdapter(mMessageAdapter);
        mMessageRecycler.smoothScrollToPosition(mMessageAdapter.getItemCount());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                if(go)
                    loadMessages(0 ,9999);
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    private void callSendMessageAPI(final String message){

        User user = SharedPrefManager.getInstance(OpenComplaintsActivity.this).getUser();
        Log.i("m_user_id", "callSendMessageAPI: "+user.getUser_id());
        Log.i("m_user_token", "callSendMessageAPI: "+user.getToken());
        HashMap<String  ,String> params=new HashMap<String, String>();
        params.put("user_id" ,user.getUser_id());
        params.put("user_token" ,user.getToken());
        params.put("message" ,message);
        params.put("to" ,"-1");
        BackgroundServices.getInstance(OpenComplaintsActivity.this).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                JSONObject object = null;
                try {
                    object = new JSONObject(s);
                    if(object.getInt("status")==1){
                        edittext .setText("");
                        java.util.Date date = new java.util.Date();
                        messageList.add(new BaseMessage("-1"
                                ,message
                                , SharedPrefManager.getInstance(getApplicationContext()).getUser().getName()
                                , SharedPrefManager.getInstance(getApplicationContext()).getUser().getUser_id()
                                ,sdf.format(date),"0"));
                        mMessageAdapter.notifyDataSetChanged();
                        btn_send.setEnabled(true);
                        edittext.setEnabled(true);
                        mMessageRecycler.smoothScrollToPosition(mMessageAdapter.getItemCount());

                    }
                } catch (JSONException e) {
                    btn_send.setEnabled(true);
                    edittext.setEnabled(true);
                    e.printStackTrace();
                }

            }
        } , APIUrl.SERVER3+"complaints" ,params);
    }

    private void loadMessages(final int t , final int tarhet){
        go=false;
        User user = SharedPrefManager.getInstance(OpenComplaintsActivity.this).getUser();
        HashMap<String  ,String> params=new HashMap<String, String>();
        params.put("user_id" ,user.getUser_id());
        params.put("user_token" ,user.getToken());
        params.put("start" ,"0");
        params.put("to" ,"-1");
        BackgroundServices.getInstance(OpenComplaintsActivity.this).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                go=true;
                if(!s.equals("")) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    JSONObject object = null;
                    try {
                        findViewById(R.id.init_loading).setVisibility(View.GONE);
                        object = new JSONObject(s);
                        if (object.getInt("status") == 1) {
                            // String user_name = SharedPrefManager.getInstance(getActivity()).getUser().getName();
                            // String date = new Date().toString();
                            // BaseMessage msg = addMessage(user_name ,rm.id ,message ,date);
                            JSONArray msgs = object.getJSONObject("data").getJSONArray("messages");
                            if(msgs.length()>0){
                                messageList.clear();
                                mMessageAdapter.notifyDataSetChanged();
                            }
                            for (int i = 0; i < msgs.length(); i++) {
                                BaseMessage bm=new BaseMessage(msgs.getJSONObject(i).getString("id")
                                        , msgs.getJSONObject(i).getString("message")
                                        ,
                                        "الإدارة"
                                        , msgs.getJSONObject(i).getString("from")
                                        , msgs.getJSONObject(i).getString("created_at"), "0");

                                messageList.add(0, bm);
                            }

                            if(msgs.length()==0)
                                messageList.add(new BaseMessage("-1" ,"أهلا بك في الدعم التقني لمستخدمي سيكويا , تفضل بطرح أي استفسار و نحن جاهزون للرد بأسرع وقت , أهلا بك.  "
                                        ,
                                        "الإدارة"
                                        , "-1","منذ لحظات..","0"));
                            mMessageAdapter.notifyDataSetChanged();
                            mMessageRecycler.smoothScrollToPosition(tarhet == 0 ? 0 : mMessageAdapter.getItemCount());
                            current+=10;
                            findViewById(R.id.button_chatbox_send).setEnabled(true);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    loadMessages(t+1 ,tarhet);
                }
            }
        } , APIUrl.SERVER3+"complaints/index",params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
