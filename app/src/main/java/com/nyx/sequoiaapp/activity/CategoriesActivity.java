package com.nyx.sequoiaapp.activity;

import android.content.Intent;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CategoriesAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Category;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;

public class CategoriesActivity extends RootActivity {

    private FloatingActionButton fab_chat;
    private TextView tv_show_all;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        String title =getIntent().getStringExtra("name") ;
        if(title==null)title= "الكل" ;
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fab_chat = findViewById(R.id.open_chat);
        tv_show_all = findViewById(R.id.categories_show_all);
        try {
            Category c = SharedPrefManager.getInstance(CategoriesActivity.this).
                    getCategory(getIntent().getStringExtra("par"));
            if (!c.getParent_id().equals("0")){
                tv_show_all.setVisibility(View.GONE);
            }
        }catch (Exception e){}
        ((TextView)findViewById(R.id.title)).setText(title);
        ((TextView)findViewById(R.id.description)).setText("اختر الفئة التي تتوقع أن تعثر على طلبك ضمنها , ستجد أصنافا فرعية في كل دخول لك ضمن الفئات.");

        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoriesActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        tv_show_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(CategoriesActivity.this, CategoryProductsActivity.class);
                ii.putExtra("id", getIntent().getStringExtra("par"));
                ii.putExtra("title", getIntent().getStringExtra("name"));
                startActivity(ii);
            }
        });
        RecyclerView recyclerView5 = (RecyclerView) findViewById(R.id.recyclerview5);
        recyclerView5.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false
        ));
        ArrayList data = SharedPrefManager.getInstance(this).getCategories(getIntent().getStringExtra("par"));
        CategoriesAdapter adapter = new CategoriesAdapter(data ,this);
        recyclerView5.setAdapter(adapter);
        BaseFunctions.runAnimationHorizontal(recyclerView5,0,adapter);
    }
    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
