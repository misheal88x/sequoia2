package com.nyx.sequoiaapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.CitiesListAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;

import java.util.ArrayList;

public class SelectCityActivity extends RootActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        String title= "اختر المدينة" ;
        getSupportActionBar().setTitle("عرض الطلبيات");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView)findViewById(R.id.title)).setText(title);
        ((TextView)findViewById(R.id.description)).setText("اختر أحد المدن لإظهار الطلبيات ضمنها");
        RecyclerView recyclerView5 = (RecyclerView) findViewById(R.id.recyclerview5);
        recyclerView5.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false
        ));
        ArrayList data = SharedPrefManager.getInstance(this).getCities();
        CitiesListAdapter adapter = new CitiesListAdapter(data ,this);
        recyclerView5.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
