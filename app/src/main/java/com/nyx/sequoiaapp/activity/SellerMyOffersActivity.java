package com.nyx.sequoiaapp.activity;

import android.app.Activity;
import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.OffersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.SellerMyOffersAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.OfferObject;
import com.nyx.sequoiaapp.models.OffersResponse;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class SellerMyOffersActivity extends RootActivity {
    private RelativeLayout root;
    private LinearLayout title_layout;
    private androidx.appcompat.widget.Toolbar toolbar;
    private ProgressBar pb_first,pb_second;
    private RecyclerView rv_recycler;
    private List<OfferObject> list;
    private LinearLayoutManager layoutManager;
    private SellerMyOffersAdapter adapter;
    private ImageView empty_image;
    private TextView tv_desc;
    private NestedScrollView scrollView;
    private int currentPage = 1;
    private int per_page = 20;
    private EditText edt_search;
    private TextView tv_search;
    private int get_type = 0;
    private FloatingActionButton fab_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_my_offers);
        init_views();
        init_events();
        init_recycler();
        hide_elements();
        get_type = 1;
        callAPI(currentPage,0);
    }

    private void init_views(){
        //Root
        root = findViewById(R.id.seller_my_offers_layout);
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("عروضي");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
        //Nested Scroll View
        scrollView = findViewById(R.id.seller_my_offers_scroll);
        //Linear layout
        title_layout = findViewById(R.id.seller_my_offers_title_layout);
        //Progress bar
        pb_first = findViewById(R.id.my_offers_first_progress);
        pb_second = findViewById(R.id.my_offers_second_progress);
        //Recycler View
        rv_recycler = findViewById(R.id.my_offers_recycler);
        //Empty image
        empty_image = findViewById(R.id.no_items_found);
        //TextView
        tv_desc = findViewById(R.id.description);
        //EditText
        edt_search = findViewById(R.id.seller_my_offers_search);
        //TExtView
        tv_search = findViewById(R.id.search_txt);
    }

    private void init_events(){
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            currentPage++;
                            if (get_type == 1){
                                callAPI(currentPage,1);
                            }else {
                                callSearchAPI(currentPage,1,edt_search.getText().toString());
                            }

                        }
                    }
                }
            }
        });
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerMyOffersActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        edt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length()>0){
                    //btn_clear.setVisibility(View.VISIBLE);
                }else {
                    pb_first.setVisibility(View.VISIBLE);
                    list.clear();
                    rv_recycler.setAdapter(new SellerMyOffersAdapter(SellerMyOffersActivity.this,list));
                    currentPage = 1;
                    get_type = 1;
                    callAPI(currentPage,0);
                    //btn_clear.setVisibility(View.GONE);
                }
            }
        });
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_search.getText().toString().equals("")){
                    Toast.makeText(SellerMyOffersActivity.this, "الرجاء كتابة اسم المنتج للبحث", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    currentPage = 1;
                    list.clear();
                    rv_recycler.setAdapter(new SellerMyOffersAdapter(SellerMyOffersActivity.this,list));
                    pb_first.setVisibility(View.VISIBLE);
                    get_type = 2;
                    callSearchAPI(currentPage,0,edt_search.getText().toString());
                }
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(SellerMyOffersActivity.this, "الرجاء كتابة اسم المنتج للبحث", Toast.LENGTH_SHORT).show();
                        return false;
                    }else {
                        currentPage = 1;
                        list.clear();
                        rv_recycler.setAdapter(new SellerMyOffersAdapter(SellerMyOffersActivity.this,list));
                        pb_first.setVisibility(View.VISIBLE);
                        get_type = 2;
                        callSearchAPI(currentPage,0,edt_search.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void hide_elements(){
        rv_recycler.setVisibility(View.GONE);
    }
    private void show_elements(){
        rv_recycler.setVisibility(View.GONE);
        pb_first.setVisibility(View.GONE);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(SellerMyOffersActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        adapter = new SellerMyOffersAdapter(SellerMyOffersActivity.this,list);
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void callAPI(final int page , final int type){
        if (type == 1){
            pb_first.setVisibility(View.GONE);
            pb_second.setVisibility(View.VISIBLE);
        }else {
            pb_first.setVisibility(View.VISIBLE);
            pb_second.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerMyOffersActivity.this).getUser().getUser_id();
        OffersAPIsClass.getAllOffers(SellerMyOffersActivity.this, user_id, page, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if(json != null){
                    String json1 = new Gson().toJson(json);
                    OffersResponse success = new Gson().fromJson(json1,OffersResponse.class);
                    if (success.getData()!=null){
                        per_page = success.getPer_page();
                        if (success.getData().size()>0){
                            if (type == 0){
                                pb_first.setVisibility(View.GONE);
                                for (OfferObject oo : success.getData()){
                                    list.add(oo);
                                    adapter.notifyDataSetChanged();
                                }
                                rv_recycler.setVisibility(View.VISIBLE);
                                empty_image.setVisibility(View.GONE);
                                tv_desc.setVisibility(View.GONE);
                                if (type == 0){
                                    BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                }
                            }else {
                                pb_second.setVisibility(View.GONE);
                                for (OfferObject oo : success.getData()){
                                    list.add(oo);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }else {
                            if (type == 0){
                                pb_first.setVisibility(View.GONE);
                                empty_image.setVisibility(View.VISIBLE);
                                tv_desc.setVisibility(View.VISIBLE);
                            }else {
                                pb_second.setVisibility(View.GONE);
                            }
                        }
                    }else {
                        if (type == 0){
                            pb_first.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_second.setVisibility(View.GONE);
                        }
                    }
                }
                hideKeyboard(SellerMyOffersActivity.this);
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                pb_first.setVisibility(View.GONE);
                pb_second.setVisibility(View.GONE);
                hideKeyboard(SellerMyOffersActivity.this);
                Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                        .setAction("أعد المحاولة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callAPI(page,type);
                            }
                        }).show();
            }
        });
    }

    private void callSearchAPI(final int page , final int type,String words){
        if (type == 1){
            pb_first.setVisibility(View.GONE);
            pb_second.setVisibility(View.VISIBLE);
        } else {
            pb_first.setVisibility(View.VISIBLE);
            pb_second.setVisibility(View.GONE);
            empty_image.setVisibility(View.GONE);
        }
        String user_id = SharedPrefManager.getInstance(SellerMyOffersActivity.this).getUser().getUser_id();
        OffersAPIsClass.search(SellerMyOffersActivity.this, user_id,words, page, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if(json != null){
                    String json1 = new Gson().toJson(json);
                    OffersResponse success = new Gson().fromJson(json1,OffersResponse.class);
                    if (success.getData()!=null){
                        per_page = success.getPer_page();
                        if (success.getData().size()>0){
                            if (type == 0){
                                pb_first.setVisibility(View.GONE);
                                for (OfferObject oo : success.getData()){
                                    list.add(oo);
                                    adapter.notifyDataSetChanged();
                                }
                                rv_recycler.setVisibility(View.VISIBLE);
                                if (type == 0){
                                    BaseFunctions.runAnimationHorizontal(rv_recycler,0,adapter);
                                }
                            }else {
                                pb_second.setVisibility(View.GONE);
                                for (OfferObject oo : success.getData()){
                                    list.add(oo);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }else {
                            if (type == 0){
                                pb_first.setVisibility(View.GONE);
                                empty_image.setVisibility(View.VISIBLE);
                                tv_desc.setVisibility(View.VISIBLE);
                            }else {
                                pb_second.setVisibility(View.GONE);
                            }
                        }
                    }else {
                        if (type == 0){
                            pb_first.setVisibility(View.GONE);
                            empty_image.setVisibility(View.VISIBLE);
                            tv_desc.setVisibility(View.VISIBLE);
                        }else {
                            pb_second.setVisibility(View.GONE);
                        }
                    }
                }
                hideKeyboard(SellerMyOffersActivity.this);
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                pb_first.setVisibility(View.GONE);
                pb_second.setVisibility(View.GONE);
                hideKeyboard(SellerMyOffersActivity.this);
                Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                        .setAction("أعد المحاولة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callAPI(page,type);
                            }
                        }).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
