package com.nyx.sequoiaapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iceteck.silicompressorr.SiliCompressor;
import com.nyx.sequoiaapp.APIClass.ProductsAPIsClass;
import com.nyx.sequoiaapp.Dialogs.CropImageDialog;
import com.nyx.sequoiaapp.Dialogs.CustomColorPicker;
import com.nyx.sequoiaapp.Interfaces.IColor;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.Interfaces.ISelectCrop;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.AddSellerProductHorizontalCategoriesAdapter;
import com.nyx.sequoiaapp.adapters.AddSellerProductHorizontalColorsAdapter;
import com.nyx.sequoiaapp.adapters.AddSellerProductHorizontalMeasuresAdapter;
import com.nyx.sequoiaapp.adapters.AddSellerProductVerticalCategoriesAdapter;
import com.nyx.sequoiaapp.adapters.AddSellerProductVerticalMeasuresAdapter;
import com.nyx.sequoiaapp.adapters.AddSellerProductVerticalSizesAdapter;
import com.nyx.sequoiaapp.adapters.SellerAddProductImagesAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Category;
import com.nyx.sequoiaapp.models.ColorObject;
import com.nyx.sequoiaapp.models.FeaturesObject;
import com.nyx.sequoiaapp.models.NewCategoryObject;
import com.nyx.sequoiaapp.models.Product;
import com.nyx.sequoiaapp.models.SellerProductImageObject;
import com.nyx.sequoiaapp.models.ShipDeliveryObejct;
import com.nyx.sequoiaapp.models.SizeObject;
import com.nyx.sequoiaapp.other.ProductFeaturesManager;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddSellerProductActivity extends RootActivity implements IPickResult {
    private RelativeLayout root;
    private LinearLayout sub_root;
    private ProgressBar pb_loading;
    private EditText edt_name,edt_desc,edt_quantity,edt_weight,edt_price;
    private Button btn_add_category,btn_add_sub_category,btn_add_color,btn_add_size,btn_add_measure;
    private TextView tv_size;
    //private RadioButton rb_deleiver_yes,rb_deleiver_no;
    private RecyclerView rv_main_categories,rv_sub_categories,rv_colors,rv_measures;
    private Button btn_create;
    private List<Category> selected_main_categories,selected_sub_categories;
    private List<Category> main_categories_list,sub_categories_list;
    private List<ColorObject> main_colors_list;
    private List<ColorObject> selected_colors;
    private List<String> selected_colors_string;
    private List<String> main_measures_list;
    private List<String> selected_measures_list;
    private List<SizeObject> main_sizes_list;
    private String selected_size = "";
    private AddSellerProductHorizontalCategoriesAdapter horizontal_main_categories_adapter,horizontal_sub_categories_adapter;
    private AddSellerProductHorizontalColorsAdapter horizontal_colors_adapter;
    private AddSellerProductHorizontalMeasuresAdapter horizontal_measures_adapter;
    private LinearLayoutManager horizontal_main_categories_layout_manager,horizontal_sub_categories_layout_manager;
    private LinearLayoutManager horizontal_colors_layout_manager,horizontal_measures_layout_manager;
    private Button btn_add_image;
    private RecyclerView rv_images;
    private List<SellerProductImageObject> listOfImages;
    private List<SellerProductImageObject> listOfImagesString;
    private SellerAddProductImagesAdapter imagesAdapter;
    private LinearLayoutManager imagesLayoutManager;
    private FloatingActionButton fab_chat;
    //private Spinner brands_spinner;
    //private ArrayList source_brands;
    private int deveiver_type = 1;
    private int image_type = 0;
    //private String selected_brand_id = "";
    private Intent myIntent;
    private String activity_type = "";
    //From Details API
    private int product_id = 0;
    private int commission_fixed = 0;
    private int commission_percent = 0;
    private int status = 0;
    private int store_id = 0;
    private int featured = 0;
    private int defaultColor = 0;

    private List<ShipDeliveryObejct> ship_delivery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_seller_product);
        init_views();
        activity_type = myIntent.getStringExtra("activity_type");
        init_events();
        init_main_categories_recycler();
        init_sub_categories_recycler();
        init_colors_recycler();
        init_measures_recycler();
        init_images_recycler();

        if (activity_type.equals("edit")){
            getSupportActionBar().setTitle("تحديث بيانات المنتج");
            btn_create.setText("تحديث البيانات");
            sub_root.setVisibility(View.GONE);
            callDetailsAPI();
        }
        defaultColor = getResources().getColor(R.color.white);
    }

    private void init_views() {
        //Root
        root = findViewById(R.id.add_seller_product_layout);
        //Sub-Root
        sub_root = findViewById(R.id.add_seller_product_sub_layout);
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("أضف منتج");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Progress bar
        pb_loading = findViewById(R.id.add_seller_product_loading);
        //EditText
        edt_name = findViewById(R.id.add_seller_product_name_edt);
        edt_quantity = findViewById(R.id.add_seller_product_quantity_edt);
        edt_desc = findViewById(R.id.add_seller_product_desc_edt);
        edt_weight = findViewById(R.id.add_seller_product_weight_edt);
        edt_price = findViewById(R.id.add_seller_product_price_edt);
        //Radio Button
        //rb_deleiver_yes = findViewById(R.id.add_seller_product_deleiver_yes);
        //rb_deleiver_no = findViewById(R.id.add_seller_product_deleiver_no);
        //Button
        btn_add_category = findViewById(R.id.add_seller_product_pick_category);
        btn_add_sub_category = findViewById(R.id.add_seller_product_pick_sub_category);
        btn_add_color = findViewById(R.id.add_seller_product_pick_color);
        btn_add_measure = findViewById(R.id.add_seller_product_pick_measures);
        btn_add_size = findViewById(R.id.add_seller_product_pick_size);
        btn_create = findViewById(R.id.add_seller_product_create);
        //TextView
        tv_size = findViewById(R.id.add_seller_product_size_txt);
        //RecyclerView
        rv_main_categories = findViewById(R.id.add_seller_product_main_categories_recycler);
        rv_sub_categories = findViewById(R.id.add_seller_product_sub_categories_recycler);
        rv_colors = findViewById(R.id.add_seller_product_colors_recycler);
        rv_measures = findViewById(R.id.add_seller_product_measures_recycler);
        rv_images = findViewById(R.id.add_seller_product_images_recycler);
        //Button
        btn_add_image = findViewById(R.id.add_seller_product_pick_image);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
        //Spinner
        /*
        brands_spinner = findViewById(R.id.brand_spinner);
        final ArrayList brands = SharedPrefManager.getInstance(this).getBrands();
        source_brands = new ArrayList();
        if (brands.size() > 0){
            for (int i = 0; i < brands.size() ; i++) {
                Brand c = (Brand) brands.get(i);
                source_brands.add(c);
            }
        }
        BrandsSpinnerAdapter adapter = new BrandsSpinnerAdapter(this, source_brands);
        brands_spinner.setAdapter(adapter);
        **/
        //Intent
        myIntent = getIntent();
    }
    private void init_events(){
        /*
        brands_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                Brand c = (Brand)source_brands.get(position);
                selected_brand_id = c.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        **/
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddSellerProductActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        btn_add_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(AddSellerProductActivity.this);
                View view=getLayoutInflater().inflate(R.layout.dialog_add_seller_product_categories,null);
                main_categories_list = new ArrayList<>();
                ArrayList x = SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories("0");
                if (x.size()>0){
                    for (int i = 0; i <x.size() ; i++) {
                        Category c = (Category) x.get(i);
                        main_categories_list.add(c);
                    }
                }
                TextView tv_ok = view.findViewById(R.id.add_seller_product_categories_dialog_ok);
                RecyclerView recycler = view.findViewById(R.id.add_seller_product_categories_dialog_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                LinearLayoutManager layoutManager = new LinearLayoutManager(AddSellerProductActivity.this,
                        LinearLayoutManager.VERTICAL,
                        false){
                    @Override
                    public boolean canScrollHorizontally() {
                        return false;
                    }

                    @Override
                    public boolean canScrollVertically() {
                        return false;
                    }
                };
                final AddSellerProductVerticalCategoriesAdapter adapter = new AddSellerProductVerticalCategoriesAdapter(AddSellerProductActivity.this, main_categories_list,selected_main_categories);
                recycler.setLayoutManager(layoutManager);
                recycler.setAdapter(adapter);
                tv_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapter.get_checked().size()>0){
                            selected_main_categories.clear();
                            rv_main_categories.setAdapter(new AddSellerProductHorizontalCategoriesAdapter(AddSellerProductActivity.this,
                                    selected_main_categories, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            for (Category c : adapter.get_checked()){
                                selected_main_categories.add(c);
                                horizontal_main_categories_adapter.notifyDataSetChanged();
                            }
                        }else {
                            Toast.makeText(AddSellerProductActivity.this, "لم تقم باختيار اي صنف", Toast.LENGTH_SHORT).show();
                            selected_main_categories.clear();
                            rv_main_categories.setAdapter(new AddSellerProductHorizontalCategoriesAdapter(AddSellerProductActivity.this,
                                    selected_main_categories, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_add_sub_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(AddSellerProductActivity.this);
                View view=getLayoutInflater().inflate(R.layout.dialog_add_seller_product_categories,null);
                sub_categories_list = new ArrayList<>();
                if (selected_main_categories.size() > 0){
                    for (Category c : selected_main_categories){
                        ArrayList x = SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(c.getId());
                        if (x.size()>0){
                            for (int i = 0; i < x.size(); i++) {
                                Category cc =(Category) x.get(i);
                                sub_categories_list.add(cc);
                            }
                        }
                    }
                }
                TextView tv_ok = view.findViewById(R.id.add_seller_product_categories_dialog_ok);
                RecyclerView recycler = view.findViewById(R.id.add_seller_product_categories_dialog_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                LinearLayoutManager layoutManager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.VERTICAL,false);
                final AddSellerProductVerticalCategoriesAdapter adapter = new AddSellerProductVerticalCategoriesAdapter(AddSellerProductActivity.this, sub_categories_list,selected_sub_categories);
                recycler.setLayoutManager(layoutManager);
                recycler.setAdapter(adapter);
                tv_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapter.get_checked().size()>0){
                            selected_sub_categories.clear();
                            rv_sub_categories.setAdapter(new AddSellerProductHorizontalCategoriesAdapter(AddSellerProductActivity.this,
                                    selected_sub_categories, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            for (Category c : adapter.get_checked()){
                                selected_sub_categories.add(c);
                                horizontal_sub_categories_adapter.notifyDataSetChanged();
                            }
                        }else {
                            selected_sub_categories.clear();
                            rv_sub_categories.setAdapter(new AddSellerProductHorizontalCategoriesAdapter(AddSellerProductActivity.this,
                                    selected_sub_categories, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_add_measure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(AddSellerProductActivity.this);
                View view=getLayoutInflater().inflate(R.layout.dialog_add_seller_product_measures,null);
                main_measures_list = new ArrayList<>();
                if (ProductFeaturesManager.generateMeasures().size()>0){
                    for (int i = 0; i <ProductFeaturesManager.generateMeasures().size() ; i++) {
                        String c = ProductFeaturesManager.generateMeasures().get(i);
                        main_measures_list.add(c);
                    }
                }
                TextView tv_ok = view.findViewById(R.id.add_seller_product_measures_dialog_ok);
                RecyclerView recycler = view.findViewById(R.id.add_seller_product_measures_dialog_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                LinearLayoutManager layoutManager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.VERTICAL,false);
                final AddSellerProductVerticalMeasuresAdapter adapter = new AddSellerProductVerticalMeasuresAdapter(AddSellerProductActivity.this, main_measures_list,selected_measures_list);
                recycler.setLayoutManager(layoutManager);
                recycler.setAdapter(adapter);
                tv_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapter.get_checked().size()>0){
                            selected_measures_list.clear();
                            rv_measures.setAdapter(new AddSellerProductHorizontalMeasuresAdapter(AddSellerProductActivity.this,
                                    selected_measures_list, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            for (String c : adapter.get_checked()){
                                selected_measures_list.add(c);
                                horizontal_measures_adapter.notifyDataSetChanged();
                            }
                        }else {
                            Toast.makeText(AddSellerProductActivity.this, "لم تقم باختيار اي قياس", Toast.LENGTH_SHORT).show();
                            selected_measures_list.clear();
                            rv_measures.setAdapter(new AddSellerProductHorizontalMeasuresAdapter(AddSellerProductActivity.this,
                                    selected_measures_list, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_add_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CustomColorPicker dialog = new CustomColorPicker(AddSellerProductActivity.this, new IColor() {
                    @Override
                    public void onColorSelected(int color) {
                        defaultColor = color;
                        String hexColor = String.format("#%06X", (0xFFFFFF & color));
                        ColorObject co = new ColorObject();
                        co.setValue(hexColor.substring(1,hexColor.length()));
                        selected_colors.add(co);
                        selected_colors_string.add(co.getValue());
                        horizontal_colors_adapter.notifyDataSetChanged();
                    }
                });
                dialog.show();
                /*
                new ColorPickerPopup.Builder(AddSellerProductActivity.this)
                        .initialColor(defaultColor) // Set initial color
                        .enableBrightness(true) // Enable brightness slider or not
                        .enableAlpha(false) // Enable alpha slider or not
                        .okTitle("اختيار")
                        .cancelTitle("إلغاء")
                        .showIndicator(true)
                        .showValue(false)
                        .build()
                        .show(v, new ColorPickerPopup.ColorPickerObserver() {
                            @Override
                            public void onColorPicked(int color) {
                                defaultColor = color;
                                String hexColor = String.format("#%06X", (0xFFFFFF & color));
                                ColorObject co = new ColorObject();
                                co.setValue(hexColor.substring(1,hexColor.length()));
                                selected_colors.add(co);
                                selected_colors_string.add(co.getValue());
                                horizontal_colors_adapter.notifyDataSetChanged();
                            }
                        });
                        **/
            }
        });
        btn_add_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(AddSellerProductActivity.this);
                View view=getLayoutInflater().inflate(R.layout.dialog_add_seller_product_sizes,null);
                main_sizes_list = new ArrayList<>();
                for (SizeObject so : ProductFeaturesManager.generateSizes()){
                    main_sizes_list.add(so);
                }
                RecyclerView recycler = view.findViewById(R.id.add_seller_product_sizes_dialog_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                LinearLayoutManager layoutManager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.VERTICAL,false);
                AddSellerProductVerticalSizesAdapter adapter = new AddSellerProductVerticalSizesAdapter(AddSellerProductActivity.this, main_sizes_list, new IMove() {
                    @Override
                    public void move(int position) {
                        selected_size = main_sizes_list.get(position).getValue();
                        tv_size.setText(selected_size);
                        dialog.cancel();
                    }
                });
                recycler.setLayoutManager(layoutManager);
                recycler.setAdapter(adapter);
                dialog.show();
            }
        });
        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listOfImages.size() < 8){
                    ActivityCompat.requestPermissions(AddSellerProductActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA},
                            1);
                }else {
                    Toast.makeText(AddSellerProductActivity.this, "لا يمكنك إضافة أكثر من ثمان صور", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
        /*
        rb_deleiver_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    deveiver_type = 1;
                }
            }
        });
        rb_deleiver_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                deveiver_type = 0;
            }
        });
**/
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Name
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد اسم المنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Categories
                if (selected_main_categories.size() == 0){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد صنف رئيسي واحد على الأقل", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Description
                if (edt_desc.getText().toString().equals("")){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد وصف المنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Quantity
                if (edt_quantity.getText().toString().equals("")){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد الكمية المتوفرة من المنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Colors
                /*
                if (selected_colors.size() == 0){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد لون واحد على الأقل من الألوان المتوفرة", Toast.LENGTH_SHORT).show();
                    return;
                }
                **/
                //Size
                //if (selected_size.equals("")){
               //     Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد حجم المنتج", Toast.LENGTH_SHORT).show();
                //    return;
               // }
                /*
                //Weight
                if (edt_weight.getText().toString().equals("")){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد وزن المنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                **/
                //Measure
                /*
                if (selected_measures_list.size()==0){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد قياسات للمنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                **/
                //Images
                if (listOfImages.size() == 0){
                        Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد صورة واحدة على الأقل للمنتج", Toast.LENGTH_SHORT).show();
                        return;
                }
                /*
                //Brand
                if (selected_brand_id.equals("")){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد الشركة التي يتبع لها المنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                **/
                //Price
                if (edt_price.getText().toString().equals("")){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد سعر المنتج", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Bounty
                /*
                if (deveiver_type == -1){
                    Toast.makeText(AddSellerProductActivity.this, "يجب عليك تحديد خيار الشحن و التوصيل", Toast.LENGTH_SHORT).show();
                    return;
                }
                **/
                if (activity_type.equals("add")){
                    callCreateProductAPI();
                }else {
                    callUpdateProductAPI();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                PickImageDialog.build(new PickSetup()
                        .setTitle("اختر..")
                        .setCancelText("إلغاء")
                        .setCameraButtonText("الكاميرا")
                        .setGalleryButtonText("المعرض"))
                        .show(AddSellerProductActivity.this);
                return;
            }
        }
    }

    private void init_main_categories_recycler(){
        selected_main_categories = new ArrayList<>();
        horizontal_main_categories_adapter = new AddSellerProductHorizontalCategoriesAdapter(AddSellerProductActivity.this, selected_main_categories, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        horizontal_main_categories_layout_manager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_main_categories.setLayoutManager(horizontal_main_categories_layout_manager);
        rv_main_categories.setAdapter(horizontal_main_categories_adapter);
    }

    private void init_sub_categories_recycler(){
        selected_sub_categories = new ArrayList<>();
        horizontal_sub_categories_adapter = new AddSellerProductHorizontalCategoriesAdapter(AddSellerProductActivity.this, selected_sub_categories, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        horizontal_sub_categories_layout_manager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_sub_categories.setLayoutManager(horizontal_sub_categories_layout_manager);
        rv_sub_categories.setAdapter(horizontal_sub_categories_adapter);
    }

    private void init_colors_recycler(){
        selected_colors = new ArrayList<>();
        selected_colors_string = new ArrayList<>();

        horizontal_colors_adapter = new AddSellerProductHorizontalColorsAdapter(AddSellerProductActivity.this, selected_colors, new IMove() {
            @Override
            public void move(int position) {
                selected_colors.remove(position);
                selected_colors_string.remove(position);
                horizontal_colors_adapter.notifyDataSetChanged();
            }
        });
        horizontal_colors_layout_manager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_colors.setLayoutManager(horizontal_colors_layout_manager);
        rv_colors.setAdapter(horizontal_colors_adapter);
    }
    private void init_measures_recycler(){
        selected_measures_list = new ArrayList<>();
        horizontal_measures_adapter = new AddSellerProductHorizontalMeasuresAdapter(AddSellerProductActivity.this, selected_measures_list, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        horizontal_measures_layout_manager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_measures.setLayoutManager(horizontal_measures_layout_manager);
        rv_measures.setAdapter(horizontal_measures_adapter);
    }
    private void init_images_recycler(){
        listOfImages = new ArrayList<>();
        imagesAdapter = new SellerAddProductImagesAdapter(AddSellerProductActivity.this, listOfImages,activity_type, new IMove() {
            @Override
            public void move(int position) {
                if (activity_type.equals("edit")){
                    if (listOfImages.get(position).getUrl()!=null){
                        if (!listOfImages.get(position).getUrl().equals("")){
                            callDeleteImageAPI(String.valueOf(getIntent().getIntExtra("product_id",0)),
                                    listOfImages.get(position).getUrl(),position);
                        }else {
                            listOfImages.remove(position);
                            imagesAdapter.notifyDataSetChanged();
                            Toast.makeText(AddSellerProductActivity.this, "تم حذف الصورة بنجاح", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        listOfImages.remove(position);
                        imagesAdapter.notifyDataSetChanged();
                        Toast.makeText(AddSellerProductActivity.this, "تم حذف الصورة بنجاح", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    listOfImages.remove(position);
                    imagesAdapter.notifyDataSetChanged();
                    Toast.makeText(AddSellerProductActivity.this, "تم حذف الصورة بنجاح", Toast.LENGTH_SHORT).show();
                }
            }
        });
        imagesLayoutManager = new LinearLayoutManager(AddSellerProductActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_images.setLayoutManager(imagesLayoutManager);
        rv_images.setAdapter(imagesAdapter);
    }


    @Override
    public void onPickResult(PickResult r) {
        if (listOfImages.size()<=8){
            final Uri pickedImage = r.getUri();
            final String destinationFileName = System.currentTimeMillis()+"SampleCropImage.jpg";
            CropImageDialog dialog = new CropImageDialog(AddSellerProductActivity.this, new ISelectCrop() {
                @Override
                public void onVerticalSelected() {
                    UCrop.of(pickedImage,
                            Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                            .withAspectRatio(9, 16)
                            .start(AddSellerProductActivity.this);
                }

                @Override
                public void onHorizontalSelected() {
                    UCrop.of(pickedImage,
                            Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                            .withAspectRatio(16, 9)
                            .start(AddSellerProductActivity.this);
                }

                @Override
                public void onSquareSelected() {
                    UCrop.of(pickedImage,
                            Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                            .withAspectRatio(3, 3)
                            .start(AddSellerProductActivity.this);
                }
            });
            dialog.show();

        }else {
            Toast.makeText(this, "لا يمكنك إضافة أكثر من ثمان صور", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP: {
                    if (data != null) {
                        final Uri resultUri = UCrop.getOutput(data);
                        try {
                            File f = new File(resultUri.getPath());
                            if (f.exists()){
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                                String fileName = "JPEG_" + timeStamp + "SampleCropImage.jpg";
                                File file = new File("/sdcard/"+fileName);
                                String filePath = SiliCompressor.with(AddSellerProductActivity.this).compress(resultUri.getPath(), file,true);
                                File ff = new File(filePath);
                                if(ff.exists()) {
                                    SellerProductImageObject spio = new SellerProductImageObject();
                                    spio.setImagePath(filePath);
                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                    Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
                                    spio.setBitmap(bitmap);
                                    listOfImages.add(spio);
                                    imagesAdapter.notifyDataSetChanged();
                                }
                                //compressImage(f);
                            }else {
                                Toast.makeText(this, "الرجاء اختيار صورة اخرى", Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){

                            Toast.makeText(this, "error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            }
        }

    }

    private void callCreateProductAPI(){
        //User ID
        String user_id = SharedPrefManager.getInstance(AddSellerProductActivity.this).getUser().getUser_id();
        //Main & Sub-Categories
        List<Integer> list_cats = new ArrayList<>() ;
        for (int i = 0; i < selected_main_categories.size() ; i++) {
            int main_id = Integer.valueOf(selected_main_categories.get(i).getId());
            list_cats.add(main_id);
            if (SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(String.valueOf(main_id)).size()>0){
                for (int j = 0; j < SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(String.valueOf(main_id)).size(); j++) {
                    Category c = (Category) SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(String.valueOf(main_id)).get(j);
                    for (Category cc : selected_sub_categories){
                        if (cc.getId().equals(c.getId())){
                            list_cats.add(Integer.valueOf(c.getId()));
                            break;
                        }
                    }
                }
            }
        }
        Integer[] array_cats = new Integer[list_cats.size()];
        List<Integer> temp_cat = new ArrayList<>();
        for (Integer x : list_cats){
            temp_cat.add(0,x);
        }
        for (int i = 0; i <temp_cat.size(); i++) {
            array_cats[i] = temp_cat.get(i);
        }
        String json_cats = new Gson().toJson(array_cats);
        Log.i("hgbf", "callCreateProductAPI: "+json_cats);
        //Features
        List<String> list_sizes = new ArrayList<>();
        if (!selected_size.equals("")){
            list_sizes.add(selected_size);
        }
        FeaturesObject fo = new FeaturesObject();
        fo.setColors(selected_colors_string);
        fo.setSizes(list_sizes);
        fo.setMeasure(selected_measures_list);
        fo.setWeight(edt_weight.getText().toString());
        String features_json = new Gson().toJson(fo);
        //Images
        List<String> list_images_paths = new ArrayList<>();
        for (SellerProductImageObject spio : listOfImages){
            Log.i("pathsss", "callCreateProductAPI: "+spio.getImagePath());
            list_images_paths.add(spio.getImagePath());
        }
        //Call the api

        ProductsAPIsClass.addProduct(AddSellerProductActivity.this,
                user_id,
                edt_name.getText().toString(),
                edt_desc.getText().toString(),
                edt_price.getText().toString(),
                edt_price.getText().toString(),
                "0",
                "0",
                "1",
                edt_quantity.getText().toString(),
                features_json
                , json_cats,
                user_id,
                "0",
                "0",
                String.valueOf(deveiver_type),
                list_images_paths,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(AddSellerProductActivity.this, "تم إضافة المنتج بنجاح", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callCreateProductAPI();
                                    }
                                }).show();
                    }
                }
        );

    }

    private void callDetailsAPI(){
        pb_loading.setVisibility(View.VISIBLE);
        String user_id = SharedPrefManager.getInstance(AddSellerProductActivity.this).getUser().getUser_id();
        ProductsAPIsClass.get_product_info(AddSellerProductActivity.this, user_id, String.valueOf(myIntent.getIntExtra("product_id", 0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            Product success = new Gson().fromJson(json1,Product.class);
                            //Id
                            product_id = success.getId();
                            //Name
                            if (success.getName()!= null){
                                edt_name.setText(success.getName());
                            }
                            //Categories
                            if (success.getCategories()!=null){
                                if (success.getCategories().size()>0){
                                    List<NewCategoryObject> temp_cat_list = new ArrayList<>();
                                    for (NewCategoryObject n : success.getCategories()){
                                        temp_cat_list.add(0,n);
                                    }
                                    ArrayList base_cats = SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories("0");
                                    List<NewCategoryObject> api_cats = temp_cat_list;
                                    for (int i = 0; i < base_cats.size(); i++) {
                                        Category c = (Category) base_cats.get(i);
                                        for (int j = 0; j <api_cats.size() ; j++) {
                                            if (Integer.valueOf(c.getId()) == api_cats.get(j).getId()){
                                                selected_main_categories.add(c);
                                                api_cats.remove(j);
                                                horizontal_main_categories_adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                    if (api_cats.size()>0){
                                        SharedPrefManager spm = SharedPrefManager.getInstance(AddSellerProductActivity.this);
                                        for (NewCategoryObject inte : api_cats){
                                            selected_sub_categories.add(spm.getCategory(String.valueOf(inte.getId())));
                                            horizontal_sub_categories_adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                            //Description
                            if (success.getDescription()!=null){
                                edt_desc.setText(success.getDescription());
                            }
                            //Quantity
                            edt_quantity.setText(String.valueOf(success.getQty_in_stock()));
                            //Features
                            if (success.getFeatures()!=null){
                                FeaturesObject fo  = success.getFeatures();
                                //Ship Delivery
                                ship_delivery = new ArrayList<>();
                                if (fo.getShip_delivery()!=null){
                                    if (fo.getShip_delivery().size()>0){
                                        for (ShipDeliveryObejct s : fo.getShip_delivery()){
                                            ship_delivery.add(s);
                                        }
                                    }
                                }
                                //Colors
                                if (fo.getColors()!=null){
                                    try{
                                        String json3 = new Gson().toJson(fo.getColors());
                                        String[] colors = new Gson().fromJson(json3,String[].class);
                                        if (colors.length>0){
                                            for (String s : colors){
                                                ColorObject co = new ColorObject();
                                                co.setValue(s);
                                                selected_colors.add(co);
                                                selected_colors_string.add(s);
                                                horizontal_colors_adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }catch (Exception e){}
                                }
                                //Sizes
                                if (fo.getSizes()!=null){
                                    try {
                                        String json2 = new Gson().toJson(fo.getSizes());
                                        String[] sizes = new Gson().fromJson(json2,String[].class);
                                        if (sizes.length>0){
                                            for (String s : sizes){
                                                for (SizeObject so : ProductFeaturesManager.generateSizes()){
                                                    if (so.getValue().equals(s)){
                                                        selected_size = so.getValue();
                                                        tv_size.setText(s);
                                                    }
                                                }
                                            }
                                        }
                                    }catch (Exception e){}
                                }
                                //Weight
                                if (fo.getWeight()!=null){
                                    edt_weight.setText(fo.getWeight());
                                }
                                //Measure
                                if (fo.getMeasure()!=null){
                                    try{
                                        String json3 = new Gson().toJson(fo.getMeasure());
                                        String[] measures = new Gson().fromJson(json3,String[].class);
                                        if (measures.length > 0){
                                            for (String s : measures){
                                                selected_measures_list.add(s);
                                                horizontal_measures_adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }catch (Exception e){}
                                }
                            }
                            //Brand_id
                            /*
                            selected_brand_id = String.valueOf(success.getBrand_id());
                            for (int i = 0; i < source_brands.size() ; i++) {
                                Brand b = (Brand) source_brands.get(i);
                                if (b.getId().equals(selected_brand_id)){
                                    brands_spinner.setSelection(i);
                                    break;
                                }
                            }
                            **/
                            //Price & Prime Price
                            edt_price.setText(String.valueOf(Math.round(success.getPrice())));
                            //Bounty
                            /*
                            if (success.getBounty() == 0){
                                rb_deleiver_no.setChecked(true);
                                deveiver_type = 0;
                            }else {
                                rb_deleiver_yes.setChecked(true);
                                deveiver_type = 1;
                            }
                            **/
                            //Commission
                            commission_fixed = Math.round(success.getCommission_fixed());
                            commission_percent = Math.round(success.getCommission_percent());
                            //Status
                            status = success.getStatus();
                            //Store id
                            store_id = success.getStore_id();
                            //Featured
                            featured = success.getFeatured();
                            sub_root.setVisibility(View.VISIBLE);
                            //Images
                            listOfImagesString = new ArrayList<>();
                            if (success.getImages()!=null){
                                if (success.getImages().getImages()!=null){
                                    if (success.getImages().getImages().size()>0){
                                        for (String s : success.getImages().getImages()){
                                            SellerProductImageObject spio = new SellerProductImageObject();
                                            spio.setUrl(s);
                                            listOfImages.add(spio);
                                            imagesAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }
                        pb_loading.setVisibility(View.GONE);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_loading.setVisibility(View.GONE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDetailsAPI();
                                    }
                                }).show();
                    }
                });
    }

    private void callUpdateProductAPI(){
        //User ID
        String user_id = SharedPrefManager.getInstance(AddSellerProductActivity.this).getUser().getUser_id();
        //Main & Sub-Categories
        List<Integer> list_cats = new ArrayList<>() ;
        for (int i = 0; i < selected_main_categories.size() ; i++) {
            int main_id = Integer.valueOf(selected_main_categories.get(i).getId());
            list_cats.add(main_id);
            if (SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(String.valueOf(main_id)).size()>0){
                for (int j = 0; j < SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(String.valueOf(main_id)).size(); j++) {
                    Category c = (Category) SharedPrefManager.getInstance(AddSellerProductActivity.this).getCategories(String.valueOf(main_id)).get(j);
                    for (Category cc : selected_sub_categories){
                        if (cc.getId().equals(c.getId())){
                            list_cats.add(Integer.valueOf(c.getId()));
                            break;
                        }
                    }
                }
            }
        }
        Integer[] array_cats = new Integer[list_cats.size()];
        List<Integer> temp_cat = new ArrayList<>();
        for (Integer x : list_cats){
            temp_cat.add(0,x);
        }
        for (int i = 0; i <temp_cat.size(); i++) {
            array_cats[i] = temp_cat.get(i);
        }
        String json_cats = new Gson().toJson(array_cats);
        //Features
        List<String> list_sizes = new ArrayList<>();
        list_sizes.add(selected_size);
        FeaturesObject fo = new FeaturesObject();
        fo.setColors(selected_colors_string);
        fo.setSizes(list_sizes);
        fo.setMeasure(selected_measures_list);
        fo.setWeight(edt_weight.getText().toString());
        fo.setShip_delivery(ship_delivery);
        String features_json = new Gson().toJson(fo);
        //Images
        List<String> list_images_paths = new ArrayList<>();
        //todo
        if (listOfImages.size()>0){
            for (SellerProductImageObject spio : listOfImages){
                if (spio.getImagePath()!=null){
                    if (!spio.getImagePath().equals("")){
                        list_images_paths.add(spio.getImagePath());
                    }
                }
            }
        }
        /*
        if (listOfImagesString.size()>0){
            for (SellerProductImageObject spio : listOfImagesString){
                list_images_paths.add(spio.getImagePath());
            }
        }
        **/
        //Call the api
        ProductsAPIsClass.updateProduct(AddSellerProductActivity.this,
                user_id,
                String.valueOf(product_id),
                edt_name.getText().toString(),
                edt_desc.getText().toString(),
                edt_price.getText().toString(),
                edt_price.getText().toString(),
                String.valueOf(commission_fixed),
                String.valueOf(commission_percent),
                String.valueOf(status),
                edt_quantity.getText().toString(),
                features_json
                , json_cats,
                String.valueOf(store_id),
                String.valueOf(featured),
                "0",
                String.valueOf(deveiver_type),
                list_images_paths,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(AddSellerProductActivity.this, "تم تحديث المنتج بنجاح", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callUpdateProductAPI();
                                    }
                                }).show();
                    }
                }
        );
    }

    private void callDeleteImageAPI(final String product_id, final String image_name, final int position){
        //final String new_name = image_name.substring(0, image_name.lastIndexOf("."));
        ProductsAPIsClass.deleteImage(
                AddSellerProductActivity.this,
                SharedPrefManager.getInstance(AddSellerProductActivity.this).getUser().getUser_id(),
                product_id,
                image_name,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        listOfImages.remove(position);
                        imagesAdapter.notifyDataSetChanged();
                        Toast.makeText(AddSellerProductActivity.this, "تم حذف الصورة بنجاح", Toast.LENGTH_SHORT).show();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDeleteImageAPI(product_id, image_name,position);
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
