package com.nyx.sequoiaapp.activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.View;

import com.nyx.sequoiaapp.adapters.FavouritesAdapter;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.other.BaseFunctions;

public class FavouritesActivity extends ViewProductsActivity {

    @Override
    void setTitle() {
        getSupportActionBar().setTitle("مفضلتي");
    }

    @Override
    void fetchData() {

        loading.setVisibility(View.GONE);
        this.items = SavedCacheUtils.getFavourites(this);
        if (this.items.size() > 0) {
            adapter = new FavouritesAdapter(this.items, this, new PostAction() {
                @Override
                public void doTask() {
                    update();
                }

                @Override
                public void doTask(String s) {

                }
            });
            GridLayoutManager lm = new GridLayoutManager(this,
                    2) {
                @Override
                public boolean supportsPredictiveItemAnimations() {
                    return true;
                }
            };
            mainRecylceView.setLayoutManager(lm);
            BaseFunctions.runAnimationVertical(mainRecylceView,0,adapter);
            mainRecylceView.setItemAnimator(new DefaultItemAnimator());
            mainRecylceView.setAdapter(adapter);
            update();
            mainScrollView.scrollTo(0, 0);

        } else {
            update();
        }
    }


    void update() {
        if (this.items.size() > 0) {
            setWindowTitle("مفضلتي");
            setDescription("ادخل الى اي منتج و اضغط على زر المفضلة لعرض المنتج لاحقا ضمن هذه القائمة حتى ان لم يكن لديك اتصال بالشبكة ..");

        } else {
            setWindowTitle("مفضلتك فارغة");
            setDescription("ادخل الى اي منتج و اضغط على زر المفضلة لعرض المنتج لاحقا ضمن هذه القائمة حتى ان لم يكن لديك اتصال بالشبكة ..");
            this.noItemsFound.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 100) fab.show();
        else
            fab.hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
