package com.nyx.sequoiaapp.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.content.Intent;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.UsersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.app.Config;
import com.nyx.sequoiaapp.fragment.*;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.LoginObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends RootActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private CircleImageView imgProfile;
    private ImageView editImageProfile;
    private ProgressBar editImageProgress;
    private TextView txtName;
    private FloatingActionButton fab;
    private TextView loginbtn,signupbtn;
    //private TextView addStore;
    public static ConnectivityManager connectivityManager;
    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private static final int PICK_FROM_GALLERY = 14436;
    private String imagePath;



    // urls to load navigation header background image
    // and profile image

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    public static String CURRENT_TAG = TAG_HOME;



    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;



    void refreshFBToken(final  int y , final String yuo){
        HashMap<String  ,String> params=new HashMap<String, String>();
        params.put("user_id" ,SharedPrefManager.getInstance(this).getUser().getUser_id());
        params.put("user_token" ,SharedPrefManager.getInstance(this).getUser().getToken());
        params.put("new_value" ,yuo);
        params.put("type" ,"8");

        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(s.equals(""))
                    if(y<50)
                    refreshFBToken(y+1,yuo);

            }
        }  , APIUrl.SERVER + "user/update",params);


    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.i("user_id", "onCreate: "+SharedPrefManager.getInstance(MainActivity.this).getUser().getUser_id());
        Log.i("profile_image", "onCreate: "+SharedPrefManager.getInstance(MainActivity.this).getUserPic());
        isOnline();
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        /*
        RxNetwork.connectivityChanges(MainActivity.this, connectivityManager)
                .subscribe(new Action1<Boolean>()
                {
                    @Override
                    public void call(Boolean connected)
                    {
                        if (connected){
                            Toast.makeText(MainActivity.this, "connected", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            **/
        //Refresh Firebase token

        try {
            if (FirebaseInstanceId.getInstance()!=null &&
                    FirebaseInstanceId.getInstance().getToken()!=null &&
                    ! FirebaseInstanceId.getInstance().getToken().equals("")) {
                // FirebaseMessaging.getInstance().subscribeToTopic("visitor");
                FirebaseMessaging.getInstance().subscribeToTopic("public");
                SharedPreferences pref =
                        getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                String regId = pref.getString("regId", FirebaseInstanceId.getInstance().getToken());
                if(SharedPrefManager.getInstance(this).IsUserLoggedIn())
                    refreshFBToken(0 ,regId);
                Log.e("EDER664", "Firebase reg id: " + regId);
            }
        }catch (Exception e){}

        if (!App.is_login_called){
            if(SharedPrefManager.getInstance(this).IsUserLoggedIn()){
                login();
            }
        }

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        mHandler = new Handler();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        imgProfile = navHeader.findViewById(R.id.img_profile);
        editImageProfile = navHeader.findViewById(R.id.edit_image_profile);
        editImageProgress = navHeader.findViewById(R.id.edit_image_progress);

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);



        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
//if the user has not completed his information
if(SharedPrefManager.getInstance(this).IsUserLoggedIn()){
    User user= SharedPrefManager.getInstance(this).getUser();
    if(user.getPhone().equals("") || user.getAddress().equals("")||
            user.getName().equals("")){
    startActivity(new Intent(this,ContinueAccountActivity.class));
    finish();
    }
}
        if(SharedPrefManager.getInstance(this).IsUserLoggedIn()) {
            String user_type = SharedPrefManager.getInstance(MainActivity.this).getUser().getUser_type();
            if (user_type.equals("2")&&SharedPrefManager.getInstance(MainActivity.this).getStoreUpdated()) {
                Menu nav_Menu = navigationView.getMenu();

                nav_Menu.findItem(R.id.nav_add_product).setVisible(true);
                nav_Menu.findItem(R.id.nav_orders_below_5000).setVisible(true);
                nav_Menu.findItem(R.id.nav_orders_up_5000).setVisible(true);
                nav_Menu.findItem(R.id.nav_my_offers).setVisible(true);
                nav_Menu.findItem(R.id.nav_my_store).setVisible(true);
                nav_Menu.findItem(R.id.nav_funds_section).setVisible(true);
                nav_Menu.findItem(R.id.nav_slider).setVisible(true);

            }
        }
    }




    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */

    private void loadNavHeader() {
        // name, website
        loginbtn = (TextView)navHeader.findViewById(R.id.go_login);
        signupbtn = (TextView)navHeader.findViewById(R.id.go_signup);
        //addStore = navHeader.findViewById(R.id.nav_add_store);

        if(SharedPrefManager.getInstance(this).IsUserLoggedIn()){
            txtName.setText(SharedPrefManager.getInstance(this).getUser().getName());
            navHeader.findViewById(R.id.login_view).setVisibility(View.GONE);
            //addStore.setVisibility(View.GONE);
         TextView utype  = (TextView)navHeader.findViewById(R.id.utype);
            String type = SharedPrefManager.getInstance(this).getUser().getUser_type();
            if(type.equals("4"))
                utype.setText("زبون");
            if(type.equals("3"))
                utype.setText("مسوق");
            if(type.equals("5"))
                utype.setText("موزع");
            if(type.equals("2"))
                utype.setText("بائع");
            if (type.equals("1"))
                utype.setText("المدير");
            if (type.equals("6"))
                utype.setText("مدير المبيعات");
            if (type.equals("7"))
                utype.setText("السكرتاريا");
            utype.setVisibility(View.VISIBLE);
            navHeader.findViewById(R.id.type_layout).setVisibility(View.VISIBLE);
            navHeader.findViewById(R.id.logout).setVisibility(View.VISIBLE);
            navHeader.findViewById(R.id.blank).setVisibility(View.GONE);
            navHeader.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPrefManager.getInstance(MainActivity.this).Logout();
                    Intent intent = new Intent(MainActivity.this ,SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });



        }else {
            editImageProfile.setVisibility(View.GONE);
            txtName.setText("زائر جديد");
            loginbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this , LoginActivty.class));
                }
            });
            signupbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this , SignupActivity.class));
                }
            });
          //  txtWebsite.setText("احصل على حسابك المجاني الآن");
        }
        // loading header background image
        // Loading profile image
        if(SharedPrefManager.getInstance(this).getUserPic().equals("")) {
            Glide.with(this).load(R.drawable.profile)
                    .thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .apply(RequestOptions.skipMemoryCacheOf(true)) //
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(RequestOptions.circleCropTransform())
                    .into(imgProfile);
        }
        else {
            Glide.with(this).load(SharedPrefManager.getInstance(this).getUserPic())
                    .thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .apply(RequestOptions.skipMemoryCacheOf(true)) //
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(imgProfile);
            editImageProfile.setVisibility(View.VISIBLE);
        }
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(SharedPrefManager.getInstance(MainActivity.this).IsUserLoggedIn())  {
                   startActivity(new Intent(MainActivity.this , AccountActivity.class));
                }else{
                  startActivity(new Intent(MainActivity.this , LoginActivty.class));
              }
            }
        });
        editImageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        /*
        addStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                View view = getLayoutInflater().inflate(R.layout.dialog_store_type,null);
                Button new_seller = view.findViewById(R.id.add_store_new_seller);
                Button use_exist = view.findViewById(R.id.add_store_use_exist);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                new_seller.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainActivity.this,SignupSellerActivity.class));
                        dialog.cancel();
                    }
                });
                use_exist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainActivity.this , LoginActivty.class));
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });
        **/
        // showing dot next to notifications label
     //   navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
      //  navigationView.getMenu().getItem(5).setTitle(SharedPrefManager.getInstance(this).IsUserLoggedIn()?"تسجيل خروج":"نسجيل دخول");
    }
    void pick(){
        Intent photoPickerIntent =  new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "لا يتم الحصول على الصلاحية", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED " ,requestCode+" and "+resultCode );

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if(data!=null) {
                        Log.e("GOTCHA " ,"yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : "  , resultUri.toString());
                        Log.e("Picsaved2 in : "  , resultUri.getPath());
                        Log.e("Picsaved3 in : "  , resultUri.getEncodedPath());

                        try {
                            imagePath=resultUri.getPath();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            Bitmap bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
                            imgProfile.setImageBitmap(bitmap);
                            callUpdateProfileAPI();
                        }catch (Exception e){
                            Toast.makeText(this, "error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case SELECT_PHOTO:
                    if(data!=null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME+".jpg";
                        UCrop.of(pickedImage,
                                Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(3, 3)
                                .withMaxResultSize(800, 800)
                                .start(this);
                    }
                    break;




            }
        }
        else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR" ,cropError.getMessage() );

        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                // photos
                PhotosFragment photosFragment = new PhotosFragment();
                return photosFragment;
            case 2:
                // movies fragment
                MoviesFragment moviesFragment = new MoviesFragment();
                return moviesFragment;
            case 3:
                // notifications fragment
                NotificationsFragment notificationsFragment = new NotificationsFragment();
                return notificationsFragment;

            case 4:
                // settings fragment
                SettingsFragment settingsFragment = new SettingsFragment();
                return settingsFragment;
            default:
                return new HomeFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setMenuCounter(@IdRes int itemId, int count) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
       if(count==0){
        view.setVisibility(View.GONE);
           return;
       }
       else
           view.setVisibility(View.VISIBLE);
        view.setText(count > 0 ? String.valueOf(count) : null);
    }

    private void setUpNavigationView() {
        if (SharedPrefManager.getInstance(MainActivity.this).IsUserLoggedIn()){
            String user_type = SharedPrefManager.getInstance(MainActivity.this).getUser().getUser_type();
            if (user_type.equals("2")&&SharedPrefManager.getInstance(MainActivity.this).getStoreUpdated()){
                Menu nav_Menu = navigationView.getMenu();

                nav_Menu.findItem(R.id.nav_add_product).setVisible(true);
                nav_Menu.findItem(R.id.nav_orders_below_5000).setVisible(true);
                nav_Menu.findItem(R.id.nav_orders_up_5000).setVisible(true);
                nav_Menu.findItem(R.id.nav_my_offers).setVisible(true);
                nav_Menu.findItem(R.id.nav_my_store).setVisible(true);
                nav_Menu.findItem(R.id.nav_funds_section).setVisible(true);
                nav_Menu.findItem(R.id.nav_slider).setVisible(true);

            }
        }

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Intent ii=null;
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;

                    case R.id.nav_home:

                        break;
                    case R.id.nav_chat:
                      ii = new Intent(MainActivity.this , ChatActivity.class);
                      ii.putExtra("title","الدعم");
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_cats:
                        ii = new Intent(MainActivity.this, CategoriesActivity.class);
                        ii.putExtra("par", "0");
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_complaints:
                        ii = new Intent(MainActivity.this , OpenComplaintsActivity.class);
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_add_product:
                        if (SharedPrefManager.getInstance(MainActivity.this).getTutorial()){
                            ii = new Intent(MainActivity.this , AddSellerProductActivity.class);
                            ii.putExtra("activity_type","add");
                            startActivity(ii);
                        }else {
                            startActivity(new Intent(MainActivity.this,OnBoardActivity.class));
                        }

                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_slider:
                            ii = new Intent(MainActivity.this , BuyerSlidersActivity.class);
                            startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_orders_below_5000:
                        ii = new Intent(MainActivity.this , SellerOrdersBelow5000.class);
                        ii.putExtra("type","less");
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_orders_up_5000:
                        ii = new Intent(MainActivity.this , SellerOrdersBelow5000.class);
                        ii.putExtra("type","up");
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_my_store:
                        ii = new Intent(MainActivity.this , SellerMyStoreActivity.class);
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_my_offers:
                        ii = new Intent(MainActivity.this , SellerMyOffersActivity.class);
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_funds_section:
                        ii = new Intent(MainActivity.this , OrdersStatisticsActivity.class);
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
//                    case R.id.nav_account:
//                        if(SharedPrefManager.getInstance(MainActivity.this).IsUserLoggedIn())
//                        {
//                            ii = new Intent(MainActivity.this , AccountActivity.class);
//                            startActivity(ii);
//                        }
//                        else{
//                        ii = new Intent(MainActivity.this , LoginActivty.class);
//                        startActivity(ii);}
//                        drawer.closeDrawers();
//                        return true;
                    case R.id.nav_favourite:
                        ii = new Intent(MainActivity.this , FavouritesActivity.class);
                        startActivity(ii);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_nots:
//                        if(!SharedPrefManager.getInstance(MainActivity.this).IsUserLoggedIn()){
//                            Toast.makeText(MainActivity.this, "سجل دخولك من فضلك", Toast.LENGTH_SHORT).show();
//                            ii = new Intent(MainActivity.this , LoginActivty.class);
//                            startActivity(ii);
//                            drawer.closeDrawers();
//                        }else
                            {
                            ii = new Intent(MainActivity.this , NotificationsActivity.class);
                            startActivity(ii);
                            drawer.closeDrawers();}
                        return true;
                    case R.id.nav_wallet:
                        if(!SharedPrefManager.getInstance(MainActivity.this).IsUserLoggedIn()){
                            Toast.makeText(MainActivity.this, "سجل دخولك من فضلك", Toast.LENGTH_SHORT).show();
                            ii = new Intent(MainActivity.this , LoginActivty.class);
                            startActivity(ii);
                            drawer.closeDrawers();
                        }else {
                        ii = new Intent(MainActivity.this , MyWalletActivity.class);
                        startActivity(ii);
                        drawer.closeDrawers();}
                        return true;
                    case R.id.nav_cp:
                        if(!SharedPrefManager.getInstance(MainActivity.this).IsUserLoggedIn()){
                            Toast.makeText(MainActivity.this, "سجل دخولك من فضلك", Toast.LENGTH_SHORT).show();
                            ii = new Intent(MainActivity.this , LoginActivty.class);
                            startActivity(ii);
                            drawer.closeDrawers();
                        }else {
                            ii = new Intent(MainActivity.this, ControlPanelActivity.class);
                            startActivity(ii);
                            drawer.closeDrawers();
                        }return true;
                         case R.id.nav_help:
                        // launch new intent instead of loading fragment
                        startActivity(new Intent(MainActivity.this, StaticPagesListActivity.class));
                        drawer.closeDrawers();
                        return true;

                    case R.id.nav_share:
                        // launch new intent instead of loading fragment
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        String shareBody = "قم بمشاركة تطبيق سيكويا مع أصدقائك ! حمله الآن" + "\r\n" + "" +
                                ""
                                +"https://play.google.com/store/apps/details?id=com.nyx.sequoiaapp";
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "مشاركة التطبيق");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                        startActivity(Intent.createChooser(sharingIntent, "شارك من خلال : "));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_exit:
                        // launch new intent instead of loading fragment
                        System.exit(0);
                        return true;
                    default:
                        navItemIndex = 0;
                }
                //Checking if the item is in checked state or not, if not make it in checked state
                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }






    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }






    // show or hide the fab
    private void toggleFab() {
        if (navItemIndex == 0)
            fab.show();
        else
            fab.hide();
    }

    public void isOnline(){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    App.is_connected =  false;
                }else {
                    App.is_connected = true;
                }
            }else {
                App.is_connected = false;
            }
        }catch (Exception e){
            Toast.makeText(this, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            App.is_connected = false;
        }

    }

    private void callUpdateProfileAPI(){
        editImageProgress.setVisibility(View.VISIBLE);
        String user_id = SharedPrefManager.getInstance(this).getUser().getUser_id();
        UsersAPIsClass.update_image(this, user_id, imagePath, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if (json!=null){
                    String json1 = new Gson().toJson(json);
                    String success = new Gson().fromJson(json1,String.class);
                    SharedPrefManager.getInstance(MainActivity.this).setUserPic(APIUrl.USERS_PICS_SERVER+success);
                    Log.i("immmmm", "onResponse: "+APIUrl.USERS_PICS_SERVER+success);
                    Toast.makeText(MainActivity.this, "تم تحديث الصورة الشخصية بنجاح", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this, "حدث خطأ ما أعد المحاولة", Toast.LENGTH_SHORT).show();
                }
                editImageProgress.setVisibility(View.GONE);
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                editImageProgress.setVisibility(View.GONE);
                Snackbar.make(findViewById(R.id.drawer_layout), "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                        .setAction("أعد المحاولة", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callUpdateProfileAPI();
                            }
                        }).show();
            }
        });
    }

    private void login(){
        final HashMap<String  ,String> params=new HashMap<String, String>();
        String type = SharedPrefManager.getInstance(MainActivity.this).getLoginType();
        LoginObject lo = SharedPrefManager.getInstance(MainActivity.this).getLoginData();
        if (type.equals("normal")){
            params.put("mobile_number",lo.getMobile_number());
            params.put("login_type" ,lo.getLogin_type());
            params.put("password" ,lo.getPassword());
        }else if (type.equals("facebook")){
            params.put("login_type" ,lo.getLogin_type());
            params.put("facebook_id" ,lo.getFacebook_id());
            params.put("access_token" , lo.getAccess_token());
        }
        BackgroundServices.getInstance(getApplicationContext()).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(!s.equals("")){
                    try {
                        JSONObject object =new JSONObject(s);
                        if(object.getInt("status")==1)
                        {
                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                                    object.getJSONObject("data") ,params.get("password"));
                            App.is_login_called = true;
                        }else{ }
                    }catch (Exception fdr){ }
                }else{}
            }
        }  , APIUrl.SERVER2 + "user/login",params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }
}
