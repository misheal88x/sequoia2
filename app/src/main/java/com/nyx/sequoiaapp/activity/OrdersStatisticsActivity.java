package com.nyx.sequoiaapp.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.OrdersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.OrdersCommissionsAdapter;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.NewOrderObject;
import com.nyx.sequoiaapp.models.OrdersCommissionsResponse;

import java.util.ArrayList;
import java.util.List;

public class OrdersStatisticsActivity extends RootActivity {

    private RelativeLayout root;
    private NestedScrollView scrollView;
    private ProgressBar pb_loading;
    private TextView tv_pending,tv_delievered,tv_shipped,tv_canceled,tv_commissions,tv_sum;
    private RecyclerView rv_commissions;
    private List<NewOrderObject> list_of_orders;
    private OrdersCommissionsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private FloatingActionButton fab_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_statistics);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Root
        root = findViewById(R.id.order_stats_layout);
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("القسم المالي");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //ScrollView
        scrollView = findViewById(R.id.order_stats_scroll);
        //Progress Bar
        pb_loading = findViewById(R.id.order_stats_loading);
        //TextView
        tv_canceled = findViewById(R.id.order_stats_canceled);
        tv_pending = findViewById(R.id.order_stats_pending);
        tv_shipped = findViewById(R.id.order_stats_shipped);
        tv_delievered = findViewById(R.id.order_stats_delivered);
        tv_commissions = findViewById(R.id.order_stats_commissions_txt);
        tv_sum = findViewById(R.id.order_stats_sum);
        //Recycler View
        rv_commissions = findViewById(R.id.order_stats_commissions_recycler);
        //Floating Action Button
        fab_chat = findViewById(R.id.open_chat);
    }

    private void init_events() {
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersStatisticsActivity.this,ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
    }

    private void init_activity() {
        init_recycler();
        callAPI();
    }

    private void init_recycler(){
        list_of_orders = new ArrayList<>();
        adapter = new OrdersCommissionsAdapter(OrdersStatisticsActivity.this,list_of_orders);
        layoutManager = new LinearLayoutManager(OrdersStatisticsActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_commissions.setLayoutManager(layoutManager);
        rv_commissions.setAdapter(adapter);
    }

    private void callAPI(){
        String user_id = SharedPrefManager.getInstance(OrdersStatisticsActivity.this).getUser().getUser_id();
        pb_loading.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        OrdersAPIsClass.get_statistics(OrdersStatisticsActivity.this, user_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            OrdersCommissionsResponse success = new Gson().fromJson(json1,OrdersCommissionsResponse.class);
                            int sum = 0;
                            if (success.getOrders_count()!=null){
                                if (success.getOrders_count().size()>0){
                                    tv_canceled.setText(String.valueOf(success.getOrders_count().get(0).getCanceled_orders_count())+" طلبية");
                                    tv_shipped.setText(String.valueOf(success.getOrders_count().get(0).getShipped_orders_count())+" طلبية");
                                    tv_pending.setText(String.valueOf(success.getOrders_count().get(0).getPending_orders_count())+" طلبية");
                                    tv_delievered.setText(String.valueOf(success.getOrders_count().get(0).getDelivered_orders_count())+" طلبية");
                                }
                            }
                            if (success.getToPaidCommission()!=null){
                                if (success.getToPaidCommission().size()>0){
                                    tv_sum.setText(success.getToPaidCommission().get(0).getPaid_amount()+" ل.س");
                                    if (success.getToPaidCommission().get(0).getPaid_amount().equals("0")){
                                        rv_commissions.setVisibility(View.GONE);
                                        tv_commissions.setVisibility(View.GONE);
                                    }
                                }else {
                                    rv_commissions.setVisibility(View.GONE);
                                    tv_commissions.setVisibility(View.GONE);
                                    tv_sum.setText("0 ل.س");
                                }
                            }else {
                                rv_commissions.setVisibility(View.GONE);
                                tv_commissions.setVisibility(View.GONE);
                                tv_sum.setText("0 ل.س");
                            }
                            if (success.getSeqouia_commissions_for_orders()!=null){
                                if (success.getSeqouia_commissions_for_orders().size()>0){
                                    for (NewOrderObject noo : success.getSeqouia_commissions_for_orders()){
                                        list_of_orders.add(noo);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    rv_commissions.setVisibility(View.GONE);
                                    tv_commissions.setVisibility(View.GONE);
                                }
                            }else {
                                rv_commissions.setVisibility(View.GONE);
                                tv_commissions.setVisibility(View.GONE);
                            }
                            scrollView.setVisibility(View.VISIBLE);
                            pb_loading.setVisibility(View.GONE);
                        }else {
                            pb_loading.setVisibility(View.GONE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_loading.setVisibility(View.GONE);
                        Snackbar.make(root, "اتصل بالانترنت من فضلك", Snackbar.LENGTH_INDEFINITE)
                                .setAction("أعد المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRealNotifCount(SharedPrefManager.getInstance(this).getNotiCount());
    }

}
