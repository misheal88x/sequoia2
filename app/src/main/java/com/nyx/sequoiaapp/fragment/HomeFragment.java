package com.nyx.sequoiaapp.fragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.nyx.sequoiaapp.activity.AllBrandsActivity;
import com.nyx.sequoiaapp.activity.AllNormalBuyersActivity;
import com.nyx.sequoiaapp.activity.AllOffersActivity;
import com.nyx.sequoiaapp.activity.App;
import com.nyx.sequoiaapp.activity.CategoriesActivity;
import com.nyx.sequoiaapp.activity.CategoryProductsActivity;
import com.nyx.sequoiaapp.activity.ChatActivity;
import com.nyx.sequoiaapp.activity.MainActivity;
import com.nyx.sequoiaapp.activity.NewSearchResultsActivity;
import com.nyx.sequoiaapp.activity.PagingPosts;
import com.nyx.sequoiaapp.activity.ProductLoadingFromIntent;
import com.nyx.sequoiaapp.activity.SignupSellerActivity;
import com.nyx.sequoiaapp.adapters.BasicRecycleviewAdapter;
import com.nyx.sequoiaapp.adapters.AdsAdapterGrid;
import com.nyx.sequoiaapp.adapters.AdsAdapterOffers;
import com.nyx.sequoiaapp.adapters.BrandsAdapter;
import com.nyx.sequoiaapp.adapters.HomeCategoriesAdapter;
import com.nyx.sequoiaapp.adapters.HomeNormalBuyersAdapter;
import com.nyx.sequoiaapp.adapters.SearchCitiesAdapter;
import com.nyx.sequoiaapp.costumes.EndlessScrollListener;
import com.nyx.sequoiaapp.costumes.EndlessScrollView;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.Config;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.CustomSliderView;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.City;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.models.NormalBuyerObject;
import com.nyx.sequoiaapp.models.Post;
import com.nyx.sequoiaapp.models.Slider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.andref.rx.network.RxNetwork;
import rx.functions.Action1;

public class HomeFragment extends Fragment implements EndlessScrollListener {
    private SliderLayout mDemoSlider;
    private EndlessScrollView scrollView;
    private RelativeLayout root;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isRefreshing = false;
    private FloatingActionButton fab;
    private FloatingActionButton fab_chat;
    private RecyclerView recyclerView2;
    private RecyclerView recyclerView3;
    private RecyclerView recyclerView4;
    private RecyclerView recyclerView5;
    private RecyclerView recyclerView6;
    private RecyclerView recyclerView7;
    private RecyclerView recyclerView8;
    private RecyclerView recyclerView9;
    private RecyclerView recyclerViewCats;
    private ImageView b1, b2, b3, b4, b5, b6, b7;
    private final ArrayList<ExtendedPost> infinteposts = new ArrayList<>();
    private AdsAdapterGrid adsAdapter;
    // String VIDEO_URL="http://ahmadt.scit.co/vids/sok.MP4";
    private String VIDEO_URL = "";
    private View up;
    private boolean loadingFlag = false;
    private ProgressBar loading;
    private int current = 0;
    private Button btn_add_market,btn_call_as_for_seller,btn_load_more;
    private String user_type= "";
    private LinearLayout brands_view,buyers_view;
    private EditText edt_search;
    private TextView tv_city;
    private String selected_city_id = "-1";
    private String selected_city_name = "كل المدن";

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_home, container, false);
        init_views(v);
        init_slider();
        init_events(v);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));
        recyclerView3.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));
        recyclerView4.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));

        recyclerView6.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));

        recyclerView7.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));
        recyclerViewCats.setLayoutManager(new GridLayoutManager(getActivity(), 3
        ));

        recyclerView8.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));
        recyclerView9.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false
        ));
        //Updating the messages count counter
        if (SharedPrefManager.getInstance(getActivity()).IsUserLoggedIn())
            updateMessagesCount(v);

        GridLayoutManager lm = new GridLayoutManager(getActivity(),
                2);


        recyclerView5.setLayoutManager(lm);
        recyclerView5.setItemAnimator(new DefaultItemAnimator());

        adsAdapter = new AdsAdapterGrid(infinteposts, getActivity()
        );
        recyclerView5.setAdapter(adsAdapter);


        //Fill Banners with data
        ArrayList banners = SharedPrefManager.getInstance(getActivity()).getBanners();
        if (banners != null && banners.size() > 0) {
            setBanner(b1, (JSONObject) banners.get(0));
            setBanner(b2, (JSONObject) banners.get(1));
            setBanner(b3, (JSONObject) banners.get(2));
            setBanner(b4, (JSONObject) banners.get(3));
            setBanner(b5, (JSONObject) banners.get(4));
            setBanner(b6, (JSONObject) banners.get(5));
            setBanner(b7, (JSONObject) banners.get(6));
        }

        isRefreshing = false;
        ArrayList brands = SharedPrefManager.getInstance(getActivity()).getBrands();
        List<NormalBuyerObject> buyers = SharedPrefManager.getInstance(getActivity()).getNormalBuyers();
        //Filling Brands With data
        if (brands.size() > 0) {
            recyclerView8.setAdapter(new BrandsAdapter(brands
                    , getActivity()
            ));

        } else {
            v.findViewById(R.id.brands_view).setVisibility(View.GONE);
            recyclerView8.setVisibility(View.GONE);

        }
        //Filling with buyers
        if (buyers.size() > 0) {
            recyclerView9.setAdapter(new HomeNormalBuyersAdapter(getActivity(),buyers
            ));

        } else {
            v.findViewById(R.id.buyers_view).setVisibility(View.GONE);
            recyclerView9.setVisibility(View.GONE);

        }

        ArrayList temp_top_paid = new ArrayList();
        ArrayList temp_featured = new ArrayList();
        ArrayList temp_top_rated = new ArrayList();
        ArrayList temp_favourite = new ArrayList();
        ArrayList top_paid = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.TOP_PAID});
        ArrayList featured = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.FEATURED});
        ArrayList top_rated = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.TOP_RATED});
        ArrayList favourite = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.FAVOURITE});
        //Filling Top Paid Products With Data
        for (int i = 0; i < top_paid.size() ; i++) {
            ExtendedPost ep = (ExtendedPost) top_paid.get(i);
            if (ep.getView().equals("0")){
                temp_top_paid.add(ep);
            }
        }
        recyclerView2.setAdapter(new BasicRecycleviewAdapter(
                temp_top_paid, getActivity()
        ));
        //Filling Featured Products With Data
        for (int i = 0; i < featured.size() ; i++) {
            ExtendedPost ep = (ExtendedPost) featured.get(i);
            Log.i("product_price", "onCreateView: "+ep.getNewprice());
            if (ep.getView().equals("0")){
                temp_featured.add(ep);
            }
        }
        recyclerView3.setAdapter(new BasicRecycleviewAdapter(
                temp_featured, getActivity()
        ));
        //Filling Top Rated Products With Data
        for (int i = 0; i < top_rated.size() ; i++) {
            ExtendedPost ep = (ExtendedPost) top_rated.get(i);
            if (ep.getView().equals("0")){
                temp_top_rated.add(ep);
            }
        }
        recyclerView4.setAdapter(new BasicRecycleviewAdapter(
                temp_top_rated, getActivity()
        ));
        //Filling Favourite Products with data
        for (int i = 0; i < favourite.size() ; i++) {
            ExtendedPost ep = (ExtendedPost) favourite.get(i);
            if (ep.getView().equals("0")){
                temp_favourite.add(ep);
            }
        }
        recyclerView6.setAdapter(new BasicRecycleviewAdapter(
                temp_favourite, getActivity()
        ));
        //Filling Featured Categories with data
        ArrayList categories1 = SharedPrefManager.getInstance(getActivity()).getCategories("0");
        ArrayList temp = new ArrayList();
        if (categories1.size()<=3){
            for (int i = 0; i < categories1.size(); i++) {
                temp.add(categories1.get(i));
            }
        }else {
            for (int i = 0; i <3 ; i++) {
                temp.add(categories1.get(i));
            }
        }
        recyclerViewCats.setAdapter(new HomeCategoriesAdapter(
                getActivity(),temp
        ));
        init(v);
        if (!SharedPrefManager.getInstance(getActivity()).IsUserLoggedIn()){
            btn_call_as_for_seller.setVisibility(View.GONE);
            btn_add_market.setVisibility(View.GONE);
        }else {
            user_type = SharedPrefManager.getInstance(getActivity()).getUser().getUser_type();
            if (user_type.equals("2")){
                btn_call_as_for_seller.setVisibility(View.GONE);
                if (SharedPrefManager.getInstance(getActivity()).getStoreUpdated()){
                    btn_add_market.setVisibility(View.GONE);
                }
            }else {
                btn_add_market.setVisibility(View.GONE);
            }
        }

            if (!App.is_connected){
                if (MainActivity.connectivityManager!=null){
                    RxNetwork.connectivityChanges(getActivity(),MainActivity.connectivityManager)
                            .subscribe(new Action1<Boolean>() {
                                @Override
                                public void call(Boolean connected)
                                {
                                    if (connected){
                                        //Toast.makeText(getActivity(), "Connected", Toast.LENGTH_SHORT).show();
                                        mSwipeRefreshLayout.setRefreshing(true);
                                        connect();
                                    }else {
                                        App.is_connected = false;
                                    }
                                }
                            });
                }
            }

        connect();
        return v;
    }

    private void init_views(View v){
        scrollView = v.findViewById(R.id.hearr_scrollview);
        root = v.findViewById(R.id.home_root);
        mDemoSlider = (SliderLayout) v.findViewById(R.id.slider);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swifeRefresh);
        b1 = (ImageView) v.findViewById(R.id.banner1);
        b2 = (ImageView) v.findViewById(R.id.banner2);
        b3 = (ImageView) v.findViewById(R.id.banner3);
        b5 = (ImageView) v.findViewById(R.id.banner5);
        b4 = (ImageView) v.findViewById(R.id.banner4);
        b6 = (ImageView) v.findViewById(R.id.banner6);
        b7 = (ImageView) v.findViewById(R.id.banner7);
        //Init the recyclers
        recyclerView2 = (RecyclerView) v.findViewById(R.id.recyclerview2);
        recyclerView3 = (RecyclerView) v.findViewById(R.id.recyclerview3);
        recyclerView4 = (RecyclerView) v.findViewById(R.id.recyclerview4);
        recyclerView5 = (RecyclerView) v.findViewById(R.id.recyclerview5);
        recyclerView6 = (RecyclerView) v.findViewById(R.id.recyclerview6);
        recyclerView7 = (RecyclerView) v.findViewById(R.id.recyclerview7);
        recyclerView8 = (RecyclerView) v.findViewById(R.id.recyclerview8);
        recyclerView9 = (RecyclerView) v.findViewById(R.id.recyclerview9);
        recyclerViewCats = (RecyclerView) v.findViewById(R.id.recyclerview_cats);
        brands_view = v.findViewById(R.id.brands_view);
        buyers_view = v.findViewById(R.id.buyers_view);
        up = getActivity().findViewById(R.id.logo_up);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab_chat = v.findViewById(R.id.open_chat);
        loading = v.findViewById(R.id.loading);
        EndlessScrollView myScrollView = (EndlessScrollView) v.findViewById(R.id.hearr_scrollview);
        myScrollView.setScrollViewListener(this);
        final EndlessScrollView sv = (EndlessScrollView) v.findViewById(R.id.hearr_scrollview);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollToTop();
                //sv.scrollTo(0, 0);
            }
        });
        btn_add_market = v.findViewById(R.id.home_add_market);
        btn_call_as_for_seller = v.findViewById(R.id.home_call_as_to_seller);
        btn_load_more = v.findViewById(R.id.get_more_btn);
        edt_search = v.findViewById(R.id.home_search);
        tv_city = v.findViewById(R.id.search_txt);
        tv_city.setText("كل المدن");
    }

    private void init_slider(){
        //Fill the slider with data coming from shared preferences
        ArrayList sliders = SharedPrefManager.getInstance(getActivity()).getSliderItems();
        for (int i = 0; i < sliders.size(); i++) {
            Slider s = (Slider) sliders.get(i);
            CustomSliderView textSliderView = new CustomSliderView(getActivity());
            textSliderView
                    .description("")
                    .image(s.getUrl())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(s.getClick());
            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(Config.SLIDER_THEME);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2000);
        mDemoSlider.startAutoCycle();
    }



    private void init_events(View v){
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ChatActivity.class);
                intent.putExtra("title","الدردشة");
                startActivity(intent);
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(getActivity(), "الرجاء كتابة ما تريد البحث عنه", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    Intent intent = new Intent(getActivity(), NewSearchResultsActivity.class);
                    intent.putExtra("query",edt_search.getText().toString());
                    intent.putExtra("city_id",selected_city_id);
                    intent.putExtra("city_name",selected_city_name);
                    edt_search.setText("");
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
        tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                View view = getLayoutInflater().inflate(R.layout.dialog_search_city,null);
                RecyclerView rv_recycler = view.findViewById(R.id.dialog_city_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                final ArrayList cities = SharedPrefManager.getInstance(getActivity()).getCities();
                final ArrayList source_cities = new ArrayList();
                City cc = new City("-1","كل المدن");
                source_cities.add(cc);
                if (cities.size() > 0){
                    for (int i = 0; i < cities.size() ; i++) {
                        City c = (City) cities.get(i);
                        if (c.getName().equals("دمشق")||c.getName().equals("طرطوس")||c.getName().equals("حمص")){
                        source_cities.add(c);
                        }
                    }
                }
                SearchCitiesAdapter adapter = new SearchCitiesAdapter(getActivity(), source_cities,selected_city_id, new IMove() {
                    @Override
                    public void move(int position) {
                        City c = (City) source_cities.get(position);
                        selected_city_id = c.getId();
                        selected_city_name = c.getName();
                        tv_city.setText(selected_city_name);
                        dialog.dismiss();
                    }
                });
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                rv_recycler.setLayoutManager(layoutManager);
                rv_recycler.setAdapter(adapter);
                dialog.show();
            }
        });
        //Handling on clicking on show all top paid button
        v.findViewById(R.id.show_all_top_paid).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getActivity(), PagingPosts.class);
                ii.putExtra("type", "2");
                ii.putExtra("name", "المنتجات الاكثر مبيعا");
                startActivity(ii);
            }
        });
        //Handling on clicking on show all top rated button
        v.findViewById(R.id.show_all_top_rated).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getActivity(), PagingPosts.class);
                ii.putExtra("type", "1");
                ii.putExtra("name", "المنتجات الأكثر تقييما");
                startActivity(ii);
            }
        });
        //Handling on clicking on show all featured button
        v.findViewById(R.id.show_all_featured).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getActivity(), PagingPosts.class);
                ii.putExtra("type", "3");
                ii.putExtra("name", "المنتجات المميزة");
                startActivity(ii);
            }
        });
        //Handling on clicking on show favourite products button
        v.findViewById(R.id.show_all_favourites).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getActivity(), PagingPosts.class);
                ii.putExtra("type", "4");
                ii.putExtra("name", "المنتجات المفضلة");
                startActivity(ii);
            }
        });

        //Handling on clicking on show all offers button
        v.findViewById(R.id.show_all_offers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getActivity(), AllOffersActivity.class);
                startActivity(ii);
            }
        });
        //Handling on clicking on show all categories button
        v.findViewById(R.id.show_all_cats).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Intent ii = new Intent(getActivity(), CategoriesActivity.class);
                ii.putExtra("par", "0");
                startActivity(ii);
                **/
                ArrayList categories1 = SharedPrefManager.getInstance(getActivity()).getCategories("0");
                ArrayList temp = new ArrayList();
                for (int i = 0; i < categories1.size(); i++) {
                    temp.add(categories1.get(i));
                }
                recyclerViewCats.setAdapter(new HomeCategoriesAdapter(
                        getActivity(),temp
                ));
                v.findViewById(R.id.show_all_cats).setVisibility(View.GONE);
            }
        });
        /*
        v.findViewById(R.id.voice_srch_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // promptSpeechInput();
                String url = "http://e-sequoia.net/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        **/
        btn_call_as_for_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getActivity(), ChatActivity.class);
                ii.putExtra("title","الدعم");
                startActivity(ii);
            }
        });
        btn_add_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SignupSellerActivity.class));
                //getActivity().finish();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                connect();
            }
        });

        v.findViewById(R.id.show_all_brands).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllBrandsActivity.class));
            }
        });
        v.findViewById(R.id.show_all_buyers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllNormalBuyersActivity.class));
            }
        });
        btn_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMore(0);
            }
        });

    }

    public void setBanner(ImageView b, final JSONObject banner) {
        try {
            Glide.with(this)
                    .load(APIUrl.ADS_PICS_SERVER + banner.getString("img"))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    // .thumbnail(Glide.with(getActivity()).load(R.drawable.loader))
                    .into(b);
            final String url = banner.getString("target_id");
            final String type = banner.getString("target_type");
            if (!url.equals("")) {
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //New Category
                        if (type.equals("1")) {
                            Intent i = new Intent(getActivity(), CategoriesActivity.class);
                            i.putExtra("par", "0");
                            startActivity(i);
                        }
                        //New Category Products
                        if (type.equals("2")) {
                            Intent ii = new Intent(getActivity(), CategoryProductsActivity.class);

                            ii.putExtra("id", url);

                            ii.putExtra("title", "");
                            startActivity(ii);
                        }
                        //New Product
                        if (type.equals("3")) {
                            Intent ii = new Intent(getActivity(), ProductLoadingFromIntent.class);

                            ii.putExtra("id", url);

                            ii.putExtra("title", "");
                            startActivity(ii);
                        }
                        //Offer
                        if (type.equals("4")) {
                            Intent ii = new Intent(getActivity(), AllOffersActivity.class);

                            startActivity(ii);
                        }
                        //A URL
                        if (type.equals("5")) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                        //A Mobile Number
                        if (type.equals("7")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + url));
                            startActivity(intent);
                        }


                    }
                });
            }
        } catch (JSONException k) {

        }
    }

    //This method is called when we open this fragment
    void connect() {
        if (ConnectionUtils.isNetworkAvailable(getActivity()) && !isRefreshing) {
            //isRefreshing = true;
            //Recall the Startup API
            BackgroundServices.getInstance(getActivity()).init(new PostAction() {
                @Override
                public void doTask() {


                }

                @Override
                public void doTask(String s) {
                    if (getView() != null) {
                        if (!s.equals("")) {
                            //Saving the data in shared preferences
                            SharedPrefManager.getInstance(getActivity())
                                    .saveData(s);
                            //Fill offers with data
                            init(getView());
                            //Clearing the products in the bottom
                            infinteposts.clear();
                            current = 0;
                            //Updating the messages count
                            if (SharedPrefManager.getInstance(getActivity()).IsUserLoggedIn())
                                updateMessagesCount(getView());
                            //------------------------------------------------------------------------------
                            //Fill Banners with data
                            ArrayList banners = SharedPrefManager.getInstance(getActivity()).getBanners();
                            if (banners != null && banners.size() > 0) {
                                setBanner(b1, (JSONObject) banners.get(0));
                                setBanner(b2, (JSONObject) banners.get(1));
                                setBanner(b3, (JSONObject) banners.get(2));
                                setBanner(b4, (JSONObject) banners.get(3));
                                setBanner(b5, (JSONObject) banners.get(4));
                                setBanner(b6, (JSONObject) banners.get(5));
                                setBanner(b7, (JSONObject) banners.get(6));
                            }

                            isRefreshing = false;
                            ArrayList brands = SharedPrefManager.getInstance(getActivity()).getBrands();
                            List<NormalBuyerObject> buyers = SharedPrefManager.getInstance(getActivity()).getNormalBuyers();
                            //Filling Brands With data
                            if (brands.size() > 0) {
                                recyclerView8.setAdapter(new BrandsAdapter(brands
                                        , getActivity()
                                ));

                            } else {
                                brands_view.setVisibility(View.GONE);
                                recyclerView8.setVisibility(View.GONE);

                            }
                            //Filling Buyers With data
                            if (buyers.size() > 0) {
                                recyclerView9.setAdapter(new HomeNormalBuyersAdapter(getActivity()
                                        , buyers
                                ));

                            } else {
                                buyers_view.setVisibility(View.GONE);
                                recyclerView9.setVisibility(View.GONE);

                            }
                            ArrayList temp_top_paid = new ArrayList();
                            ArrayList temp_featured = new ArrayList();
                            ArrayList temp_top_rated = new ArrayList();
                            ArrayList temp_favourite = new ArrayList();
                            ArrayList top_paid = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.TOP_PAID});
                            ArrayList featured = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.FEATURED});
                            ArrayList top_rated = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.TOP_RATED});
                            ArrayList favourite = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home", Post.FAVOURITE});
                            //Filling Top Paid Products With Data
                            for (int i = 0; i < top_paid.size(); i++) {
                                ExtendedPost ep = (ExtendedPost) top_paid.get(i);
                                if (ep.getView().equals("0")) {
                                    temp_top_paid.add(ep);
                                }
                            }
                            recyclerView2.setAdapter(new BasicRecycleviewAdapter(
                                    temp_top_paid, getActivity()
                            ));
                            //Filling Featured Products With Data
                            for (int i = 0; i < featured.size(); i++) {
                                ExtendedPost ep = (ExtendedPost) featured.get(i);
                                if (ep.getView().equals("0")) {
                                    temp_featured.add(ep);
                                }
                            }
                            recyclerView3.setAdapter(new BasicRecycleviewAdapter(
                                    temp_featured, getActivity()
                            ));
                            //Filling Top Rated Products With Data
                            for (int i = 0; i < top_rated.size(); i++) {
                                ExtendedPost ep = (ExtendedPost) top_rated.get(i);
                                if (ep.getView().equals("0")) {
                                    temp_top_rated.add(ep);
                                }
                            }
                            recyclerView4.setAdapter(new BasicRecycleviewAdapter(
                                    temp_top_rated, getActivity()
                            ));
                            //Filling Favourite Products with data
                            for (int i = 0; i < favourite.size(); i++) {
                                ExtendedPost ep = (ExtendedPost) favourite.get(i);
                                if (ep.getView().equals("0")) {
                                    temp_favourite.add(ep);
                                }
                            }
                            recyclerView6.setAdapter(new BasicRecycleviewAdapter(
                                    temp_favourite, getActivity()
                            ));/*
                            //Filling Featured Categories with data
                            ArrayList categories1 = SharedPrefManager.getInstance(getActivity()).getFeaturedCategories();
                            ArrayList temp = new ArrayList();
                            if (categories1.size()<=4){
                                for (int i = 0; i < categories1.size(); i++) {
                                    temp.add(categories1.get(i));
                                }
                            }else {
                                for (int i = 0; i <4 ; i++) {
                                    temp.add(categories1.get(i));
                                }
                            }
                            recyclerViewCats.setAdapter(new HomeCategoriesAdapter(
                                    getActivity(),temp
                            ));
                            **/
                            //------------------------------------------------------------------------------

                            //Filling the bottom products with data
                            mSwipeRefreshLayout.setRefreshing(false);
                            loadMore(0);
                        } else {
                            connect();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        ((MainActivity) getActivity()).setNotifCount(SavedCacheUtils.getCart(getActivity()).size());
        //int noti_count = SharedPrefManager.getInstance(getActivity()).getNotiCount();
        //noti_count++;
        //SharedPrefManager.getInstance(getActivity()).saveNotiCount(noti_count);
        ((MainActivity) getActivity()).setRealNotifCount(SharedPrefManager.getInstance(getActivity()).getNotiCount());
        //connect();
        super.onResume();
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    //This method filled the offers recycler with data
    void init(View v) {
        ArrayList offers = SharedPrefManager.getInstance(getActivity()).getExtendedPosts(new String[]{"home",
                Post.OFFERS});
        //
        if (offers.size() > 0) {
            ArrayList temp_offers = new ArrayList();
            for (int i = 0; i < offers.size(); i++) {
                ExtendedPost ep = (ExtendedPost) offers.get(i);
                if (Integer.valueOf(ep.getRemaining())>0&&ep.getDiscount_view().equals("0")){
                    temp_offers.add(ep);
                }
            }
            recyclerView7.setAdapter(new AdsAdapterOffers(
                    temp_offers, getActivity()
            ));
        } else {
            v.findViewById(R.id.offers_vv).setVisibility(View.GONE);
            recyclerView7.setVisibility(View.GONE);
        }


        v.findViewById(R.id.gload).setVisibility(View.GONE);
        //  v.findViewById(R.id.gonna).setVisibility(View.VISIBLE);

    }
    //Filling the bottom products with data
    void loadMore(final int t) {
        loadingFlag = true;
        loading.setVisibility(View.VISIBLE);
        BackgroundServices.getInstance(getActivity()).Call(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                loading.setVisibility(View.GONE);
                btn_load_more.setVisibility(View.GONE);
                try {
                    JSONObject heart = new JSONObject(s);
                    //  Toast.makeText(getActivity(), heart.getJSONObject("data").getInt("total_count")+"", Toast.LENGTH_SHORT).show();
                    JSONArray products = heart.getJSONObject("data").getJSONArray("products");
                    for (int i = 0; i < products.length(); i++) {
                        infinteposts.add(new ExtendedPost(products.getJSONObject(i))
                        );
                    }
                    if (products.length() > 0) {
                        adsAdapter.notifyDataSetChanged();
                        current += 50;
                    }
                    loadingFlag = false;


                } catch (JSONException jex) {
                    //Log.d("ESX" , jex.getMessage());
                    loadingFlag = false;
                    if (t < 10) loadMore(t + 1);
                    else if (getContext() != null)
                        Toast.makeText(getContext(), "مشكلة في الشبكة !", Toast.LENGTH_SHORT).show();
                }
            }
        }, APIUrl.SERVER + "product/get_all/" + current);
    }

    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {

        if (y > 400) {
            fab.show();
            up.setVisibility(View.VISIBLE);
        } else {
            fab.hide();
            up.setVisibility(View.GONE);

        }
        if (!loadingFlag) {
            if (ConnectionUtils.isNetworkAvailable(getActivity())) {
                // We take the last son in the scrollview
                View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
                int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                // if diff is zero, then the bottom has been reached
                if (distanceToEnd == 0) {
                    //todo
                    //loadMore(0);
                    btn_load_more.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    void updateMessagesCount(final View v) {
        HashMap<String, String> map = new HashMap<>();
        User user = SharedPrefManager.getInstance(getActivity()).getUser();
        map.put("user_id", user.getUser_id());
        map.put("user_token", user.getToken());
        //
        BackgroundServices.getInstance(getActivity()).CallPost(new PostAction() {
                                                                   @Override
                                                                   public void doTask() {

                                                                   }

                                                                   @Override
                                                                   public void doTask(String s) {
                                                                       if (!s.equals("")) {
                                                                           try {
                                                                               JSONObject jox = new JSONObject(s);
                                                                               int c = jox.getJSONObject("data").getInt("total_unread");
                                                                               // setMenuCounter(R.id.nav_chat,c);
                                                                               if (c == 0) {
                                                                                   //v.findViewById(R.id.ur_counter).setVisibility(View.GONE);
                                                                               } else {
                                                                                   //v.findViewById(R.id.ur_counter).setVisibility(View.VISIBLE);
                                                                                   //((TextView) v.findViewById(R.id.ur_counter)).setText(String.valueOf(c));

                                                                               }

                                                                           } catch (JSONException e) {
                                                                               e.printStackTrace();
                                                                           }
                                                                       } else
                                                                           updateMessagesCount(v);
                                                                   }
                                                               }, APIUrl.SERVER + "message/unread_messages_count", map);
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    public void scrollToTop() {
        int x = 0;
        int y = 0;

        ObjectAnimator xTranslate = ObjectAnimator.ofInt(scrollView, "scrollX", x);
        ObjectAnimator yTranslate = ObjectAnimator.ofInt(scrollView, "scrollY", y);

        AnimatorSet animators = new AnimatorSet();
        animators.setDuration(1000L);
        animators.playTogether(xTranslate, yTranslate);

        animators.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator arg0) {

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {


            }

            @Override
            public void onAnimationEnd(Animator arg0) {


            }

            @Override
            public void onAnimationCancel(Animator arg0) {


            }
        });
        animators.start();
    }
}
