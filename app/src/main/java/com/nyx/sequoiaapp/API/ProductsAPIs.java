package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 10/21/2019.
 */

public interface ProductsAPIs {
    @Multipart
    @POST("products")
    Call<BaseResponse> addProduct(@Header("token") String token,
                                  @Header("user_id") String user_id,
                                  @Part("name")RequestBody name,
                                  @Part("description")RequestBody description,
                                  @Part("prime_price")RequestBody prime_price,
                                  @Part("price")RequestBody price,
                                  @Part("commission_fixed")RequestBody commission_fixed,
                                  @Part("commission_percent")RequestBody commission_percent,
                                  @Part("status")RequestBody status,
                                  @Part("qty_in_stock")RequestBody qty,
                                  @Part("features")RequestBody features,
                                  @Part("categories")RequestBody categories,
                                  @Part("store_id")RequestBody store_id,
                                  @Part("featured")RequestBody featured,
                                  @Part("brand_id")RequestBody brand_id,
                                  @Part("bounty") RequestBody bounty,
                                  @Part MultipartBody.Part[] images);
    @Multipart
    @POST("products/{product_id}/update")
    Call<BaseResponse> updateProduct(@Header("token") String token,
                                  @Header("user_id") String user_id,
                                  @Path("product_id") String product_id,
                                  @Part("name")RequestBody name,
                                  @Part("description")RequestBody description,
                                  @Part("prime_price")RequestBody prime_price,
                                  @Part("price")RequestBody price,
                                  @Part("commission_fixed")RequestBody commission_fixed,
                                  @Part("commission_percent")RequestBody commission_percent,
                                  @Part("status")RequestBody status,
                                  @Part("qty_in_stock")RequestBody qty,
                                  @Part("features")RequestBody features,
                                  @Part("categories")RequestBody categories,
                                  @Part("store_id")RequestBody store_id,
                                  @Part("featured")RequestBody featured,
                                  @Part("brand_id")RequestBody brand_id,
                                  @Part("bounty") RequestBody bounty,
                                  @Part MultipartBody.Part[] images);
    @GET("products/me")
    Call<BaseResponse> getMyProducts(@Header("token") String token,
                                     @Header("user_id") String user_id,
                                     @Query("page") int page);
    @GET("products/price/less/5000")
    Call<BaseResponse> getProductsLess(@Header("token") String token,
                                     @Header("user_id") String user_id,
                                     @Query("page") int page);
    @POST("products/{product_id}/view/{status}")
    Call<BaseResponse> switch_visibility(@Header("token") String token,
                                         @Path("product_id") String product_id,
                                         @Path("status") String status);
    @GET("products/{product_id}")
    Call<BaseResponse> product_info(@Header("token") String token,
                                    @Header("user_id") String user_id,
                                    @Path("product_id") String product_id);
    @POST("products/{product_id}/remove")
    Call<BaseResponse> deleteProduct(@Header("token") String token,
                                     @Header("user_id") String user_id,
                                     @Path("product_id") String product_id);
    @FormUrlEncoded
    @POST("search-by-user")
    Call<BaseResponse> search(@Header("token") String token,
                              @Header("user_id") String user_id,
                              @Field("words") String words,
                              @Query("page") int page);
    @POST("products/{product_id}/images/{image_name}/remove")
    Call<BaseResponse> deleteImage(@Header("token") String token,
                                   @Header("user_id") String user_id,
                                   @Path("product_id") String product_id,
                                   @Path("image_name") String image_name);

}
