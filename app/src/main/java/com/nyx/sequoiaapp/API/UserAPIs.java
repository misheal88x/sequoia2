package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Misheal on 10/22/2019.
 */

public interface UserAPIs {
    @POST("buyer/check-profile")
    Call<BaseResponse> check_seller_status(@Header("token") String token,
                                           @Header("user_id") String user_id);
    @FormUrlEncoded
    @POST("buyer/update")
    Call<BaseResponse> update_seller_info(@Header("token") String token,
                                          @Header("user_id") String user_id,
                                          @Field("store_name") String store_name,
                                          @Field("cities_ids") String cities_ids,
                                          @Field("has_branches") int has_branches,
                                          @Field("orders_delivery") int orders_delivery,
                                          @Field("accept_commission") int accept_commission,
                                          @Field("orders_delivery_days") int orders_delivery_days,
                                          @Field("rejection_commission_text") String rejection_commission_text,
                                          @Field("gender") String gender);
    @Multipart
    @POST("user/profile/image/update")
    Call<BaseResponse> update_image(@Header("token") String token,
                                    @Header("user_id") String user_id,
                                    @Part MultipartBody.Part image_file);
}
