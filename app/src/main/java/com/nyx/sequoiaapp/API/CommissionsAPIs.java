package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Misheal on 11/12/2019.
 */

public interface CommissionsAPIs {
    @GET("commission/info")
    Call<BaseResponse> get_info(@Header("token") String token);
}
