package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Misheal on 11/3/2019.
 */

public interface DispenserAPIs {
    @FormUrlEncoded
    @POST("dispenser/search-by-user/orders")
    Call<BaseResponse> search(@Header("token") String token,
                              @Header("user_id") String user_id,
                              @Field("words") String words,
                              @Field("start") int start);
}
