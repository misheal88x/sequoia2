package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 10/23/2019.
 */

public interface OrdersAPIs {
    @GET("orders/less/5000")
    Call<BaseResponse> getOrders(@Header("token") String token,
                                 @Header("user_id") String user_id,
                                 @Query("page") int page);
    @GET("orders/more/5000")
    Call<BaseResponse> getOrdersMore(@Header("token") String token,
                                 @Header("user_id") String user_id,
                                 @Query("page") int page);

    @FormUrlEncoded
    @POST("search-by-user/orders")
    Call<BaseResponse> search(@Header("token") String token,
                              @Header("user_id") String user_id,
                              @Field("words") String words,
                              @Query("page") int page);
    @FormUrlEncoded
    @POST("search-by-user/orders/less/5000")
    Call<BaseResponse> search_less(@Header("token") String token,
                              @Header("user_id") String user_id,
                              @Field("words") String words,
                              @Query("page") int page);
    @FormUrlEncoded
    @POST("search-by-user/orders/more/5000")
    Call<BaseResponse> search_more(@Header("token") String token,
                                   @Header("user_id") String user_id,
                                   @Field("words") String words,
                                   @Query("page") int page);
    @GET("orders/{order_id}/details")
    Call<BaseResponse> order_details(@Header("token") String token,
                                     @Header("user_id") String user_id,
                                     @Path("order_id") String order_id);

    @GET("orders/statistics/info")
    Call<BaseResponse> get_stats(@Header("token") String token,
                                 @Header("user_id") String user_id);
    @FormUrlEncoded
    @POST("buyer/confirm-order-delivery")
    Call<BaseResponse> confirm_order(@Header("token") String token,
                                     @Header("user_id") String user_id,
                                     @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("order")
    Call<BaseResponse> add_order(@Header("token") String token,
                                 @Field("user_id") String user_id,
                                 @Field("user_token") String user_token,
                                 @Field("items") String items,
                                 @Field("notes") String notes);

}
