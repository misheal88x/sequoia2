package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface SellerSlidersAPIs {
    @GET("users-sliders")
    Call<BaseResponse> getSliders(
            @Header("token") String token,
            @Header("user_id") String user_id
    );
    @Multipart
    @POST("users-sliders")
    Call<BaseResponse> addSlider(
            @Header("token") String token,
            @Header("user_id") String user_id,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("users-sliders/{slider_id}")
    Call<BaseResponse> deleteSlider(
            @Header("token") String token,
            @Header("user_id") String user_id,
            @Field("_method") String method,
            @Path("slider_id") String slider_id
    );
}
