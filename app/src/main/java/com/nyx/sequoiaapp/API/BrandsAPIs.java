package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Misheal on 11/1/2019.
 */

public interface BrandsAPIs {
    @GET("brands")
    Call<BaseResponse> get_brands(@Header("token") String token);
}
