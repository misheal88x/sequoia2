package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 10/16/2019.
 */

public interface OfferAPIs {
    @FormUrlEncoded
    @POST("offers")
    Call<BaseResponse> addOffer(@Header("token") String token,
                                @Header("user_id") String user_id,
                                @Field("description") String description,
                                @Field("fixed") int fixed,
                                @Field("percent") int percent,
                                @Field("start_date") String start_date,
                                @Field("end_date") String end_date,
                                @Field("product_id") String product_id);
    @FormUrlEncoded
    @POST("offers/{offer_id}/update")
    Call<BaseResponse> updateOffer(@Header("token") String token,
                                @Header("user_id") String user_id,
                                @Path("offer_id") String offer_id,
                                @Field("description") String description,
                                @Field("fixed") int fixed,
                                @Field("percent") int percent,
                                @Field("start_date") String start_date,
                                @Field("end_date") String end_date,
                                @Field("product_id") String product_id);

    @GET("offers/{offer_id}")
    Call<BaseResponse> getOffer(@Header("token") String token,
                                @Header("user_id") String user_id,
                                @Path("offer_id") String offer_id);
    @GET("offers")
    Call<BaseResponse> getAllOffers(@Header("token") String token,
                                    @Header("user_id") String user_id,
                                    @Query("page") int page);
    @POST("offers/{offer_id}/remove")
    Call<BaseResponse> remove(@Header("token") String token,
                              @Header("user_id") String user_id,
                              @Path("offer_id") String offer_id);
    @FormUrlEncoded
    @POST("search-by-user/offers")
    Call<BaseResponse> search(@Header("token") String token,
                              @Header("user_id") String user_id,
                              @Field("words") String words,
                              @Query("page") int page);
}
