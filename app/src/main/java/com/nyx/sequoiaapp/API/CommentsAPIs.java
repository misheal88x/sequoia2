package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Misheal on 11/9/2019.
 */

public interface CommentsAPIs {
    @FormUrlEncoded
    @POST("comments/{id}/replies")
    Call<BaseResponse> create_reply(@Header("token") String token,
                                    @Header("user_id") String user_id,
                                    @Path("id") String id,
                                    @Field("content") String content);
}
