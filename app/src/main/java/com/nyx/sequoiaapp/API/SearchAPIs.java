package com.nyx.sequoiaapp.API;

import com.nyx.sequoiaapp.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Misheal on 10/21/2019.
 */

public interface SearchAPIs {
    @FormUrlEncoded
    @POST("search")
    Call<BaseResponse> search(@Header("token") String token,
                              @Field("words") String words,
                              @Field("city_id") String city_id,
                              @Query("page") int page);
}
