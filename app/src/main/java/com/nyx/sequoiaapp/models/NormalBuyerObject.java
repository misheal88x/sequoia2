package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NormalBuyerObject {
    @SerializedName("id") private String id = "";
    @SerializedName("mobile") private String mobile = "";
    @SerializedName("store_name") private String store_name = "";
    @SerializedName("fullname") private String fullname = "";
    @SerializedName("image") private String image = "";
    @SerializedName("city_name") private String city_name = "";
    @SerializedName("rating") private String rating = "";
    @SerializedName("image_sliders") private List<BuyerSliderObject> image_sliders = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<BuyerSliderObject> getImage_sliders() {
        return image_sliders;
    }

    public void setImage_sliders(List<BuyerSliderObject> image_sliders) {
        this.image_sliders = image_sliders;
    }
}
