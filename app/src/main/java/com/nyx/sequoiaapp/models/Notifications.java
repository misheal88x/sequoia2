package com.nyx.sequoiaapp.models;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.activity.ChatActivity;
import com.nyx.sequoiaapp.activity.OrderDetailsActivity;
import com.nyx.sequoiaapp.activity.ProductLoadingFromIntent;

/**
 * Created by Luminance on 12/15/2018.
 */

public class Notifications {
    private String id,title,content,time , target_type ,target_id,payload;
    private View.OnClickListener click;

    public String getTarget_type() {
        return target_type;
    }

    public void setTarget_type(String target_type) {
        this.target_type = target_type;
    }

    public String getTarget_id() {
        return target_id;
    }

    public void setTarget_id(String target_id) {
        this.target_id = target_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public Notifications(String id, String title, final String content, String time, final String target_type,
                         final  String target_id, final String payload, final Context c) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.time = time;
        this.target_type = target_type;
        this.target_id = target_id;
        this.payload = payload;
        click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(target_type.equals("1")
                        || target_type.equals("2")){
                    Intent resultIntent = new Intent(c, ProductLoadingFromIntent.class);
                    resultIntent.putExtra("id",target_id);
                    c.startActivity(resultIntent);
                }
                if(target_type.equals("3")){
                    Intent intent = new Intent(c, ChatActivity.class);
                    intent.putExtra("title","الدعم");
                    c.startActivity( intent);
                }
                if (target_type.equals("122")){
                    if (payload!=null){
                        if (!payload.equals("")){
                            try{
                                PayloadObject po = new Gson().fromJson(payload,PayloadObject.class);
                                if (po.isRequire_confirmation()){
                                    if (po.getOrder_id()!=null){
                                        if (!po.getOrder_id().equals("")){
                                            Intent intent = new Intent(c,OrderDetailsActivity.class);
                                            intent.putExtra("type","with");
                                            intent.putExtra("order_id",po.getOrder_id());
                                            c.startActivity(intent);
                                        }
                                    }
                                }
                            }catch (Exception e){}
                        }
                    }
                }

            }
        };
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public View.OnClickListener getClick() {
        return click;
    }

    public void setClick(View.OnClickListener click) {
        this.click = click;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
