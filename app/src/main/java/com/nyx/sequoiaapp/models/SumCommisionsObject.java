package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class SumCommisionsObject {
    @SerializedName("paid_amount") private String paid_amount = "";

    public String getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(String paid_amount) {
        this.paid_amount = paid_amount;
    }
}
