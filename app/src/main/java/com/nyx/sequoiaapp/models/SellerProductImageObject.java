package com.nyx.sequoiaapp.models;

import android.graphics.Bitmap;

/**
 * Created by Misheal on 10/18/2019.
 */

public class SellerProductImageObject {
    private Bitmap bitmap ;
    private String imagePath = "";
    private String url = "";

    public SellerProductImageObject(Bitmap bitmap, String imagePath,String url) {
        this.bitmap = bitmap;
        this.imagePath = imagePath;
        this.url = url;
    }

    public SellerProductImageObject() {}

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
