package com.nyx.sequoiaapp.models;

/**
 * Created by Luminance on 5/30/2018.
 */

public class MyComissionRecord {
    private String id ,uid , oid, value , status ,date , customer ,ref ,product_name,pic;


    public MyComissionRecord(String id, String uid, String oid, String value, String status, String date, String customer, String ref, String product_name, String pic) {
        this.id = id;
        this.uid = uid;
        this.oid = oid;
        this.value = value;
        this.status = status;
        this.date = date;
        this.customer = customer;
        this.ref = ref;
        this.product_name = product_name;
        this.pic = pic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
