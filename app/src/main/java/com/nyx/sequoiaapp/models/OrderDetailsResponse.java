package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 11/12/2019.
 */

public class OrderDetailsResponse {
    @SerializedName("userInfo") private UserObject userInfo = new UserObject();
    @SerializedName("order") private NewOrderObject order = new NewOrderObject();

    public UserObject getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserObject userInfo) {
        this.userInfo = userInfo;
    }

    public NewOrderObject getOrder() {
        return order;
    }

    public void setOrder(NewOrderObject order) {
        this.order = order;
    }
}
