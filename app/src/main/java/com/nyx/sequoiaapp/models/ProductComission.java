package com.nyx.sequoiaapp.models;

/**
 * Created by Luminance on 5/30/2018.
 */

public class ProductComission {
    private String id , name , comission , pic;

    public ProductComission(String id, String name, String comission, String pic) {
        this.id = id;
        this.name = name;
        this.comission = comission;
        this.pic = pic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComission() {
        return comission;
    }

    public void setComission(String comission) {
        this.comission = comission;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
