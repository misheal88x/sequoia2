package com.nyx.sequoiaapp.models;

import android.util.Log;

import com.nyx.sequoiaapp.helper.APIUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luminance on 5/20/2018.
 */

public class ExtendedPost  implements Serializable{
    private String id=""  , title="" , thumb="" , oldprice="" , newprice="";
    private String description="";
    private String[] imgs;
    private String[] good_images;
   private String searchCounter="0";
    private String rating="0";
    private String status="1" , is_offer="0" , remaining="1",view = "0",discount_view = "0";
    private String store_name = "";
    private String city_name = "";

    public ExtendedPost() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getDiscount_view() {
        return discount_view;
    }

    public void setDiscount_view(String discount_view) {
        this.discount_view = discount_view;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getOldprice() {
        return oldprice;
    }

    public void setOldprice(String oldprice) {
        this.oldprice = oldprice;
    }

    public String getNewprice() {
        return newprice;
    }

    public void setNewprice(String newprice) {
        this.newprice = newprice;
    }

    public String[] getGood_images(){
        return good_images;
    }

    public ExtendedPost(JSONObject o) {

    try {
    JSONArray picsJson = new JSONArray();
    try {
        if (o.getString("id").equals("5301")){
            JSONObject oo = o;
            String s = "hello";
        }
        String quer = o.getString("images");
        if (quer != null && !quer.equals("null"))
            picsJson = o.getJSONArray("all_pics");
        JSONObject goodObj = new JSONObject(quer);
        JSONArray goodArr = goodObj.getJSONArray("images");
        if (goodArr.length()==0)
            goodArr = goodObj.getJSONArray("thumbs");
        good_images = new String[goodArr.length()];
        if (goodArr.length()>0){
            for (int i = 0; i < goodArr.length(); i++) {
                good_images[i] = goodArr.getString(i);
            }
        }
    } catch (Exception e) {
        Log.i("exception_ece", "ExtendedPost: "+id+" "+e.getMessage());
    }
    String[] pics = new String[picsJson.length()];
    for (int j = 0; j < picsJson.length(); j++)
        pics[j] =
   picsJson.getString(j);
   this.id = o.getString("id");
    this.title = o.getString("name");

    this.oldprice = o.getString("price");
    this.newprice = o.getString("prime_price");
    this.thumb = APIUrl.POSTS_PICS_SERVER +
            (new JSONObject(o.getString("thumb"))).getJSONArray("thumbs").getString(0);
    this.description = o.getString("description");
    this.imgs = pics;
    this.searchCounter = o.getString("search_count");
    this.rating = o.getString("avg_rate");
    this.status = "1";
    this.is_offer = o.getString("is_offer");
    this.remaining = o.getString("time_remaining");


    //Log.d("ADDED..","yes");


    //  super(id, title, thumb, oldprice, newprice);
    this.description = description;
    this.imgs = imgs;
    this.searchCounter = searchCounter;
    this.rating = rating;
    this.status = status;
    this.is_offer = is_offer;
    this.remaining = remaining;
    this.city_name = o.getString("city_name");
    this.store_name = o.getString("store_name");
}catch (JSONException jeop){
    Log.d("VVXJHSJJU",jeop.getMessage());
}
    }


    public String getSearchCounter() {
        return searchCounter;
    }

    public void setSearchCounter(String searchCounter) {
        this.searchCounter = searchCounter;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_offer() {
        return is_offer;
    }

    public void setIs_offer(String is_offer) {
        this.is_offer = is_offer;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getImgs() {
        return imgs;
    }

    public void setImgs(String[] imgs) {
        this.imgs = imgs;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }
}
