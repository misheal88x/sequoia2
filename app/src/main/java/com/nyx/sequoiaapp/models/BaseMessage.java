package com.nyx.sequoiaapp.models;

import androidx.annotation.NonNull;

import java.io.Serializable;


/**
 * Created by Luminance on 1/20/2018.
 */

public class BaseMessage implements Serializable,Comparable<BaseMessage>{
    String id, message;
    String senderName, senderID;
    String createdAt, status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseMessage that = (BaseMessage) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public BaseMessage(String id, String message, String senderName, String senderID, String createdAt, String status) {
        this.id = id;
        this.message = message;
        this.senderName = senderName;
        this.senderID = senderID;
        this.createdAt = createdAt;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int compareTo(@NonNull BaseMessage o) {
        int x1 = Integer.parseInt(this.id);
        int x2 = Integer.parseInt(o.id);
     return x2-x1;
    }
}
