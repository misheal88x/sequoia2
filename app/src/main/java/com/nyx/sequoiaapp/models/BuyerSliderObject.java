package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class BuyerSliderObject {
    @SerializedName("id") private String id = "";
    @SerializedName("image_url") private String image_url = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
