package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 15/11/2019.
 */

public class StatisticsObject {
    @SerializedName("canceled_orders_count") private int canceled_orders_count = 0;
    @SerializedName("delivered_orders_count") private int delivered_orders_count = 0;
    @SerializedName("shipped_orders_count") private int shipped_orders_count = 0;
    @SerializedName("pending_orders_count") private int pending_orders_count = 0;

    public int getCanceled_orders_count() {
        return canceled_orders_count;
    }

    public void setCanceled_orders_count(int canceled_orders_count) {
        this.canceled_orders_count = canceled_orders_count;
    }

    public int getDelivered_orders_count() {
        return delivered_orders_count;
    }

    public void setDelivered_orders_count(int delivered_orders_count) {
        this.delivered_orders_count = delivered_orders_count;
    }

    public int getShipped_orders_count() {
        return shipped_orders_count;
    }

    public void setShipped_orders_count(int shipped_orders_count) {
        this.shipped_orders_count = shipped_orders_count;
    }

    public int getPending_orders_count() {
        return pending_orders_count;
    }

    public void setPending_orders_count(int pending_orders_count) {
        this.pending_orders_count = pending_orders_count;
    }
}
