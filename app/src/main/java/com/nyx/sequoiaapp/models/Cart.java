package com.nyx.sequoiaapp.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Luminance on 5/20/2018.
 */

public class Cart implements Serializable{
    private Date last_edited;
    private ArrayList<CartItem> items;

    public Cart(Date last_edited, ArrayList<CartItem> items) {
        this.last_edited = last_edited;
        this.items = items;
    }


    public int size(){
        int count=0;
        for(CartItem c : items)count+=c.getQuantity();
      return count;
    }

    public Date getLast_edited() {
        return last_edited;
    }

    public void setLast_edited(Date last_edited) {
        this.last_edited = last_edited;
    }

    public ArrayList<CartItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<CartItem> items) {
        this.items = items;
    }

    public  void addToCart(ExtendedPost post , int quantity , String color , String size ,String measure){
this.items.add(new CartItem(post ,quantity , color , size , measure));
        this.last_edited = new Date();
    }

    public void deleteFromCart(String id){
    for(int i=0;i<items.size();i++)
        if(items.get(i).getPost().getId().equals(id)){
            items.remove(i);
            break;
        }
        this.last_edited = new Date();

    }

    public boolean checkIfExists(String id){
        for(int i=0;i<items.size();i++)
            if(items.get(i).getPost().getId().equals(id)){
               return true;
            }
        return false;

    }
    public void changeItemQuantity(String id , int quantity){
        for(int i=0;i<items.size();i++)
            if(items.get(i).getPost().getId().equals(id)){
                items.get(i).setQuantity(quantity);
                break;
            }
        this.last_edited = new Date();

    }



}
