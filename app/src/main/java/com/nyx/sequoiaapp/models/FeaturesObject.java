package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/21/2019.
 */

public class FeaturesObject {
    @SerializedName("sizes") private Object sizes = new ArrayList<>();
    @SerializedName("colors") private Object colors = new ArrayList<>();
    @SerializedName("weight") private String weight = "";
    @SerializedName("measure") private Object measure = new ArrayList<>();
    @SerializedName("ship_delivery") private List<ShipDeliveryObejct> ship_delivery = new ArrayList<>();


    public FeaturesObject() {}

    public FeaturesObject(List<String> sizes, List<String> colors, String weight, List<String> measure) {
        this.sizes = sizes;
        this.colors = colors;
        this.weight = weight;
        this.measure = measure;
    }

    public Object getSizes() {
        return sizes;
    }

    public void setSizes(Object sizes) {
        this.sizes = sizes;
    }

    public Object getColors() {
        return colors;
    }

    public void setColors(Object colors) {
        this.colors = colors;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Object getMeasure() {
        return measure;
    }

    public void setMeasure(Object measure) {
        this.measure = measure;
    }

    public List<ShipDeliveryObejct> getShip_delivery() {
        return ship_delivery;
    }

    public void setShip_delivery(List<ShipDeliveryObejct> ship_delivery) {
        this.ship_delivery = ship_delivery;
    }
}
