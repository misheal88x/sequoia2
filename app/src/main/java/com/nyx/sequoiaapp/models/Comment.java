package com.nyx.sequoiaapp.models;

import org.json.JSONArray;

/**
 * Created by Luminance on 5/17/2018.
 */

public class Comment {
    private String id , name , pic , rate , date , commnet;
    private JSONArray replies;

    public Comment(String id, String name, String pic, String rate, String date, String commnet,JSONArray replies) {
        this.id = id;
        this.name = name;
        this.pic = pic;
        this.rate = rate;
        this.date = date;
        this.commnet = commnet;
        this.replies = replies;
    }

    public Comment() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCommnet() {
        return commnet;
    }

    public void setCommnet(String commnet) {
        this.commnet = commnet;
    }

    public JSONArray getReplies() {
        return replies;
    }

    public void setReplies(JSONArray replies) {
        this.replies = replies;
    }
}
