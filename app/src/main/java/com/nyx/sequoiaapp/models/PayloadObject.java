package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class PayloadObject {
    @SerializedName("order_id") private String order_id = "";
    @SerializedName("require_confirmation") private boolean require_confirmation = false;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public boolean isRequire_confirmation() {
        return require_confirmation;
    }

    public void setRequire_confirmation(boolean require_confirmation) {
        this.require_confirmation = require_confirmation;
    }
}
