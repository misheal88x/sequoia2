package com.nyx.sequoiaapp.models;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Luminance on 5/27/2018.
 */

public class Order {
    private String id , date , status , Info;
    String total;
    JSONArray items;

    public Order() {}

    public Order(String id, String date, String status, JSONArray tt , String t) {
        this.id = id;
        this.date = date;
        this.status = status;
        //this.items = items;
        items = tt;
        this.total =t;

    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getItemsReport(){
   return Info;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JSONArray getItems() {
        return items;
    }

    public void setItems(JSONArray items) {
        this.items = items;
    }
}
