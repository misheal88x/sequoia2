package com.nyx.sequoiaapp.models;

import org.json.JSONArray;

/**
 * Created by Luminance on 5/27/2018.
 */

public class ExtendedOrder  extends Order{
    private String fullname , status_id , note , user_details ,note_c;

    public ExtendedOrder(String id, String date, String status,
                         JSONArray tt,String total , String fullname, String status_id, String note,
                         String user_details ,String user_m) {
        super(id, date, status, tt ,total);
        this.fullname = fullname;
        this.status_id = status_id;
        this.note = note;
        this.user_details = user_details;
        this.note_c = user_m;
    }

    public String getNote_c() {
        return note_c;
    }

    public void setNote_c(String note_c) {
        this.note_c = note_c;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUser_details() {
        return user_details;
    }

    public void setUser_details(String user_details) {
        this.user_details = user_details;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public ExtendedOrder(String id, String date, String status, JSONArray tt,String t , String fullname) {
        super(id, date, status, tt ,t);
        this.fullname = fullname;
    }
}
