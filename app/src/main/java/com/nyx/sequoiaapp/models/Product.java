package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/20/2019.
 */

public class Product {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("description") private String description = "";
    @SerializedName("prime_price") private float prime_price = 0;
    @SerializedName("price") private float price = 0;
    @SerializedName("final_price") private int final_price = 0;
    @SerializedName("commission_fixed") private float commission_fixed = 0;
    @SerializedName("commission_percent") private float commission_percent = 0;
    @SerializedName("status") private int status = 0;
    @SerializedName("qty_in_stock") private int qty_in_stock = 0;
    @SerializedName("features") private FeaturesObject features = new FeaturesObject();
    @SerializedName("category_id") private int category_id = 0;
    @SerializedName("store_id") private int store_id = 0;
    @SerializedName("thumb") private ImagesObject thumb = new ImagesObject();
    @SerializedName("images") private ImagesObject images = new ImagesObject();
    @SerializedName("featured") private int featured = 0;
    @SerializedName("view") private int view = 0;
    @SerializedName("brand_id") private int brand_id = 0;
    @SerializedName("view_rating") private int view_rating = 0;
    @SerializedName("search_count") private int search_count = 0;
    @SerializedName("seller_fixed") private int seller_fixed = 0;
    @SerializedName("seller_percent") private int seller_percent = 0;
    @SerializedName("p_views") private int p_views = 0;
    @SerializedName("fb_image") private ImagesObject fb_image = new ImagesObject();
    @SerializedName("is_offer") private boolean is_offer = false;
    @SerializedName("time_remaining") private int time_remaining = 0;
    @SerializedName("discount_price") private float discount_price = 0;
    @SerializedName("categories") private List<NewCategoryObject> categories = new ArrayList<>();
    @SerializedName("bounty") private int bounty = 0;
    @SerializedName("qty") private int qty = 0;
    @SerializedName("store_name") private String store_name = "";
    @SerializedName("city_name") private String city_name = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrime_price() {
        return prime_price;
    }

    public void setPrime_price(float prime_price) {
        this.prime_price = prime_price;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getCommission_fixed() {
        return commission_fixed;
    }

    public void setCommission_fixed(float commission_fixed) {
        this.commission_fixed = commission_fixed;
    }

    public float getCommission_percent() {
        return commission_percent;
    }

    public void setCommission_percent(float commission_percent) {
        this.commission_percent = commission_percent;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getQty_in_stock() {
        return qty_in_stock;
    }

    public void setQty_in_stock(int qty_in_stock) {
        this.qty_in_stock = qty_in_stock;
    }

    public FeaturesObject getFeatures() {
        return features;
    }

    public void setFeatures(FeaturesObject features) {
        this.features = features;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public ImagesObject getThumb() {
        return thumb;
    }

    public void setThumb(ImagesObject thumb) {
        this.thumb = thumb;
    }

    public ImagesObject getImages() {
        return images;
    }

    public void setImages(ImagesObject images) {
        this.images = images;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getView_rating() {
        return view_rating;
    }

    public void setView_rating(int view_rating) {
        this.view_rating = view_rating;
    }

    public int getSearch_count() {
        return search_count;
    }

    public void setSearch_count(int search_count) {
        this.search_count = search_count;
    }

    public int getSeller_fixed() {
        return seller_fixed;
    }

    public void setSeller_fixed(int seller_fixed) {
        this.seller_fixed = seller_fixed;
    }

    public int getSeller_percent() {
        return seller_percent;
    }

    public void setSeller_percent(int seller_percent) {
        this.seller_percent = seller_percent;
    }

    public int getP_views() {
        return p_views;
    }

    public void setP_views(int p_views) {
        this.p_views = p_views;
    }

    public ImagesObject getFb_image() {
        return fb_image;
    }

    public void setFb_image(ImagesObject fb_image) {
        this.fb_image = fb_image;
    }

    public boolean getIs_offer() {
        return is_offer;
    }

    public void setIs_offer(boolean is_offer) {
        this.is_offer = is_offer;
    }

    public int getTime_remaining() {
        return time_remaining;
    }

    public void setTime_remaining(int time_remaining) {
        this.time_remaining = time_remaining;
    }

    public float getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(float discount_price) {
        this.discount_price = discount_price;
    }

    public boolean isIs_offer() {
        return is_offer;
    }

    public List<NewCategoryObject> getCategories() {
        return categories;
    }

    public void setCategories(List<NewCategoryObject> categories) {
        this.categories = categories;
    }

    public int getBounty() {
        return bounty;
    }

    public void setBounty(int bounty) {
        this.bounty = bounty;
    }

    public int getFinal_price() {
        return final_price;
    }

    public void setFinal_price(int final_price) {
        this.final_price = final_price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }
}
