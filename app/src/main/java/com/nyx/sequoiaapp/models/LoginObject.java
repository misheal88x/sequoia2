package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class LoginObject {
    @SerializedName("mobile_number") private String mobile_number = "";
    @SerializedName("password") private String password = "";
    @SerializedName("login_type") private String login_type = "";
    @SerializedName("facebook_id") private String facebook_id = "";
    @SerializedName("access_token") private String access_token = "";

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
