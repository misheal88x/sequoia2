package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 11/9/2019.
 */

public class ReplyObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("content") private String content = "";
    @SerializedName("comment_time") private String comment_time = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("product_id") private int product_id = 0;
    @SerializedName("parent_id") private int parent_id = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComment_time() {
        return comment_time;
    }

    public void setComment_time(String comment_time) {
        this.comment_time = comment_time;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }
}
