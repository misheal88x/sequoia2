package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 10/16/2019.
 */

public class OfferObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("description") private String description = "";
    @SerializedName("fixed") private int fixed = 0;
    @SerializedName("percent") private int percent = 0;
    @SerializedName("condition_qty") private int condition_qty = 0;
    @SerializedName("start_date") private String start_date = "";
    @SerializedName("end_date") private String end_date = "";
    @SerializedName("product_id") private int product_id = 0;
    @SerializedName("view") private int view = 0;
    @SerializedName("product_name") private String product_name = "";
    @SerializedName("product_description") private String product_description = "";
    @SerializedName("price") private int price = 0;
    @SerializedName("prime_price") private int prime_price = 0;
    @SerializedName("discount_price") private int discount_price = 0;
    @SerializedName("images") private ImagesObject images = new ImagesObject();

    public OfferObject() {}

    public OfferObject(int id,
                       String description,
                       int fixed,
                       int percent,
                       int condition_qty,
                       String start_date,
                       String end_date,
                       int product_id,
                       int view,
                       String product_name,
                       String product_description,
                       int price,
                       int prime_price,
                       int discount_price,
                       ImagesObject images) {
        this.id = id;
        this.description = description;
        this.fixed = fixed;
        this.percent = percent;
        this.condition_qty = condition_qty;
        this.start_date = start_date;
        this.end_date = end_date;
        this.product_id = product_id;
        this.view = view;
        this.product_name = product_name;
        this.product_description = product_description;
        this.price = price;
        this.prime_price = prime_price;
        this.discount_price = discount_price;
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFixed() {
        return fixed;
    }

    public void setFixed(int fixed) {
        this.fixed = fixed;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getCondition_qty() {
        return condition_qty;
    }

    public void setCondition_qty(int condition_qty) {
        this.condition_qty = condition_qty;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrime_price() {
        return prime_price;
    }

    public void setPrime_price(int prime_price) {
        this.prime_price = prime_price;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public ImagesObject getImages() {
        return images;
    }

    public void setImages(ImagesObject images) {
        this.images = images;
    }
}
