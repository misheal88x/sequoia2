package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class ShipDeliveryObejct {
    @SerializedName("delivery") private String delivery = "";
    @SerializedName("ship") private String ship = "";

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getShip() {
        return ship;
    }

    public void setShip(String ship) {
        this.ship = ship;
    }
}
