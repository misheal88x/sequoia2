package com.nyx.sequoiaapp.models;

/**
 * Created by Luminance on 5/27/2018.
 */

public class OrderItem{
    private String product_id ,price , qty,size , color , measure;

    public OrderItem(String product_id, String price, String qty, String size, String color, String measure) {
        this.product_id = product_id;
        this.price = price;
        this.qty = qty;
        this.size = size;
        this.color = color;
        this.measure = measure;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
