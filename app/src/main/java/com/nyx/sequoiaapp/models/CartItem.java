package com.nyx.sequoiaapp.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Luminance on 5/20/2018.
 */

public class CartItem implements Serializable {

    private ExtendedPost post;
    private  int quantity;
    private String color , size , measure;

    public CartItem(ExtendedPost post, int quantity, String color, String size , String measure) {
        this.post = post;
        this.quantity = quantity;
        this.color = color;
        this.size = size;
        this.measure = measure;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public ExtendedPost getPost() {
        return post;
    }

    public void setPost(ExtendedPost post) {
        this.post = post;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
