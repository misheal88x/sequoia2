package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class OrderObject {
    @SerializedName("id") String id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
