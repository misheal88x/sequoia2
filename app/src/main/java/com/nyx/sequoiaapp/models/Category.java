package com.nyx.sequoiaapp.models;

/**
 * Created by Luminance on 5/19/2018.
 */

public class Category {
    private String id , name ,desc , parent_id,image;

    public Category() {}

    public Category(String id, String name, String desc, String parent_id,String image) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.parent_id = parent_id;
        this.image = image;
    }

    public Category(String id, String name, String desc, String parent_id) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.parent_id = parent_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
