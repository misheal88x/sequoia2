package com.nyx.sequoiaapp.models;

/**
 * Created by Luminance on 5/24/2018.
 */

public class City {
    private  String id , name;

    public City(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
