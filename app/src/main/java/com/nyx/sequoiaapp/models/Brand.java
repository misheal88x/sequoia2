package com.nyx.sequoiaapp.models;

import org.json.JSONArray;

/**
 * Created by Luminance on 5/24/2018.
 */

public class Brand {
    private String id , name , image,desc,rating;
    private JSONArray image_sliders;

    public Brand(String id, String name, String image, String desc,String rating,JSONArray image_sliders) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.desc = desc;
        this.rating = rating;
        this.image_sliders = image_sliders;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public JSONArray getImage_sliders() {
        return image_sliders;
    }

    public void setImage_sliders(JSONArray image_sliders) {
        this.image_sliders = image_sliders;
    }
}
