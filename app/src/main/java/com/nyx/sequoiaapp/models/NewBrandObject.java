package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 11/1/2019.
 */

public class NewBrandObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("image") private String image = "";
    @SerializedName("description") private String description = "";
    @SerializedName("rating") private String rating = "";
    @SerializedName("image_sliders") private List<BuyerSliderObject> image_sliders = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<BuyerSliderObject> getImage_sliders() {
        return image_sliders;
    }

    public void setImage_sliders(List<BuyerSliderObject> image_sliders) {
        this.image_sliders = image_sliders;
    }
}
