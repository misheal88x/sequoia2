package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/16/2019.
 */

public class ImagesObject {
    @SerializedName("images") private List<String> images = new ArrayList<>();
    @SerializedName("thumbs") private List<String> thumbs = new ArrayList<>();

    public ImagesObject() {
    }

    public ImagesObject(List<String> images, List<String> thumbs) {
        this.images = images;
        this.thumbs = thumbs;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getThumbs() {
        return thumbs;
    }

    public void setThumbs(List<String> thumbs) {
        this.thumbs = thumbs;
    }
}
