package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/23/2019.
 */

public class NewOrderObject {
    @SerializedName("id") private String id = "";
    @SerializedName("create_date") private String create_date = "";
    @SerializedName("delevery_date") private String delevery_date = "";
    @SerializedName("status") private int status = 0;
    @SerializedName("value") private int value = 0;
    @SerializedName("shipping") private int shipping = 0;
    @SerializedName("total") private int total = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("ref_note") private String ref_note = "";
    @SerializedName("notes") private String notes = "";
    @SerializedName("total_after_commission") private int total_after_commission = 0;
    @SerializedName("items") private List<Product> items = new ArrayList<>();
    @SerializedName("sum_commission_fixed") private int sum_commission_fixed = 0;
    @SerializedName("commission_result") private int commission_result = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getDelevery_date() {
        return delevery_date;
    }

    public void setDelevery_date(String delevery_date) {
        this.delevery_date = delevery_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getShipping() {
        return shipping;
    }

    public void setShipping(int shipping) {
        this.shipping = shipping;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getRef_note() {
        return ref_note;
    }

    public void setRef_note(String ref_note) {
        this.ref_note = ref_note;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getTotal_after_commission() {
        return total_after_commission;
    }

    public void setTotal_after_commission(int total_after_commission) {
        this.total_after_commission = total_after_commission;
    }

    public List<Product> getItems() {
        return items;
    }

    public void setItems(List<Product> items) {
        this.items = items;
    }

    public int getSum_commission_fixed() {
        return sum_commission_fixed;
    }

    public void setSum_commission_fixed(int sum_commission_fixed) {
        this.sum_commission_fixed = sum_commission_fixed;
    }

    public int getCommission_result() {
        return commission_result;
    }

    public void setCommission_result(int commission_result) {
        this.commission_result = commission_result;
    }
}
