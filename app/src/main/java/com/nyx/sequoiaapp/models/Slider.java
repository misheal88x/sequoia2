package com.nyx.sequoiaapp.models;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.nyx.sequoiaapp.activity.AllOffersActivity;
import com.nyx.sequoiaapp.activity.CategoriesActivity;
import com.nyx.sequoiaapp.activity.CategoryProductsActivity;
import com.nyx.sequoiaapp.activity.ProductLoadingFromIntent;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Luminance on 5/20/2018.
 */

public class Slider {
    private String id , url , link , product;
    private BaseSliderView.OnSliderClickListener click;

    public BaseSliderView.OnSliderClickListener getClick() {
        return click;
    }

    public Slider(final JSONObject objet ,final Context c) {


     try{
       this.id = objet.getString("id");
         if(objet.getString("target_type").equals("1")){
            click = new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    Intent i = new Intent(c , CategoriesActivity.class);
                    i.putExtra("par", "0");
                    c.startActivity(i);
                }
            };
         }
       else  if(objet.getString("target_type").equals("2")){
             click = new BaseSliderView.OnSliderClickListener() {
                 @Override
                 public void onSliderClick(BaseSliderView slider) {

                     try {

                     boolean hasChildren =
                             SharedPrefManager.getInstance(c).getCategories(
                                     objet.getString("target_id")).size()>0;
                     if(hasChildren) {
                         Intent ii = new Intent(c, CategoriesActivity.class);
                         ii.putExtra("par", objet.getString("target_id"));
                         ii.putExtra("name", "");
                         c.startActivity(ii);
                     }else{
                         Intent ii = new Intent(c, CategoryProductsActivity.class);
                         ii.putExtra("id", objet.getString("target_id"));
                         ii.putExtra("title", "");
                         c.startActivity(ii);
                     }




                     } catch (JSONException e) {
                         e.printStackTrace();
                     }


                 }
             };
         }

       else  if(objet.getString("target_type").equals("3")){
             click = new BaseSliderView.OnSliderClickListener() {
                 @Override
                 public void onSliderClick(BaseSliderView slider) {
                     Intent ii = new Intent(c, ProductLoadingFromIntent.class);
                     try {
                         ii.putExtra("id", objet.getString("target_id"));
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                     ii.putExtra("title", "");
                     c.startActivity(ii);
                 }
             };
         }

      else   if(objet.getString("target_type").equals("4")){
             click = new BaseSliderView.OnSliderClickListener() {
                 @Override
                 public void onSliderClick(BaseSliderView slider) {
                     Intent ii = new Intent(c, AllOffersActivity.class);
                     c.startActivity(ii);
                 }
             };
         }


       else  if(objet.getString("target_type").equals("5")){
             click = new BaseSliderView.OnSliderClickListener() {
                 @Override
                 public void onSliderClick(BaseSliderView slider) {
                     String url = null;
                     try {
                         url = objet.getString("target_id");
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                     Intent i = new Intent(Intent.ACTION_VIEW);
                     i.setData(Uri.parse(url));
                    c. startActivity(i);
                 }
             };
         }
       else  if(objet.getString("target_type").equals("7")){
             click = new BaseSliderView.OnSliderClickListener() {
                 @Override
                 public void onSliderClick(BaseSliderView slider) {
                     String url = "";
                     try {
                         url = objet.getString("target_id");
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                     Intent intent = new Intent(Intent.ACTION_DIAL);
                     intent.setData(Uri.parse("tel:"+url));
                    c. startActivity(intent);
                 }
             };
         }
         else if(objet.getString("target_type").equals("-1")){
             click=new BaseSliderView.OnSliderClickListener() {
                 @Override
                 public void onSliderClick(BaseSliderView slider) {


                 }
             };
         }

       this.url = new JSONObject(objet.getString("background"))
               .getJSONArray("images").getString(0);

     }catch (JSONException ex){}
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return APIUrl.POSTS_PICS_SERVER + url;
    }


}
