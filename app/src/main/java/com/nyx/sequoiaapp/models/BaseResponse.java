package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 10/16/2019.
 */

public class BaseResponse {
    @SerializedName("status") private int status = 0;
    @SerializedName("message_id") private int message_id = 0;
    @SerializedName("message") private String message = "";
    @SerializedName("data") private Object data = new Object();

    public BaseResponse(int status, int message_id, String message, Object data) {
        this.status = status;
        this.message_id = message_id;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
