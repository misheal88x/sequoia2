package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/21/2019.
 */

public class SearchResponse {
    @SerializedName("categories") private List<Category> categories = new ArrayList<>();
    @SerializedName("products") private ProductsResponse products = new ProductsResponse();

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public ProductsResponse getProducts() {
        return products;
    }

    public void setProducts(ProductsResponse products) {
        this.products = products;
    }
}
