package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

public class CommissionObject {
    @SerializedName("name") private String name = "";
    @SerializedName("commission") private String commission = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }
}
