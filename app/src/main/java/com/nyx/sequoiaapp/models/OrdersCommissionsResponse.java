package com.nyx.sequoiaapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 15/11/2019.
 */

public class OrdersCommissionsResponse {
    @SerializedName("orders_count") private List<StatisticsObject> orders_count = new ArrayList<>();
    @SerializedName("seqouia_commissions_for_orders") private List<NewOrderObject> seqouia_commissions_for_orders = new ArrayList<>();
    @SerializedName("toPaidCommission") private List<SumCommisionsObject> toPaidCommission = new ArrayList<>();

    public List<StatisticsObject> getOrders_count() {
        return orders_count;
    }

    public void setOrders_count(List<StatisticsObject> orders_count) {
        this.orders_count = orders_count;
    }

    public List<NewOrderObject> getSeqouia_commissions_for_orders() {
        return seqouia_commissions_for_orders;
    }

    public void setSeqouia_commissions_for_orders(List<NewOrderObject> seqouia_commissions_for_orders) {
        this.seqouia_commissions_for_orders = seqouia_commissions_for_orders;
    }

    public List<SumCommisionsObject> getToPaidCommission() {
        return toPaidCommission;
    }

    public void setToPaidCommission(List<SumCommisionsObject> toPaidCommission) {
        this.toPaidCommission = toPaidCommission;
    }
}
