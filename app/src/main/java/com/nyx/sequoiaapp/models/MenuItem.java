package com.nyx.sequoiaapp.models;

/**
 * Created by Luminance on 5/29/2018.
 */

 public abstract class MenuItem {
    private String title;
    private int icon;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public MenuItem(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }
     public abstract void doTask();

}
