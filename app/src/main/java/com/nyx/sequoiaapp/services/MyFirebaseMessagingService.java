package com.nyx.sequoiaapp.services;

/**
 * Created by Luminance on 5/4/2018.
 */


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.ChatActivity;
import com.nyx.sequoiaapp.activity.LoginActivty;
import com.nyx.sequoiaapp.activity.NotificationsActivity;
import com.nyx.sequoiaapp.activity.OrderDetailsActivity;
import com.nyx.sequoiaapp.activity.ProductLoadingFromIntent;
import com.nyx.sequoiaapp.app.Config;
import com.nyx.sequoiaapp.helper.SharedPrefManager;

import org.json.JSONObject;

import java.util.Map;


/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "SEQ_FIRBASE";
    public static final String NOTIFICATION_CHANNEL_ID = "10001";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG ,"message arrived !");
        Map<String,String> keys  = remoteMessage.getData();

        JSONObject jsonObject = new JSONObject(keys);
        Log.i("JSON_OBJECT", "the : " + jsonObject.toString());
        String   target_id = keys.get("target_id");
        String target_type = keys.get("target_type");
        String title = keys.get("title");
        String body = keys.get("text");
        int noti_count = SharedPrefManager.getInstance(this).getNotiCount();
        noti_count++;
        SharedPrefManager.getInstance(this).saveNotiCount(noti_count);

        // app is in background, show the notification in notification tray
        Intent resultIntent = new Intent(getApplicationContext(), NotificationsActivity.class);
        if (target_type.toString().equals("1") || target_type.toString().equals("2")||target_type.equals("121")||
                target_type.equals("212")) {
            resultIntent = new Intent(getApplicationContext(), ProductLoadingFromIntent.class);
            resultIntent.putExtra("id", target_id.toString());
        }else if (target_type.toString().equals("3")) {
            resultIntent = new Intent(getApplicationContext(), ChatActivity.class);
            resultIntent.putExtra("title","الدعم");
            //Converting user to Seller
        }else if (target_type.equals("120")){
            resultIntent = new Intent(getApplicationContext(), LoginActivty.class);
            SharedPrefManager.getInstance(getApplication()).Logout();
            try {
                //startActivity(new Intent(getApplicationContext(),LoginActivty.class));
            }catch (Exception e){}

        }else if (target_type.equals("122")){
            resultIntent = new Intent(getApplicationContext(), OrderDetailsActivity.class);
            resultIntent.putExtra("order_id",String.valueOf(target_id));
            resultIntent.putExtra("type","with");

        }
        final int icon = R.drawable.high_logo;

//        showNotificationMessage(getApplicationContext(),
//                title,
//                body, System.currentTimeMillis()+"", resultIntent);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this);


        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(body);
        final Uri alarmSound = (Uri.parse("android.resource://" +
               getPackageName() + "/" + R.raw.seq_2));
        NotificationCompat.Builder notification;
        if (target_type.equals("122")){
            Intent intent = new Intent(getApplicationContext(),OrderDetailsActivity.class);
            intent.putExtra("order_id",String.valueOf(target_id));
            intent.putExtra("type","with");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent orderPendingIntent = PendingIntent.getActivity(getApplicationContext(),0,intent,PendingIntent.FLAG_ONE_SHOT);
            notification = new NotificationCompat.Builder(getApplicationContext()).setSmallIcon(icon).setTicker(title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(resultPendingIntent)
                    .setStyle(inboxStyle)
                    .setSound(alarmSound)
                    .setVibrate(new long[]{500,500,500})
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.high_logo)
                    //.setChannelId(Config.NOTIFICATION_ID+"")
                    .setContentText(body)
                    .addAction(R.drawable.high_logo,"تأكيد استلام الطلب",orderPendingIntent);
        }else {
            notification = new NotificationCompat.Builder(getApplicationContext()).setSmallIcon(icon).setTicker(title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(resultPendingIntent)
                    .setStyle(inboxStyle)
                    .setSound(alarmSound)
                    .setVibrate(new long[]{500,500,500})
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.high_logo)
                    //.setChannelId(Config.NOTIFICATION_ID+"")
                    .setContentText(body);
        }

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{500, 500, 500});
            assert notificationManager != null;
            notification.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(Config.NOTIFICATION_ID, notification.build());




        //  handleDataMessage(remoteMessage);
    }
}