package com.nyx.sequoiaapp.costumes;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.adapters.OrderItemsAdapter;
import com.nyx.sequoiaapp.models.ExtendedOrder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Luminance on 5/28/2018.
 */
public class CustomDialogClassExtended extends Dialog implements
        View.OnClickListener {

    public Activity c;
    public Dialog d;
    public ImageButton continue_nw;
    public ExtendedOrder o;

    public CustomDialogClassExtended(Activity a, ExtendedOrder s) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.o = s;
    }
/*

 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);

        continue_nw = (ImageButton) findViewById(R.id.continue_now);
        ((TextView) findViewById(R.id.txt_dia)).setText("تفاصيل " + o.getId());
        ((TextView) findViewById(R.id.text_user_details))
                .setText(o.getUser_details());
        ArrayList<String> info = new ArrayList<>();
        double total = 0.0;
        try {
            JSONArray contents = o.getItems();
            for (int i = 0; i < contents.length(); i++) {
                double price =
                        Double.parseDouble(contents.getJSONObject(i).getString("qty")) *
                                Double.parseDouble(contents.getJSONObject(i).getString("price"));
                total += price;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RecyclerView lv = (RecyclerView) findViewById(R.id.details);
        OrderItemsAdapter adapter = new OrderItemsAdapter(o.getItems() , c);
        LinearLayoutManager lm = new LinearLayoutManager(c) ;
        lv.setLayoutManager(lm);
        lv.setAdapter(adapter);
        ((TextView) findViewById(R.id.price)).setText("إجمالي الطلبية : " +
                o.getTotal() + " " + c.getResources().getString(R.string.curremcy));
        continue_nw.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continue_now:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}