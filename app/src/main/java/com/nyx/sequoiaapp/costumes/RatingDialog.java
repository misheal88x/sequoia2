package com.nyx.sequoiaapp.costumes;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.nyx.sequoiaapp.R;

/**
 * Created by Luminance on 7/21/2018.
 */

public abstract class RatingDialog extends Dialog {

    public Activity c;
    public Dialog d;
  public EditText input;
    Button yes ,no;
    String r;

    public RatingDialog(Activity a ,String t) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.r=t;

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.rating_dialog);
    yes = (Button)findViewById(R.id.btn_yes);
    no = (Button)findViewById(R.id.btn_no);
        input = (EditText)findViewById(R.id.txt_dia);
        input.setHint(r);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
     /*  if(input.getText().toString().trim().equals("")){
           Toast.makeText(c, "ادخل التقييم من فضلك..", Toast.LENGTH_SHORT).show();
           return;
       }*/
        onYesClicked(input);
    }
});

    }

    public abstract void onYesClicked(final EditText t);


}