package com.nyx.sequoiaapp.costumes;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;

import com.nyx.sequoiaapp.adapters.SliderAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Luminance on 6/28/2018.
 */

public class SequoiaViewPager extends ViewPager {

  private  ArrayList items;
    private Timer timer;

    public SequoiaViewPager(@NonNull Context context) {
        super(context);
    }

    public SequoiaViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setItems(ArrayList items) {
        this.items = items;
    }

    public void init(final Activity a){
        this.setAdapter(new SliderAdapter(a ,items));
        if(items.size()<1 || a==null  || a.isFinishing())return;

        timer = new Timer();
        TimerTask task  = new TimerTask() {
            @Override
            public void run() {
                if(a==null  || a.isFinishing())return;
               a.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (getCurrentItem() < items.size() - 1) {
                            setCurrentItem(getCurrentItem() + 1);
                        } else {
                            timer.cancel();
                        }
                    }
                });
            }
        };


        timer.scheduleAtFixedRate(task, 5000, 6000);
    }
public void release(){
    if(timer!=null) {
        timer.cancel();
        timer = null;
    }
}
}
