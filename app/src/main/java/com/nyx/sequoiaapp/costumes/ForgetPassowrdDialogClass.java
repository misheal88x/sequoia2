package com.nyx.sequoiaapp.costumes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;

import java.util.HashMap;

/**
 * Created by Luminance on 5/28/2018.
 */
public class ForgetPassowrdDialogClass extends Dialog {

    public Activity c;
    public Dialog d;
    public Button yes,no;
    private TextView tv_whatsapp;

    public ForgetPassowrdDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }
/*

 */


    void go_forget_password(final int try_ , final String email ){
     yes.setEnabled(false);
        final HashMap<String  ,String> params=new HashMap<String, String>();
        params.put("email" ,email);
        BackgroundServices.getInstance(c).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if(!s.equals("")){

                    try {

                        Toast.makeText(c, "تم الارسال" +
                                " , قم بتفقد ايميلك ..", Toast.LENGTH_SHORT).show();
                        dismiss();

                    }catch (Exception fdr){
                        //Log.d("FDR SIGNUP : " , fdr.getMessage());
                        LoginManager.getInstance().logOut();
                    }
                }else{
                    if(try_<10)go_forget_password(try_+1 , email);
                    else
                    {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        }  , APIUrl.SERVER + "user/reset_password",params);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.reset_pasword_dialog);
        yes =  findViewById(R.id.go_send);
        no =  findViewById(R.id.go_cancel);
        tv_whatsapp = findViewById(R.id.reset_whatsapp);
       final EditText email =  findViewById(R.id.input_email);

        findViewById(R.id.go_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dismiss();
            }
        });   findViewById(R.id.go_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = email.getText().toString();
                if(s.equals("")){
                    Toast.makeText(c, "أدخل الايميل من فضلك ..", Toast.LENGTH_SHORT).show();
                    return;
                }
                go_forget_password(0,s);

            }
        });
        tv_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone=+963 "+"949101014";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                c.startActivity(i);
            }
        });



    }


}