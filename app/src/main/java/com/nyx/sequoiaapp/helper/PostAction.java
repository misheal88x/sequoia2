package com.nyx.sequoiaapp.helper;

/**
 * Created by Luminance on 5/19/2018.
 */

public interface PostAction {
    public abstract void doTask();
    public abstract void doTask(String s);
}
