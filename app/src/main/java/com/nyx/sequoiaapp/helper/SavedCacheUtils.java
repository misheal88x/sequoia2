package com.nyx.sequoiaapp.helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.nyx.sequoiaapp.models.Cart;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.models.ExtendedPost;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Luminance on 5/20/2018.
 */

public class SavedCacheUtils {


    public static boolean updateInFavourite(Context c, ExtendedPost p) {

        try {
            ArrayList<ExtendedPost> favs = SharedPrefManager.getInstance(c).readFavourite();
            for (int i = 0; i < favs.size(); i++)
                if (favs.get(i).getId().equals(p.getId())) {
                    favs.set(i, p);
                    SharedPrefManager.getInstance(c).saveFavourites(favs);
                    return true;
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isInFavourietes(Context c, String id) {

        try {
            ArrayList<ExtendedPost> favs = SharedPrefManager.getInstance(c).readFavourite();
            for (int i = 0; i < favs.size(); i++)
                if (favs.get(i).getId().equals(id)) {

                    return true;
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static ArrayList<ExtendedPost> getFavourites(Context c) {

        try {
            return SharedPrefManager.getInstance(c).readFavourite();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void removeFromFavourite(Context c, String id) {

        try {
            ArrayList<ExtendedPost> favs = SharedPrefManager.getInstance(c).readFavourite();
            for (int i = 0; i < favs.size(); i++)
                if (favs.get(i).getId().equals(id)) {
                    favs.remove(i);
                    SharedPrefManager.getInstance(c).saveFavourites(favs);
                    break;
                }

        } catch (Exception e) {
            Log.e("DB6" ,e.getMessage());
            e.printStackTrace();
        }
    }

    public static boolean addToFavourites(Context c, ExtendedPost p) {

        try {
            ArrayList<ExtendedPost> favs = SharedPrefManager.getInstance(c).readFavourite();
            boolean found = false;
            for (int i = 0; i < favs.size(); i++)
                if (favs.get(i).getId().equals(p.getId())) {
                    found = true;
                    break;
                }
            if (!found) {
                if (favs.size() < 50) {
                    favs.add(p);
                    SharedPrefManager.getInstance(c).saveFavourites(favs);
                    return true;
                }else{
                    Toast.makeText(c, "لا يمكنك حفظ أكثر من 50 منتجا في المفضلة..", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }else{
                Toast.makeText(c, "العنصر موجود في مفضلتك ..", Toast.LENGTH_SHORT).show();
                return false;
            }

        } catch (Exception e) {
            Log.e("DB7" ,e.getMessage());
            e.printStackTrace();
        }
        Toast.makeText(c, "لم يتم الاضافة", Toast.LENGTH_SHORT).show();
        return false;
    }


    public static Cart getCart(Context c) {
        try {
            return SharedPrefManager.getInstance(c).readCart();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getNotiCount(Context c){
        try {
            return SharedPrefManager.getInstance(c).getNotiCount();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void clearCart(Context c) {
        try {
            SharedPrefManager.getInstance(c).saveCart(new Cart(new Date(), new ArrayList<CartItem>()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateCartItemQuantity(Context c, String id, int quantity) {
        Cart cart = getCart(c);
        cart.changeItemQuantity(id, quantity);
        try {
            SharedPrefManager.getInstance(c).saveCart(cart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFromCart(Context c, String id) {
        Cart cart = getCart(c);
        cart.deleteFromCart(id);
        try {
            SharedPrefManager.getInstance(c).saveCart(cart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean addToCart(Context c, ExtendedPost p, int q , String color , String size , String measure) {
        Cart cart = getCart(c);
        if (cart.getItems().size() > 20) {
            Toast.makeText(c, "السلة ممتلئة ! لا يمكن وضع اكثر من 20 عنصر", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (cart.checkIfExists(p.getId())) {
            Toast.makeText(c, "المنتج موجود في سلتك ! تم تحديث قيمة الكمية", Toast.LENGTH_SHORT).show();
            cart.changeItemQuantity(p.getId(), q);
            try {
                SharedPrefManager.getInstance(c).saveCart(cart);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        cart.addToCart(p, q , color , size , measure);
        try {
            SharedPrefManager.getInstance(c).saveCart(cart);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(c, "حدث خطأ اثناء الاضافة للسلة !", Toast.LENGTH_SHORT).show();
            return false;
        }

    }


}
