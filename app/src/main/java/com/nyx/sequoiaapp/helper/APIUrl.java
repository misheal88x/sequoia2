package com.nyx.sequoiaapp.helper;

/**
 * Created by Luminance on 2/27/2018.
 */

public class APIUrl {

 //public static final String SERVER = "http://e-sequoia.net/seqouia/public/api/v2/";
 public static final String SERVER = "http://e-sequoia.net/test-api/public/api/v2/";
 public static final String SERVER2 = "http://e-sequoia.net/test-api/public/api/v2/";
 public static final String SERVER3 = "http://e-sequoia.net/test-api/public/api/v4/";

 public static final String POSTS_PICS_SERVER =
         "http://e-sequoia.net/upload/product/";
 public static final String CATEGORIES_PICS_SERVER =
         "http://e-sequoia.net/upload/category/";
 public static final String USERS_PICS_SERVER =
         "http://e-sequoia.net/upload/users/";
 public static final String SELLERS_SLIDERS_SERVER =
         "http://e-sequoia.net/upload/";
 //For banners
 public static final String ADS_PICS_SERVER =
         "http://e-sequoia.net/upload/ads/";
 public static final String token = "6c4cec0e9a851b1fdc9ed5981adab4f24dabe76b9daa7cd92872f6f30abbc5c3";

}