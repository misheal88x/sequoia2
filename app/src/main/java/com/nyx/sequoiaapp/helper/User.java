package com.nyx.sequoiaapp.helper;

/**
 * Created by Luminance on 2/27/2018.
 */

public class User {
    private String user_id , name , user_type ,phone , password , email , city ,address , token , gender;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public User(String user_id, String name, String user_type, String phone, String password, String email, String city, String address, String token,String gender) {
        this.user_id = user_id;
        this.name = name;
        this.user_type = user_type;
        this.phone = phone;
        this.password = password;
        this.email = email;
        this.city = city;
        this.address = address;
        this.token = token;
        this.gender = gender;
    }
}
