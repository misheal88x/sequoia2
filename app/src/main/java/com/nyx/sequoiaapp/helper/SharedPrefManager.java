package com.nyx.sequoiaapp.helper;

/**
 * Created by Luminance on 1/18/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.nyx.sequoiaapp.models.Brand;
import com.nyx.sequoiaapp.models.BuyerSliderObject;
import com.nyx.sequoiaapp.models.Cart;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.models.Category;
import com.nyx.sequoiaapp.models.City;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.models.LoginObject;
import com.nyx.sequoiaapp.models.NormalBuyerObject;
import com.nyx.sequoiaapp.models.Slider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class SharedPrefManager {

    private static SharedPrefManager mInstance;
    private static Context mCtx;


    private static final String SHARED_PREF_NAME = "nyx.com.sequoia.v3";
    private static final String KEY_USER_ID = "key11";
    private static final String KEY_USER_NAME = "key22";
    private static final String KEY_USER_TYPE = "key33";
    private static final String KEY_USER_PHONE = "key44";
    private static final String KEY_USER_PASSWORD = "key55";
    private static final String KEY_USER_EMAIL = "key66";
    private static final String KEY_USER_CITY = "key77";
    private static final String KEY_USER_ADDRESS = "key88";
    private static final String KEY_USER_TOKEN = "key99";
    private static final String KEY_USER_PIC= "key9679";
    private static final String KEY_USER_GENDER = "key1010";

    private static final String KEY_INIT_DATA = "keyalldatav2";
    private static final String KEY_CART = "ketseqcartv2";
    private static final String KEY_FAVOURITE = "ketseqfavouritev2";
    private static final String KEY_STORE_UPDATED = "storeupdated";
    private static final String KEY_TUTOIAL_SHOWN = "tutorial";
    private static final String KEY_LOGIN_TYPE = "login_type";
    private static final String KEY_LOGIN_DATA = "login_data";
    private static final String KEY_NOTI_COUNT = "noti_count";
    private SharedPrefManager(Context context) {
        mCtx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }
    //Save user data when login
    public boolean userLogin(JSONObject user, String password) throws Exception {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_ID, user.getString("id"));
        editor.putString(KEY_USER_NAME, user.getString("fullname"));
        editor.putString(KEY_USER_TYPE, user.getString("type"));
        //editor.putString(KEY_USER_TYPE, "2");
        editor.putString(KEY_USER_PHONE, user.getString("mobile"));
        editor.putString(KEY_USER_PASSWORD, password);
        editor.putString(KEY_USER_EMAIL, user.getString("email"));
        editor.putString(KEY_USER_CITY, user.getString("city"));
        editor.putString(KEY_USER_ADDRESS, user.getString("address"));
        editor.putString(KEY_USER_TOKEN, user.getString("reset_token"));
        editor.putString(KEY_USER_PIC, user.getString("image"));
        editor.putString(KEY_USER_GENDER,user.getString("gender"));
        editor.apply();
        return true;
    }

    public void setGender(String gender){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_GENDER,gender);
        editor.apply();
    }
    //Method to save the data comming from splash activity
    public boolean saveData(String data) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_INIT_DATA, data);
        editor.apply();
        return true;
    }

    public void saveLoginType(String type){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LOGIN_TYPE,type);
        editor.apply();
    }

    public void saveNotiCount(int count){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_NOTI_COUNT,count);
        editor.apply();
    }

    public String getLoginType(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_LOGIN_TYPE,"");
    }

    public int getNotiCount(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_NOTI_COUNT,0);
    }
    public void saveLoginData(LoginObject loginObject){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LOGIN_DATA,new Gson().toJson(loginObject));
        editor.apply();
    }

    public LoginObject getLoginData(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (!sharedPreferences.getString(KEY_LOGIN_DATA,"").equals("")){
            LoginObject loginObject = new Gson().fromJson(sharedPreferences.getString(KEY_LOGIN_DATA,""),LoginObject.class);
            return loginObject;
        }else {
            return new LoginObject();
        }
    }



    public ArrayList<ExtendedPost> readFavourite() throws Exception {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String fav = sharedPreferences.getString(KEY_FAVOURITE, "");
        if (fav.equals("")) return new ArrayList<ExtendedPost>();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<ExtendedPost>>() {
        }.getType();
        return (ArrayList<ExtendedPost>) gson.fromJson(fav, type);
    }


    public boolean saveFavourites(ArrayList<ExtendedPost> favs) throws Exception {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<ExtendedPost>>() {
        }.getType();
        String jsonInString = gson.toJson(favs, type);
        editor.putString(KEY_FAVOURITE, jsonInString);
        editor.apply();
        return true;
    }


    public Cart readCart() throws Exception {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String cart = sharedPreferences.getString(KEY_CART, "");
        if (cart.equals("")) return new Cart(new Date(), new ArrayList<CartItem>());
        Gson gson = new Gson();
        return gson.fromJson(cart, Cart.class);
    }


    public boolean saveCart(Cart cart) throws Exception {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String jsonInString = gson.toJson(cart);
        editor.putString(KEY_CART, jsonInString);
        editor.apply();
        return true;
    }


    public ArrayList getExtendedPosts(String[] args) {
        String fetchType = args[0];
        String key = "";
        if (fetchType.equals("home")) {
            key = args[1];
        }

        SharedPreferences sharedPreferences =
                mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        String debug="";
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray(key);
                for (int i = 0; i < array.length(); i++) {
//                    String pic = "";
//                    String[] pics=new String[0];
                    JSONObject object = array.getJSONObject(i);
//                     String quer = object.getString("t");
//                  //  if (quer != null && !quer.equals("null"))
//                    try {
//
//                        JSONObject thumbs = new JSONObject(quer);
//                        JSONArray ag= thumbs.getJSONArray("thumbs");
//                        if (ag.length() > 0) {
//                            pic = APIUrl.POSTS_PICS_SERVER + ag.getString(0);
//                        }
//
//                            JSONArray picsJson =object.getJSONArray("all_pics");
//                            pics = new String[picsJson.length()];
//                            for (int j = 0; j < picsJson.length(); j++)
//                                pics[j] =
//                                        //APIUrl.POSTS_PICS_SERVER +
//                            picsJson.getString(j);
//
//                    }catch (Exception e){Log.e("EFC139",object.getString("imgs")
//                            + "nana :  "+ e.getMessage());}
//                    ExtendedPost item=null;


                    JSONObject jox = array.getJSONObject(i);
                    debug = jox.getString("id");


                    data.add(new ExtendedPost(jox
                    ));
                }
            }

        } catch (JSONException jsx) {
            Log.e("EFC123 IN "+key + " id : " + debug, jsx.getMessage());
        }
      //  Collections.shuffle(data);
        return data;
    }


    public ArrayList getSliderItems() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("slider");
                for (int i = 0; i < array.length(); i++) {
                    data.add(new Slider(array.getJSONObject(i) ,mCtx));
                }
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return data;
    }

    public List<NormalBuyerObject> getNormalBuyers() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        List<NormalBuyerObject> data = new ArrayList<>();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("normal_buyers");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jo = array.getJSONObject(i);
                    List<BuyerSliderObject> listOfSliders = new ArrayList<>();
                    JSONArray joSliders = jo.getJSONArray("image_sliders");
                    if (joSliders!=null&&joSliders.length()>0){
                        for (int j = 0; j <joSliders.length() ; j++) {
                            BuyerSliderObject bso = new BuyerSliderObject();
                            bso.setId(joSliders.getJSONObject(j).getString("id"));
                            bso.setImage_url(joSliders.getJSONObject(j).getString("image_url"));
                            listOfSliders.add(bso);
                        }
                    }
                    NormalBuyerObject nbo = new NormalBuyerObject();
                    nbo.setId(jo.getString("id"));
                    nbo.setMobile(jo.getString("mobile"));
                    nbo.setStore_name(jo.getString("store_name"));
                    nbo.setFullname(jo.getString("fullname"));
                    nbo.setImage(jo.getString("image"));
                    nbo.setCity_name(jo.getString("city_name"));
                    nbo.setRating(jo.getString("rating"));
                    nbo.setImage_sliders(listOfSliders);
                    data.add(nbo);
                }
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return data;
    }

    public String getCurrentVersion(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                String version = new JSONObject(all).getJSONObject("data").getJSONObject("update_app_condition").getString("current_version");
                return version;
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return "";
    }
    public boolean getCanIgnore(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                boolean igonre = new JSONObject(all).getJSONObject("data").getJSONObject("update_app_condition").getBoolean("can_ignore");
                return igonre;
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return false;
    }
    public ArrayList getBrands() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("brands");
                for (int i = 0; i < array.length(); i++) {
                    data.add(new Brand(array.getJSONObject(i).getString("id")
                            ,array.getJSONObject(i).getString("name")
                            ,array.getJSONObject(i).getString("image")
                            ,array.getJSONObject(i).getString("description")
                            ,array.getJSONObject(i).getString("rating")
                            ,array.getJSONObject(i).getJSONArray("image_sliders")));

                }
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return data;
    }
    public String getSHareLink() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                String url = new JSONObject(all).getJSONObject("data").getString("share_url");
               return url;
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return "";
    }
    public ArrayList getCities() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("cities");
                for (int i = 0; i < array.length(); i++) {
                    data.add(new City(array.getJSONObject(i).getString("id") ,array.getJSONObject(i).getString("name")));

                }
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        return data;
    }
    public ArrayList getBanners() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("ads");
                for (int i = 0; i < array.length(); i++) {
                    data.add(array.getJSONObject(i));
                }
            }
        } catch (JSONException jsx) {
            Log.e("EFC124", jsx.getMessage());
        }
        Collections.shuffle(data);
        return data;
    }

    public int getLowPrice(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        int result = 0;
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            JSONObject o = new JSONObject(all).getJSONObject("data").getJSONObject("free_delivery_conditions");
            result = o.getInt("low_price");
        }catch (JSONException jsx){}
        return result;
    }

    public int getValueToPay(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        int result = 0;
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            JSONObject o = new JSONObject(all).getJSONObject("data").getJSONObject("free_delivery_conditions");
            result = o.getInt("value_to_pay");
        }catch (JSONException jsx){}
        return result;
    }

    public String getFreeDeliveryDetails(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String result = "";
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            result = new JSONObject(all).getJSONObject("data").getString("free_delivery_details");
        }catch (JSONException jsx){}
        return result;
    }



    public ArrayList getFeaturedCategories() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("featured_categories");
                for (int i = 0; i < array.length(); i++) {
                        Category item = new Category(
                                array.getJSONObject(i).getString("id"),
                                array.getJSONObject(i).getString("parent_name")+
                                        (array.getJSONObject(i).getString("parent_name").trim().equals("")?"":" - ")
                                        +
                                        array.getJSONObject(i).getString("category_name")
                                  //      + " ("
                                //+array.getJSONObject(i).getString("total_count")+" منتج )"
                                 ,
                                ""
                                ,
                               "",
                                array.getJSONObject(i).getString("image"));
                        data.add(item);
                }
            }

        } catch (JSONException jsx) {
            Log.e("EFC123", jsx.getMessage());
        }
        return data;
    }


    public ArrayList getCategories(String pid) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        ArrayList data = new ArrayList();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("categories");
                for (int i = 0; i < array.length(); i++) {
                    if (pid.equals(array.getJSONObject(i).getString("pid"))) {
                        Category item = new Category(
                                array.getJSONObject(i).getString("id"),
                                array.getJSONObject(i).getString("n"),
                                array.getJSONObject(i).getString("d"),
                                array.getJSONObject(i).getString("pid"),
                                array.getJSONObject(i).getString("image"));
                        data.add(item);
                    }
                }
            }

        } catch (JSONException jsx) {
            Log.e("EFC123", jsx.getMessage());
        }
        return data;
    }

    public Category getCategory(String id){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Category c = new Category();
        try {
            String all = sharedPreferences.getString(KEY_INIT_DATA, "");
            if (!all.equals("")) {
                JSONArray array = new JSONObject(all).getJSONObject("data").getJSONArray("categories");
                for (int i = 0; i < array.length(); i++) {
                    if (id.equals(array.getJSONObject(i).getString("id"))) {
                        c.setId(array.getJSONObject(i).getString("id"));
                        c.setName(array.getJSONObject(i).getString("n"));
                        c.setDesc(array.getJSONObject(i).getString("d"));
                        c.setParent_id(array.getJSONObject(i).getString("pid"));
                    }
                }
            }
        }catch (JSONException jsx){
            Log.e("EFC123", jsx.getMessage());
        }
        return c;
    }
    public String getUserPic() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return   sharedPreferences.getString(KEY_USER_PIC, "");
    }

    public void setUserPic(String image){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_PIC,image);
        editor.apply();
    }

    public User getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getString(KEY_USER_ID, null),
                sharedPreferences.getString(KEY_USER_NAME, null),
                sharedPreferences.getString(KEY_USER_TYPE, null),
                sharedPreferences.getString(KEY_USER_PHONE, null),
                sharedPreferences.getString(KEY_USER_PASSWORD, null),
                sharedPreferences.getString(KEY_USER_EMAIL, null) ,
                sharedPreferences.getString(KEY_USER_CITY, null) ,
                sharedPreferences.getString(KEY_USER_ADDRESS, null) ,
                sharedPreferences.getString(KEY_USER_TOKEN, null),
                sharedPreferences.getString(KEY_USER_GENDER,null)
        );
    }

    //Method to check if splash activity api has data
    public boolean hasData() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String o = sharedPreferences.getString(KEY_INIT_DATA, null);
        if( o != null && !o.trim().equals(""))
        {
         try{
             JSONObject k = new JSONObject(o);
             return true;
         }catch (JSONException j){
             return false;
         }
        }
        return false;
    }

    public boolean IsUserLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String o = sharedPreferences.getString(KEY_USER_ID, null);
        return o != null && !o.trim().equals("");

    }


    public void Logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_ID, "");
        editor.putString(KEY_USER_PIC, "");
        editor.putString(KEY_USER_TYPE,"");
        editor.putBoolean(KEY_STORE_UPDATED,false);
        SavedCacheUtils.clearCart(mCtx);
        editor.apply();
        //LoginManager.getInstance().logOut();
    }

    public void setStoreUpdated(boolean in){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_STORE_UPDATED,in);
        editor.apply();
    }

    public boolean getStoreUpdated(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_STORE_UPDATED,false);
    }

    public void setTutorial(boolean value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_TUTOIAL_SHOWN,value);
        editor.apply();
    }
    public boolean getTutorial(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_TUTOIAL_SHOWN,false);
    }
}