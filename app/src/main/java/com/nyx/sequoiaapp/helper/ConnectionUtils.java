package com.nyx.sequoiaapp.helper;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
/**
 * Created by Luminance on 2/11/2018.
 */
public class ConnectionUtils {

    public static  boolean isNetworkAvailable(Context a) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String sendPostRequest(String requestURL,
                                  HashMap<String, String> params , boolean isGet) {

        Log.d("okhttp_url"  , requestURL);

        String response = "";
        Request request = null;
        OkHttpClient client = new OkHttpClient();

        if(!isGet){
        MultipartBody.Builder mb =  new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (Map.Entry<String, String> entry : params.entrySet()) {
            mb.addFormDataPart(entry.getKey(), entry.getValue());
        }
        mb.addFormDataPart("time", System.currentTimeMillis()+"");
         request = new Request.Builder()
                .url(requestURL)
                  .post(mb.build())
                  .addHeader("token", "6c4cec0e9a851b1fdc9ed5981adab4f24dabe76b9daa7cd92872f6f30abbc5c3")
                  .addHeader("X-localization", "ar")
                  .build();}
        else
            request = new Request.Builder()
                    .url(requestURL)
                    .get()
                    .addHeader("token", "6c4cec0e9a851b1fdc9ed5981adab4f24dabe76b9daa7cd92872f6f30abbc5c3")
                    .addHeader("X-localization", "ar")
                    .build();
        Response responses = null;

        try {
            responses = client.newCall(request).execute();
        } catch (IOException e) {

          //Log.d("RET ERR "  , e.getMessage());
        }
        try {
            response = responses.body().string();
            Log.d("OKHTTP3 : "  ,response);

        }catch (IOException e){    //Log.d("RET2 ERR "  , e.getMessage());
             }
   return response;
    }

}
