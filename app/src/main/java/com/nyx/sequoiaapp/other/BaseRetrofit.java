package com.nyx.sequoiaapp.other;

import android.content.SharedPreferences;
import android.webkit.URLUtil;

import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.helper.APIUrl;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Misheal on 10/16/2019.
 */

public class BaseRetrofit {
    public static final String BASE_URL = "http://e-sequoia.net/test-api/public/api/v4/";
    public static Retrofit retrofit;
    public static IResponse onResponse;
    public static IFailure onFailure;
    public static Retrofit configureRetrofitWithoutBearer(){
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES).build();
        retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }
}
