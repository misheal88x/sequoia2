package com.nyx.sequoiaapp.other;

import com.nyx.sequoiaapp.models.ColorObject;
import com.nyx.sequoiaapp.models.FeaturesObject;
import com.nyx.sequoiaapp.models.SizeObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/25/2019.
 */

public class ProductFeaturesManager {

    public static List<SizeObject> generateSizes(){
        List<SizeObject> main_sizes_list = new ArrayList<>();
        main_sizes_list.add(new SizeObject(1,"صغير"));
        main_sizes_list.add(new SizeObject(2,"وسط"));
        main_sizes_list.add(new SizeObject(3,"كبير"));
        return main_sizes_list;
    }

    public static List<ColorObject> gererateColors(){
        List<ColorObject> main_colors_list = new ArrayList<>();
        main_colors_list.add(new ColorObject(1,"أبيض","#FFFFFF"));
        main_colors_list.add(new ColorObject(2,"أسود","#000000"));
        main_colors_list.add(new ColorObject(3,"أخضر","#369654"));
        main_colors_list.add(new ColorObject(4,"أزرق","#365b96"));
        main_colors_list.add(new ColorObject(5,"أصفر","#cccc00"));
        main_colors_list.add(new ColorObject(6,"أحمر","#cc0000"));
        main_colors_list.add(new ColorObject(7,"أرجواني","#cc00b1"));
        return main_colors_list;
    }
    public static List<String> generateMeasures(){
        List<String> list = new ArrayList<>();
        list.add("XS");
        list.add("S");
        list.add("M");
        list.add("L");
        list.add("XL");
        list.add("XXL");
        list.add("XXXL");
        return list;
    }
}
