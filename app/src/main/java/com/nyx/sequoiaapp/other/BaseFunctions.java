package com.nyx.sequoiaapp.other;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.BaseResponse;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * Created by Misheal on 10/11/2019.
 */

public class BaseFunctions {
    public static void processResponse(Response<BaseResponse> response, IResponse onResponse, Context context){
        if (response.body()!=null){
            if (response.body().getStatus()==1){
                if (response.body().getData()!=null){
                    onResponse.onResponse(response.body().getData());
                }
            }else {
                try {
                    Toast.makeText(context, String.valueOf(response.body().getMessage()), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    Toast.makeText(context, "حدث خطأ ما, أعد المحاولة", Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(context, "يوجد خطأ بالخدمة أعد المحاولة", Toast.LENGTH_SHORT).show();
        }
    }
    public static String dateExtractor(String datetime){
        String myDate = "";
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '){
                myDate+=datetime.charAt(i);
            }else {
                break;
            }
        }
        return myDate;
    }

    public static String timeExtractor(String datetime){
        String myTime = "";
        boolean isTime = false;
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '&&isTime){
                myTime+=datetime.charAt(i);
            }else if (datetime.charAt(i)==' '){
                isTime = true;
            }
        }
        return myTime;
    }

    public static Date stringToDateConverter(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = sdf.parse(stringDate);
            return d;
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
            return null;
        }
    }
    public static String dateToDayConverter(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String dayOfTheWeek = sdf.format(date);
        return dayOfTheWeek;
    }

    public static String todayDateString(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static long deferenceBetweenTwoDates(Date d1,Date d2){
        return d1.getTime()-d2.getTime();
    }

    public static void runAnimationHorizontal(RecyclerView recyclerView, int type, RecyclerView.Adapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    public static void runAnimationVertical(RecyclerView recyclerView, int type, RecyclerView.Adapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    public static void lunchPlayStore(Context context,String packageName){
        Intent intent = null;
        try{
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="+packageName));
            context.startActivity(intent);
        }catch (android.content.ActivityNotFoundException anfe){
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+packageName)));
        }
    }
    public static int getVersionCode(Context context){
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode;
        }catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }
        return 0;
    }

    public static String getVersioName(Context context){
        String output = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            output = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static MultipartBody.Part uploadFileImageConverter(String imageFieldName, String imagePath){
        File file = new File(imagePath);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData(imageFieldName,file.getName(),requestBody);
        return body;
    }

    public static RequestBody uploadFileStringConverter(String input){
        return RequestBody.create(MultipartBody.FORM,input);
    }

    public static void setFrescoImage(SimpleDraweeView image, String url){
        try {
            Uri uri = Uri.parse(url);
            image.setImageURI(uri);
        }catch (Exception e){}
    }

    public static void openBrowser(Context context,String url){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        context.startActivity(i);
    }
}
