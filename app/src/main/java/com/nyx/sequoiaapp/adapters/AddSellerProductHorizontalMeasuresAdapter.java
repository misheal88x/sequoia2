package com.nyx.sequoiaapp.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;

import java.util.List;

/**
 * Created by Misheal on 11/3/2019.
 */

public class AddSellerProductHorizontalMeasuresAdapter extends  RecyclerView.Adapter<AddSellerProductHorizontalMeasuresAdapter.ViewHolder> {
    private List<String> list;
    private Context context;
    private IMove iMove;


    public AddSellerProductHorizontalMeasuresAdapter(Context context, List<String> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_seller_product_category_name);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_product_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.name.setText(list.get(position));
    }
}
