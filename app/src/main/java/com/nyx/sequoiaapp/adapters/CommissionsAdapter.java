package com.nyx.sequoiaapp.adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.CommissionObject;

import java.util.List;

public class CommissionsAdapter extends  RecyclerView.Adapter<CommissionsAdapter.ViewHolder>{
    private List<CommissionObject> list;
    private Activity context;


    public CommissionsAdapter(Activity context, List<CommissionObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_title,tv_value;
        public ViewHolder(View view) {
            super(view);
            tv_title =  view.findViewById(R.id.item_commission_title);
            tv_value =  view.findViewById(R.id.item_commission_value);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_commission, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        CommissionObject co = list.get(position);
        holder.tv_title.setText(co.getName());
        holder.tv_value.setText("قيمة العمولة : "+co.getCommission());
    }
}
