package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.SingleProductActivity;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Luminance on 5/20/2018.
 */

public class CartAdapter extends BasicRecycleviewAdapter {
    PostAction op;
    public CartAdapter(ArrayList cats, Activity c , PostAction op) {
        super(cats, c);
        this.op = op;
    }

    public class MyCartView extends MyView {

        Button plus , minus , delete;
        EditText quanity_center;


        public MyCartView(View view) {
            super(view);
            plus= view.findViewById(R.id.btn_plus);
            minus= view.findViewById(R.id.btn_minus);
            delete= view.findViewById(R.id.delete_from_cart);
            quanity_center= view.findViewById(R.id.quantity);

        }
    }




    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_ad_layout_cart, parent, false);

        return new MyCartView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        String trimmedTitle = "";
        final CartItem p = ((CartItem)cats.get(position));

        ((MyCartView)holder).  quanity_center.setText(p.getQuantity()+"");
        String[] words =p.getPost().getTitle().split(" ");
        int min = Math.min(5 , words.length);
        ((MyCartView)holder).plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int x = Integer.parseInt(((MyCartView)holder).quanity_center.getText().toString().trim());

                ((MyCartView)holder).  quanity_center.setText((x+1)+"");

            }
        });
        ((MyCartView)holder).  minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int x = Integer.parseInt(((MyCartView)holder).quanity_center.getText().toString().trim());
                if(x>1)
                    ((MyCartView)holder).  quanity_center.setText((x-1)+"");

            }
        });

        ((MyCartView)holder).  delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new AlertDialog.Builder(context)
                        .setTitle("حذف منتج من السلة")
                        .setMessage("هل ترغب بازالة " + p.getPost().getTitle() + " من سلة مشترياتك ؟ ")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("نعم", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                               try{
                                   if (cats.remove(position)!=null) {
                                       //CartAdapter.this.notifyItemRemoved(position);
                                       CartAdapter.this.notifyDataSetChanged();
                                       SavedCacheUtils.deleteFromCart(context, p.getPost().getId());
                                       op.doTask();
                                   }
                                   }catch (IndexOutOfBoundsException er){
                               }
                            }})
                        .setNegativeButton("لا", null).show();

            }
        });


        ((MyCartView)holder).  quanity_center.addTextChangedListener(
                new TextWatcher() {
                    @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
                    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                    private Timer timer=new Timer();
                    private final long DELAY = 1000; // milliseconds

                    @Override
                    public void afterTextChanged(final Editable s) {
                        timer.cancel();
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        // TODO: do what you need here (refresh list)
                                        // you will probably need to use runOnUiThread(Runnable action) for some specific actions
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                SavedCacheUtils.updateCartItemQuantity(context ,p.getPost().getId() ,
                                                        Integer.parseInt(((MyCartView)holder).quanity_center.getText().toString().trim()));
                                                op.doTask();
                                            }
                                        });

                                    }
                                },
                                DELAY
                        );
                    }
                }
        );







        /*
        for(int i=0;i<min;i++)trimmedTitle+=words[i]+" ";
        holder.title.setText(trimmedTitle.trim() + ((min==5)?"...":""));
        **/
        holder.title.setText(p.getPost().getTitle());
        holder.newproice.setText(p.getPost().getNewprice()+" "+context.getResources().getString(R.string.curremcy));
        BaseFunctions.setFrescoImage(holder.imageview,p.getPost().getThumb());
        holder.imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, SingleProductActivity.class);
                in.putExtra("post" , p.getPost());
                context.startActivity(in);
            }
        });
    }

}
