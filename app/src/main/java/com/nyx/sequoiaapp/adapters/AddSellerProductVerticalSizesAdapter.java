package com.nyx.sequoiaapp.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.SizeObject;

import java.util.List;

/**
 * Created by Misheal on 10/23/2019.
 */

public class AddSellerProductVerticalSizesAdapter extends  RecyclerView.Adapter<AddSellerProductVerticalSizesAdapter.ViewHolder> {
    private List<SizeObject> list;
    private Context context;
    private IMove iMove;


    public AddSellerProductVerticalSizesAdapter( Context context,List<SizeObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private RadioButton tv_radio;

        public ViewHolder(View view) {
            super(view);
            tv_radio = view.findViewById(R.id.item_seller_product_category_row_radio);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_product_size_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_radio.setText(list.get(position).getValue());
        holder.tv_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    iMove.move(position);
                }
            }
        });
    }
}
