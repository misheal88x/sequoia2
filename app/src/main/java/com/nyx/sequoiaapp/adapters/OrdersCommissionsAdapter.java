package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.OrderDetailsActivity;
import com.nyx.sequoiaapp.models.NewOrderObject;

import java.util.List;

/**
 * Created by Misheal on 15/11/2019.
 */

public class OrdersCommissionsAdapter extends  RecyclerView.Adapter<OrdersCommissionsAdapter.ViewHolder> {
    private List<NewOrderObject> list;
    private Activity context;


    public OrdersCommissionsAdapter(Activity context, List<NewOrderObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView id  ,sum;
        public LinearLayout layout;
        public ViewHolder(View view) {
            super(view);
            id =  view.findViewById(R.id.item_commissions_order_id);
            sum =  view.findViewById(R.id.item_commissions_order_sum);
            layout = view.findViewById(R.id.order_details);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_commissions_order, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.id.setText("الطلبية رقم : "+list.get(position).getId());
        holder.sum.setText(String.valueOf(list.get(position).getCommission_result())+" ل.س");
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("order_id",list.get(position).getId());
                intent.putExtra("type","without");
                context.startActivity(intent);
            }
        });
    }
}
