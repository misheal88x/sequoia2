package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.OrderDetailsActivity;
import com.nyx.sequoiaapp.models.NewOrderObject;

import java.util.List;

/**
 * Created by Misheal on 10/23/2019.
 */

public class OrdersUnder5000Adapter extends  RecyclerView.Adapter<OrdersUnder5000Adapter.ViewHolder> {
    private List<NewOrderObject> list;
    private Activity context;


    public OrdersUnder5000Adapter(Activity context, List<NewOrderObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView status , id  ,date;
        LinearLayout details;
        public ViewHolder(View view) {
            super(view);
            status = (TextView) view.findViewById(R.id.order_status);
            id = (TextView) view.findViewById(R.id.order_id);
            date = (TextView) view.findViewById(R.id.order_date);
            details = (LinearLayout) view.findViewById(R.id.order_details);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_order_layout, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final NewOrderObject p = list.get(position);
        holder.id.setText(p.getId());
        holder.date.setText(p.getCreate_date());
        holder.status.setText(statusConverter(p.getStatus()));
        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("order_id",list.get(position).getId());
                intent.putExtra("type","without");
                context.startActivity(intent);
            }
        });
    }

    private String statusConverter(int in){
        String out = "";
        switch (in) {
            case 1 : out = "تم الإنشاء";break;
            case 2 : out = "تم التسليم";break;
            case -1 : out = "ملغاة";break;
            case -2 : out = "محذوفة";break;
            case 3 : out = "تم الشحن";break;
        }
        return out;
    }
}
