package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.ControlPanelActivity;
import com.nyx.sequoiaapp.costumes.CustomDialogClassExtended;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.ConnectionUtils;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.ExtendedOrder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OrderAdapterEditable extends RecyclerView.Adapter<OrderAdapterEditable.MyView> {

    String SHIPPED="#e7f2ff";
    String DELIVERED="#e9ffe7";
    String CANCELLED="#ffe7e7";
    String RETURNED="#fff";
    ArrayList cats;
    public Activity context;

    public class MyView extends RecyclerView.ViewHolder {

        public TextView status, id, date, name  , noteView  ,notes_cus;
        Button cancel, ship, details, deliver  , returned  , note;
        //, delete;
        LinearLayout body;


        public MyView(View view) {
            super(view);

            status = (TextView) view.findViewById(R.id.order_status);
            notes_cus = (TextView) view.findViewById(R.id.note_cus);
            id = (TextView) view.findViewById(R.id.order_id);
            date = (TextView) view.findViewById(R.id.order_date);
            noteView = (TextView) view.findViewById(R.id.note_view);
            name = (TextView) view.findViewById(R.id.order_customer);
            body = (LinearLayout) view.findViewById(R.id.order_details);

            cancel = (Button) view.findViewById(R.id.cancelled);
            note = (Button) view.findViewById(R.id.do_note);
            ship = (Button) view.findViewById(R.id.shipped);
            details = (Button) view.findViewById(R.id.details);
            deliver = (Button) view.findViewById(R.id.delivered);
            returned = (Button) view.findViewById(R.id.returned);
           // delete = (Button) view.findViewById(R.id.delete);


        }
    }

    public OrderAdapterEditable(ArrayList cats, Activity c) {
        this.cats = cats;
        this.context = c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_order_layout_editable, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        final ExtendedOrder p = ((ExtendedOrder) cats.get(position));


        if(p.getStatus_id().equals("2"))holder.body.setBackgroundColor(Color.parseColor(DELIVERED));
        if(p.getStatus_id().equals("-1"))holder.body.setBackgroundColor(Color.parseColor(CANCELLED));
        if(p.getStatus_id().equals("3"))holder.body.setBackgroundColor(Color.parseColor(SHIPPED));


        holder.id.setText(p.getId());
        holder.name.setText(p.getFullname());
        holder.date.setText(p.getDate());
        holder.status.setText(p.getStatus());
        if(p.getNote()!=null && !p.getNote().equals(""))
        holder.noteView.setText("ملاحظة المسوق : "+p.getNote());
        if(p.getNote_c()!=null && !p.getNote_c().equals(""))
            holder.notes_cus.setText("ملاحظة الزبون : "+p.getNote_c());

        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialogClassExtended cdd = new CustomDialogClassExtended(context, p);
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cdd.show();
            }
        });

        if(SharedPrefManager.getInstance(context).getUser()
                .getUser_type().equals(ControlPanelActivity.MARKETER)) {
            holder.note.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("ارسال ملاحظة");

// Set up the input
                    final EditText input = new EditText(context);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                    input.setInputType(InputType.TYPE_CLASS_TEXT
                            );
                    builder.setView(input);

// Set up the buttons
                    builder.setPositiveButton("ارسال", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                         String   m_Text = input.getText().toString();
                            sendNote(p.getId() ,m_Text ,v ,position ,holder.noteView ,0);
                        }
                    });
                    builder.setNegativeButton("إالغاء", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();


                }
            });

        }else{
         holder.note.setVisibility(View.GONE);
        }


      if(SharedPrefManager.getInstance(context).getUser()
              .getUser_type().equals(ControlPanelActivity.DISPENSER)) {

          holder.deliver.setEnabled(false);
          holder.cancel.setEnabled(false);
          holder.ship.setEnabled(false);
          holder.returned.setEnabled(false);



          if (p.getStatus_id().equals("1")) {

              holder.deliver.setEnabled(true);
              holder.cancel.setEnabled(true);
              holder.ship.setEnabled(true);
          }
          if (p.getStatus_id().equals("3")) {

              holder.deliver.setEnabled(true);
              holder.cancel.setEnabled(true);
              //    holder.delete.setVisibility(View.VISIBLE);
          }
          if (p.getStatus_id().equals("2")) {

              holder.returned.setEnabled(true);
          }



//          holder.delete.setOnClickListener(new View.OnClickListener() {
//              @Override
//              public void onClick(View v) {
//                  if (ConnectionUtils.isNetworkAvailable(context)) {
//                      changeOrderStatus(p.getId(), "-2", v, "محذوفة", position, 0);
//                  } else {
//                      Toast.makeText(context, "لا يوجد شبكة", Toast.LENGTH_SHORT).show();
//                  }
//              }
//          });
          holder.returned.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(final View v) {
                  if (ConnectionUtils.isNetworkAvailable(context)) {
                      AlertDialog.Builder builder = new AlertDialog.Builder(context);
                      builder.setTitle("طلبية مسترجعة")
                              .setMessage("هل أنت متأكد من أنك تريد جعل هذه الطلبية مسترجعة ؟")
                              .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {
                                      changeOrderStatus(p.getId(), "1", v, "مسترجعة", position, 0);
                                  }
                              })
                              .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {

                                  }
                              }).show();
                  } else {
                      Toast.makeText(context, "لا يوجد شبكة", Toast.LENGTH_SHORT).show();
                  }
              }
          });


          holder.cancel.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(final View v) {
                  if (ConnectionUtils.isNetworkAvailable(context)) {
                      AlertDialog.Builder builder = new AlertDialog.Builder(context);
                      builder.setTitle("طلبية ملغاة")
                              .setMessage("هل أنت متأكد من أنك تريد جعل هذه الطلبية ملغاة ؟")
                              .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {
                                      changeOrderStatus(p.getId(), "-1", v, "ملغاة", position, 0);
                                  }
                              })
                              .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {

                                  }
                              }).show();
                  } else {
                      Toast.makeText(context, "لا يوجد شبكة", Toast.LENGTH_SHORT).show();
                  }
              }
          });

          holder.deliver.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(final View v) {
                  if (ConnectionUtils.isNetworkAvailable(context)) {
                      AlertDialog.Builder builder = new AlertDialog.Builder(context);
                      builder.setTitle("طلبية تم تسليمها")
                              .setMessage("هل أنت متأكد من أنك ترغب في تأكيد تسليم هذه الطلبية ؟")
                              .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {
                                      changeOrderStatus(p.getId(), "2", v, "تم التسليم", position, 0);
                                  }
                              })
                              .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {

                                  }
                              }).show();
                  } else {
                      Toast.makeText(context, "لا يوجد شبكة", Toast.LENGTH_SHORT).show();
                  }
              }
          });
          holder.ship.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(final View v) {
                  if (ConnectionUtils.isNetworkAvailable(context)) {
                      AlertDialog.Builder builder = new AlertDialog.Builder(context);
                      builder.setTitle("طلبية تم شحنها")
                              .setMessage("هل أنت متأكد من أنك ترغب في تأكيد شحن هذه الطلبية ؟")
                              .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {
                                      changeOrderStatus(p.getId(), "3", v, "تم الشحن", position, 0);
                                  }
                              })
                              .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {

                                  }
                              }).show();
                  } else {
                      Toast.makeText(context, "لا يوجد شبكة", Toast.LENGTH_SHORT).show();
                  }
              }
          });
      }else {
          holder.deliver.setVisibility(View.INVISIBLE);
          holder.cancel.setVisibility(View.INVISIBLE);
          holder.ship.setVisibility(View.INVISIBLE);          holder.returned.setVisibility(View.INVISIBLE);
      }
    }

    @Override
    public int getItemCount() {
        return cats.size();
    }

    boolean isUpdating = false;

    void changeOrderStatus(final String id, final String status, final View v, final String state, final int position
            , final int try_) {
        if (isUpdating) {
            if (try_ == 0)
                Toast.makeText(context, "يرجى الانتظار , هناك طلب قيد التعديل..", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!SharedPrefManager.getInstance(context).IsUserLoggedIn()) {
            Toast.makeText(context, "يرجى تسجيل الدخول من جديد", Toast.LENGTH_SHORT).show();
            return;
        }
        isUpdating = true;
        v.setEnabled(false);
        User user = SharedPrefManager.getInstance(context).getUser();
        HashMap<String, String> params = new HashMap();
        params.put("user_id", user.getUser_id());
        params.put("user_token", user.getToken());
        params.put("order_id", id);
        params.put("new_value", status);
        params.put("type", "1");

        BackgroundServices.getInstance(context).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if (!s.equals("")) {
                    try {
                        isUpdating = false;
                        v.setEnabled(true);
                        JSONObject object = new JSONObject(s);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                        if (object.getInt("status") == 1) {
                            ((ExtendedOrder) cats.get(position)).setStatus(state);
                            ((ExtendedOrder) cats.get(position)).setStatus_id(status);
                            notifyDataSetChanged();
                        }


                    } catch (Exception fdr) {
                        isUpdating = false;
                        v.setEnabled(true);
                        //Log.d("FDRX", fdr.getMessage());
                    }
                } else {
                    isUpdating = false;
                    if (try_ < 10) {
                        changeOrderStatus(id, status, v, state, position, try_ + 1);
                    } else {
                        v.setEnabled(true);
                        Toast.makeText(context, "خطأ في الشبكة , حاول من جديد", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        }, APIUrl.SERVER+"order/update", params);


    }


    void sendNote(final String id, final String message,
                  final View v,final int position , final TextView notes
            , final int try_) {
        if (isUpdating) {
            if (try_ == 0)
                Toast.makeText(context, "يرجى الانتظار , هناك طلب قيد التعديل..", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!SharedPrefManager.getInstance(context).IsUserLoggedIn()) {
            Toast.makeText(context, "يرجى تسجيل الدخول من جديد", Toast.LENGTH_SHORT).show();
            return;
        }
        isUpdating = true;
        v.setEnabled(false);
        User user = SharedPrefManager.getInstance(context).getUser();
        HashMap<String, String> params = new HashMap();
        params.put("user_id", user.getUser_id());
        params.put("user_token", user.getToken());
        params.put("order_id", id);
        params.put("note", message);
        BackgroundServices.getInstance(context).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if (!s.equals("")) {
                    try {
                        isUpdating = false;
                        v.setEnabled(true);
                        JSONObject object = new JSONObject(s);
                        ((ExtendedOrder)cats.get(position)).setNote(message);
                        notes.setText(message);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();



                    } catch (Exception fdr) {
                        isUpdating = false;
                        v.setEnabled(true);
                        //Log.d("FDRX", fdr.getMessage());
                    }
                } else {
                    isUpdating = false;
                    if (try_ < 10) {
                        sendNote(id, message, v,position, notes, try_ + 1 );
                    } else {
                        v.setEnabled(true);
                        Toast.makeText(context, "خطأ في الشبكة , حاول من جديد", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        }, APIUrl.SERVER+"order/note", params);


    }
}