package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.SingleProductActivity;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;

/**
 * Created by Luminance on 5/20/2018.
 */

public class FavouritesAdapter extends BasicRecycleviewAdapter {
    PostAction op;
    public FavouritesAdapter(ArrayList cats, Activity c , PostAction op) {
        super(cats, c);
        this.op = op;
    }

    public class MyCartView extends MyView {

        Button  delete;


        public MyCartView(View view) {
            super(view);
            delete= view.findViewById(R.id.delete_from_favourites);

        }
    }




    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_ad_layout_favourite, parent, false);
        return new MyCartView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        String trimmedTitle = "";
        final ExtendedPost p = ((ExtendedPost)cats.get(position));

        ((MyCartView)holder).  delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new AlertDialog.Builder(context)
                        .setTitle("حذف منتج من المفضلة")
                        .setMessage("هل ترغب بازالة " + p.getTitle() + " من مفضلتك ؟ ")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("نعم", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                try{
                                    if(cats.remove(position)!=null) {
                                FavouritesAdapter.this.notifyItemRemoved(position);
                                SavedCacheUtils.removeFromFavourite(context , p.getId());
                                op.doTask();}}catch (IndexOutOfBoundsException er){}
                            }})
                        .setNegativeButton("لا", null).show();

            }
        });
         String[] words =p.getTitle().split(" ");
        int min = Math.min(5 , words.length);
        for(int i=0;i<min;i++)trimmedTitle+=words[i]+" ";
        holder.title.setText(trimmedTitle.trim() + ((min==5)?"...":""));
        holder.newproice.setText(p.getNewprice()+" "+context.getResources().getString(R.string.curremcy));
        holder.oldprice.setText(p.getOldprice()+" "+context.getResources().getString(R.string.curremcy));
        BaseFunctions.setFrescoImage(holder.imageview,p.getThumb());
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, SingleProductActivity.class);
                in.putExtra("post" , p);
                context.startActivity(in);
            }
        });
    }

}
