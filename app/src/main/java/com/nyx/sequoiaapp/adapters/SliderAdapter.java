package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.nyx.sequoiaapp.Dialogs.ViewImageDialog;
import com.nyx.sequoiaapp.R;

import java.util.List;

/**
 * Created by Luminance on 6/27/2018.
 */

public class SliderAdapter extends PagerAdapter {

    private Activity context;
    private List items;

    public SliderAdapter(Activity context, List color) {
        this.context = context;
        this.items = color;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       final View view = inflater.inflate(R.layout.single_slider_image_layout, null);
      final   String item = (String)items.get(position);
        Glide.with(context).load(item)
                .apply(new RequestOptions().fitCenter())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        view.findViewById(R.id.progress).setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                   DataSource dataSource, boolean isFirstResource) {
                     view.findViewById(R.id.progress).setVisibility(View.GONE);
                        view.findViewById(R.id.pic).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                ViewImageDialog dialog = new ViewImageDialog(context,item);
                                dialog.show();
                                /*
                                AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                                View mView = context.getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                                final PhotoView photoView = mView.findViewById(R.id.imageView);

                                Glide.with(context)
                                        .load(item)
                                        .apply(new RequestOptions().centerCrop())
                                        // .thumbnail(Glide.with(SingleProductActivity.this).load(R.drawable.loader_slow))
                                        .into(photoView);
                                mBuilder.setView(mView);
                                AlertDialog mDialog = mBuilder.create();
                                mDialog.show();
                                **/

                            }
                        });
                        return false;
                    }
                }) .into((ImageView) view.findViewById(R.id.pic));
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}