package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.Customer;

import java.util.ArrayList;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView name , email  ,phone ,city;
        ImageView pic;


        public MyView(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.email);
            phone = (TextView) view.findViewById(R.id.phone);
            city = (TextView) view.findViewById(R.id.city);
            pic = (ImageView) view.findViewById(R.id.profile);
        }
    }
    public UsersAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_user_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

     final Customer p = ((Customer) cats.get(position));
        holder.name.setText(p.getName());
        holder.phone.setText(p.getMobile());
        holder.email.setText(p.getEmail());
        holder.city.setText(p.getCity());

        if(!p.getPic().trim().equals(""))
        {
            Glide.with(context).load(p.getPic().trim()).into(
                    holder.pic
            )  ;
        }

    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}