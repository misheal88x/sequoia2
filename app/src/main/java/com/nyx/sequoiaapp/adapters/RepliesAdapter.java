package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.Comment;

import java.util.ArrayList;

/**
 * Created by Misheal on 11/8/2019.
 */

public class RepliesAdapter extends RecyclerView.Adapter<RepliesAdapter.MyView> {
    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        private TextView name , date  ,comment;
        private RatingBar bar;
        private Button btn_reply;
        private RelativeLayout lyt_make_reply;
        private RelativeLayout lyt_reply;
        private EditText edt_reply;
        private ImageView btn_send_reply;

        private RecyclerView rv_replies;

        public MyView(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            comment = (TextView) view.findViewById(R.id.comment);
            bar = (RatingBar) view.findViewById(R.id.rate);
            btn_reply = view.findViewById(R.id.reply_btn);
            lyt_reply = view.findViewById(R.id.single_comment_reply_layout);
            edt_reply = view.findViewById(R.id.single_comment_reply_edit);
            btn_send_reply = view.findViewById(R.id.single_comment_send_btn);
            lyt_make_reply = view.findViewById(R.id.single_comment_make_reply_layout);
            rv_replies = view.findViewById(R.id.single_comments_layout_replies_recycler);
        }
    }


    public RepliesAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_comment_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        Comment p = ((Comment) cats.get(position));
        holder.name.setText(p.getName());
        holder.date.setText(p.getDate());
        if(!p.getCommnet().equals(""))
            holder.comment.setText(p.getCommnet());
        else
            holder.comment.setVisibility(View.GONE);

        holder.bar.setVisibility(View.GONE);
        holder.btn_reply.setVisibility(View.GONE);
    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}
