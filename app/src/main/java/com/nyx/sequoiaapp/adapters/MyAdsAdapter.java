package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.BackgroundServices;
import com.nyx.sequoiaapp.helper.PostAction;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.helper.User;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Luminance on 5/20/2018.
 */

public class MyAdsAdapter extends BasicRecycleviewAdapter {
    PostAction op;
    public MyAdsAdapter(ArrayList cats, Activity c , PostAction op) {
        super(cats, c);
        this.op = op;
    }

    public class MyCartView extends MyView {

        Button  show , hide;
        ImageView seen_status;


        public MyCartView(View view) {
            super(view);
            show= view.findViewById(R.id.show);
            hide= view.findViewById(R.id.hide);
            seen_status= view.findViewById(R.id.seen_icon);


        }
    }




    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_ad_layout_editable, parent, false);
        return new MyCartView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        String trimmedTitle = "";
        final ExtendedPost p = ((ExtendedPost)cats.get(position));

        if(p.getStatus().equals("0"))Glide.with(context).load(R.drawable.seen).into( ((MyCartView)holder).seen_status);
        else Glide.with(context).load(R.drawable.unseen).into( ((MyCartView)holder).seen_status);


        ((MyCartView)holder).  hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                new AlertDialog.Builder(context)
                        .setTitle("اخفاء منتج")
                        .setMessage("هل ترغب باخفاء " + p.getTitle())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("نعم", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
changeProductStatus(p.getId() ,"1" ,v ,position ,0);
                              //

                            }})
                        .setNegativeButton("لا", null).show();

            }
        });

        ((MyCartView)holder).  show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                new AlertDialog.Builder(context)
                        .setTitle("اظهار منتج")
                        .setMessage("هل ترغب باظهار " + p.getTitle())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("نعم", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                changeProductStatus(p.getId() ,"0" ,v ,position ,0);
                                //

                            }})
                        .setNegativeButton("لا", null).show();

            }
        });





         String[] words =p.getTitle().split(" ");
        int min = Math.min(5 , words.length);
        for(int i=0;i<min;i++)trimmedTitle+=words[i]+" ";
        holder.title.setText(trimmedTitle.trim() + ((min==5)?"...":""));
        BaseFunctions.setFrescoImage(holder.imageview,p.getThumb());
//        holder.body.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in = new Intent(context, SingleProductActivity.class);
//                in.putExtra("post" , p);
//                context.startActivity(in);
//            }
//        });
    }



    boolean isUpdating = false;

    void changeProductStatus(final String id, final String status, final View v  ,final int position
            , final int try_) {
        if (isUpdating) {
            if (try_ == 0)
                Toast.makeText(context, "يرجى الانتظار , هناك طلب قيد التعديل..", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!SharedPrefManager.getInstance(context).IsUserLoggedIn()) {
            Toast.makeText(context, "يرجى تسجيل الدخول من جديد", Toast.LENGTH_SHORT).show();
            return;
        }
        isUpdating = true;
        v.setEnabled(false);
        User user = SharedPrefManager.getInstance(context).getUser();
        HashMap<String, String> params = new HashMap();
        params.put("user_id", user.getUser_id());
        params.put("user_token", user.getToken());
        params.put("product_id", id);
        params.put("new_value", status);
        params.put("type", "2");

        BackgroundServices.getInstance(context).CallPost(new PostAction() {
            @Override
            public void doTask() {

            }

            @Override
            public void doTask(String s) {
                if (!s.equals("")) {
                    try {
                        isUpdating = false;
                        v.setEnabled(true);
                        JSONObject object = new JSONObject(s);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                        if (object.getInt("status") == 1) {
                            ((ExtendedPost) cats.get(position)).setStatus((status));
                            notifyDataSetChanged();
                        }


                    } catch (Exception fdr) {
                        isUpdating = false;
                        v.setEnabled(true);
                        //Log.d("FDRX", fdr.getMessage());
                    }
                } else {
                    isUpdating = false;
                    if (try_ < 10) {
                        changeProductStatus(id, status, v, position, try_ + 1);
                    } else {
                        v.setEnabled(true);

                    }
                }
            }
        }, APIUrl.SERVER+"product/update", params);


    }

}
