package com.nyx.sequoiaapp.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.ColorObject;

import java.util.List;

/**
 * Created by Misheal on 10/23/2019.
 */

public class AddSellerProductHorizontalColorsAdapter extends  RecyclerView.Adapter<AddSellerProductHorizontalColorsAdapter.ViewHolder> {
    private List<ColorObject> list;
    private Context context;
    private IMove iMove;


    public AddSellerProductHorizontalColorsAdapter(Context context, List<ColorObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView btn_delete;
        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_seller_product_category_name);
            btn_delete = view.findViewById(R.id.item_seller_product_category_delete);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_product_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getValue());
        holder.name.setBackgroundColor(Color.parseColor("#"+list.get(position).getValue()));
        holder.btn_delete.setVisibility(View.VISIBLE);
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("هل أنت متأكد من حذف هذا اللون ؟").setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //list.remove(position);
                        //notifyDataSetChanged();
                        iMove.move(position);
                    }
                }).setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }
}
