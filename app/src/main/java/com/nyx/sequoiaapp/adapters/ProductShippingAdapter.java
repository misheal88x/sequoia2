package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;

import java.util.List;

public class ProductShippingAdapter extends RecyclerView.Adapter<ProductShippingAdapter.ViewHolder> {
    private List<String> list;
    private Context context;

    public ProductShippingAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_text;
        public ViewHolder(View view) {
            super(view);
            tv_text = view.findViewById(R.id.item_product_shipping_txt);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_shipping, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_text.setText(list.get(position));
    }
}
