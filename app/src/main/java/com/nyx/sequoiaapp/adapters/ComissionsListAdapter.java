package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.ProductComission;

import java.util.ArrayList;

public class ComissionsListAdapter extends RecyclerView.Adapter<ComissionsListAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView name , price;
        ImageView pic;


        public MyView(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name);
            price = (TextView) view.findViewById(R.id.comission);
            pic = (ImageView) view.findViewById(R.id.pic);
        }
    }
    public ComissionsListAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    public void filterList(ArrayList filterdNames) {
        this.cats = filterdNames;
        notifyDataSetChanged();
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_product_comission_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

     final ProductComission p = ((ProductComission) cats.get(position));
        holder.name.setText(p.getName());
        holder.price.setText(p.getComission()+" "+context.getResources().getString(R.string.curremcy));
        if(!p.getPic().trim().equals(""))
        {
            Glide.with(context).load(p.getPic().trim()).into(
                    holder.pic
            )  ;
        }

    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}