package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.costumes.CustomDialogClass;
import com.nyx.sequoiaapp.models.Order;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView status , id  ,date;
        LinearLayout  details;


        public MyView(View view) {
            super(view);

            status = (TextView) view.findViewById(R.id.order_status);
            id = (TextView) view.findViewById(R.id.order_id);
            date = (TextView) view.findViewById(R.id.order_date);

            details = (LinearLayout) view.findViewById(R.id.order_details);






        }
    }
    public OrderAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_order_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

     final   Order p = ((Order) cats.get(position));
        holder.id.setText(p.getId());
        holder.date.setText(p.getDate());
        holder.status.setText(p.getStatus());
        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialogClass cdd = new CustomDialogClass(context ,p);
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cdd.show();
            }
        });



    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}