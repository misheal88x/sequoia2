package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import android.media.MediaPlayer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nyx.sequoiaapp.APIClass.CommentsAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Comment;
import com.nyx.sequoiaapp.models.ReplyObject;

import org.json.JSONException;

import java.util.ArrayList;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    private String store_id;
    private String store_name;
    public class MyView extends RecyclerView.ViewHolder {

        private TextView name , date  ,comment;
        private ImageView profile;
        private RatingBar bar;
        private Button btn_reply;
        private RelativeLayout lyt_make_reply;
        private RelativeLayout lyt_reply;
        private EditText edt_reply;
        private ImageView btn_send_reply;
        private ProgressBar pb_loading;
        private RecyclerView rv_replies;


        public MyView(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            comment = (TextView) view.findViewById(R.id.comment);
            profile = (ImageView) view.findViewById(R.id.profile);
            bar = (RatingBar) view.findViewById(R.id.rate);
            btn_reply = view.findViewById(R.id.reply_btn);
            lyt_reply = view.findViewById(R.id.single_comment_reply_layout);
            edt_reply = view.findViewById(R.id.single_comment_reply_edit);
            btn_send_reply = view.findViewById(R.id.single_comment_send_btn);
            lyt_make_reply = view.findViewById(R.id.single_comment_make_reply_layout);
            pb_loading = view.findViewById(R.id.single_comment_progress);
            rv_replies = view.findViewById(R.id.single_comments_layout_replies_recycler);
        }
    }


    public CommentsAdapter(ArrayList cats , Activity c,String store_id,String store_name) {
        this.cats = cats;
        this.context=c;
        this.store_id = store_id;
        this.store_name = store_name;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_comment_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        final Comment p = ((Comment) cats.get(position));
        Log.i("comment_id111", "onBindViewHolder: "+p.getId());
        holder.name.setText(p.getName());
        holder.date.setText(p.getDate());
        if(!p.getCommnet().equals(""))
        holder.comment.setText(p.getCommnet());
        else
            holder.comment.setVisibility(View.GONE);

   if(!p.getPic().equals(""))
        Glide.with(context)
                .load(p.getPic())
                .into(holder.profile);
      holder.bar.setRating(Float.parseFloat(p.getRate()));
      try {
          if (SharedPrefManager.getInstance(context).IsUserLoggedIn()){
              String user_id = SharedPrefManager.getInstance(context).getUser().getUser_id();
              String user_type = SharedPrefManager.getInstance(context).getUser().getUser_type();
              if (user_id.equals(store_id)||user_type.equals("1")||user_type.equals("7")){
                  holder.lyt_make_reply.setVisibility(View.VISIBLE);
              }
          }
      }catch (Exception e){}
      holder.btn_reply.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              holder.lyt_reply.setVisibility(View.VISIBLE);
              holder.lyt_make_reply.setVisibility(View.GONE);
          }
      });
      final ArrayList replies = new ArrayList();
      if (p.getReplies().length() > 0){
          for (int i = 0; i < p.getReplies().length(); i++) {
              Comment cc = new Comment();
              try {
                  cc.setName(store_name);
                  cc.setDate(p.getReplies().getJSONObject(i).getString("comment_time"));
                  cc.setCommnet(p.getReplies().getJSONObject(i).getString("content"));
                  replies.add(cc);
              } catch (JSONException e) {
                  e.printStackTrace();
              }
          }
      }
      final RepliesAdapter repliesAdapter = new RepliesAdapter(replies,context);
      LinearLayoutManager replies_layout_manager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
      holder.rv_replies.setLayoutManager(replies_layout_manager);
      holder.rv_replies.setAdapter(repliesAdapter);

      holder.btn_send_reply.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              if (holder.edt_reply.getText().toString().equals("")){
                  Toast.makeText(context, "يجب عليك كتابة نص التعليق", Toast.LENGTH_SHORT).show();
                  return;
              }
              holder.btn_send_reply.setVisibility(View.GONE);
              holder.pb_loading.setVisibility(View.VISIBLE);
              String user_id = SharedPrefManager.getInstance(context).getUser().getUser_id();
              CommentsAPIsClass.create_reply(context,
                      user_id,
                      p.getId(),
                      holder.edt_reply.getText().toString(),
                      new IResponse() {
                          @Override
                          public void onResponse() {
                              holder.btn_send_reply.setVisibility(View.VISIBLE);
                              holder.pb_loading.setVisibility(View.GONE);
                          }

                          @Override
                          public void onResponse(Object json) {
                              if (json!=null){
                                  String json1 = new Gson().toJson(json);
                                  ReplyObject success = new Gson().fromJson(json1,ReplyObject.class);
                                  Comment c = new Comment();
                                  c.setId(String.valueOf(c));
                                  c.setCommnet(success.getContent());
                                  c.setDate(success.getComment_time());
                                  c.setName(store_name);
                                  replies.add(c);
                                  repliesAdapter.notifyDataSetChanged();
                                  Toast.makeText(context, "تم إضافة الرد بنجاح", Toast.LENGTH_SHORT).show();
                                  holder.btn_send_reply.setVisibility(View.VISIBLE);
                                  holder.pb_loading.setVisibility(View.GONE);
                                  holder.lyt_reply.setVisibility(View.GONE);
                                  holder.lyt_make_reply.setVisibility(View.VISIBLE);
                                  MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.reply);
                                  mPlayer.start();
                              }else {
                                  holder.btn_send_reply.setVisibility(View.VISIBLE);
                                  holder.pb_loading.setVisibility(View.GONE);
                              }
                          }
                      }, new IFailure() {
                          @Override
                          public void onFailure() {
                              holder.btn_send_reply.setVisibility(View.VISIBLE);
                              holder.pb_loading.setVisibility(View.GONE);
                              Toast.makeText(context, "لا يوجد اتصال بالانترنت حاول مجددا", Toast.LENGTH_SHORT).show();
                          }
                      });
          }
      });
    }
    @Override
    public int getItemCount() {
        return cats.size();
    }

}