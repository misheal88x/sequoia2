package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.City;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


/**
 * Created by Luminance on 2/17/2018.
 */

public
class SizesAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    JSONArray cities;
    public SizesAdapter(Activity con , JSONArray cities) {
        // TODO Auto-generated constructor stub
        mInflater = LayoutInflater.from(con);
        this.cities = cities;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return cities==null?0:cities.length();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ListContent holder;
        View v = convertView;
        if (v == null) {
            v = mInflater.inflate(R.layout.my_spinner_style, null);
            holder = new ListContent();
            holder.name = (TextView) v.findViewById(R.id.textView1);
            v.setTag(holder);
        } else {

            holder = (ListContent) v.getTag();
        }

try {
    holder.name.setText("" + (cities.getString(position)));
}catch (JSONException e){}
        return v;
    }
    static class ListContent {
        TextView name;
    }
}
