package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.OrdersOfCityActivity;
import com.nyx.sequoiaapp.models.City;

import java.util.ArrayList;

public class CitiesListAdapter extends RecyclerView.Adapter<CitiesListAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView title;
        public LinearLayout body;


        public MyView(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
                body = (LinearLayout) view.findViewById(R.id.body);


        }
    }


    public CitiesListAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_cat_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

       final City p = ((City)cats.get(position));

        holder.title.setText(p.getName());
holder.body.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent ii = new Intent(context, OrdersOfCityActivity.class);
        ii.putExtra("id", p.getId());
        ii.putExtra("name", p.getName());
        context.startActivity(ii);
    }
});

    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}