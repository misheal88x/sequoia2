package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.MyView> {

    JSONArray cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView id , name , quantity , discount  ,price ;
        public ImageView pic;


        public MyView(View view) {
            super(view);

            id = (TextView) view.findViewById(R.id.sn);
            name = (TextView) view.findViewById(R.id.name);
            quantity = (TextView) view.findViewById(R.id.quantity);
            discount = (TextView) view.findViewById(R.id.discount);
            price = (TextView) view.findViewById(R.id.price);
            pic = (ImageView) view.findViewById(R.id.pic);







        }
    }
    public OrderItemsAdapter(JSONArray cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_order_item_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        final JSONObject p;
        try {
            p = cats.getJSONObject(position);


            holder.id.setText("رقم المنتج : " +  position+"");
            holder.name.setText("الاسم : " + p.getString("name"));
            holder.quantity.setText ("الكمية المطلوبة : "  +p.getString("qty"));
            holder.price.setText("السعر :  "  + p.getString("final_price"));
            holder.discount.setText("مقدار الحسم : " + p.getString("discount"));
            Glide.with(context).load(p.getString("pic")).into(holder.pic);





        } catch (JSONException e) {
            e.printStackTrace();
        }





    }
    @Override
    public int getItemCount() {
        return cats.length();
    }
}