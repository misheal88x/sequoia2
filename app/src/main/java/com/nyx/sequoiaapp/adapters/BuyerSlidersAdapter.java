package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nyx.sequoiaapp.Dialogs.ViewImageDialog;
import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.BuyerSliderObject;

import java.util.List;

public class BuyerSlidersAdapter extends  RecyclerView.Adapter<BuyerSlidersAdapter.ViewHolder> {
    private List<BuyerSliderObject> list;
    private Activity context;
    private IMove iMove;

    public BuyerSlidersAdapter(Activity context, List<BuyerSliderObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_image,btn_delete;
        private ProgressBar pb_load;
        public ViewHolder(View view) {
            super(view);
            img_image =  view.findViewById(R.id.item_buyer_slider_image);
            btn_delete =  view.findViewById(R.id.item_buyer_slider_remove);
            pb_load =  view.findViewById(R.id.item_buyer_slider_load);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_buyer_slider, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Glide.with(context)
                .load(APIUrl.SELLERS_SLIDERS_SERVER+list.get(position).getImage_url())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        holder.pb_load.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                   DataSource dataSource, boolean isFirstResource) {
                        holder.pb_load.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.img_image);
        holder.img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewImageDialog dialog = new ViewImageDialog(context,APIUrl.SELLERS_SLIDERS_SERVER+list.get(position).getImage_url());
                dialog.show();
            }
        });
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }
}
