package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.Brand;
import com.nyx.sequoiaapp.models.City;

import java.util.ArrayList;

/**
 * Created by Misheal on 10/23/2019.
 */

public class BrandsSpinnerAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    ArrayList brands;
    public BrandsSpinnerAdapter(Activity con , ArrayList brands) {
        // TODO Auto-generated constructor stub
        mInflater = LayoutInflater.from(con);
        this.brands = brands;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return brands==null?0:brands.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ListContent holder;
        View v = convertView;
        if (v == null) {
            v = mInflater.inflate(R.layout.my_spinner_style, null);
            holder = new ListContent();
            holder.name = (TextView) v.findViewById(R.id.textView1);
            v.setTag(holder);
        } else {

            holder = (ListContent) v.getTag();
        }


        holder.name.setText("" + ((Brand)brands.get(position)).getName());
        return v;
    }
    static class ListContent {
        TextView name;
    }
}
