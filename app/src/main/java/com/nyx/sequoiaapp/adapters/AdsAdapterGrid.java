package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nyx.sequoiaapp.R;

import java.util.ArrayList;

/**
 * Created by Luminance on 5/16/2018.
 */

public class AdsAdapterGrid extends BasicRecycleviewAdapter {
    public AdsAdapterGrid(ArrayList cats, Activity c) {
        super(cats, c);
    }


    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_ad_layout_mid, parent, false);

        return new MyView(itemView);
    }


}
