package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.SellerProductImageObject;

import java.util.List;

/**
 * Created by Misheal on 10/18/2019.
 */

public class SellerAddProductImagesAdapter extends  RecyclerView.Adapter<SellerAddProductImagesAdapter.ViewHolder> {
    private List<SellerProductImageObject> list;
    private Context context;
    private IMove iMove;
    private String type;

    public SellerAddProductImagesAdapter(Context context, List<SellerProductImageObject> list,String type,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
        this.type = type;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_image,btn_delete;
        private ProgressBar pb_loading;
        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_seller_product_image);
            btn_delete = view.findViewById(R.id.item_seller_product_delete);
            pb_loading = view.findViewById(R.id.loading);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_product_image, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SellerProductImageObject spio = list.get(position);
        if (spio.getUrl().equals("")){
            holder.img_image.setImageBitmap(spio.getBitmap());
        }else {
            try{
                holder.pb_loading.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(APIUrl.POSTS_PICS_SERVER+spio.getUrl())
                        .apply(new RequestOptions().fitCenter())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pb_loading.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pb_loading.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.img_image);
            }catch (Exception e){}
        }
        if (position == 0){
            if (type.equals("edit")){
                //holder.btn_delete.setVisibility(View.GONE);
            }
        }
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("هل تريد بالتأكيد حذف هذه الصورة ؟");
                builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        iMove.move(position);
                    }
                });
                builder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }
}
