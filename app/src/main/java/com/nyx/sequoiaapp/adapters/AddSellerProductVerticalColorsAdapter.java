package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.ColorObject;

import java.util.List;

/**
 * Created by Misheal on 10/23/2019.
 */

public class AddSellerProductVerticalColorsAdapter extends  RecyclerView.Adapter<AddSellerProductVerticalColorsAdapter.ViewHolder> {
    private List<ColorObject> list;
    private Context context;
    private IMove iMove;


    public AddSellerProductVerticalColorsAdapter( Context context,List<ColorObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private View v_color;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.name);
            v_color = view.findViewById(R.id.color_view);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_add_product_vertical_color, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_name.setText(list.get(position).getName());
        holder.v_color.setBackgroundColor(Color.parseColor(list.get(position).getValue()));
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }
}
