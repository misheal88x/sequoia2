package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;

import java.util.ArrayList;

public class MenuRecycleviewAdapter extends RecyclerView.Adapter<MenuRecycleviewAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView imageview;
        public LinearLayout body;


        public MyView(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.item_title);
            imageview = (ImageView) view.findViewById(R.id.item_icon);
                body = (LinearLayout) view.findViewById(R.id.item_layout);


        }
    }


    public MenuRecycleviewAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.grid_menu_item_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {
        final com.nyx.sequoiaapp.models.MenuItem item = (com.nyx.sequoiaapp.models.MenuItem)cats.get(position);
        holder.title.setText(item.getTitle());
        Glide.with(context).load(item.getIcon()).into(holder.imageview);
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              item.doTask();
            }
        });




    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}