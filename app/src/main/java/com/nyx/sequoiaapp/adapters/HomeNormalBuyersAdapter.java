package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.BuyerProductsActivity;
import com.nyx.sequoiaapp.models.BuyerSliderObject;
import com.nyx.sequoiaapp.models.NormalBuyerObject;

import java.util.ArrayList;
import java.util.List;

public class HomeNormalBuyersAdapter extends  RecyclerView.Adapter<HomeNormalBuyersAdapter.ViewHolder> {
    private List<NormalBuyerObject> list;
    private Activity context;


    public HomeNormalBuyersAdapter(Activity context, List<NormalBuyerObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_title,tv_city;
        private SimpleRatingBar rb_rate;
        private LinearLayout layout;
        public ViewHolder(View view) {
            super(view);
            tv_title =  view.findViewById(R.id.buyer_name);
            tv_city =  view.findViewById(R.id.buyer_city);
            rb_rate =  view.findViewById(R.id.buyer_rate);
            layout =  view.findViewById(R.id.product_body);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_normal_buyer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final NormalBuyerObject p = list.get(position);
        holder.tv_title.setText(p.getStore_name());
        holder.tv_city.setText(p.getCity_name());
        try {
            holder.rb_rate.setRating(Float.valueOf(String.valueOf(Math.round(Float.valueOf(p.getRating())))));
        }catch (Exception e){}
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<BuyerSliderObject> listOfSliders = new ArrayList<>();
                if (p.getImage_sliders()!=null){
                    if (p.getImage_sliders().size()>0){
                        for (int i = 0; i < p.getImage_sliders().size(); i++) {
                            listOfSliders.add(p.getImage_sliders().get(i));
                        }
                    }
                }
                Intent intent = new Intent(context, BuyerProductsActivity.class);
                intent.putExtra("id",String.valueOf(p.getId()));
                intent.putExtra("name",p.getStore_name());
                intent.putExtra("rate",p.getRating());
                intent.putExtra("sliders",new Gson().toJson(listOfSliders));
                context.startActivity(intent);
            }
        });
    }
}
