package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.SingleProductActivity;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.models.Cart;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;

/**
 * Created by Luminance on 5/16/2018.
 */

public class AdsAdapterOffers extends BasicRecycleviewAdapter {
    public AdsAdapterOffers(ArrayList cats, Activity c) {
        super(cats, c);
    }




    public class MyOfferView extends MyView {

        TextView offerTime;


        public MyOfferView(View view) {
            super(view);
            offerTime= view.findViewById(R.id.offer_time);
        }
    }









    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_ad_layout_offer, parent, false);

        return new MyOfferView(itemView);
    }
    @Override
    public void onBindViewHolder(final MyView holder, final int position) {
        String trimmedTitle = "";
        final ExtendedPost p = ((ExtendedPost)cats.get(position));
        /*
            String[] words =p.getTitle().split(" ");
            int min = Math.min(5 , words.length);
            for(int i=0;i<min;i++)trimmedTitle+=words[i]+" ";
            holder.title.setText(trimmedTitle.trim() + ((min==5)?"...":""));
            **/
        holder.title.setText(p.getTitle());
            ((MyOfferView)holder).offerTime.setText("باقي "+p.getRemaining()+" يوماً");
            holder.newproice.setText(p.getNewprice()+" "+context.getResources().getString(R.string.curremcy));
            if(!p.getOldprice().equals("")) {
                holder.oldprice.setText(p.getOldprice()+" "+context.getResources().getString(R.string.curremcy));
                holder.oldprice.setPaintFlags(  holder.oldprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        BaseFunctions.setFrescoImage(holder.imageview,p.getThumb());
            holder.body.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(context, SingleProductActivity.class);
                    in.putExtra("post" , p);
                    in.putExtra("is_offer",true);
                    in.putExtra("remaining","باقي "+p.getRemaining()+" يوماً");
                    in.putExtra("old_price",p.getOldprice()+" "+context.getResources().getString(R.string.curremcy));
                    context.startActivity(in);
                }
            });

        holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean item_found_in_cart = false;
                Cart cart = SavedCacheUtils.getCart(context);
                if (cart.getItems().size()>0){
                    for (CartItem ci : cart.getItems()){
                        if (ci.getPost().getId().equals(p.getId())){
                            Toast.makeText(context, "هذا المنتج موجود مسبقا في السلة", Toast.LENGTH_SHORT).show();
                            item_found_in_cart = true;
                            break;
                        }
                    }
                }
                if (!item_found_in_cart){
                    try {
                        addToCart(v, p, "", "" , "");
                    }catch (Exception e){}
                }
            }
        });

    }

}
