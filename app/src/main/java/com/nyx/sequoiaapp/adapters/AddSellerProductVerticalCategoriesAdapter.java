package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/12/2019.
 */

public class AddSellerProductVerticalCategoriesAdapter extends  RecyclerView.Adapter<AddSellerProductVerticalCategoriesAdapter.ViewHolder> {
    private List<Category> list;
    private List<Category> checked,previous;
    private Context context;


    public AddSellerProductVerticalCategoriesAdapter( Context context,List<Category> list,List<Category> previous) {
        this.context = context;
        this.list = list;
        this.checked = new ArrayList<>();
        this.previous = previous;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox tv_radio;

        public ViewHolder(View view) {
            super(view);
            tv_radio = view.findViewById(R.id.item_seller_product_category_row_radio);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_product_categoty_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Category c = list.get(position);
        if (previous.size()>0){
            for (Category cc : previous){
                if (c.getId().equals(cc.getId())){
                    holder.tv_radio.setChecked(true);
                    checked.add(c);
                }
            }
        }
        holder.tv_radio.setText(list.get(position).getName());
        holder.tv_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked.add(c);
                }else {
                    for (int i = 0; i < checked.size(); i++) {
                        if (checked.get(i).getId().equals(c.getId())){
                            checked.remove(i);
                        }
                    }
                }
            }
        });
    }

    public List<Category> get_checked(){
        return checked;
    }
}
