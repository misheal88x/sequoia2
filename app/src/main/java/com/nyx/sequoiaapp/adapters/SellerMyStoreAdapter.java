package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nyx.sequoiaapp.APIClass.ProductsAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.AddSellerProductActivity;
import com.nyx.sequoiaapp.activity.SellerAddOfferActivity;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Product;

import java.util.List;

/**
 * Created by Misheal on 10/14/2019.
 */

public class SellerMyStoreAdapter extends  RecyclerView.Adapter<SellerMyStoreAdapter.ViewHolder> {
    private List<Product> list;
    private Context context;


    public SellerMyStoreAdapter(Context context, List<Product> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView price;
        private TextView status;
        private TextView add_offer,edit,delete;
        private ImageView img_image;
        private ProgressBar progressBar;
        private Switch aSwitch;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_my_store_name);
            price = view.findViewById(R.id.item_my_store_price);
            status = view.findViewById(R.id.item_my_store_visibility_txt);
            add_offer = view.findViewById(R.id.item_my_store_add_offer);
            edit = view.findViewById(R.id.item_my_store_edit_);
            delete = view.findViewById(R.id.item_my_store_delete);
            img_image = view.findViewById(R.id.item_my_store_image);
            progressBar = view.findViewById(R.id.item_my_store_progress);
            aSwitch = view.findViewById(R.id.item_my_store_visibility_switch);
            layout = view.findViewById(R.id.item_my_store_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_store_product_temp, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Product ep = list.get(position);
        if (ep.getName() != null){
            holder.name.setText(ep.getName());
        }
        if (ep.getIs_offer()){
            holder.price.setText(String.valueOf(Math.round(ep.getDiscount_price())));
            holder.add_offer.setVisibility(View.GONE);
        }else {
            holder.price.setText(String.valueOf(Math.round(ep.getPrice()))+ " " + context.getResources().getString(R.string.curremcy));
        }
        if (ep.getImages()!=null){
            if (ep.getImages().getThumbs()!=null){
                if (ep.getImages().getThumbs().size()>0){
                    Glide.with(context)
                            .load(APIUrl.POSTS_PICS_SERVER+ep.getImages().getThumbs().get(0))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                            Target<Drawable> target, boolean isFirstResource) {
                                    holder.progressBar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                               DataSource dataSource, boolean isFirstResource) {
                                    holder.progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(holder.img_image);
                }
            }
        }

        holder.add_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SellerAddOfferActivity.class);
                intent.putExtra("activity_type","add");
                intent.putExtra("product_id",ep.getId());
                intent.putExtra("product_name",ep.getName());
                intent.putExtra("prime_price",Math.round(ep.getPrime_price()));
                context.startActivity(intent);
            }
        });
        if (ep.getView() == 1){
            holder.aSwitch.setChecked(false);
            holder.status.setText("مخفي عن الزبائن");
        }else {
            holder.aSwitch.setChecked(true);
            holder.status.setText("ظاهر للزبائن");
        }
        holder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    callSwitchAPI(holder,String.valueOf(ep.getId()),"0");
                }else {
                    callSwitchAPI(holder,String.valueOf(ep.getId()),"1");
                }
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Intent in = new Intent(context, SingleProductActivity.class);
                ExtendedPost p1 = new ExtendedPost();
                p1.setId(String.valueOf(ep.getId()));
                p1.setTitle(ep.getName());
                if (ep.getImages().getThumbs()!=null){
                    if (ep.getImages().getThumbs().size()>0){
                        p1.setThumb(ep.getImages().getThumbs().get(0));
                    }
                }

                p1.setOldprice(String.valueOf(ep.getPrime_price()));
                p1.setNewprice(String.valueOf(ep.getPrice()));
                p1.setDescription(ep.getDescription());
                if (ep.getImages().getThumbs()!=null){
                    if (ep.getImages().getThumbs().size()>0){
                        String[] temp = new String[ep.getImages().getThumbs().size()];
                        for (int i = 0; i < ep.getImages().getThumbs().size(); i++) {
                            temp[i] = ep.getImages().getThumbs().get(i);
                        }
                        p1.setImgs(temp);
                    }
                }
                p1.setSearchCounter(String.valueOf(ep.getSearch_count()));
                p1.setRating(String.valueOf(ep.getView_rating()));
                p1.setStatus(String.valueOf(ep.getStatus()));
                p1.setIs_offer(booleanToStringConverter(ep.getIs_offer()));
                p1.setRemaining(String.valueOf(ep.getTime_remaining()));
                p1.setRemaining(String.valueOf(ep.getQty_in_stock()));
                in.putExtra("post", p1);
                context.startActivity(in);
                **/
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddSellerProductActivity.class);
                intent.putExtra("product_id",ep.getId());
                intent.putExtra("activity_type","edit");
                context.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("هل تريد بالتأكيد حذف هذا المنتج ؟").
                        setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callDeleteAPI(String.valueOf(ep.getId()),position);
                            }
                        }).setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }

    private String booleanToStringConverter(boolean in){
        if (in) return "1";
        return "0";
    }

    private void callSwitchAPI(final ViewHolder holder, String product_id, final String status){
        ProductsAPIsClass.switch_state(context, product_id, status, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if (json!=null){
                    if (status.equals("0")){
                        holder.status.setText("ظاهر للزبائن");
                        Toast.makeText(context, "تم إظهار المنتج", Toast.LENGTH_SHORT).show();
                    }else {
                        holder.status.setText("مخفي عن الزبائن");
                        Toast.makeText(context, "تم إخفاء المنتج", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (status.equals("1")){
                        holder.aSwitch.setChecked(true);
                        holder.status.setText("ظاهر للزبائن");
                    }else {
                        holder.aSwitch.setChecked(false);
                        holder.status.setText("مخفي عن الزبائن");
                    }
                }
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                Toast.makeText(context, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                if (status.equals("1")){
                    holder.aSwitch.setChecked(true);
                    holder.status.setText("ظاهر للزبائن");
                }else {
                    holder.aSwitch.setChecked(false);
                    holder.status.setText("مخفي عن الزبائن");
                }
            }
        });
    }

    private void callDeleteAPI(String product_id, final int postion){
        String user_id = SharedPrefManager.getInstance(context).getUser().getUser_id();
        ProductsAPIsClass.deleteProduct(context, user_id, product_id, new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json) {
                if (json!=null){
                    Toast.makeText(context, "تم حذف المنتج بنجاح", Toast.LENGTH_SHORT).show();
                    list.remove(postion);
                    notifyDataSetChanged();
                }
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                Toast.makeText(context, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
