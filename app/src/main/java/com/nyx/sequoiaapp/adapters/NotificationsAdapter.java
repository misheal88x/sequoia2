package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.OrderDetailsActivity;
import com.nyx.sequoiaapp.models.Notifications;
import com.nyx.sequoiaapp.models.PayloadObject;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView title ,time ,content,confirm_order;
        public  View body;


        public MyView(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            content = (TextView) view.findViewById(R.id.content);
            time = (TextView) view.findViewById(R.id.time);
            body =view.findViewById(R.id.body);
            confirm_order =view.findViewById(R.id.confirm_order);
        }
    }


    public NotificationsAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_notifications_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

       final Notifications p = ((Notifications)cats.get(position));

        holder.title.setText(p.getTitle());
        holder.content.setText(p.getContent());
        holder.time.setText(p.getTime());
        holder.body.setOnClickListener(p.getClick());

        if (p.getTarget_type()!=null){
            if (!p.getTarget_type().equals("")){
                if (p.getTarget_type().equals("122")){
                    try{
                        final PayloadObject po = new Gson().fromJson(p.getPayload(),PayloadObject.class);
                        if (po.isRequire_confirmation()){
                            if (po.getOrder_id()!=null){
                                if (!po.getOrder_id().equals("")){
                                    holder.confirm_order.setVisibility(View.VISIBLE);
                                    holder.confirm_order.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(context,OrderDetailsActivity.class);
                                            intent.putExtra("type","with");
                                            intent.putExtra("order_id",po.getOrder_id());
                                            context.startActivity(intent);
                                        }
                                    });
                                }
                            }
                        }
                    }catch (Exception e){}
                }
            }
        }




    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}