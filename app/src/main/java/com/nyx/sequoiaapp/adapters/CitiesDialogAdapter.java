package com.nyx.sequoiaapp.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/11/2019.
 */

public class CitiesDialogAdapter extends  RecyclerView.Adapter<CitiesDialogAdapter.ViewHolder> {
    private ArrayList<City> sourceList;
    private List<String> selectedCities;
    private Context context;
    private IMove iMove1;


    public CitiesDialogAdapter( Context context) {
        this.context = context;
        this.sourceList = new ArrayList<>();
        this.selectedCities = new ArrayList<>();
        try {
            ArrayList x = SharedPrefManager.getInstance(context).getCities();
            if (x.size()>0){
                for (int i = 0; i < x.size(); i++) {
                    City c = (City) x.get(i);
                    if (c.getName().equals("دمشق")||c.getName().equals("طرطوس")||c.getName().equals("حمص")){
                        sourceList.add(c);
                    }

                }
            }
        }catch (Exception e){}
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkBox;

        public ViewHolder(View view) {
            super(view);
            checkBox = view.findViewById(R.id.item_select_city_check);
        }
    }

    @Override
    public int getItemCount() {
        return sourceList.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_city, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.checkBox.setText(sourceList.get(position).getName());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selectedCities.add(sourceList.get(position).getId());
                }else {
                    for (int i = 0; i < selectedCities.size(); i++) {
                        if (selectedCities.get(i).equals(sourceList.get(position).getId())){
                            selectedCities.remove(i);
                        }
                    }
                }
            }
        });
    }

    public List<String> getSelectedCities(){
        return selectedCities;
    }
}
