package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.RootActivity;
import com.nyx.sequoiaapp.activity.SingleProductActivity;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.models.Cart;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.models.Product;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Misheal on 10/21/2019.
 */

public class SearchProductsAdapter extends  RecyclerView.Adapter<SearchProductsAdapter.ViewHolder> {
    private List<Product> list;
    private Activity context;


    public SearchProductsAdapter(Activity context, List<Product> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView oldprice, newproice, title, offer_time,tv_address;
        public SimpleDraweeView imageview;
        public LinearLayout body;
        public RelativeLayout add_to_cart;
        public ViewHolder(View view) {
            super(view);
            oldprice = (TextView) view.findViewById(R.id.old_price);
            offer_time = (TextView) view.findViewById(R.id.offer_time);
            newproice = (TextView) view.findViewById(R.id.curr_price);
            title = (TextView) view.findViewById(R.id.ad_title);
            imageview =  view.findViewById(R.id.pic);
            body = (LinearLayout) view.findViewById(R.id.product_body);
            tv_address = view.findViewById(R.id.address_txt);
            add_to_cart = view.findViewById(R.id.add_to_cart);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_product, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        String trimmedTitle = "";
        final Product p = ((Product) list.get(position));
        String[] words = p.getName().split(" ");
        int min = Math.min(5, words.length);
        for (int i = 0; i < min; i++) trimmedTitle += words[i] + " ";
        holder.title.setText(trimmedTitle.trim() + ((min == 5) ? "..." : ""));
        holder.newproice.setText(Math.round(p.getPrice()) + " " + context.getResources().getString(R.string.curremcy));

        //todo missing is offer field
        if (!p.getIs_offer())
        {
            holder.offer_time.setVisibility(View.GONE);
            holder.oldprice.setVisibility(View.GONE);
        }else
        {
            holder.offer_time.setVisibility(View.VISIBLE);
            holder.oldprice.setVisibility(View.VISIBLE);
            holder.oldprice.setText(Math.round(p.getDiscount_price()) + " " + context.getResources().getString(R.string.curremcy));
            holder.oldprice.setPaintFlags(holder.oldprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.offer_time.setText("باقي " + p.getTime_remaining() + " يوماً");
        }
        String address = "";
        if (p.getCity_name()!= null && !p.getCity_name().equals("")){
            address+=p.getCity_name();
            if (p.getStore_name()!=null&&!p.getStore_name().equals("")){
                address+=", المتجر : "+p.getStore_name();
            }
            try {
                holder.tv_address.setText(address);
                holder.tv_address.setVisibility(View.VISIBLE);
            }catch (Exception e){}
        }else {
            //holder.tv_address.setVisibility(View.GONE);
        }
        if (p.getImages()!=null){
            if (p.getThumb().getThumbs()!=null){
                if (p.getThumb().getThumbs().size()>0){
                    BaseFunctions.setFrescoImage(holder.imageview,APIUrl.POSTS_PICS_SERVER+p.getThumb().getThumbs().get(0));
                }
            }
        }

        holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean item_found_in_cart = false;
                Cart cart = SavedCacheUtils.getCart(context);
                if (cart.getItems().size()>0){
                    for (CartItem ci : cart.getItems()){
                        if (ci.getPost().getId().equals(String.valueOf(p.getId()))){
                            Toast.makeText(context, "هذا المنتج موجود مسبقا في السلة", Toast.LENGTH_SHORT).show();
                            item_found_in_cart = true;
                            break;
                        }
                    }
                }
                if (!item_found_in_cart){
                    try {
                        ExtendedPost p1 = new ExtendedPost();
                        p1.setId(String.valueOf(p.getId()));
                        p1.setTitle(p.getName());
                        p1.setThumb(APIUrl.POSTS_PICS_SERVER + p.getThumb().getThumbs().get(0));
                        p1.setOldprice(String.valueOf(p.getPrice()));
                        p1.setNewprice(String.valueOf(p.getPrime_price()));
                        p1.setDescription(p.getDescription());
                        String[] imgs;
                        List<String> imgs_list = new ArrayList<>();
                        if (p.getImages()!=null){
                            if (p.getImages().getImages()!=null){
                                if (p.getImages().getImages().size()>0){
                                    for (String s : p.getImages().getImages()){
                                        imgs_list.add(s);
                                    }
                                    imgs = new String[imgs_list.size()];
                                    for (int i = 0; i < imgs_list.size(); i++) {
                                        imgs[i] = imgs_list.get(i);
                                    }
                                }else {
                                    imgs = new String[0];
                                }
                            }else {
                                imgs = new String[0];
                            }
                        }else {
                            imgs = new String[0];
                        }
                        p1.setImgs(imgs);
                        p1.setSearchCounter(String.valueOf(p.getSearch_count()));
                        p1.setStatus("1");
                        p1.setIs_offer(String.valueOf(p.getIs_offer()));
                        p1.setRemaining(String.valueOf(p.getTime_remaining()));
                        p1.setView("0");
                        p1.setDiscount_view("0");
                        p1.setStore_name(p.getStore_name());
                        p1.setCity_name(p.getCity_name());
                        addToCart(v, p1, "", "", "");
                    }catch (Exception e){}
                }
            }
        });
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, SingleProductActivity.class);
                ExtendedPost p1 = new ExtendedPost();
                p1.setId(String.valueOf(p.getId()));
                p1.setTitle(p.getName());
                if (p.getThumb().getThumbs()!=null){
                    if (p.getThumb().getThumbs().size()>0){
                        p1.setThumb(p.getThumb().getThumbs().get(0));
                    }
                }

                p1.setOldprice(String.valueOf(Math.round(p.getPrime_price())));
                p1.setNewprice(String.valueOf(Math.round(p.getPrice())));
                p1.setDescription(p.getDescription());
                if (p.getThumb().getThumbs()!=null){
                    if (p.getThumb().getThumbs().size()>0){
                        String[] temp = new String[p.getThumb().getThumbs().size()];
                        for (int i = 0; i < p.getThumb().getThumbs().size(); i++) {
                            temp[i] = APIUrl.POSTS_PICS_SERVER+p.getThumb().getThumbs().get(i);
                        }
                        p1.setImgs(temp);
                    }
                }
                p1.setSearchCounter(String.valueOf(p.getSearch_count()));
                p1.setRating(String.valueOf(p.getView_rating()));
                p1.setStatus(String.valueOf(p.getStatus()));
                p1.setIs_offer(booleanToStringConverter(p.getIs_offer()));
                p1.setRemaining(String.valueOf(p.getQty_in_stock()));
                in.putExtra("post", p1);
                if (p.isIs_offer()){
                    in.putExtra("is_offer",true);
                    in.putExtra("remaining","باقي "+p.getTime_remaining()+" يوماً");
                    in.putExtra("old_price",p.getDiscount_price()+" "+context.getResources().getString(R.string.curremcy));
                }
                context.startActivity(in);
                //if (context instanceof NewSearchResultsActivity) context.finish();
            }
        });
    }

    void addToCart(View v, final ExtendedPost p , String color , String size , String measure) {

        boolean b = SavedCacheUtils.addToCart(context, p, 1
                ,color , size , measure
        );
        if (b) {
            setNotifCount(SavedCacheUtils.getCart(context).size());
            /*
            Snackbar snack = Snackbar.make(findViewById(R.id.root_layout), "اذهب إلى السلة لتأكيد طلبك", Snackbar.LENGTH_LONG);
            View view = snack.getView();
            FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(getResources().getColor(R.color.white));
            snack.show();
            **/
            Cart cart = SavedCacheUtils.getCart(context);
            if (cart.getItems().size()==1){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = context.getLayoutInflater().inflate(R.layout.dialog_go_to_cart,null);
                ImageView arro = view.findViewById(R.id.dialog_go_to_cart_arrow);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                dialog.setCancelable(false);
                arro.setRotation(-45f);
                dialog.show();
                CountDownTimer countDownTimer = new CountDownTimer(2000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        dialog.cancel();
                    }
                }.start();
            }else {
                Toast.makeText(context, "تم إضافة المنتج إلى السلة", Toast.LENGTH_SHORT).show();
            }

            //Snackbar.make(v, "تم اضافة " + quantity_amount + " عنصر إلى سلة المشتريات", Snackbar.LENGTH_LONG)
            // .show();

        }
        setNotifCount(SavedCacheUtils.getCart(context).size());
    }

    public void setNotifCount(int count){
        try {
            RootActivity.mNotifCount = count;
            context.invalidateOptionsMenu();
        }catch (Exception e){}
    }

    private String booleanToStringConverter(boolean in){
        if (in) return "1";
        return "0";
    }
}
