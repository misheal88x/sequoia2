package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.BrandProductsActivity;
import com.nyx.sequoiaapp.models.Brand;
import com.nyx.sequoiaapp.models.BuyerSliderObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luminance on 5/20/2018.
 */

public class BrandsAdapter  extends RecyclerView.Adapter<BrandsAdapter.MyView>{
    ArrayList cats;
    public Activity context;
    @Override
    public int getItemCount() {
        return cats.size();
    }

    public BrandsAdapter(ArrayList cats, Activity c) {
        this.cats = cats;
        this.context=c;
    }

    public class MyView  extends RecyclerView.ViewHolder {

        TextView name ;
        SimpleDraweeView img;
        private SimpleRatingBar rb_rate;
        View body;


        public MyView(View view) {
            super(view);
            name= view.findViewById(R.id.brand_name);
            img= view.findViewById(R.id.pic);
            body= view.findViewById(R.id.product_body);
            rb_rate = view.findViewById(R.id.brand_rate);
        }
    }




    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_brand_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        String trimmedTitle = "";
        final Brand p = ((Brand)cats.get(position));
        /*
        String[] words =p.getName().split(" ");
        int min = Math.min(5 , words.length);


        for(int i=0;i<min;i++)trimmedTitle+=words[i]+" ";
        holder.name.setText(trimmedTitle.trim() + ((min==5)?"...":""));
**/
        try {
            holder.rb_rate.setRating(Float.valueOf(String.valueOf(Math.round(Float.valueOf(p.getRating())))));
        }catch (Exception e){}
        holder.name.setText(p.getName());
        BaseFunctions.setFrescoImage(holder.img,p.getImage());
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<BuyerSliderObject> listOfSliders = new ArrayList<>();
                if (p.getImage_sliders()!=null){
                    if (p.getImage_sliders().length()>0){
                        for (int i = 0; i < p.getImage_sliders().length(); i++) {
                            BuyerSliderObject bso = new BuyerSliderObject();
                            try {
                                bso.setId(p.getImage_sliders().getJSONObject(i).getString("id"));
                                bso.setImage_url(p.getImage_sliders().getJSONObject(i).getString("image_url"));
                                listOfSliders.add(bso);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                Intent ii = new Intent(context , BrandProductsActivity.class);
                ii.putExtra("name" , p.getName());
                ii.putExtra("id" , p.getId());
                ii.putExtra("desc" , p.getDesc());
                ii.putExtra("rate",p.getRating());
                ii.putExtra("sliders",new Gson().toJson(listOfSliders));
                context.startActivity(ii);

            }
        });
    }

}
