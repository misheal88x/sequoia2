package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.RootActivity;
import com.nyx.sequoiaapp.activity.SingleProductActivity;
import com.nyx.sequoiaapp.helper.SavedCacheUtils;
import com.nyx.sequoiaapp.models.Cart;
import com.nyx.sequoiaapp.models.CartItem;
import com.nyx.sequoiaapp.models.ExtendedPost;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;

public class BasicRecycleviewAdapter extends RecyclerView.Adapter<BasicRecycleviewAdapter.MyView> {

    ArrayList cats;
    public Activity context;

    public class MyView extends RecyclerView.ViewHolder {

        public TextView oldprice, newproice, title, offer_time,tv_address;
        public SimpleDraweeView imageview;
        public LinearLayout body;
        public RelativeLayout add_to_cart;

        public MyView(View view) {
            super(view);

            oldprice = (TextView) view.findViewById(R.id.old_price);
            offer_time = (TextView) view.findViewById(R.id.offer_time);
            newproice = (TextView) view.findViewById(R.id.curr_price);
            title = (TextView) view.findViewById(R.id.ad_title);
            imageview = view.findViewById(R.id.pic);
            tv_address = view.findViewById(R.id.address_txt);
            body = (LinearLayout) view.findViewById(R.id.product_body);
            add_to_cart = view.findViewById(R.id.add_to_cart);
        }
    }


    public BasicRecycleviewAdapter(ArrayList cats, Activity c) {
        this.cats = cats;
        this.context = c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_ad_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {


        String trimmedTitle = "";
        final ExtendedPost p = ((ExtendedPost) cats.get(position));
        /*
        String[] words = p.getTitle().split(" ");
        int min = Math.min(5, words.length);
        for (int i = 0; i < min; i++) trimmedTitle += words[i] + " ";
        holder.title.setText(trimmedTitle.trim() + ((min == 5) ? "..." : ""));
        **/
        holder.title.setText(p.getTitle());
        holder.newproice.setText(p.getNewprice() + " " + context.getResources().getString(R.string.curremcy));


        if (p.getIs_offer().equals("0"))
        {
            holder.offer_time.setVisibility(View.GONE);
            holder.oldprice.setVisibility(View.GONE);
        }else
        {
            holder.offer_time.setVisibility(View.VISIBLE);
            holder.oldprice.setVisibility(View.VISIBLE);
            holder.oldprice.setText(p.getOldprice() + " " + context.getResources().getString(R.string.curremcy));
            holder.oldprice.setPaintFlags(holder.oldprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.offer_time.setText("باقي " + p.getRemaining() + " يوماً");
        }
        String address = "";
        if (p.getCity_name()!= null && !p.getCity_name().equals("")){
            address+=p.getCity_name();
            if (p.getStore_name()!=null&&!p.getStore_name().equals("")){
                address+=", المتجر : "+p.getStore_name();
            }
            try {
                holder.tv_address.setText(address);
                holder.tv_address.setVisibility(View.VISIBLE);
            }catch (Exception e){}
        }else {
            //holder.tv_address.setVisibility(View.GONE);
        }

        BaseFunctions.setFrescoImage(holder.imageview,p.getThumb());
        holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean item_found_in_cart = false;
                Cart cart = SavedCacheUtils.getCart(context);
                if (cart.getItems().size()>0){
                    for (CartItem ci : cart.getItems()){
                        if (ci.getPost().getId().equals(p.getId())){
                            Toast.makeText(context, "هذا المنتج موجود مسبقا في السلة", Toast.LENGTH_SHORT).show();
                            item_found_in_cart = true;
                            break;
                        }
                    }
                }
                if (!item_found_in_cart){
                    try {
                        addToCart(v, p, "", "","");
                    }catch (Exception e){}
                }
            }
        });
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, SingleProductActivity.class);
                if (p.getId().equals("5301")){
                    ExtendedPost p1 = p;
                    String s = "hello";
                }
                in.putExtra("post", p);
                context.startActivity(in);
                if (context instanceof SingleProductActivity) context.finish();

            }
        });
    }

    @Override
    public int getItemCount() {
        return cats.size();
    }

    void addToCart(View v, final ExtendedPost p , String color , String size , String measure) {

        boolean b = SavedCacheUtils.addToCart(context, p, 1
                ,color , size , measure
        );
        if (b) {
            setNotifCount(SavedCacheUtils.getCart(context).size());
            /*
            Snackbar snack = Snackbar.make(findViewById(R.id.root_layout), "اذهب إلى السلة لتأكيد طلبك", Snackbar.LENGTH_LONG);
            View view = snack.getView();
            FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(getResources().getColor(R.color.white));
            snack.show();
            **/
            Cart cart = SavedCacheUtils.getCart(context);
            if (cart.getItems().size()==1){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = context.getLayoutInflater().inflate(R.layout.dialog_go_to_cart,null);
                ImageView arro = view.findViewById(R.id.dialog_go_to_cart_arrow);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                dialog.setCancelable(false);
                arro.setRotation(-45f);
                dialog.show();
                CountDownTimer countDownTimer = new CountDownTimer(2000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        dialog.cancel();
                    }
                }.start();
            }else {
                Toast.makeText(context, "تم إضافة المنتج إلى السلة", Toast.LENGTH_SHORT).show();
            }

            //Snackbar.make(v, "تم اضافة " + quantity_amount + " عنصر إلى سلة المشتريات", Snackbar.LENGTH_LONG)
            // .show();

        }
        setNotifCount(SavedCacheUtils.getCart(context).size());
    }

    public void setNotifCount(int count){
        try {
            RootActivity.mNotifCount = count;
            context.invalidateOptionsMenu();
        }catch (Exception e){}
    }
}