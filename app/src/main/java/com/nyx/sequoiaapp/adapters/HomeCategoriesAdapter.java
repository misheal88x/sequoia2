package com.nyx.sequoiaapp.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.CategoriesActivity;
import com.nyx.sequoiaapp.activity.CategoryProductsActivity;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.Category;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;

/**
 * Created by Misheal on 11/4/2019.
 */

public class HomeCategoriesAdapter extends  RecyclerView.Adapter<HomeCategoriesAdapter.ViewHolder> {
    private ArrayList list;
    private Activity context;


    public HomeCategoriesAdapter(Activity context, ArrayList list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_image;
        private TextView tv_name;
        private LinearLayout lyt_layout;
        public ViewHolder(View view) {
            super(view);
            img_image =  view.findViewById(R.id.item_home_category_image);
            tv_name =  view.findViewById(R.id.item_home_category_text);
            lyt_layout =  view.findViewById(R.id.item_home_category_layout);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Category c = (Category) list.get(position);
        holder.tv_name.setText(c.getName());
        Log.i("image_url", "onBindViewHolder: "+APIUrl.CATEGORIES_PICS_SERVER+c.getImage());
        String url = APIUrl.CATEGORIES_PICS_SERVER+c.getImage();
        BaseFunctions.setFrescoImage(holder.img_image,url);
        holder.lyt_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean hasChildren =
                        SharedPrefManager.getInstance(context).getCategories(c.getId()).size()>0;
                if(hasChildren) {
                    Intent ii = new Intent(context, CategoriesActivity.class);
                    ii.putExtra("par", c.getId());
                    ii.putExtra("name", c.getName());
                    context.startActivity(ii);
                }else{
                    Intent ii = new Intent(context, CategoryProductsActivity.class);
                    ii.putExtra("id", c.getId());
                    ii.putExtra("title", c.getName());
                    context.startActivity(ii);
                }
            }
        });
    }
}
