package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.BrandProductsActivity;
import com.nyx.sequoiaapp.models.BuyerSliderObject;
import com.nyx.sequoiaapp.models.NewBrandObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 11/1/2019.
 */

public class AllBrandsAdapter extends  RecyclerView.Adapter<AllBrandsAdapter.ViewHolder> {
    private List<NewBrandObject> list;
    private Context context;



    public AllBrandsAdapter( Context context,List<NewBrandObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name ;
        private SimpleDraweeView img;
        private SimpleRatingBar rb_rate;
        private View body;

        public ViewHolder(View view) {
            super(view);
            name= view.findViewById(R.id.brand_name);
            img= view.findViewById(R.id.pic);
            body= view.findViewById(R.id.product_body);
            rb_rate = view.findViewById(R.id.brand_rate);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_brand_wide_layout, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        String trimmedTitle = "";
        final NewBrandObject p = list.get(position);


        String[] words =p.getName().split(" ");
        int min = Math.min(5 , words.length);


        for(int i=0;i<min;i++)trimmedTitle+=words[i]+" ";
        holder.name.setText(trimmedTitle.trim() + ((min==5)?"...":""));
        try {
            holder.rb_rate.setRating(Float.valueOf(String.valueOf(Math.round(Float.valueOf(p.getRating())))));
        }catch (Exception e){}
        BaseFunctions.setFrescoImage(holder.img,p.getImage());
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<BuyerSliderObject> listOfSliders = new ArrayList<>();
                if (p.getImage_sliders()!=null){
                    if (p.getImage_sliders().size()>0){
                        for (int i = 0; i < p.getImage_sliders().size(); i++) {
                            listOfSliders.add(p.getImage_sliders().get(i));
                        }
                    }
                }
                Intent ii = new Intent(context , BrandProductsActivity.class);
                ii.putExtra("name" , p.getName());
                ii.putExtra("id" , String.valueOf(p.getId()));
                ii.putExtra("desc" , p.getDescription());
                ii.putExtra("rate",p.getRating());
                ii.putExtra("sliders",new Gson().toJson(listOfSliders));
                context.startActivity(ii);

            }
        });
    }
}
