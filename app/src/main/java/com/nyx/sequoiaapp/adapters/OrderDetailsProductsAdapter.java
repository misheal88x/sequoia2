package com.nyx.sequoiaapp.adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.models.Product;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 11/12/2019.
 */

public class OrderDetailsProductsAdapter extends  RecyclerView.Adapter<OrderDetailsProductsAdapter.ViewHolder> {
    private List<Product> list;
    private Activity context;


    public OrderDetailsProductsAdapter(Activity context, List<Product> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        Button plus , minus , delete;
        EditText quanity_center;
        SimpleDraweeView iv_image;
        TextView tv_title,tv_price;
        public ViewHolder(View view) {
            super(view);
            plus= view.findViewById(R.id.btn_plus);
            minus= view.findViewById(R.id.btn_minus);
            delete= view.findViewById(R.id.delete_from_cart);
            quanity_center= view.findViewById(R.id.quantity);
            iv_image = view.findViewById(R.id.pic);
            tv_title = view.findViewById(R.id.ad_title);
            tv_price = view.findViewById(R.id.curr_price);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_ad_layout_cart, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Product p = list.get(position);
        holder.plus.setVisibility(View.GONE);
        holder.minus.setVisibility(View.GONE);
        holder.delete.setVisibility(View.GONE);
        holder.quanity_center.setActivated(false);
        holder.quanity_center.setClickable(false);
        if (p.getImages()!=null){
            if (p.getImages().getThumbs()!=null){
                if (p.getImages().getThumbs().size()>0){
                    BaseFunctions.setFrescoImage(holder.iv_image,APIUrl.POSTS_PICS_SERVER+p.getImages().getThumbs().get(0));
                }
            }
        }
        holder.tv_title.setText(p.getName());
        holder.tv_price.setText(String.valueOf(p.getFinal_price())+" ل.س");
        holder.quanity_center.setText(String.valueOf(p.getQty()));

    }
}
