package com.nyx.sequoiaapp.adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.nyx.sequoiaapp.Interfaces.IMove;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.City;

import java.util.ArrayList;

/**
 * Created by Misheal on 11/5/2019.
 */

public class SearchCitiesAdapter extends  RecyclerView.Adapter<SearchCitiesAdapter.ViewHolder>{
    private ArrayList list;
    private Activity context;
    private String selected_city_id;
    private IMove iMove;

    public SearchCitiesAdapter(Activity context, ArrayList list,String selected_city_id,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
        this.selected_city_id = selected_city_id;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private RadioButton rb_radio;
        public ViewHolder(View view) {
            super(view);
            rb_radio =  view.findViewById(R.id.item_seller_product_category_row_radio);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_seller_product_size_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        City c = (City) list.get(position);
        holder.rb_radio.setText(c.getName());
        if (c.getId().equals(selected_city_id)){
            holder.rb_radio.setChecked(true);
        }
        holder.rb_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    iMove.move(position);
                }
            }
        });
    }
}
