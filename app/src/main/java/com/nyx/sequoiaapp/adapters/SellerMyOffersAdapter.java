package com.nyx.sequoiaapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nyx.sequoiaapp.APIClass.OffersAPIsClass;
import com.nyx.sequoiaapp.Interfaces.IFailure;
import com.nyx.sequoiaapp.Interfaces.IResponse;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.activity.SellerAddOfferActivity;
import com.nyx.sequoiaapp.helper.APIUrl;
import com.nyx.sequoiaapp.helper.SharedPrefManager;
import com.nyx.sequoiaapp.models.OfferObject;
import com.nyx.sequoiaapp.other.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 10/16/2019.
 */

public class SellerMyOffersAdapter extends  RecyclerView.Adapter<SellerMyOffersAdapter.ViewHolder> {
    private List<OfferObject> list;
    private Context context;


    public SellerMyOffersAdapter(Context context, List<OfferObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_image;
        private TextView tv_name;
        private TextView tv_old_price,tv_new_price,tv_start_date,tv_end_date;
        private Button btn_delete,btn_edit;
        private LinearLayout layout;
        private ProgressBar progressBar;
        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_my_offers_image);
            tv_name = view.findViewById(R.id.item_my_offers_name);
            tv_old_price = view.findViewById(R.id.item_my_offers_old_price);
            tv_new_price = view.findViewById(R.id.item_my_offers_new_price);
            tv_start_date = view.findViewById(R.id.item_my_offers_start_date);
            tv_end_date = view.findViewById(R.id.item_my_offers_end_date);
            btn_delete = view.findViewById(R.id.item_my_offers_delete);
            btn_edit = view.findViewById(R.id.item_my_offers_edit);
            progressBar = view.findViewById(R.id.item_my_offers_progress);
            layout = view.findViewById(R.id.item_my_offers_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_offers, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OfferObject oo = list.get(position);
        if (oo.getProduct_name() != null) {
            holder.tv_name.setText(oo.getProduct_name());
        }
        holder.tv_old_price.setText(String.valueOf(oo.getPrime_price()) + " ل.س");
        holder.tv_new_price.setText(String.valueOf(oo.getDiscount_price()) + " ل.س");
        if (oo.getStart_date() != null) {
            holder.tv_start_date.setText("من : " + BaseFunctions.dateExtractor(oo.getStart_date()));
        }
        if (oo.getEnd_date() != null) {
            holder.tv_end_date.setText("إلى : " + BaseFunctions.dateExtractor(oo.getEnd_date()));
        }
        if (oo.getImages().getThumbs().size() > 0) {
            Glide.with(context)
                    .load(APIUrl.POSTS_PICS_SERVER+oo.getImages().getThumbs().get(0))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.img_image);
        }
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("هل تريد بالتأكيد حذف هذا العرض ؟").
                        setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callDeleteAPI(String.valueOf(oo.getId()),position);
                            }
                        }).setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SellerAddOfferActivity.class);
                intent.putExtra("activity_type","edit");
                intent.putExtra("product_id",oo.getProduct_id());
                intent.putExtra("offer_id",oo.getId());
                intent.putExtra("product_name",oo.getProduct_name());
                intent.putExtra("prime_price",oo.getPrime_price());
                context.startActivity(intent);
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void callDeleteAPI(String offer_id, final int position){
        String user_id = SharedPrefManager.getInstance(context).getUser().getUser_id();
        OffersAPIsClass.remove(context, user_id, offer_id, new IResponse() {
            @Override
            public void onResponse() {
                list.remove(position);
                notifyDataSetChanged();
                Toast.makeText(context, "تم حذف العرض بنجاح", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Object json) {
                Toast.makeText(context, "حدث خطأ ما أعد المحاولة", Toast.LENGTH_SHORT).show();
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                Toast.makeText(context, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
