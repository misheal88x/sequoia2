package com.nyx.sequoiaapp.adapters;

/**
 * Created by Luminance on 1/6/2018.
 */

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.sequoiaapp.R;
import com.nyx.sequoiaapp.models.MyComissionRecord;

import java.util.ArrayList;

public class ComissionsRecordsAdapter extends RecyclerView.Adapter<ComissionsRecordsAdapter.MyView> {

    ArrayList cats;
    public Activity context;
    public class MyView extends RecyclerView.ViewHolder {

        public TextView uid , oid , status ,date  ,value , product , customer;
        ImageView pic;


        public MyView(View view) {
            super(view);

            uid = (TextView) view.findViewById(R.id.uid);
            oid = (TextView) view.findViewById(R.id.oid);
            status = (TextView) view.findViewById(R.id.status);
            date = (TextView) view.findViewById(R.id.date);
            value = (TextView) view.findViewById(R.id.value);
            product = (TextView) view.findViewById(R.id.product);
            customer = (TextView) view.findViewById(R.id.customer);
            pic = (ImageView) view.findViewById(R.id.pic);
        }
    }
    public ComissionsRecordsAdapter(ArrayList cats , Activity c) {
        this.cats = cats;
        this.context=c;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.single_comission_record_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

     final MyComissionRecord p = ((MyComissionRecord) cats.get(position));
        holder.uid.setText(p.getUid());
        holder.oid.setText(p.getOid());
        holder.status.setText(p.getStatus());
        holder.date.setText(p.getDate());
        holder.product.setText(p.getProduct_name());
        holder.customer.setText(p.getCustomer());
        holder.value.setText(p.getValue()+" "+context.getResources().getString(R.string.curremcy));
        if(!p.getPic().trim().equals(""))
        {
            Glide.with(context).load(p.getPic().trim()).into(
                    holder.pic
            )  ;
        }

    }
    @Override
    public int getItemCount() {
        return cats.size();
    }
}