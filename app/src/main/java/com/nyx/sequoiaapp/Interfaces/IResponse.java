package com.nyx.sequoiaapp.Interfaces;

/**
 * Created by Misheal on 10/16/2019.
 */

public interface IResponse {
    void onResponse();
    void onResponse(Object json);
}
