package com.nyx.sequoiaapp.Interfaces;

public interface IColor {
    void onColorSelected(int color);
}
