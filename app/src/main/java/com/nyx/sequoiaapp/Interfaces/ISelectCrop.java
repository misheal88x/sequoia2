package com.nyx.sequoiaapp.Interfaces;

public interface ISelectCrop {
    void onVerticalSelected();
    void onHorizontalSelected();
    void onSquareSelected();
}
