package com.nyx.sequoiaapp.Dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.nyx.sequoiaapp.Interfaces.ISelectCrop;
import com.nyx.sequoiaapp.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

public class CropImageDialog extends AlertDialog {

    private LinearLayout horizontal,vertical,square;
    private ISelectCrop iSelectCrop;
    public CropImageDialog(@NonNull Context context,ISelectCrop iSelectCrop) {
        super(context);
        this.iSelectCrop = iSelectCrop;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_crop);
        horizontal = findViewById(R.id.crop_horizontal);
        vertical = findViewById(R.id.crop_vertical);
        square = findViewById(R.id.crop_square);

        horizontal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iSelectCrop.onHorizontalSelected();
                cancel();
            }
        });

        vertical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iSelectCrop.onVerticalSelected();
                cancel();
            }
        });

        square.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iSelectCrop.onSquareSelected();
                cancel();
            }
        });
    }
}
