package com.nyx.sequoiaapp.Dialogs;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.sequoiaapp.Interfaces.IColor;
import com.nyx.sequoiaapp.R;

import top.defaults.colorpicker.ColorObserver;
import top.defaults.colorpicker.ColorPickerView;

public class CustomColorPicker extends AlertDialog {

    private Context context;
    private ColorPickerView colorPickerView;
    private Button btn_ok,btn_cancel;
    private IColor iColor;
    private TextView tv_value;
    private int selected_color = 0;

    public CustomColorPicker(@NonNull Context context, IColor iColor) {
        super(context);
        this.context = context;
        this.iColor = iColor;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_custom_color_picker);
        /*
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
                **/
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //ColorPicker
        colorPickerView = findViewById(R.id.colorPicker);
        //Button
        btn_ok = findViewById(R.id.custom_color_picker_ok);
        btn_cancel = findViewById(R.id.custom_color_picker_cancel);
        //TextView
        tv_value = findViewById(R.id.selected_value);
    }

    private void init_events(){
        colorPickerView.subscribe(new ColorObserver() {
            @Override
            public void onColor(int color, boolean fromUser, boolean shouldPropagate) {
                selected_color = color;
                String hexColor = String.format("#%06X", (0xFFFFFF & color));
                tv_value.setBackgroundColor(Color.parseColor(hexColor));
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_color == 0){
                    Toast.makeText(context, "يجب علك اختيار لون أولا", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    iColor.onColorSelected(selected_color);
                    dismiss();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void init_dialog(){
    }
}
